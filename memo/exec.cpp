///////////////////////////
// State Controller Hell //
///////////////////////////
const int SC_Null               = 0x0000; //==ForceFeedback
const int SC_ChangeState        = 0x0001;
const int SC_SelfState          = 0x0002;
const int SC_VarSet             = 0x0004;
const int SC_VarAdd             = 0x0005;
const int SC_VarRandom          = 0x0006;
const int SC_VarRangeSet        = 0x0007;
const int SC_LifeSet            = 0x000a;
const int SC_LifeAdd            = 0x000b;
const int SC_PowerSet           = 0x000c;
const int SC_PowerAdd           = 0x000d;
const int SC_CtrlSet            = 0x0014;
const int SC_PosSet             = 0x0015;
const int SC_PosAdd             = 0x0016;
const int SC_PosFreeze          = 0x0017;
const int SC_VelSet             = 0x0018;
const int SC_VelAdd             = 0x0019;
const int SC_VelMul             = 0x001a;
const int SC_HitBy              = 0x001e;
const int SC_ChangeAnim         = 0x0020;
const int SC_ChangeAnim2        = 0x0021;
const int SC_PlaySnd            = 0x0022;
const int SC_StopSnd            = 0x0023;
const int SC_SndPan             = 0x0024;
const int SC_HitDef             = 0x0025;
const int SC_ReversalDef        = 0x0026;
const int SC_Projectile         = 0x0027;
const int SC_Width              = 0x0028;
const int SC_ScreenBound        = 0x0032;
const int SC_SprPriority        = 0x0033;
const int SC_AngleDraw          = 0x003c;
const int SC_AngleSet           = 0x003d;
const int SC_AngleAdd           = 0x003e;
const int SC_AngleMul           = 0x003f;
const int SC_Trans              = 0x0041;
const int SC_TargetBind         = 0x0046;
const int SC_BindToTarget       = 0x0047;
const int SC_TargetState        = 0x0048;
const int SC_TargetPowerAdd     = 0x0049;
const int SC_TargetLifeAdd      = 0x004a;
const int SC_TargetVelSet       = 0x004b;
const int SC_TargetVelAdd       = 0x004c;
const int SC_TargetFacing       = 0x004d;
const int SC_TargetDrop         = 0x004e;
const int SC_AttackDist         = 0x005a;
const int SC_PlayerPush         = 0x005b;
const int SC_HitFallSet         = 0x0064;
const int SC_HitVelSet          = 0x0065;
const int SC_HitFallDamage      = 0x0066;
const int SC_HitFallVel         = 0x0067;
const int SC_HitAdd             = 0x0068;
const int SC_AttackMulSet       = 0x0069;
const int SC_DefenceMulSet      = 0x006a;
const int SC_StateTypeSet       = 0x006e;
const int SC_HitOverride        = 0x0078;
const int SC_MoveHitReset       = 0x0082;
const int SC_AssertSpecial      = 0x0096;
const int SC_EnvShake           = 0x00c8;
const int SC_FallEnvShake       = 0x00c9;
const int SC_EnvColor           = 0x00ca;
const int SC_GameMakeAnim       = 0x00cb;
const int SC_AfterImage         = 0x00d2;
const int SC_AfterImageTime     = 0x00d3;
const int SC_Pause              = 0x00d4;
const int SC_SuperPause         = 0x00d6;
const int SC_MakeDust           = 0x00d7;
const int SC_BGPalFX            = 0x00d8; //==AllPalFX
const int SC_Explod             = 0x00da;
const int SC_ExplodBindTime     = 0x00dc;
const int SC_RemoveExplod       = 0x00dd;
const int SC_ModifyExplod       = 0x00df;
const int SC_Helper             = 0x012c;
const int SC_DestroySelf        = 0x012d;
const int SC_ParentVarSet       = 0x0130;
const int SC_ParentVarAdd       = 0x0131;
const int SC_DisplayToClipboard = 0x0136;
const int SC_AppendToClipboard  = 0x0137;
const int SC_ClearClipboard     = 0x0138;
const int SC_BindToParent       = 0x0140;
const int SC_BindToRoot         = 0x0141;
const int SC_TagIn              = 0x2743;
const int SC_TagOut             = 0x2744;

//////////////////
// Trigger Hell //
//////////////////
const int TR0_Time              = 0x0000; //==StateTime
const int TR0_AnimTime          = 0x0001;
const int TR0_AnimElem          = 0x0002;
const int TR0_Anim              = 0x0003;
const int TR0_Pos               = 0x0004;
const int TR0_ScreenPos         = 0x0005;
const int TR0_Vel               = 0x0006;
const int TR0_TimeMod           = 0x0007;
const int TR0_P2Dist            = 0x0008;
const int TR0_P2BodyDist        = 0x0009;
const int TR0_P2Life            = 0x000a;
const int TR0_FrontEdgeDist     = 0x000b; //==FrontEdgeBodyDist
const int TR0_BackEdgeDist      = 0x000c; //==BackEdgeBodyDist
const int TR0_Life              = 0x000f;
const int TR0_LifeMax           = 0x0010;
const int TR0_Power             = 0x0011;
const int TR0_PowerMax          = 0x0012;
const int TR0_Alive             = 0x0013;
const int TR0_Command           = 0x0014;
const int TR0_HitCount          = 0x001e;
const int TR0_UniqHitCount      = 0x001f;
const int TR0_MoveHit           = 0x0020;
const int TR0_MoveGuarded       = 0x0021;
const int TR0_MoveContact       = 0x0022;
const int TR0_MoveReversed      = 0x0023;
const int TR0_InGuardDist       = 0x0024;
const int TR0_StateNo           = 0x0028;
const int TR0_PrevStateNo       = 0x0029;
const int TR0_P2StateNo         = 0x002a;
const int TR0_StateType         = 0x002b;
const int TR0_P2StateType       = 0x002c;
const int TR0_MoveType          = 0x002d;
const int TR0_P2MoveType        = 0x002e;
const int TR0_Ctrl              = 0x002f;
const int TR0_HitDefAttr        = 0x0030;
const int TR0_Random            = 0x0033;
const int TR0_NumProj           = 0x003c;
const int TR0_ProjHit           = 0x003e;
const int TR0_ProjGuarded       = 0x003f;
const int TR0_ProjContact       = 0x0040;
const int TR0_Name              = 0x005a; //==P1Name
const int TR0_P2Name            = 0x005b;
const int TR0_P3Name            = 0x005c;
const int TR0_P4Name            = 0x005d;
const int TR0_AuthorName        = 0x005e;
const int TR0_NumPartner        = 0x005f;
const int TR0_NumEnemy          = 0x0060;
const int TR0_ID                = 0x0061;
const int TR0_Facing            = 0x0063;
const int TR0_HitFall           = 0x0064;
const int TR0_HitShakeOver      = 0x0065;
const int TR0_HitOver           = 0x0066;
const int TR0_HitVel            = 0x0067;
const int TR0_HitPauseTime      = 0x0068;
const int TR0_CanRecover        = 0x0069;
const int TR0_PalNo             = 0x006b;
const int TR0_WinPerfect        = 0x006c;
const int TR0_Win               = 0x006d; //==WinTime ==WinKO
const int TR0_Lose              = 0x006e; //==LoseTime ==LoseKO
const int TR0_DrawGame          = 0x006f;
const int TR0_MatchOver         = 0x0070;
const int TR0_GameTime          = 0x0071;
const int TR0_RoundNo           = 0x0072;
const int TR0_RoundState        = 0x0073;
const int TR0_MatchNo           = 0x0074;
const int TR0_RoundsExisted     = 0x0075;
const int TR0_TeamSide          = 0x0078;
const int TR0_TeamMode          = 0x0079;
const int TR0_IsHomeTeam        = 0x007a;
const int TR0_NumExplod         = 0x0096;
const int TR1_SNDExec           = 0x0098;
const int TR0_NumTarget         = 0x00a0;
const int TR0_GetHitVar         = 0x00b4;
const int TR0_Const             = 0x00b5;
const int TR0_StageVar          = 0x00b6;
const int TR0_TicksPerSecond    = 0x00be;
const int TR0_IsHelper          = 0x00c9;
const int TR0_NumHelper         = 0x00ca;
const int TR0_ParentDist        = 0x00cb;
const int TR0_RootDist          = 0x00cc;
const int TR0_Pi                = 0x012c;
const int TR0_E                 = 0x012d;
const int TR3_IfElse            = 0x0131;
const int TR2_Log               = 0x0136;
const int TR1_BeaExec           = 0x013d;
const int TR1_BeaXVar           = 0x013e;
const int TR1_BeaXFVar          = 0x013f;
const int TR1_Sin               = 0x0140;
const int TR1_Cos               = 0x0141;
const int TR1_Tan               = 0x0142;
const int TR1_ASin              = 0x0143;
const int TR1_ACos              = 0x0144;
const int TR1_ATan              = 0x0145;
const int TR1_Exp               = 0x0146;
const int TR1_Ln                = 0x0147;
const int TR1_Abs               = 0x0148;
const int TR1_Floor             = 0x0149;
const int TR1_Ceil              = 0x014a;
const int TR1_AnimElemTime      = 0x014b;
const int TR1_AnimExist         = 0x014c;
const int TR1_SelfAnimExist     = 0x014d;
const int TR1_PlayerIDExist     = 0x014e;
const int TR1_IsHelper          = 0x014f;
const int TR1_NumHelper         = 0x0150;
const int TR1_ProjHitTime       = 0x0151;
const int TR1_ProjGuardedTime   = 0x0152;
const int TR1_ProjContactTime   = 0x0153;
const int TR1_NumProjID         = 0x0154;
const int TR1_NumExplod         = 0x0155;
const int TR1_NumTarget         = 0x0156;
const int TR1_Var               = 0x0157;
const int TR1_FVar              = 0x0158;
const int TR1_SysVar            = 0x0159;
const int TR1_SysFVar           = 0x015a;
const int TR1_ProjCancelTime    = 0x015b;
const int TR1_AnimElemNo        = 0x015c;

//////////////////
// Operand Hell //
//////////////////
const int OP2_Equal             = 0x0001; //a=b         | Endpoint
const int OP2_Inequal           = 0x0002; //a!=b        | Endpoint
const int OP2_Greater           = 0x0003; //a>b         | Endpoint
const int OP2_Lesser            = 0x0004; //a<b         | Endpoint
const int OP2_Minimum           = 0x0005; //a>=b        | Endpoint
const int OP2_Maximum           = 0x0006; //a<=b        | Endpoint
const int OP2_RangeExEx         = 0x0007; //(a,b)       | Endpoint
const int OP2_RangeExIn         = 0x0008; //(a,b]       | Endpoint
const int OP2_RangeInEx         = 0x0009; //[a,b)       | Endpoint
const int OP2_RangeInIn         = 0x000a; //[a,b]       | Endpoint
const int OP2_LogicAND          = 0x000b; //a&&b        | Recurse w/ Dependency
const int OP2_LogicOR           = 0x000c; //a||b        | Recurse
const int OP1_LogicNOT          = 0x000d; //!a          | Skip + Inv
const int OP2_LogicXOR          = 0x000e; //a^^b        | Recurse w/ Inv Dependency
const int OP2_Exponent          = 0x000f; //a**b        | Null Right
const int OP2_BitAND            = 0x0010; //a&b         | Recurse w/ Dependency
const int OP2_BitOR             = 0x0011; //a|b         | Recurse
const int OP2_BitXOR            = 0x0012; //a^b         | Recurse w/ Inv Dependency
const int OP1_Negate            = 0x0013; //-a          | Skip
const int OP2_IntAssign         = 0x0014; //var(a):=b   | Null Left
const int OP2_FloatAssign       = 0x0015; //fvar(a):=b  | Null Left
const int OP1_BitNOT            = 0x0016; //~a          | Skip
const int OP2_Add               = 0x0017; //a+b         | Endpoint
const int OP2_Sub               = 0x0018; //a-b         | Endpoint
const int OP2_Mul               = 0x0019; //a*b         | Recurse w/ Dependency
const int OP2_Div               = 0x001a; //a/b         | Recurse w/ Dependency
const int OP2_Mod               = 0x001b; //a%b         | Recurse w/ Dependency
const int OP2_BeaXIntAssign     = 0x001c; //xvar(a):=b  | Null Left
const int OP2_BeaXFloatAssign   = 0x001d; //xfvar(a):=b | Null Left

/////////////////
// Actual Code //
/////////////////
//NOTE: 0x1500 = 0x0900 in the raw
#include <cstdlib>
#include <cmath>
#include <cstring>
using byte = unsigned char;
using word = unsigned short;
using dword = unsigned int;
#define asmi(_a) asm(".intel_syntax noprefix\n" _a);
#define ASSERT_FAIL(_a) if (!_a) //Useful for later
#define SAFEDIV(_a,_b) (_a == -2147483648 && _b == -1 ? -2147483648 : (_b == 0 ? 0 : _a/_b)) //Fuck this crash in particular
#define SAFEMOD(_a,_b) (_a == -2147483648 && _b == -1 ? 0 : (_b == 0 ? 0 : _a%_b)) //Again, fuck this crash in particular
#define M_PI 3.14159265358979323846

inline void memcpyrev (void* dst, void* src, size_t num) {
    asm(".intel_syntax noprefix\n"
        "std;"
        "lea esi,[esi+ecx*1];"
        "add edi,ecx;"
        "lea esi,[esi-0x01];"
        "dec edi;"
        "rep movsb;"
        "cld;"
        :
        : "S"(dst), "D"(src), "c"(num)
    );
}
int main () { //For debugging and code snatching, this is just an infinite loop
    int i = 0;
    while (true) {i++;}
    return 0;
}

//Align
asm(".org 0x0ff0, 0x00;");

//From hereon, raw asm functions will be listed
/*
============================================
Default Register Values
--------------------------------------------
eax        = MUGEN's address (obtained manually)
ecx        = Code Execution Address (you can probably figure out how to access the extended variable space from here if you know the ID)
edx        = Param0 (use XVar(512))
edx + 0x04 = Param1, etc.
ebx        = Self Address [Really easy to get to]
esi        = Additional Self Address
edi - 0x08 = ???? (it's a pointer tho)
[esp+0x64] = Return Value [Int] (0x40 + the 0x24 from pusha/call)
============================================
*/
asm(R"(.intel_syntax noprefix
    .int offset _X00, offset _X01, offset _X02, offset _X03, offset _X04, offset _XRT, offset _X06, offset _X07 # exec(0x00~0x07)
    .int offset _XRT, offset _X09, offset _X0A, offset _XRT, offset _X0C, offset _X0D, offset _X0E, offset _X0F # exec(0x08~0x0f)
    .int offset _X10, offset _X11, offset _X12, offset _X13, offset _X14, offset _X15, offset _X16, offset _XRT # exec(0x10~0x17)
    .int offset _X18, offset _XRT, offset _X1A, offset _X1B, offset _X1C, offset _X1D, offset _XRT, offset _XRT # exec(0x18~0x1f)
    .int offset _XRT, offset _XRT, offset _X22, offset _XRT, offset _XRT, offset _X25, offset _X26, offset _X27 # exec(0x20~0x27)
    .int offset _X28, offset _X29, offset _X2A, offset _XRT, offset _X2C, offset _X2D, offset _X2E, offset _XRT # exec(0x28~0x2f)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x30~0x37)
    .int offset _X38, offset _X39, offset _XRT, offset _XRT, offset _XRT, offset _X3D, offset _XRT, offset _X3F # exec(0x38~0x3f)
    .int offset _X40, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x40~0x47)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x48~0x4f)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x50~0x57)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x58~0x5f)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x60~0x67)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x68~0x6f)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x70~0x77)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x78~0x7f)
    .int offset _X80, offset _X81, offset _X82, offset _X83, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x80~0x87)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x88~0x8f)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x90~0x97)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0x98~0x9f)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xa0~0xa7)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xa8~0xaf)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xb0~0xb7)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xb8~0xbf)
    .int offset _XC0, offset _XC1, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xc0~0xc7)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xc8~0xcf)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xd0~0xd7)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xd8~0xdf)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xe0~0xe7)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xe8~0xef)
    .int offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XRT # exec(0xf0~0xf7)
    .int offset _XF8, offset _XF9, offset _XFA, offset _XRT, offset _XRT, offset _XRT, offset _XRT, offset _XFF # exec(0xf8~0xff)
_XRT:                                            # ----------------------------------------
    ret                                          # OoB Error handler
_rand:                                           # ----------------------------------------
  push 0x00492f37                                # just go here????
  ret                                            #
)");

/*
##############################################################################################################
# C++ default functions that I have to include bc they're not included in non-exe files automatically fsr :/ #
##############################################################################################################
*/
__declspec(naked) double floor(double x) {
    asm(R"(.intel_syntax noprefix
    _floor:
      fld    QWORD PTR [esp+0x4]
      sub    esp,0x8
      fstcw  WORD PTR [esp+0x4]
      mov    edx,0x400
      or     edx,DWORD PTR [esp+0x4]
      and    edx,0xf7ff
      mov    DWORD PTR [esp],edx
      fldcw  WORD PTR [esp]
      frndint
      fldcw  WORD PTR [esp+0x4]
      add    esp,0x8
      ret
    )");
}
__declspec(naked) float floor(float x) {
    asm(R"(.intel_syntax noprefix
    _floorf:
      fld    DWORD PTR [esp+0x4]
      sub    esp,0x8
      fstcw  WORD PTR [esp+0x4]
      mov    edx,0x400
      or     edx,DWORD PTR [esp+0x4]
      and    edx,0xf7ff
      mov    DWORD PTR [esp],edx
      fldcw  WORD PTR [esp]
      frndint 
      fldcw  WORD PTR [esp+0x4]
      add    esp,0x8
      ret
    )");
}
__declspec(naked) double ceil(double x) {
    asm(R"(.intel_syntax noprefix
    _ceil:
      fld    QWORD PTR [esp+0x4]
      sub    esp,0x8
      fstcw  WORD PTR [esp+0x4]
      mov    edx,0x800
      or     edx,DWORD PTR [esp+0x4]
      and    edx,0xfbff
      mov    DWORD PTR [esp],edx
      fldcw  WORD PTR [esp]
      frndint
      fldcw  WORD PTR [esp+0x4]
      add    esp,0x8
      ret
    )");
}
__declspec(naked) double round(double x) {
    asm(R"(.intel_syntax noprefix
    _round:
      sub    esp,0xc
      fldz
      fld    QWORD PTR [esp+0x10]
      fcomi  st,st(1)
      fstp   st(1)
      jb     _round_neg
      fnstcw WORD PTR [esp+0x6]
      fld    st(0)
      movzx  eax,WORD PTR [esp+0x6]
      and    ah,0xf3
      or     ah,0x8
      mov    WORD PTR [esp+0x4],ax
      fldcw  WORD PTR [esp+0x4]
      frndint
      fldcw  WORD PTR [esp+0x6]
      fsubr  st(1),st
      fld    DWORD PTR ds:_FCONST_HALF
      fxch   st(2)
      fcomip st,st(2)
      fstp   st(1)
      jbe    _round+0x45
      fsub   DWORD PTR ds:_FCONST_ONE
      add    esp,0xc
      ret
    _round_neg:
      fnstcw WORD PTR [esp+0x6]
      fld    st(0)
      fchs
      movzx  eax,WORD PTR [esp+0x6]
      and    ah,0xf3
      or     ah,0x8
      mov    WORD PTR [esp+0x4],ax
      fldcw  WORD PTR [esp+0x4]
      frndint
      fldcw  WORD PTR [esp+0x6]
      fadd   st(1),st
      fld    DWORD PTR ds:_FCONST_HALF
      fxch   st(2)
      fcomip st,st(2)
      fstp   st(1)
      jbe    _round_neg+0x38
      fsub   DWORD PTR ds:_FCONST_ONE
      fchs
      add    esp,0xc
      ret
      .balign 0x08,0x00
    )");
}
__declspec(naked) double log2(double x) {
    asm(R"(.intel_syntax noprefix
    _log2:
      fld1
      fld    QWORD PTR [esp+0x4]
      fxam   
      fnstsw ax
      fld    st(0)
      sahf   
      jb     _log2+0x2c
      fsub   st,st(2)
      fld    st(0)
      fabs   
      fcomp  QWORD PTR ds:_DCONST_HALFSQRT2
      fnstsw ax
      and    ah,0x45
      je     _log2+0x27
      fstp   st(1)
      fyl2xp1 
      ret    
      fstp   st(0)
      fyl2x  
      ret    
      jp     _log2+0xf
      fstp   st(1)
      fstp   st(1)
      ret
      .balign 0x08,0x00
    )");
}

/*
################################################################################################
# Misc. Functions that if I didn't write in asm would cause references to weird data tables :/ #
################################################################################################
*/
__declspec(naked) void errorRounding(float& in) { //For adjusting significant digits in gametime formulae
    //To prevent this code from placing constants in ridiculously asinine locations, I'm writing the compiled version manually
    asm(R"(.intel_syntax noprefix
    _errorRounding:
      push   ebp
      mov    ebp,esp
      push   ebx
      mov    ebx,eax
      sub    esp,0x14
      fld    DWORD PTR ds:_DCONST_100
      fld    DWORD PTR [eax]
      fmul   st,st(1)
      fdivrp st(1),st
      fstp   QWORD PTR [esp]
      call   _round
      fstp   DWORD PTR [ebp-0xc]
      fld    DWORD PTR [ebp-0xc]
      fld    st(0)
      fld    QWORD PTR ds:_DCONST_PSEUDOPI
      fxch   st(1)
      fucomi st,st(1)
      fstp   st(1)
      jp     _errorRounding+0x41
      jne    _errorRounding+0x41
      fstp   st(0)
      fstp   st(0)
      mov    DWORD PTR [ebx],0x40490fdb
      jmp    _errorRounding+0x5d
      fld    QWORD PTR ds:_DCONST_PSEUDOE
      fxch   st(1)
      fucomip st,st(1)
      fstp   st(0)
      jp     _errorRounding+0x51
      je     _errorRounding+0x55
      fstp   DWORD PTR [ebx]
      jmp    _errorRounding+0x5d
      fstp   st(0)
      mov    DWORD PTR [ebx],0x402df854
      add    esp,0x14
      pop    ebx
      pop    ebp
      ret    
      .balign 0x08,0x00
    )");
};


////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN FUNCTION BLOCK
////////////////////////////////////////////////////////////////////////////////////////////////

/*
----------------
.SelfID (ID = 00)
--------------------------------------------
Address Acquisition/Reversion, essentially
Param0 = 0 plays it straight:
    This will set our Const(Data.AirJuggle) to our own address and MUGEN's to Const(Size.Z.Width) for easy access.
    Similarly, Const(Size.Draw.Offset.X) and its Y variant will be set to any enemy address that we come across.
    For the sake of proper access and modification, all obsfucated Explods will be given back to us if the user is the root.
When Param0 is 1, this protocol is inverted
    All addresses obtained through the above are reset to their original values
    All Explods owned by the root will be obsfucated... except if the Non-Display flag is set properly. This serves as a preservation marker.
*/
asm(R"(.intel_syntax noprefix
_X00:
  cmp DWORD PTR [edx],0x00                       # Inversion mode
  lea edx,[ebx+0x64]                             # Set up edx for writing Size.Z.Width in a more compact way
  lea edi,[ebx+0x00000148]                       # Move edi over Const(Size.Draw.Offset.X)
  jne _X00.ProtocolInversion                     # 
  cmp DWORD PTR [ebx+0x00002624],0x00            # Not root, skip
  jne _X00.SkipRestoration                       # 
  push eax                                       # We'll need to use this later
  mov eax,DWORD PTR [eax+0x0000a2e4]             # Enter Explod space
  mov esi,DWORD PTR [eax]                        # 
  mov ecx,DWORD PTR [eax+0x04]                   # Fetch ExplodMax (precaution)
  mov eax,DWORD PTR [eax+0x08]                   # Fetch NumExplod
  mov ebp,DWORD PTR [ebx+0x04]                   # Fetch ID
  or ebp,0x80000000                              # |-2147483648
_X00.RestorationLoop:                            # ----------------------------------------
  cmp DWORD PTR [esi],0x00                       # Activity flag
  je _X00.RestorationNext                        # 
  cmp DWORD PTR [esi+0x0c],ebp                   # ID matches what we'd expect it to be if obsfucated
  jne _X00.RestorationNextD                      # 
  and BYTE PTR [esi+0x0f],0x7f                   # Restore ID
_X00.RestorationNextD:                           # - - - - - - - - - - - - - - - - - - - -
  dec eax                                        # We ran out of Explods that exist, continue
  jz _X00.SkipRestorationP                       # 
_X00.RestorationNext:                            # - - - - - - - - - - - - - - - - - - - -
  dec ecx                                        # We ran out of possible Explod slots, continue
  lea esi,[esi+0x000000e4]                       # Next Explod
  jnz _X00.RestorationLoop                       # 
_X00.SkipRestorationP:                           # - - - - - - - - - - - - - - - - - - - -
  pop eax                                        # Use it now
_X00.SkipRestoration:                            # ----------------------------------------
  mov DWORD PTR [edx],ebx                        # Store self in Const(Data.AirJuggle)
  mov DWORD PTR [edx+0x30],eax                   # Store MUGEN in Const(Size.Z.Width)
  lea esi,[eax+0x0000b754]                       # Set esi to the player space's location, adjusted accordingly
  mov ecx,DWORD PTR [ebx+0x00002624]             # Fetch what should be the root's address
  test ecx,ecx                                   # If non-zero, assume that this address is the root and continue.
  cmovz ecx,ebx                                  # Otherwise just take our own value
  mov ecx,DWORD PTR [ecx+0x08]                   # Fetch PlayerNo
  and cl,0x01                                    # Fetch the appropriate player set to fetch through PlayerNo
  lea esi,[esi+ecx*4]                            # Adjust appropriately
  movsd                                          # Copy the enemy's address to Const(Size.Draw.Offset.X)
  add esi,0x04                                   # Get the partner too
  movsd                                          # Copy the 2nd enemy's address (or 0, whichever) to Const(Size.Draw.Offset.Y)
  xor eax,eax                                    # Store a billion 0s
  mov edi,DWORD PTR [ebx]                        # Set DisplayName as appropriate
  add edi,0x30                                   # 
  mov edx,0x07f44db1                             # XOR Key
  mov eax,0x739528f3                             # Beat
  xor eax,edx                                    # 
  stosd                                          # 
  mov eax,0x278c24c3                             # rix 
  xor eax,edx                                    # 
  stosd                                          # 
  mov eax,0x6f8428e3                             # Reph
  xor eax,edx                                    # 
  stosd                                          # 
  mov eax,0x07f44dd0                             # a<NUL>
  xor eax,edx                                    # 
  stosd                                          # 
  ret                                            # 
_X00.ProtocolInversion:                          # ========================================
  cmp DWORD PTR [ebx+0x00002624],0x00            # Not root, skip
  jne _X00.SkipObsfucation                       # 
  mov eax,DWORD PTR [eax+0x0000a2e4]             # Enter Explod space
  mov esi,DWORD PTR [eax]                        # 
  mov ecx,DWORD PTR [eax+0x04]                   # Fetch ExplodMax (precaution)
  mov eax,DWORD PTR [eax+0x08]                   # Fetch NumExplod
  mov ebp,DWORD PTR [ebx+0x04]                   # Fetch ID
_X00.ObsfucationLoop:                            # ----------------------------------------
  cmp DWORD PTR [esi],0x00                       # Activity flag
  je _X00.ObsfucationNext                        # 
  cmp DWORD PTR [esi+0x0c],ebp                   # ID matches
  jne _X00.ObsfucationNextD                      # 
  cmp BYTE PTR [esi+0x08],0xf4                   # Obsfucation Immunity Marker
  je _X00.ObsfucationNextD                       # 
  or BYTE PTR [esi+0x0f],0x80                    # Obsfucate ID
_X00.ObsfucationNextD:                           # - - - - - - - - - - - - - - - - - - - -
  dec eax                                        # We ran out of Explods that exist, continue
  jz _X00.SkipObsfucation                        # 
_X00.ObsfucationNext:                            # - - - - - - - - - - - - - - - - - - - -
  dec ecx                                        # We ran out of possible Explod slots, continue
  lea esi,[esi+0x000000e4]                       # Next Explod
  jnz _X00.ObsfucationLoop                       # 
_X00.SkipObsfucation:                            # ----------------------------------------
  xor eax,eax                                    # Store a billion 0s
  mov DWORD PTR [edx],0x00000309                 # Const(Data.AirJuggle) = 777
  mov DWORD PTR [edx+0x30],eax                   # Const(Size.Z.Width) = 0
  mov DWORD PTR [edi],eax                        # Const(Size.Draw.Offset.X) = 0
  mov DWORD PTR [edi+0x04],eax                   # Const(Size.Draw.Offset.Y) = 0
  ret                                            # 
  .balign 0x10, 0x00                             #
)");

/*
----------------
.GenericRangeX (ID = 01)
--------------------------------------------
Executes a copy instruction depending on Param6.
Each will repeat Param5 times.
Nowadays in the small scale, these functions have been kinda deprecated by xvar assignment/reading.
However, for large-scale writing, it's faster to use a GenericCopy instruction.
Thus, they're still kept functional for efficiency reasons.
*/
asm(R"(.intel_syntax noprefix
_X01:                                            # ========================================
  mov ebp,DWORD PTR [edx+0x18]                   # Fetch the function ID  
  cmp ebp,0x06                                   # DUMBASS
  jnb _X01.ForceRet                              #
  inc ebp                                        # Add 1 because yea
  shl ebp,0x07                                   # *0x80
  lea ebp,[ebp+ecx*1]                            # Adjust to functionspace
  mov esi,DWORD PTR [edx]                        # Get the value/location we're writing from
  mov edi,DWORD PTR [edx+0x04]                   # Get the initial location we're writing to
  cmp edi,0x00003474                             # If within standard player space, adjust
  jnb _X01.SkipEDIAdd                            # 
  add edi,ebx                                    # Adjust to self+offset
_X01.SkipEDIAdd:                                 # ---------------------------------------- 
  mov ecx,DWORD PTR [edx+0x14]                   # Get LoopCount
  jmp ebp                                        # Head to appropriate
_X01.ForceRet:                                   # ----------------------------------------
  ret                                            # Dumbass clause
  .balign 0x80, 0x00                             # ========================================
                                                 # Param6 = 0:
                                                 #   Effective stosd.
                                                 #   [Param1] := Param0
                                                 #   Param1 += Param4
  mov eax,esi                                    # No need to re-interpret as an address
  mov ebx,DWORD PTR [edx+0x10]                   # Fetch adder
  sub ebx,0x04                                   # If not 4, manually use stosd
  jnz _X01.NonRepStoSD                           # 
  inc ecx                                        # As rep has an ending of ecx=0
  rep stosd                                      # 
  ret                                            # 
_X01.NonRepStoSD:                                # ----------------------------------------
  stosd                                          # 
  sub ecx,0x01                                   # 
  lea edi,[edi+ebx*1]                            # 
  jnl _X01.NonRepStoSD                           # 
  ret                                            # 
  .balign 0x80, 0x00                             # ========================================
                                                 # Param6 = 1:
                                                 #   Effective stosb.
                                                 #   Identical to stosd in functionality
  mov eax,esi                                    # 
  mov ebx,DWORD PTR [edx+0x10]                   # 
  sub ebx,0x01                                   # If not 1, manually use stosb
  jnz _X01.NonRepStoSB                           # 
  inc ecx                                        # 
  rep stosb                                      # 
  ret                                            # 
_X01.NonRepStoSB:                                # ----------------------------------------
  stosb                                          # 
  sub ecx,0x01                                   # 
  lea edi,[edi+ebx*1]                            # 
  jnl _X01.NonRepStoSB                           # 
  ret                                            # 
  .balign 0x80, 0x00                             # ========================================
                                                 # Param6 = 2:
                                                 #   Effective movsd.
                                                 #   [Param1] := [Param0]
                                                 #   Param0 += Param3
                                                 #   Param1 += Param4
                                                 #   If Param3 and Param4 are -4, we'll use a special handler involving a backwards rep (feedtape processing)
  cmp esi,0x00003474                             # If within standard player space, adjust
  jnb _X01.SkipESIAddSD                          # 
  add esi,ebx                                    # Adjust to self+offset
_X01.SkipESIAddSD:                               # ----------------------------------------
  mov ebx,DWORD PTR [edx+0x10]                   # Fetch adder like before
  sub ebx,0x04                                   # If not 4, check for -8 (formerly -4) and try again
  mov edx,DWORD PTR [edx+0x0c]                   # 
  lea edx,[edx-0x04]                             # We do need an adjustment factor for Param0 though
  jnz _X01.InvRepMovSDCheck                      # 
  test edx,edx                                   # Check both
  jz _X01.RepMovSD                               # 
_X01.InvRepMovSDCheck:                           # - - - - - - - - - - - - - - - - - - - -
  cmp ebx,0xfffffff8                             # Check for the inversion case
  jne _X01.NonRepMovSD                           # Otherwise, it's similar to stosd
  cmp edx,0xfffffff8                             #
  jne _X01.NonRepMovSD                           #
  std                                            # Invert directionality
_X01.RepMovSD:                                   # ----------------------------------------
  inc ecx                                        # 
  rep movsd                                      # 
  cld                                            # Clear direction flag just in case the backwards route is used
  ret                                            # 
_X01.NonRepMovSD:                                # ----------------------------------------
  movsd                                          # 
  lea esi,[esi+edx*1]                            # 
  sub ecx,0x01                                   # 
  lea edi,[edi+ebx*1]                            # 
  jnl _X01.NonRepMovSD                           # 
  ret                                            # 
  .balign 0x80, 0x00                             # ========================================
                                                 # Param6 = 3:
                                                 #   Effective movsb.
                                                 #   Identical to movsd in nature
  cmp esi,0x00003474                             # Same deal as movsd
  jnb _X01.SkipESIAddSB                          # 
  add esi,ebx                                    # 
_X01.SkipESIAddSB:                               # ----------------------------------------
  mov ebx,DWORD PTR [edx+0x10]                   # 
  sub ebx,0x01                                   # If not 1, check for -2 (formerly -1) and try again
  mov edx,DWORD PTR [edx+0x0c]                   # 
  lea edx,[edx-0x01]                             # 
  jnz _X01.InvRepMovSBCheck                      # 
  test edx,edx                                   # 
  jz _X01.RepMovSB                               # 
_X01.InvRepMovSBCheck:                           # - - - - - - - - - - - - - - - - - - - -
  cmp ebx,0xfffffffe                             # 
  jne _X01.NonRepMovSB                           # 
  cmp edx,0xfffffffe                             #
  jne _X01.NonRepMovSB                           #
  std                                            # 
_X01.RepMovSB:                                   # ----------------------------------------
  inc ecx                                        # 
  rep movsb                                      # 
  cld                                            # 
  ret                                            # 
_X01.NonRepMovSB:                                # ----------------------------------------
  movsb                                          # 
  lea esi,[esi+edx*1]                            # 
  sub ecx,0x01                                   # 
  lea edi,[edi+ebx*1]                            # 
  jnl _X01.NonRepMovSB                           # 
  ret                                            # 
  .balign 0x80, 0x00                             # ========================================
                                                 # Param6 = 4:
                                                 #   Effective movsd from a playerID source.
                                                 #   [Param1] := [PlayerID(Param2)+Param0]
                                                 #   Param0 += Param3
                                                 #   Param1 += Param4
  mov ebx,DWORD PTR [edx+0x08]                   # Get the initial player ID we want
  add eax,0x0000b84c                             # Adjust MUGEN's address to the last player's address
  mov ebp,0xffffffc2                             # Set ecx to -62
_X01.FPLoopStart:                                # ----------------------------------------
  mov esi,DWORD PTR [eax+ebp*0x04]               # Generic Forwards Player-Seeking Loop
  test esi,esi                                   # If the player doesn't exist, skip it
  jz _X01.FPLoopNext                             # 
  cmp DWORD PTR [esi+0x04],ebx                   # Check if the ID matches
  je _X01.FPLoopEnd                              # 
_X01.FPLoopNext:                                 # 
  inc ebp                                        # 
  jng _X01.FPLoopStart                           # 
  ret                                            # Return if nothing is found
_X01.FPLoopEnd:                                  # ========================================
  add esi,DWORD PTR [edx]                        # Adjust as appropriate
  mov ebx,DWORD PTR [edx+0x10]                   # Fetch adder as usual
  sub ebx,0x04                                   # If not 4, manually use movsd
  mov edx,DWORD PTR [edx+0x0c]                   # 
  lea edx,[edx-0x04]                             # 
  jnz _X01.NonRepMovPD                           # 
  test edx,edx                                   # 
  jnz _X01.NonRepMovPD                           # 
  inc ecx                                        # 
  rep movsd                                      # 
  ret                                            # 
_X01.NonRepMovPD:                                # ----------------------------------------
  movsd                                          # 
  lea esi,[esi+edx*1]                            # 
  sub ecx,0x01                                   # 
  lea edi,[edi+ebx*1]                            # 
  jnl _X01.NonRepMovPD                           # 
  ret                                            # 
  .balign 0x80, 0x00                             # ========================================
                                                 # Param6 = 5:
                                                 #   Exchange information between 2 sets of addresses.
                                                 #   [Param1] <=> [Param0]
                                                 #   Param0 += Param3
                                                 #   Param1 += Param4
  cmp esi,0x00003474                             # If within standard player space, adjust
  jnb _X01.SkipESIAddXD                          # 
  add esi,ebx                                    # Adjust to self+offset
_X01.SkipESIAddXD:                               # ----------------------------------------
  mov ebx,DWORD PTR [edx+0x10]                   # Fetch adders like before
  mov edx,DWORD PTR [edx+0x0c]                   # 
_X01.NonRepXchgD:                                # ----------------------------------------
  mov eax,DWORD PTR [edi]                        # As we need to add 4, we have to use this method instead
  xchg eax,DWORD PTR [esi]                       # Use an actual exchanger
  add esi,edx                                    # We can add now since we won't need it past here
  mov DWORD PTR [edi],eax                        # 
  sub ecx,0x01                                   # 
  lea edi,[edi+ebx*1]                            # 
  jnl _X01.NonRepXchgD                           # 
  ret                                            # 
  .balign 0x10, 0x00
)");


/*
----------------
.NextPlayer (ID = 02)
--------------------------------------------
Searches for the next player to execute code.
Assumes that return pointers for state processing are untampered and that the code is executed from a trigger.
If this assumption is invalid, it will instead check for whether the user is MT=A or not.
*/
asm(R"(.intel_syntax noprefix
_X02:                                            # ========================================
  xor edi,edi                                    # Default return value of 0
  mov ebp,DWORD PTR [eax+0x0000b3fc]             # GameTime
  xor esi,esi                                    # Loop Restart Tracking flag
  mov ecx,DWORD PTR [esp+0x00000138]             # Check for obvious values
  cmp ecx,0x0041f60f                             # MT!=A
  je _X02.ScanPlayersDefHeader                   #
  cmp ecx,0x0041f68d                             # Reiteration
  je _X02.ScanPlayersDefHeader                   #
  cmp ecx,0x0041f5ab                             # MT=A
  je _X02.ScanPlayersMTAHeader                   #
  cmp DWORD PTR [ebx+0x00000e04],0x01            # In all other cases, check movetype
  jne _X02.ScanPlayersDefHeader                  #
_X02.ScanPlayersMTAHeader:                       # ========================================
  mov ecx,DWORD PTR [ebx+0x08]                   # Initialize NextPlayer (PlayerNo+1)
  cmp ecx,0x3f                                   # oh come on
  jae _X02.ScanPlayersMTARedo                    # 
_X02.ScanPlayersMTALoop:                         # ----------------------------------------
  mov edi,DWORD PTR [eax+ecx*4+0x0000b754]       # The player must exist
  test edi,edi                                   #
  jz _X02.ScanPlayersMTANext                     #
  cmp DWORD PTR [edi+0x00000158],0x00            # ...again, the player must exist
  je _X02.ScanPlayersMTANext                     #
  cmp DWORD PTR [edi+0x00000e04],0x01            # MT=A requirement
  jne _X02.ScanPlayersMTANext                    #
  cmp DWORD PTR [eax+ecx*4+0x0000ba68],0x00      # The player must be able to act
  jne _X02.ReturnPlayer                          # If all of these conditions are satisfied, return it
_X02.ScanPlayersMTANext:                         # ----------------------------------------
  inc ecx                                        # Next player
  cmp ecx,0x3f                                   # Reached the end
  jb _X02.ScanPlayersMTALoop                     #
_X02.ScanPlayersMTARedo:                         # ----------------------------------------
  inc esi                                        # Realistically speaking, this is just branching to MT=I processing
  xor ecx,ecx                                    #
  jmp _X02.ScanPlayersDefLoop                    #
_X02.ScanPlayersDefHeader:                       # ========================================
  mov ecx,DWORD PTR [ebx+0x08]                   # Initialize NextPlayer (PlayerNo+1)
  cmp ecx,0x3f                                   # oh come on
  jae _X02.ScanPlayersDefRedo                    # 
_X02.ScanPlayersDefLoop:                         # ----------------------------------------
  mov edi,DWORD PTR [eax+ecx*4+0x0000b754]       # The player must exist
  test edi,edi                                   #
  jz _X02.ScanPlayersDefNext                     #
  cmp DWORD PTR [edi+0x00000158],0x00            # ...again, the player must exist
  je _X02.ScanPlayersDefNext                     #
  cmp DWORD PTR [edi+0x00000e2c],ebp             # The player must not have already moved this frame
  je _X02.ScanPlayersDefNext                     #
  cmp DWORD PTR [eax+ecx*4+0x0000ba68],0x00      # The player must be able to act
  jne _X02.ReturnPlayer                          # If all of these conditions are satisfied, return it
_X02.ScanPlayersDefNext:                         # ----------------------------------------
  inc ecx                                        # Next player
  cmp ecx,0x3f                                   # Reached the end
  jb _X02.ScanPlayersDefLoop                     #
  xor edi,edi                                    # Return 0...
  test esi,esi                                   # if we've already performed reiteration
  jnz _X02.ReturnPlayer                          #
_X02.ScanPlayersDefRedo:                         # ----------------------------------------
  cmp DWORD PTR [eax+0x0000bb64],0x00            # If the reiteration flag is set, execute it, otherwise return 0
  je _X02.ReturnPlayer                           #
  inc esi                                        # Reboot until we cycle through everything again
  xor ecx,ecx                                    #
  jmp _X02.ScanPlayersDefLoop                    #
_X02.ReturnPlayer:                               # ========================================
  mov DWORD PTR [esp+0x64],edi                   # Return the player (or 0 if one isn't going to act)
  ret                                            #
  .balign 0x10, 0x00
)");

/*
----------------
.GenericScanAX (ID = 03)
--------------------------------------------
[Param0] = Param1?
Param0 += Param5;
Repeat the above Param2 times. Return value of either the instance of it matching or -1.
Param4:
  0 = Check for EQUAL
  1 = Check for NOT EQUAL
  2 = Variable Scan (Equal)
  3 = Check for EQUAL, replace with Param3
  4 = Check for (index&Param3) = Param1
In the case of a Variable Scan, Param2 is instead used as a starting variable, and Param5 is an assumed 4
*/
asm(R"(.intel_syntax noprefix
_X03:                                            # ========================================
  mov edi,DWORD PTR [edx]                        # Get the initial location we're reading from
  mov eax,DWORD PTR [edx+0x04]                   # Get the comparison source
  mov ecx,DWORD PTR [edx+0x08]                   # Get loop counter
  cmp DWORD PTR [edx+0x10],0x02                  # Check for variable scanning?
  jne _X03.NonVariable                           # 
_X03.Variables:                                  # ========================================
  finit                                          # For the sake of fcomp
  fild DWORD PTR [edx]                           # 
  fstp DWORD PTR [edx]                           # Wait I can just store it here lol
  neg ecx                                        # Cap at 109
  mov ebx,0x0000006d                             # 
  add ecx,ebx                                    # 
  mov DWORD PTR [edx+0x08],ebx                   # 
  fwait                                          # good practice
  mov ebx,DWORD PTR [edx]                        # Nice
_X03.VariableLoop:                               # ----------------------------------------
  cmp ecx,0x05                                   # Check for SysFVar [105~109, so < 109-105 = 104]
  jl _X03.FloatParser                            # 
  cmp ecx,0x32                                   # Check for Var [000~059, so ≥ 109-59 = 50]
  jnl _X03.IntParser                             # 
  cmp ecx,0x0a                                   # Check for FVar [060~099, so ≥ 109-99 = 10]
  jnl _X03.FloatParser                           # (SysVar satisfies none of these criteria and will continue to Int)
_X03.IntParser:                                  # ----------------------------------------
  cmp DWORD PTR [edi],eax                        # Easy comparison right :p
  je _X03.Found                                  # 
  jmp _X03.Continue                              # 
_X03.FloatParser:                                # ----------------------------------------
  cmp DWORD PTR [edi],ebx                        # This will just check for absolute equality bc i'm lazy
  je _X03.Found                                  # 
_X03.Continue:                                   # ----------------------------------------
  sub ecx,0x01                                   # Next Variable down the line
  lea edi,[edi+0x04]                             # 
  jnl _X03.VariableLoop                          # 
  jmp _X03.ReturnSet                             # it's -1 so no `not ecx` needed
_X03.NonVariable:                                # ========================================
  cmp DWORD PTR [edx+0x10],0x04                  # Check for mask scanning?
  je _X03.MaskScan                               #
  mov ebx,DWORD PTR [edx+0x14]                   # Get adjustment value
  cmp ebx,0x04                                   # Account for DWORD scanning
  lea ecx,[ecx+0x01]                             # Might as well
  jne _X03.NonRep                                # Use standard cmp instead of scasd
  cmp DWORD PTR [edx+0x10],0x01                  # 
  je _X03.NonEqualityR                           # Non-equality type since it's 1
  jb _X03.EqualityR                              # Equality type since it's 0
  mov edx,DWORD PTR [edx+0x0c]                   # It's sub, so let's substitute the appropriate value
_X03.SubstitutionR:                              # ========================================
  repne scasd                                    # repne loops while not equal, checks for equal
  jne _X03.ReturnSet                             # We're done, end processing
  mov DWORD PTR [edi-0x04],edx                   # Automatically adds 4 to edi, so edi-0x04
  jmp _X03.SubstitutionR                         # Continue until we reach the end
_X03.EqualityR:                                  # ========================================
  repne scasd                                    # repne loops while not equal, checks for equal
  je _X03.Found                                  # If we found it, process as appropriate
  not ecx                                        # Set to -1 otherwise
  jmp _X03.ReturnSet                             # 
_X03.NonEqualityR:                               # ----------------------------------------
  repe scasd                                     # repe loops while equal, checks for not equal
  jne _X03.Found                                 # If we found it, process as appropriate
  not ecx                                        # Set to -1 otherwise
  jmp _X03.ReturnSet                             # 
_X03.NonRep:                                     # ========================================
  cmp DWORD PTR [edx+0x10],0x01                  # 
  je _X03.NonEqualityN                           # Non-equality type since it's 1
  jb _X03.EqualityN                              # Equality type since it's 0
  mov edx,DWORD PTR [edx+0x0c]                   # It's sub, so let's substitute the appropriate value
_X03.SubstitutionN:                              # ========================================
  cmp DWORD PTR [edi],eax                        # Check for equality
  jne _X03.NoSubstitutionN                       # cmove does not work here sadly
  mov DWORD PTR [edi],edx                        # Does not automatically add 4 to edi this time
_X03.NoSubstitutionN:                            # ----------------------------------------
  sub ecx,0x01                                   # Next elem
  lea edi,[edi+ebx*1]                            # 
  jnz _X03.SubstitutionN                         # 
  jmp _X03.ReturnSet                             # Return 0
_X03.EqualityN:                                  # ========================================
  cmp DWORD PTR [edi],eax                        # Check for equality
  je _X03.FoundN                                 # We found the thing good job
  sub ecx,0x01                                   # Decrement
  lea edi,[edi+ebx*1]                            # Next elem
  jnz _X03.EqualityN                             # 
  not ecx                                        # Set to -1 otherwise
  jmp _X03.ReturnSet                             # 
_X03.NonEqualityN:                               # ----------------------------------------
  cmp DWORD PTR [edi],eax                        # Check for inequality
  jne _X03.FoundN                                # We found the thing good job
  sub ecx,0x01                                   # Decrement
  lea edi,[edi+ebx*1]                            # Next elem
  jnz _X03.NonEqualityN                          # 
  not ecx                                        # Set to -1 otherwise
  jmp _X03.ReturnSet                             # 
_X03.MaskScan:                                   # ========================================
  mov ebx,DWORD PTR [edx+0x14]                   # Get adjustment value
  lea ecx,[ecx+0x01]                             # Might as well
_X03.MaskEqualityN:                              # ----------------------------------------
  mov ebp,DWORD PTR [edi]                        # Fetch value
  and ebp,DWORD PTR [edx+0x0c]                   # And with mask
  cmp eax,ebp                                    # Check for equality
  je _X03.FoundN                                 # We found the thing good job
  sub ecx,0x01                                   # Decrement
  lea edi,[edi+ebx*1]                            # Next elem
  jnz _X03.MaskEqualityN                         # 
  not ecx                                        # Set to -1 otherwise
  jmp _X03.ReturnSet                             # 
_X03.FoundN:                                     # ----------------------------------------
  dec ecx                                        # Emulate scasd
_X03.Found:                                      # ----------------------------------------
  neg ecx                                        # LoopCounter - ecx = Index
  add ecx,DWORD PTR [edx+0x08]                   # 
_X03.ReturnSet:                                  # ========================================
  mov DWORD PTR [esp+0x64],ecx                   # Return the appropriate position
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.FindElem (ID = 04)
--------------------------------------------
In the object specified in Param0, find the first active element that matches [Param1, Param2] and report it
*/
asm(R"(.intel_syntax noprefix
_X04:                                            # ========================================
  mov edi,DWORD PTR [edx]                        # Get the initial object
  mov ecx,DWORD PTR [edi+0x28]                   # Last Active Elem
  mov esi,DWORD PTR [edi+0x18]                   #
  mov eax,DWORD PTR [edx+0x04]                   # Get the designated pair
  mov edx,DWORD PTR [edx+0x08]                   #
_X04.ElemSeek:                                   # ========================================
  cmp DWORD PTR [esi],0x00                       # Must be active
  jz _X04.ElemNotFound                           #
  cmp DWORD PTR [esi+0x08],eax                   # Param1
  jne _X04.ElemNotFound                          #
  cmp DWORD PTR [esi+0x0c],edx                   # Param2
  je _X04.ElemFound                              #
_X04.ElemNotFound:                               # ----------------------------------------
  dec ecx                                        # Next elem
  lea esi,[esi+0x10]                             # 
  jg _X04.ElemSeek                               #
  xor eax,eax                                    # Return NULL
  jmp _X04.Return                                #
_X04.ElemFound:                                  # ----------------------------------------
  neg ecx                                        # Get index
  add ecx,DWORD PTR [edi+0x28]                   #
  mov eax,ecx                                    # Multiply by ElemLength
  cdq                                            # 
  mul DWORD PTR [edi+0x04]                       #
  add eax,DWORD PTR [edi+0x14]                   # Get to elem
_X04.Return:                                     # ========================================
  mov DWORD PTR [esp+0x64],eax                   # Return value
  ret                                            #
  .balign 0x10, 0x00
)");

/*
----------------
.GetSelfAnimListEntry (ID = 05)
--------------------------------------------
Finds the first active animation with a number in between Param0 and Param1, inclusive, and returns its animlist entry.
*/
asm(R"(.intel_syntax noprefix
_X05:                                            # ========================================
  mov eax,DWORD PTR [ebx]                        # Get to animArrayEx
  mov ecx,DWORD PTR [eax+0x000003cc]             #
  mov esi,DWORD PTR [ecx]                        #
  mov eax,DWORD PTR [esi+0x18]                   # animList
  mov ecx,DWORD PTR [esi+0x28]                   # 
  mov ebx,DWORD PTR [edx]                        # Minima
  mov edx,DWORD PTR [edx+0x04]                   # Maxima
_X05.AnimSeek:                                   # ========================================
  cmp DWORD PTR [eax],0x00                       # Must be active
  jz _X05.AnimNotFound                           #
  cmp DWORD PTR [eax+0x08],0x00                  # Param1 (explicitly anim, so)
  jne _X05.AnimNotFound                          #
  cmp DWORD PTR [eax+0x0c],ebx                   # Param2 min
  jl _X05.AnimNotFound                           #
  cmp DWORD PTR [eax+0x0c],edx                   # Param2 max
  jng _X05.Return                                #
_X05.AnimNotFound:                               # ----------------------------------------
  dec ecx                                        # Next elem
  lea eax,[eax+0x10]                             # 
  jg _X05.AnimSeek                               #
  xor eax,eax                                    # Return NULL
_X05.Return:                                     # ========================================
  mov DWORD PTR [esp+0x64],eax                   # Return value
  ret                                            #
  .balign 0x10, 0x00
)");

/*
----------------
.Polymorph (ID = 06)
--------------------------------------------
Will modify Anim 1777777700's first animation element to be what Beatrix's currently is.
This way animation won't matter in normal processing.
In addition, this alters the element's C1/C2 data.
Param0 corresponds to how we should handle Clsn1.
  If the value is 0, Clsn1 will be treated as it normally would be.
    HitBox Counter = Default
    All X and Y coords variable. Set the highest byte to the second-highest byte.
  If the value is greater than 0, Clsn1 will be changed to a full screen animation.
    HitBox Counter = 1
    First X and Y coords have the highest byte set to the second-lowest byte of the argument,
    Second coord set's highest bytes are set to the lowest byte.
    Usually, this argument is 0x9669.
  If the value is less than 0, Clsn1 will be nullified.
    HitBox Counter = 0. That's all
Param1 corresponds to how we should handle Clsn2. It gets the same exact treatment otherwise.
*/
asm(R"(.intel_syntax noprefix
_X06:                                            # ========================================
  mov ebx,DWORD PTR [ebx+0x000013bc]             # Access the AnimID database directly from our animations for ChangeAnim2 compatability
  mov edi,DWORD PTR [ebx]                        # Enter Anim Databank
  mov edi,DWORD PTR [edi]                        # 
  mov ebp,DWORD PTR [edi+0x14]                   # 
  mov edi,DWORD PTR [ebp]                        # Assume Anim 1777777700 is the first anim, adjust accordingly
  mov eax,DWORD PTR [ebx+0x0c]                   # Fetch current Animation ID
  mov edi,DWORD PTR [edi+0x14]                   # Fetch AnimElem 1 of our Polymorph anim
  lea eax,[eax+eax*4]                            # Prepare source animation offset
  add edi,0x08                                   # Don't overwrite any important pointers by adding 8 to edi
  mov esi,DWORD PTR [ebp+eax*4]                  # Fetch source Anim
  mov ecx,DWORD PTR [ebx+0x1c]                   # Fetch current AnimElemTime(1) due to a desync
  mov eax,DWORD PTR [esi+0x28]                   # Maximum AnimElemCount - 1
  mov esi,DWORD PTR [esi+0x14]                   # Fetch Source AnimElem Database
_X06.ElemGet:                                    # ========================================
  add esi,0x30                                   # Adjust to the next AnimElem
  sub eax,0x01                                   # The last one possible
  jl _X06.UseFrame                               # 
  cmp DWORD PTR [esi],ecx                        # If this one occurs later, end the loop
  jle _X06.ElemGet                               # 
_X06.UseFrame:                                   # ========================================
  sub esi,0x28                                   # Re-adjust, add 8 to esi accordingly
  xor ecx,ecx                                    # Set ecx to 8 such that we only overwrite visual data 
  mov cl,0x08                                    # 
  rep movsd                                      # Replace [Currently, esi @ Source+0x28. Conveniently, this is Clsn1]
  mov edi,DWORD PTR [esi+0x04]                   # Fetch Clsn2
  test edi,edi                                   # Skip if no Clsn2 exists
  mov eax,DWORD PTR [edx]                        # Fetch Clsn1 flags for later
  mov esi,DWORD PTR [esi]                        # Fetch Clsn1 address for later
  jz _X06.TweakClsn1                             # 
_X06.TweakClsn2:                                 # ----------------------------------------
  mov edx,DWORD PTR [edx+0x04]                   # Fetch Clsn2 flags
  test edx,edx                                   # Check flags
  setg cl                                        # If >0, 1 hitbox. If <0, 0 hitboxes
  movzx ecx,cl                                   #
  cmovz ecx,DWORD PTR [edi+0x0c]                 # If =0, restore hitboxes
  mov DWORD PTR [edi+0x04],ecx                   # Set hitboxes as appropriate
  jl _X06.TweakClsn1                             # This is all that we need to do if <0
  mov ebx,DWORD PTR [edi+0x08]                   # Fetch Coordinate pointer
  cmovz dx,WORD PTR [ebx+0x05]                   # Restoration protocol, X Coord 0 [use 2nd highest byte | 0x96]
  mov BYTE PTR [ebx+0x07],dh                     # 
  cmovz dx,WORD PTR [ebx+0x09]                   # Restoration protocol, Y Coord 0 [use 2nd highest byte | 0x96]
  mov BYTE PTR [ebx+0x0b],dh                     # 
  cmovz dx,WORD PTR [ebx+0x0e]                   # Restoration protocol, X Coord 1 [use 2nd highest byte | 0x69]
  mov BYTE PTR [ebx+0x0f],dl                     # 
  cmovz dx,WORD PTR [ebx+0x12]                   # Restoration protocol, Y Coord 1 [use 2nd highest byte | 0x69]
  mov BYTE PTR [ebx+0x13],dl                     # 
_X06.TweakClsn1:                                 # ----------------------------------------
  test esi,esi                                   # If no Clsn1 is present, end prematurely
  jz _X06.Finit                                  # 
  test eax,eax                                   # Identical procedure to Clsn2
  setg cl                                        # 
  movzx ecx,cl                                   # 
  cmovz ecx,DWORD PTR [esi+0x0c]                 # 
  mov DWORD PTR [esi+0x04],ecx                   # 
  jl _X06.Finit                                  #
  mov ebx,DWORD PTR [esi+0x08]                   # 
  cmovz ax,WORD PTR [ebx+0x05]                   # 
  mov BYTE PTR [ebx+0x07],ah                     # 
  cmovz ax,WORD PTR [ebx+0x09]                   # 
  mov BYTE PTR [ebx+0x0b],ah                     # 
  cmovz ax,WORD PTR [ebx+0x0e]                   # 
  mov BYTE PTR [ebx+0x0f],al                     # 
  cmovz ax,WORD PTR [ebx+0x12]                   # 
  mov BYTE PTR [ebx+0x13],al                     # 
_X06.Finit:                                      # ----------------------------------------
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.EVReset (ID = 07)
--------------------------------------------
Will set the extended variable space of the user's values to 0.
Prevents weird crosstalk from happening, and also prevents Beatrix from crashing if she fights herself.
*/
asm(R"(.intel_syntax noprefix
_X07:                                            # ========================================
  lea edi,[ecx+0x0003ff00]                       # Get to variable space
  sub edi,offset _X07                            # Properly adjust
  mov ebx,DWORD PTR [ebx+0x08]                   # Fetch Player number directly instead of looping to find it
  xor eax,eax                                    # Set eax to 0
  sub ebx,0x01                                   # Adjust accordingly
  xor ecx,ecx                                    # Set ecx to 0x0400 (1024)
  shl ebx,0x0c                                   # Adjust to the appropriate player
  mov ch,0x04                                    # 
  add edi,ebx                                    # 
  rep stosd                                      # XVar(###) := 0 || XFVar(###) := 0
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.QWORDMultiply (ID = 09)
--------------------------------------------
Multiplies Param0 by FVar(0) in a QWORD format.
To divide, prepare 1/FVar(0) in advance. fdiv is much slower, so I won't be including it.
Returns the value in Var(0) and Var(1).
Param1 will determine the rounding mode.
     0: Round Normally
     1: Round Up
     2: Round Down
     3: Truncate
If Param2 is non-zero, 0.5 will be added before the rounding operation.
*/
asm(R"(.intel_syntax noprefix
_X09:                                            # ========================================
  finit                                          # Initialize FPU
  mov ax,WORD PTR [edx+0x04]                     # Load control word value
  or al,0x04                                     # Respect ±∞ as a precautionary measure
  shl eax,0x0a                                   # Reposition to bits 10 and 11
  fstcw WORD PTR [edx+0x04]                      # Store control word for loading later
  or WORD PTR [edx+0x04],ax                      # Set rounding mode as appropriate
  fldcw WORD PTR [edx+0x04]                      # Reload control word with the correct rounding mode (+ the infinity flag too)
  fld DWORD PTR [ebx+0x00000f30]                 # Load float value to multiply by
  fimul DWORD PTR [edx]                          # Multiply by the Integer argument
  cmp DWORD PTR [edx+0x08],0x00                  # Check for zero
  je _X09.SkipTruncate                           # Skip past the addition of 0.5
  fld QWORD PTR [0x0049f300]                     # Load 0.5 into the FPU {st0=0.5, st1=PRODUCT}
  faddp st(1),st(0)                              # Add 0.5 to the product
_X09.SkipTruncate:                               # ========================================
  fistp QWORD PTR [ebx+0x00000e40]               # Store to Var(0), Var(1)
  finit                                          # Re-initialize FPU
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.DamageCalc (ID = 10)
--------------------------------------------
Complicated. Here's the params
    Param0: Damage To Be Returned
    Param1: Damage Blacklist Element 0
    Param2: Damage Blacklist Element 1
    Param3: DefenceMul [use xfvar]
    Param4: Damage Incrementation Value
    Param5: Damage that you should deal [RETURN]
    Param6: Damage that will be dealt [RETURN]
  Param7: NULL
  Param8: Settlement Value
Uses fdiv, despite its more costly nature, for the sake of better accuracy and, thus, lessening the amount of times this function needs to be called.
*/
asm(R"(.intel_syntax noprefix
_X0A:                                            # ========================================
  finit                                          # Initialize FPU
  mov esi,DWORD PTR [edx+0x04]                   # Store Blacklist in esi and edi
  mov edi,DWORD PTR [edx+0x08]                   # 
  fstcw WORD PTR [edx+0x04]                      # Store control word for loading later
  or BYTE PTR [edx+0x05],0x04                    # Set rounding mode to floor, store it in [edx+0x08]
  mov ax,WORD PTR [edx+0x04]                     # 
  fld DWORD PTR [edx+0x0c]                       # While we wait, proc this {st0=DefenceMul}
  or ah,0x08                                     # Set rounding mode to truncate, store it in [edx+0x0a] such that the FPU can alternate as needed
  mov WORD PTR [edx+0x06],ax                     # 
  mov ebp,DWORD PTR [edx+0x10]                   # Incrementation value in ebp
  xor ebx,ebx                                    # Set up 2**32
  mov DWORD PTR [edx+0x08],ebx                   # Don't need this anymore, right
  inc ebx                                        # 
  mov DWORD PTR [edx+0x0c],ebx                   # Store 2**32 here overall
  fild QWORD PTR [edx+0x08]                      # Load 2**32 from here {st0=2**32, st1=DefenceMul}
  fld QWORD PTR [0x0049f300]                     # Load 0.5 {st0=0.5, st1=2.0**32, st2=DefenceMul}
  fld1                                           # Load 1.0 {st0=1.0, st1=0.5, st2=2.0**32, st3=DefenceMul}
  xor ebx,ebx                                    # Reinitialize ebx
  jmp _X0A.Midway                                # Minimize Loop Overhead by jumping midway into the loop (FPU listed above is called "st0=DEFAULT")
_X0A.NextDamage:                                 # ========================================
  fstp st(0)                                     # Discard current iterator and damage values
  fstp st(0)                                     # 
  fstp st(0)                                     # {st0=DEFAULT}
  add DWORD PTR [edx],ebp                        # Move onto the next damage value
_X0A.BlacklistLogic:                             # ;-----------------------------
  cmp DWORD PTR [edx],0x00                       # If 0, increment
  je _X0A.TryAgain                               # 
  cmp DWORD PTR [edx],esi                        # If blacklisted, increment
  je _X0A.TryAgain                               # 
  cmp DWORD PTR [edx],edi                        # If not blacklisted, leave
  jne _X0A.Midway                                # 
_X0A.TryAgain:                                   # -----------------------------
  add DWORD PTR [edx],ebp                        # 
  jmp _X0A.BlacklistLogic                        # Keep going until we're good
_X0A.Midway:                                     # ========================================
  mov eax,DWORD PTR [edx]                        # To facilitate a comparison for later
  fild DWORD PTR [edx]                           # Load the desired damage value {st0=DMG, st1=DEFAULT}
  xor ecx,ecx                                    # Set ecx to 148 as this is the maximum amount of additional loops we'll need for this operation
  mov cl,0x9e                                    # 
  sub DWORD PTR [edx],0x01                       # We'll load a second copy now
  fild DWORD PTR [edx]                           # Load a second copy, with 1 subtracted {st0=DMG2, st1=DMG, st2=DEFAULT}
  fldz                                           # Load 0 for the purpose of abs/addition later {st0=0.0, st1=DMG2, st2=DMG, st3=DEFAULT}
  fwait                                          # 
  mov DWORD PTR [edx],eax                        # 
  fld st(2)                                      # Reload desired damage value {st0=DMG, st1=0.0, st2=DMG2, st3=DMG, st4=DEFAULT}
_X0A.DMGIterate:                                 # ========================================
  fldcw WORD PTR [edx+0x04]                      # Reset rounding mode to Floor
  fdiv st(0),st(7)                               # Divide {st0=ADJ, st1=n*2**32, st2=DMG2, st3=DMG, st4=DEFAULT}
  fistp QWORD PTR [edx+0x14]                     # Use our outputs as a new buffer for damage calculation {st0=n*2**32, st1=DMG2, st2=DMG, st3=DEFAULT}
  fwait                                          # Since we're reading data exported from the FPU, wait patiently
  cmp DWORD PTR [edx+0x14],0x00                  # If 0, skip directly to the end since it's a guaranteed 0 regardless of the result
  je _X0A.SkipIteration                          # 
  jg _X0A.SkipSubtract                           # If > 0, the below subtraction doesn't apply as accounting for a negative value need not occur
  sub DWORD PTR [edx+0x14],0x01                  # Subtract 1 to account for negative storage
_X0A.SkipSubtract:                               # -----------------------------
  fild DWORD PTR [edx+0x14]                      # Reload adjusted value {st0=READJ, st1=n*2**32, st2=DMG2, st3=DMG, st4=DEFAULT}
  fldcw WORD PTR [edx+0x06]                      # Set rounding mode to Truncate
  fadd st(0),st(5)                               # Perform Damage Truncation
  frndint                                        # {st0=READJ', st1=n*2**32, st2=DMG2, st3=DMG, st4=DEFAULT}
  fmul st(0),st(7)                               # Multiply by DefenceMul {st0=READJ", st1=n*2**32, st2=DMG2, st3=DMG, st4=DEFAULT}
  fadd st(0),st(5)                               # Perform Damage Truncation
  fistp QWORD PTR [edx+0x18]                     # Store new damage value {st0=n*2**32, st1=DMG2, st2=DMG, st3=DEFAULT}
  fwait                                          # Wait on FPU to export
  cmp DWORD PTR [edx+0x18],eax                   # If the damage values are equal, exit loop
  je _X0A.ExitLoop                               # 
_X0A.SkipIteration:                              # ========================================
  xor bl,0x01                                    # Check for second wind {st0=n*2**32, st1=DMG2, st2=DMG, st3=DEFAULT}
  jz _X0A.PrepDamage                             # 
  fld st(1)                                      # Check the -1 variant {st0=DMG2, st1=n*2**32, st2=DMG2, st3=DMG, st4=DEFAULT}
  jmp _X0A.DMGIterate                            # 
_X0A.PrepDamage:                                 # -----------------------------
  sub ecx,0x01                                   # Continue looping over these damage values about 159 times total (±0x4f + 0)
  jl _X0A.NextDamage                             # 
  fabs                                           # Set to a positive value for the sake of proper incrementation {st0=|n[i]|*2**32, st1=DMG2, st2=DMG, st3=DEFAULT}
  fadd st(0),st(5)                               # We'll also need to add 2**32 for the sake of swapping back and forth {st0=|n[i+1]|*2**32, st1=DMG2, st2=DMG, st3=DEFAULT}
  test cl,0x01                                   # On even passes, negate the current value
  jnz _X0A.SkipNegate                            # 
  fchs                                           # This is the negative thing {st0=n[i+1]*2**32, st1=DMG2, st2=DMG, st3=DEFAULT}
_X0A.SkipNegate:                                 # -----------------------------
  fadd st(1),st(0)                               # Adjust both damage backups
  fadd st(2),st(0)                               #  {st0=n[i+1]*2**32, st1=DMG2[i+1], st2=DMG[i+1], st3=DEFAULT}
  fld st(2)                                      # Reload DMG {st0=DMG[i+1], st0=n[i+1]*2**32, st1=DMG2[i+1], st2=DMG[i+1], st3=DEFAULT}
  sub DWORD PTR [edx+0x20],0x01                  # If we've iterated too much just end the loop and try to have the rest of the code patch up our mistakes
  jg _X0A.DMGIterate                             # (setting this to 1000 allows us to kill DarknessSevenNight, probably stick to that)
_X0A.ExitLoop:                                   # ========================================
  finit                                          # Re-initialize FPU
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.PlayerHelperX (ID = 12)
--------------------------------------------
Seeks out the first helper with a HelperID specified by Param0 owned by the player ID specified by Param1
The helper's ID is then returned if it exists, 0 otherwise
*/
asm(R"(.intel_syntax noprefix
_X0C:                                            # ========================================
  add eax,0x0000b84c                             # Set to the last player possible
  xor ecx,ecx                                    # Set ecx to -(62 - 4)
  sub ecx,0x3a                                   # 
  mov edi,DWORD PTR [edx]                        # Fetch desired HelperID
  mov edx,DWORD PTR [edx+0x04]                   # Fetch desired ParentID
_X0C.FPLoopStart:                                # ========================================
  mov esi,DWORD PTR [eax+ecx*0x04]               # Generic Forwards Player-Seeking Loop
  test esi,esi                                   # If the player doesn't exist, skip it
  jz _X0C.FPLoopNext                             # 
  cmp DWORD PTR [esi+0x00002618],edi             # Check if the HelperID matches
  jne _X0C.FPLoopNext                            # 
  mov ebp,DWORD PTR [esi+0x00002624]             # Fetch Root address
  test ebp,ebp                                   # Cool I guess
  jz _X0C.FPLoopNext                             # 
  cmp DWORD PTR [ebp+0x04],edx                   # Check if the Root's ID matches
  cmove ecx,DWORD PTR [esi+0x04]                 # Transfer the helper's ID to ecx if it does
  je _X0C.FPLoopEnd                              # 
_X0C.FPLoopNext:                                 # ----------------------------------------
  add ecx,0x01                                   # 
  jng _X0C.FPLoopStart                           # 
  xor ecx,ecx                                    # If nothing is found, set to 0
_X0C.FPLoopEnd:                                  # ========================================
  mov DWORD PTR [esp+0x64],ecx                   # Return the appropriate ID
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.NextProj (ID = 13)
--------------------------------------------
Grabs the address that the next projectile called by this player will spawn into and returns it
For attribute modification purposes (no need to use SCA,AA,AT,AP | SC,AA,AT,AP | S,NA | etc.)
*/
asm(R"(.intel_syntax noprefix
_X0D:                                            # ========================================
  mov esi,DWORD PTR [ebx+0x00002624]             # Fetch root address
  test esi,esi                                   # If zero, will use our own projectile space, as per MUGEN processing.
  cmovnz ebx,esi                                 # If not, will use the projectile space of that player.
  mov ecx,DWORD PTR [ebx+0x0000021c]             # Obtain Projectile Databank Pointer
  mov eax,DWORD PTR [ecx]                        # 
  mov ecx,DWORD PTR [eax+0x08]                   # Active ElemCount
  test ecx,ecx                                   # If 0, then it's just the first entry
  mov edi,DWORD PTR [eax+0x14]                   # Info
  jz _X0D.StoreProj                              # 
  mov ebx,DWORD PTR [eax+0x24]                   # First Active in ebx
  mov edx,DWORD PTR [eax+0x28]                   # Last Active in edx
  mov esi,edx                                    # Fetch the difference between the two
  sub esi,ebx                                    # 
  cmp esi,ecx                                    # If this difference is smaller than the number of elems, then there's no free space.
  jl _X0D.NoFreeSpace                            # 
  inc ebx                                        # Loop until we find what's in the free space
  mov esi,ebx                                    # 
  shl esi,0x04                                   # 
  add esi,DWORD PTR [eax+0x18]                   # 
_X0D.ProjLoop:                                   # ========================================
  cmp DWORD PTR [esi],0x00                       # If it doesn't exist, use it
  je _X0D.AdjustToProj                           # 
  inc ebx                                        # Loop until we find it
  lea esi,[esi+0x10]                             # 
  jmp _X0D.ProjLoop                              # (for the record, this potential infinite loop also exists in the engine itself)
_X0D.NoFreeSpace:                                # ========================================
  test ebx,ebx                                   # If this is an actual value, then take this -1.
  jz _X0D.UseLastElem                            # 
  dec ebx                                        # 
  jmp _X0D.AdjustToProj                          # 
_X0D.UseLastElem:                                # ----------------------------------------
  lea ebx,[edx+0x01]                             # If the elem after the current last is in range, then use it 
  cmp ebx,DWORD PTR [eax+0x0c]                   # 
  jl _X0D.AdjustToProj                           # 
  xor edi,edi                                    # Otherwise return NULL
  jmp _X0D.StoreProj                             # 
_X0D.AdjustToProj:                               # ========================================
  imul ebx,DWORD PTR [eax+0x04]                  # Multiply by the byte length
  add edi,ebx                                    # Get the proj's reference
_X0D.StoreProj:                                  # ----------------------------------------
  mov DWORD PTR [esp+0x64],edi                   # Store Address
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.ModifyAnim (ID = 14)
--------------------------------------------
Will change all Explods with an ID of Param0's animation to Param1. Set to -1 for NOCHANGE
If Param2 is non-zero, will also keep the time elapsed the same.
*/
asm(R"(.intel_syntax noprefix
_X0E:                                          # ========================================
  mov esi,DWORD PTR [eax+0x0000a2e4]           # Access the Explod databank
  mov eax,0xffffffff                           # 
  cmp DWORD PTR [edx+0x04],0xffffffff          # NOCHANGE
  je _X0E.SkipAnim                             # 
  mov eax,DWORD PTR [ebx]                      # Enter AnimID Database
  mov edi,DWORD PTR [eax+0x000003cc]           # 
  mov eax,DWORD PTR [edi]                      # 
  mov edi,DWORD PTR [eax+0x18]                 # 
  mov ecx,DWORD PTR [eax+0x28]                 # LoopMax
  mov eax,ecx                                  # Storage
  mov ebp,DWORD PTR [edx+0x04]                 # Comparison value
_X0E.AnimSeek:                                 # ========================================
  cmp DWORD PTR [edi+0x0c],ebp                 # Check for activity
  je _X0E.StoreAnim                            # 
  sub ecx,0x01                                 # The usual
  lea edi,[edi+0x10]                           # 
  jnl _X0E.AnimSeek                            # Continue until failure
  lea eax,[ecx-0x01]                           # No need for a jmp if I just do this
_X0E.StoreAnim:                                # ----------------------------------------
  sub eax,ecx                                  # Fetch ID by taking storedMax - remainder
_X0E.SkipAnim:                                 # ----------------------------------------
  mov ecx,DWORD PTR [esi+0x08]                 # Fetch currently loaded Explods to make our life easier
  mov ebx,DWORD PTR [ebx+0x04]                 # Fetch the ID now, may as well
  mov esi,DWORD PTR [esi]                      # 
  add esi,0x04                                 # Convenience
  mov ebp,DWORD PTR [edx]                      # Comparison value
_X0E.ExplodAcq:                                # ========================================
  cmp DWORD PTR [esi-0x04],0x00                # Check for activity
  jz _X0E.SkipExplod                           # 
  cmp DWORD PTR [esi+0x08],ebx                 # If this Explod is ours, continue
  lea ecx,[ecx-0x01]                           # Decrement either way
  jne _X0E.SkipExplod                          # 
  cmp DWORD PTR [esi+0x0c],ebp                 # If this Explod is the correct one, continue
  jne _X0E.SkipExplod                          # 
  cmp eax,0xffffffff                           # If -1 DNE
  mov edi,DWORD PTR [esi+0x7c]                 # The reason for the convenience add
  je _X0E.SkipExplod                           # 
  mov DWORD PTR [edi+0x0c],eax                 # Set AnimID
  cmp DWORD PTR [edx+0x08],0x00                # Check for Stasis
  jnz _X0E.SkipExplod                          # 
  xor ebp,ebp                                  # Space-saving
  mov DWORD PTR [edi+0x14],ebp                 # 
  mov DWORD PTR [edi+0x18],ebp                 # 
  mov DWORD PTR [edi+0x1c],ebp                 # 
  mov ebp,DWORD PTR [edx]                      # Restore convenience
_X0E.SkipExplod:                               # ========================================
  cmp ecx,0x00                                 # If we have any left to iterate through
  lea esi,[esi+0x000000e4]                     # Move to the next Explod
  jg _X0E.ExplodAcq                            # 
  ret                                          # 
  .balign 0x10, 0x00
)");

/*
----------------
.ExplodRotato (ID = 15) [DEPRECATED]
--------------------------------------------
Explods get passed between players as follows: Param0 -> Param1 -> Param2
This is so that way Beatrix appears to have a NumExplod = 0 at all times, unless otherwise needed.
*/
asm(R"(.intel_syntax noprefix
_X0F:                                            # ========================================
  mov edi,DWORD PTR [eax+0x0000a2e4]             # Access Explod Databank
  mov ecx,DWORD PTR [edi+0x08]                   # Try to be more efficient by fetching the total number of NumExplods and using that value instead
  mov esi,DWORD PTR [edi]                        # 
  mov eax,DWORD PTR [edx]                        # eax = Param0
  mov ebx,DWORD PTR [edx+0x04]                   # ebx = Param1
  mov edx,DWORD PTR [edx+0x08]                   # edx = Param2
_X0F.ExplodLoop:                                 # ========================================
  cmp DWORD PTR [esi],0x00                       # If this Explod is active, continue (.ExplodAcq)
  je $+0x17                                      # 
  sub ecx,0x01                                   # Decrement remaining Explod counter
  cmp DWORD PTR [esi+0x0c],eax                   # Check for rotation to Param1
  jne $+0x07                                     # 
  mov DWORD PTR [esi+0x0c],ebx                   # 
  jmp $+0x0a                                     # Skip rotation check if it just occurred
  cmp DWORD PTR [esi+0x0c],ebx                   # Check for rotation to Param2
  jne $+0x05                                     # 
  mov DWORD PTR [esi+0x0c],edx                   # 
_X0F.NextExplod:                                 # ----------------------------------------
  add esi,0x000000e4                             # Move to the next Explod
  test ecx,ecx                                   # If we have any left to iterate through
  jg _X0F.ExplodLoop                             # 
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.NextHelper (ID = 16)
--------------------------------------------
Grabs the address that the next helper will spawn into and returns its address.
This is so we don't have to detect anything that could be tampered with.
*/
asm(R"(.intel_syntax noprefix
_X10:                                            # ========================================
  add eax,0x0000b84c                             # Adjust MUGEN's address to the last player's address
  xor ecx,ecx                                    # Set ecx to -(62 - 4)
  sub ecx,0x3a                                   # 
  xor ebp,ebp                                    # Helper counter
  xor ebx,ebx                                    # Set ebx to 0 for checksum's sake
_X10.FPLoopStart:                                # ========================================
  mov esi,DWORD PTR [eax+ecx*0x04]               # Generic Forwards Player-Seeking Loop
  test esi,esi                                   # 
  jz _X10.FPLoopNext                             # 
  cmp DWORD PTR [esi+0x00000158],0x00            # Check if the player flag is zero
  je _X10.FPLoopExit                             # 
_X10.FPLoopNext:                                 # ----------------------------------------
  add ecx,0x01                                   # Loop until no players are left
  jng _X10.FPLoopStart                           # 
  xor esi,esi                                    # Return 0
_X10.FPLoopExit:                                 # ========================================
  mov DWORD PTR [esp+0x64],esi                   # Return the appropriate player address
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.NextExplod (ID = 17)
--------------------------------------------
(Formerly .HelperInitX)
Seeks out the next empty Explod. Returns the address (or 0 if it cannot spawn)
If an Occupancy Explod is present, this will delete that Explod to make room.
*/
asm(R"(.intel_syntax noprefix
_X11:                                            # ========================================
  mov edi,DWORD PTR [eax+0x0000a2e4]             # Access Explod Databank
  mov ecx,DWORD PTR [edi+0x04]                   # Fetch ExplodMax
  mov esi,DWORD PTR [edi]                        # Explod List
  mov ebx,DWORD PTR [ebx+0x04]                   # Fetch ID
_X11.ExplodLoop:                                 # ========================================
  cmp DWORD PTR [esi],0x00                       # If this Explod is active, continue (.ExplodAcq)
  jz _X11.DoneZo                                 #
  cmp DWORD PTR [esi+0x0c],ebx                   # Belongs to us
  jne _X11.NextExplod                            #
  cmp DWORD PTR [esi+0x10],0x34fb5e38            # Occupancy Explod
  jne _X11.NextExplod                            #
  mov DWORD PTR [esi],0x00                       # Explod Deletion
  sub DWORD PTR [edi+0x08],0x01                  # Decrement ExplodCount
  jmp _X11.DoneZo                                # 
_X11.NextExplod:                                 # ----------------------------------------
  sub ecx,0x01                                   # Move to the next Explod
  lea esi,[esi+0x000000e4]                       # 
  jg _X11.ExplodLoop                             # 
  xor esi,esi                                    # DNE
_X11.DoneZo:                                     # ========================================
  mov DWORD PTR [esp+0x64],esi                   # 
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.EnemyInit (ID = 18)
--------------------------------------------
Will have the enemy grab their own address for usage later on.
Output to GetHitVar(Fall.RecoverTime)
*/
asm(R"(.intel_syntax noprefix
_X12:                                            # ========================================
  mov DWORD PTR [ebx+0x00001088],ebx             # Set Fall.RecoverTime to Self Address
  xor eax,eax                                    # Store a billion 0s
  mov DWORD PTR [0x004b445c],eax                 # Tier Defense Crash Avoidance
  mov DWORD PTR [0x004b4460],eax                 # 
  mov BYTE PTR [0x004b6981],al                   # Disable the ESC key's weird 1-off behaviors
  mov DWORD PTR [0x004b48e8],0x00496651          # Restore %f's address, just as a precaution since some DTC uses %f for its intended purpose
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
---------------
.RedirectFree (ID = 19)
--------------------------------------------
Really this just blows up Beatrix's helper with the correct player ID if it exists.
Done to guarantee redirect helpers spawn properly.
Limited to one of 5 cases: enemy(facing < 0),var(3)/100*100 + [2, 4, 6, 8, 10]
[TBO]
*/
asm(R"(.intel_syntax noprefix
_X13:                                            # ========================================
  mov edi,DWORD PTR [edx]                        # Fetch the desired player ID to blow up
  lea ebx,[eax+0x0000b64c]                       # Fetch PlayerSetEnabled for decrementation later
  add eax,0x0000b84c                             # Adjust MUGEN's address to the last player's address
  xor ecx,ecx                                    # Set ecx to -(62 - 4)
  sub ecx,0x3a                                   # 
_X13.PlayerLoop:                                 # ========================================
  mov edx,DWORD PTR [eax+ecx*0x04]               # Generic Forwards Player-Seeking Loop (.PLoopStart)
  test edx,edx                                   # 
  jz _X13.NextPlayer                             # 
  cmp DWORD PTR [edx+0x04],edi                   # Check if the ID matches
  je _X13.FoundPlayer                            # 
_X13.NextPlayer:                                 # ----------------------------------------
  inc ecx                                        # Continue until done
  jng _X13.PlayerLoop                            # 
  ret                                            # Can't find anything else!
_X13.FoundPlayer:                                # ========================================
  sub DWORD PTR ds:[ebx],0x01                    # Decrement PlayerSet Enabled
  and DWORD PTR ds:[edx+0x00000158],0x00         # バイツァ・ダスト
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.EnhancedDEK (ID = 20)
--------------------------------------------
Will copy the enemy/enemies' state numbers and save them as our own.
This utilizes the free space provided in the DEK file
*/
asm(R"(.intel_syntax noprefix
_X14:                                            # ========================================
  mov edi,DWORD PTR [ebx+0x00000be8]             # Fetch Base State Code pointer
  mov edi,DWORD PTR [edi]                        # 
  mov edi,DWORD PTR [edi+0x18]                   # Enter our StateNo table
_X14.SeekDef:                                    # ========================================
  add edi,0x10                                   # Try to find StateDef -1777777777
  cmp DWORD PTR [edi-0x08],0x9609438f            # 
  jne _X14.SeekDef                               # We'll eventually find it
  add edi,0x08                                   # Adjust to StateNo
  xor eax,eax                                    # Reset Iteration Flag
  xor ecx,ecx                                    # Set ecx to 32768 such that we don't overwrite anything by accident
  mov ch,0x80                                    # 
_X14.DefGrabber:                                 # ========================================
  mov esi,DWORD PTR [ebx+0x00000148]             # Fetch the appropriate enemy address
  test esi,esi                                   # If the enemy doesn't exist, return prematurely
  jz _X14.Return                                 # 
  mov esi,DWORD PTR [esi+0x00000be8]             # Largely going to do the same thing as getting our own state data
  mov esi,DWORD PTR [esi]                        # 
  mov edx,DWORD PTR [esi+0x28]                   # Fetch StateDef Count
  mov esi,DWORD PTR [esi+0x18]                   # Enter the enemy's StateNo table
  add esi,0x08                                   # Adjust to StateNo
  cmp edx,ecx                                    # Just in case
  cmovg edx,ecx                                  # 
  sub ecx,edx                                    # Update loop counter as appropriate
_X14.CopyPaste:                                  # ------------------------
  movsd                                          # Transfer StateNo
  add esi,0x0c                                   # Move to next State
  lea edi,[edi+0x0c]                             # 
  sub edx,0x01                                   # Continue until we run out of StateDefs
  jnl _X14.CopyPaste                             # 
_X14.Return:                                     # ========================================
  test eax,eax                                   # If 2 iterations have occurred, return
  mov al,0x01                                    # (Set eax to 1, add 4 neutrally to ebx)
  lea ebx,[ebx+0x04]                             # 
  jz _X14.DefGrabber                             # 
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.DEKRevision (ID = 21)
--------------------------------------------
Will copy a desired player's StateNo and PrevStateNo and store it accordingly in our StateDefs.
This also utilizes the free space provided in the DEK file
If Param0 is less than 0, this operation will occur for all players.
Otherwise, it will only occur for the player with a number corresponding to Param0.
    In this particular case, Param1 and Param2 will dictate the states to actually write.
*/
asm(R"(.intel_syntax noprefix
_X15:                                            # ========================================
  mov edi,DWORD PTR [ebx+0x00000be8]             # Fetch Base State Code pointer
  mov edi,DWORD PTR [edi]                        # 
  mov edi,DWORD PTR [edi+0x18]                   # Enter our StateNo table
_X15.MinusSearch:                                # ========================================
  add edi,0x10                                   # Try to find StateDef -1777777777
  cmp DWORD PTR ds:[edi-0x08],0x9609438f         # 
  jne _X15.MinusSearch                           # We'll eventually find it
  add edi,0x00080008                             # Re-adjust edi to be positioned over the appropriate StateDef number (FreeSpace + 32768 + 1 StateDefs)
  add eax,0x0000b754                             # Adjust MUGEN's address to the first player's address
  xor ecx,ecx                                    # Set ecx to either 62 or Param0
  cmp DWORD PTR [edx],0x00                       # Check for Param0 ≥ 0
  lea esi,[edx+0x04]                             # For the sake of SingleIter, store the default value of Param1
  mov edx,DWORD PTR [edx]                        # Store Param 0 in edx for later purposes
  jnl _X15.SingleIter                            # 
  mov cl,0x3e                                    # 
_X15.BPLoopStart:                                # ========================================
  mov esi,DWORD PTR [eax+ecx*4]                  # Generic Backwards Player-Seeking Loop
_X15.BPLoopMidway:                               # ----------------------------------------
  test esi,esi                                   # If the player doesn't exist, skip it
  jz _X15.BPLoopNext                             # 
  add esi,0x00000bf4                             # Copy StateNo and PrevStateNo to our DEK buffer
  movsd                                          # 
  add edi,0x0c                                   # Get to the next DEK buffer state
  movsd                                          # 
  lea edi,[edi+0x0c]                             # 
_X15.BPLoopNext:                                 # ----------------------------------------
  sub ecx,0x01                                   # 
  jnl _X15.BPLoopStart                           # 
  ret                                            # 
_X15.SingleIter:                                 # ========================================
  lea ebx,[edx+edx]                              # Store ebx as edx*2 for the sake of easy *4 multiplication later
  add ebx,ebx                                    # Make it 4*edx in order to do 0x20 properly
  lea edi,[edi+ebx*8]                            # Add edx*0x20 to edi for proper adjustment
  movsd                                          # Copy Param1 and Param2 to our DEK buffer
  add edi,0x0c                                   # Get to the next DEK buffer state
  movsd                                          # 
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.AnimRevision (ID = 22)
--------------------------------------------
Copies the designated animation number into Beatrix's animation data, and changes to that anim.
To use efficiently, specify as follows:
value = 1777777777
elem = ? + 0*exec(22)
*/
asm(R"(.intel_syntax noprefix
_X16:                                            # ========================================
  xor edi,edi                                    # Reset Anim Address for the below comparison
  mov eax,DWORD PTR [edx]                        # Get the desired animation number
  add edi,DWORD PTR [ebx+0x000013c0]             # Fetch ChangeAnim2 Address
  mov ebp,DWORD PTR [ebx+0x000013bc]             # In addition, grab our true animation reference in order for later swapping
  cmovz edi,DWORD PTR [ebp+0x00]                 # If there isn't anything just take from our own
  mov esi,DWORD PTR [edi]                        # 
  mov edi,DWORD PTR [esi+0x18]                   # 
  mov edx,DWORD PTR [ebp+0x0c]                   # Is this the trivial solution?
  lea ecx,[edx+edx*1]                            # 
  cmp DWORD PTR [edi+ecx*8+0x0c],0x69f6bc71      # In other words, is this the freespace marker?
  je _X16.TrivialSolution                        # If so (more likely), branch to the trivial solution
  mov ecx,DWORD PTR [esi+0x28]                   # Loop End Counter init
  xor edx,edx                                    # If not, continue
  lea ecx,[ecx-0x3f]                             # Overflow Error Prevention
_X16.AnimSeek:                                   # ========================================
  cmp DWORD PTR [edi+0x0c],0x69f6bc71            # See if this animation number is freespace
  je _X16.AnimFound                              # Get to it eventually
  add edi,0x10                                   # Move to next animation number
  sub ecx,0x01                                   # Continue until nothing remains
  lea edx,[edx+0x01]                             # 
  jnl _X16.AnimSeek                              # 
  ret                                            # Do nothing if we can't find it
_X16.TrivialSolution:                            # ========================================
  lea edi,[edi+ecx*8]                            # Adjust address to the appropriate value
_X16.AnimFound:                                  # ----------------------------------------
  mov ebx,DWORD PTR [ebx+0x08]                   # Assume that player number is the correct offset
  add edx,ebx                                    # 
  mov DWORD PTR [ebp+0x0c],edx                   # Set our anim ID to this new ID
  shl ebx,0x04                                   # 
  mov DWORD PTR [edi+ebx*1+0x0c],eax             # Replace animation number for proper impersonation
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.BruteHOrce (ID = 24)
--------------------------------------------
Will take HitByAttr (Param0) and & it with all HitOverrideAttr (Param1~8) until a hole in the armor is found.
UPDATE NOTES: This is split on a WORD basis, not a 4096 basis
Outputs the result. If we can't find one, it returns 0.
*/
asm(R"(.intel_syntax noprefix
_X18:                                            # ========================================
  xor ecx,ecx                                    # Set ecx to 0x0100
  mov ch,0x01                                    # 
  xor eax,eax                                    # Reset eax (compatability)
  xor ebx,ebx                                    # Reset ebx
_X18.LoopReboot:                                 # ----------------------------------------
  mov bl,0x07                                    # Set ebx to 7
  movzx esi,WORD PTR [edx]                       # Fetch HitByAttr
_X18.AndLoop:                                    # ========================================
  bt ecx,ebx                                     # If this bit is set, use the upper WORD
  setc al                                        # 
  add eax,0x02                                   # 
  lea eax,[eax+ebx*2]                            # 
  and si,WORD PTR [edx+eax*2]                    # AND with the correct set of attributes
  jz _X18.NextIter                               # Bitwise and sets the zero flag, so we can skip prematurely if this is 0
  sub bl,0x01                                    # Subtract 1 from bl such that only the lower byte is impacted during looping
  jnl _X18.AndLoop                               # 
  test si,0x0007                                 # If SCA isn't present to some degree, return to the loop
  jz _X18.NextIter                               # 
  test si,0x0ff8                                 # If both SCA and ,AA,AT,AP are present, return immediately
  jnz _X18.EndLoop                               # 
_X18.NextIter:                                   # ----------------------------------------
  xor esi,esi                                    # Output should be 0 at this point
  sub ecx,0x01                                   # Decrement ecx
  jnz _X18.LoopReboot                            # 
_X18.EndLoop:                                    # ========================================
  mov DWORD PTR [esp+0x64],esi                   # Output the result
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
---------------
.StCtrlCheck (ID = 26)
--------------------------------------------
"How many state controllers are ACTUALLY in this state?"
Param0 = StateNo
Param1 = State Code
Value is state-dependent
  Regardless of circumstance, 0 means the state doesn't exist
  513+ is overflow
If Param1 is -1, the number of controllers is ignored and it simply returns if the state exists or not, outside of our own DEK
*/
asm(R"(.intel_syntax noprefix
_X1A:                                            # ========================================
  mov edi,DWORD PTR [edx+0x04]                   # Load state code
  cmp edi,0xffffffff                             # Beatrix Self-Check
  mov edx,DWORD PTR [edx]                        # While we're here, grab StateNo too
  je _X1A.ThisIsBeatrix                          # 
  mov ebx,DWORD PTR [edi]                        # 
  xor eax,eax                                    # Set flag to 0 (Default case: DNE)
  mov ecx,DWORD PTR [ebx+0x28]                   # Get the maximum index so we know when to stop
  mov esi,DWORD PTR [ebx+0x18]                   # Let's try to find this state
  test esi,esi                                   # Root Deleter ! Oops !
  jz _X1A.ReturnValue                            #
_X1A.StateSeek:                                  # ========================================
  cmp DWORD PTR [esi+0x08],edx                   # If this happens to be the state we're looking for, end the loop (.StateSeek)
  je _X1A.ProcessState                           # 
  sub ecx,0x01                                   # 
  lea esi,[esi+0x10]                             # Adjust accordingly
  jnl _X1A.StateSeek                             # Continue looping until we reach the end of the list
  jmp _X1A.ReturnValue                           # Return 0 if we have
_X1A.ProcessState:                               # ----------------------------------------
  mov ecx,DWORD PTR [esi+0x04]                   # Fetch the state's ID
  mov esi,DWORD PTR [ebx+0x14]                   # Enter the actual state code definition table
  imul ecx,ecx,0x52                              # Multiply ecx by 0xa4/2 to get half of the correct offset
  mov ebx,DWORD PTR [esi+ecx*2]                  # Fetch the corresponding state code
  mov eax,DWORD PTR [ebx+0x0c]                   # Fetch the amount of state controllers
  jmp _X1A.ReturnValue                           # Return it
_X1A.ThisIsBeatrix:                              # ========================================
  mov edi,DWORD PTR [ebx+0x00000be8]             # Fetch Self State Code
  mov ebx,DWORD PTR [edi]                        # 
  xor eax,eax                                    # Set flag to 0 (Default case: DNE)
  mov esi,DWORD PTR [ebx+0x18]                   # Let's try to find this state
_X1A.StateSeekBeatrix:                           # ========================================
  cmp DWORD PTR [esi+0x08],edx                   # If this happens to be the state we're looking for, end the loop (.StateSeek)
  sete al                                        # Set to 1 and return, basically
  je _X1A.ReturnValue                            # 
  cmp DWORD PTR [esi+0x08],0x9609438f            # Def -1777777777 marks the start of DEK
  lea esi,[esi+0x10]                             # Adjust accordingly
  jne _X1A.StateSeekBeatrix                      # As long as we don't run into it, we can continue freely
_X1A.ReturnValue:                                # ========================================
  mov DWORD PTR [esp+0x64],eax                   # literally
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.ExMiracle (ID = 27)
--------------------------------------------
From the animation data provided as Param0, depending on context (Param1), will either:

Param1 = 0:
  Scan every animation and store the resulting information in the information storage provided
  Param2 = InfoStorage
  OUTPUT: Length of InfoStorage
Param1 = 1:
  Search our information storage for the an output that matches the appropriate criteria
  Param2 = InfoStorage
  Param3 = Position of the output (OoB = random, -1 = count)
  Param4 = Delay Timer
  Param5 = FlagList
        &1: Clsn1 must exist
        &2: Clsn2 must exist
        &4: The anim's number must be non-negative
        &8: It cannot be 0-length
        &16: It cannot be a dupe
  OUTPUT: -1 (doesn't exist) / Count (if Param3 is -1) / Position in InfoStorage (otherwise)
Param1 = 2:
  Search our information storage for the anim specified.
  Param2 = InfoStorage
  Param3 = Anim
  Param4 = Anim isID
  OUTPUT: -1 (doesn't exist) / Position in InfoStorage (otherwise)
*/
asm(R"(.intel_syntax noprefix
_X1B:                                            # ========================================
  cmp DWORD PTR [edx+0x04],0x01                  # Check type
  mov eax,DWORD PTR [edx]                        # AnimData in ebx (there's an outer shell)
  mov ebx,DWORD PTR [eax]                        # 
  je _X1B.MiracleSeek                            # 
  ja _X1B.MiracleSearch                          # 
_X1B.MiracleScanner:                             # ========================================
  mov edi,DWORD PTR [edx+0x08]                   # InfoStorage in edi
  xor eax,eax                                    # Reset eax
  xor ebp,ebp                                    # Reset ebp
_X1B.MiracleScanLoop:                            # ----------------------------------------
  imul edx,ebp,0x14                              # Fetch offset from ebp
  add edx,DWORD PTR [ebx+0x14]                   # AnimDex
  mov edx,DWORD PTR [edx]                        # Fetch the anim located here
  mov ecx,DWORD PTR [edx+0x0c]                   # 
  cmp ecx,0x01                                   # If we found an empty animation, store it as a crash anim
  jnl _X1B.MiracleScanParse                      # 
  mov DWORD PTR [edi+0x04],0xfffffffd            # Crash Anim Flag
_X1B.MiracleScanIncrement:                       # ----------------------------------------
  mov DWORD PTR [edi],ebp                        # AnimID
  inc eax                                        # Next position
  lea edi,[edi+0x14]                             # 
  cmp ax,0x1000                                  # Run out of space, hard return 4096
  jnb _X1B.Return                                # 
  jmp _X1B.MiracleScanNext                       # 
_X1B.MiracleScanParse:                           # ----------------------------------------
  mov esi,DWORD PTR [edx+0x14]                   # Fetch AnimElemData for this elem
  xor edx,edx                                    # Reset edx (last we need of it)
  sub ecx,0x01                                   # For ease of comparison
_X1B.MiracleScanElemLoop:                        # - - - - - - - - - - - - - - - - - - - -
  cmp DWORD PTR [esi+0x04],0x00                  # If 0, skip
  jne _X1B.MiracleScanClsn1Check                 # 
  cmp edx,ecx                                    # Only if non-last element 
  jne _X1B.MiracleScanElemNext                   # 
  cmp edx,0x00                                   # Only if the 1st element (last is too unstable)
  jne _X1B.MiracleScanElemNext                   # 
_X1B.MiracleScanClsn1Check:                      # - - - - - - - - - - - - - - - - - - - -
  cmp DWORD PTR [edi+0x0c],0x00                  # Clsn1 already is found so check for Clsn2
  jnl _X1B.MiracleScanClsn2Check                 # 
  cmp DWORD PTR [esi+0x28],0x00                  # Clsn1 exists
  je _X1B.MiracleScanClsn2Check                  # (i'm not gonna check hitbox count as i'm too lazy)
  push DWORD PTR [esi]                           # need to borrow this for a moment
  mov DWORD PTR [edi+0x04],edx                   # C1Elem Storage
  cmp DWORD PTR [esi+0x04],0x00                  # If AnimElemTime = 0, store -2
  jne _X1B.MiracleScanClsn1StoreTime             # 
  mov DWORD PTR [edi+0x04],0xfffffffe            # 
_X1B.MiracleScanClsn1StoreTime:                  # - - - - - - - - - - - - - - - - - - - -
  pop DWORD PTR [edi+0x0c]                       # C1Time Storage
_X1B.MiracleScanClsn2Check:                      # - - - - - - - - - - - - - - - - - - - -
  cmp DWORD PTR [edi+0x10],0x00                  # Clsn2 already is found so check if C1 exists
  jnl _X1B.MiracleScanClsn2Found                 # 
  cmp DWORD PTR [esi+0x2c],0x00                  # Clsn2 exists
  je _X1B.MiracleScanElemNext                    # (i'm not gonna check hitbox count as i'm too lazy)
  push DWORD PTR [esi]                           # need to borrow this for a moment
  mov DWORD PTR [edi+0x08],edx                   # C2Elem Storage
  cmp DWORD PTR [esi+0x04],0x00                  # If AnimElemTime = 0, store -2
  jne _X1B.MiracleScanClsn2StoreTime             # 
  mov DWORD PTR [edi+0x08],0xfffffffe            # 
_X1B.MiracleScanClsn2StoreTime:                  # - - - - - - - - - - - - - - - - - - - -
  pop DWORD PTR [edi+0x10]                       # C2Time Storage
_X1B.MiracleScanClsn2Found:                      # - - - - - - - - - - - - - - - - - - - -
  cmp DWORD PTR [edi+0x0c],0x00                  # C1 already is found so we're done here
  jnl _X1B.MiracleScanIncrement                  # 
_X1B.MiracleScanElemNext:                        # - - - - - - - - - - - - - - - - - - - -
  add edx,0x01                                   # Next elem
  lea esi,[esi+0x30]                             # 
  cmp edx,ecx                                    # 
  jg _X1B.MiracleScanPostProcess                 # If OoB skip this extra check
  cmp DWORD PTR [esi],0x00                       # Negative is invalid! DNE if it is
  jnl _X1B.MiracleScanElemLoop                   # Otherwise continue
_X1B.MiracleScanPostProcess:                     # - - - - - - - - - - - - - - - - - - - -
  cmp DWORD PTR [edi+0x0c],0x00                  # C1 is found so increment
  jnl _X1B.MiracleScanIncrement                  # 
  cmp DWORD PTR [edi+0x10],0x00                  # C2 is found so increment
  jnl _X1B.MiracleScanIncrement                  # 
_X1B.MiracleScanNext:                            # ----------------------------------------
  add ebp,0x01                                   # Next anim
  cmp ebp,DWORD PTR [ebx+0x0c]                   # 
  jl _X1B.MiracleScanLoop                        # 
  jmp _X1B.Return                                # 
_X1B.MiracleSeek:                                # ========================================
  mov edi,DWORD PTR [edx+0x08]                   # InfoStorage in edi
  xor eax,eax                                    # Position is output
  xor esi,esi                                    # Counter
  mov ebx,DWORD PTR [ebx+0x18]                   # I only need the AnimList in this case
  mov ecx,DWORD PTR [edx+0x0c]                   # Store the counter on the stack to prevent unnecessary ExpStack writes
  push ecx                                       # 
_X1B.MiracleSeekLoop:                            # ----------------------------------------
  cmp DWORD PTR [edi],0xffffffff                 # We've reached the end of the table
  je _X1B.MiracleSeekEval                        # 
  cmp DWORD PTR [edi+0x04],0xfffffffd            # Undefined behavior/crash anim
  jng _X1B.MiracleSeekNext                       # 
  test BYTE PTR [edx+0x14],0x04                  # If the non-negative flag is set, check in advance
  jz _X1B.MiracleSeekDupeCheck                   # 
  mov ecx,DWORD PTR [edi]                        # ID*0x10+0x0c = AnimNo
  shl ecx,0x04                                   # 
  cmp DWORD PTR [ebx+ecx*1+0x0c],0x00            # If AnimNo < 0, skip
  jl _X1B.MiracleSeekNext                        # 
_X1B.MiracleSeekDupeCheck:                       # ----------------------------------------
  test BYTE PTR [edx+0x14],0x10                  # If unimportant, skip
  jnc _X1B.MiracleSeekTestClsn1                  # 
  mov ecx,DWORD PTR [edi]                        # 
  shl ecx,0x04                                   # 
  mov ebp,DWORD PTR [ebx+ecx*1+0x0c]             # Fetch our current AnimNo
_X1B.MiracleSeekDupeLoop:                        # - - - - - - - - - - - - - - - - - - - -
  sub ecx,0x10                                   # Backwards we go
  js _X1B.MiracleSeekTestClsn1                   # 
  cmp DWORD PTR [ebx+ecx*1+0x0c],ebp             # If equal, congratulations, we found a dupe.
  je _X1B.MiracleSeekNext                        # 
  jmp _X1B.MiracleSeekDupeLoop                   # Continue until termination
_X1B.MiracleSeekTestClsn1:                       # ----------------------------------------
  mov ebp,DWORD PTR [edx+0x14]                   # FlagList in ebp (to be tampered with soon)
  test ebp,0x01                                  # If we don't need it, skip
  jz _X1B.MiracleSeekTestClsn2                   # 
  cmp DWORD PTR [edi+0x04],0xffffffff            # Does Not Exist
  je _X1B.MiracleSeekNext                        # 
  test ebp,0x08                                  # Zero-length check
  setnz cl                                       # 
  cmp DWORD PTR [edi+0x04],0xfffffffe            # 0-Length anim = -2
  sete ch                                        # 
  and cl,ch                                      # 
  jnz _X1B.MiracleSeekNext                       # 
  mov ecx,DWORD PTR [edi+0x0c]                   # Is our delay time in range?
  cmp ecx,DWORD PTR [edx+0x10]                   # 
  ja _X1B.MiracleSeekNext                        # It's not, so don't do anything with it
  and ebp,0xfffffffe                             # Eliminate bit 0
_X1B.MiracleSeekTestClsn2:                       # ----------------------------------------
  test ebp,0x02                                  # If we don't need it, skip
  jz _X1B.MiracleSeekTestTotal                   # 
  cmp DWORD PTR [edi+0x08],0xffffffff            # Does Not Exist
  je _X1B.MiracleSeekNext                        # 
  test ebp,0x08                                  # Zero-length check
  setnz cl                                       # 
  cmp DWORD PTR [edi+0x08],0xfffffffe            # 0-Length anim = -2
  sete ch                                        # 
  and cl,ch                                      # 
  jnz _X1B.MiracleSeekNext                       # 
  mov ecx,DWORD PTR [edi+0x10]                   # Is our delay time in range?
  cmp ecx,DWORD PTR [edx+0x10]                   # 
  ja _X1B.MiracleSeekNext                        # It's not, so don't do anything with it
  and ebp,0xfffffffd                             # Eliminate bit 1
_X1B.MiracleSeekTestTotal:                       # ----------------------------------------
  test ebp,0x03                                  # If both bits are eliminated, this is a valid animation
  jnz _X1B.MiracleSeekNext                       # 
  cmp esi,DWORD PTR [esp]                        # If this is the correct one in the list, return it
  je _X1B.MiracleSeekReturn                      # 
  inc esi                                        # Otherwise just add 1 and continue
_X1B.MiracleSeekNext:                            # ----------------------------------------
  inc eax                                        # Next position
  lea edi,[edi+0x14]                             # 
  cmp ax,0x1000                                  # Position OoB
  jb _X1B.MiracleSeekLoop                        # 
_X1B.MiracleSeekEval:                            # ----------------------------------------
  cmp DWORD PTR [esp],0xffffffff                 # Is it -1?
  jne _X1B.MiracleSeekReexamine                  # 
  mov eax,esi                                    # If it is, return the count
_X1B.MiracleSeekReturn:                          # ----------------------------------------
  add esp,0x04                                   # Correct esp before returning
  jmp _X1B.Return                                # 
_X1B.MiracleSeekReexamine:                       # ----------------------------------------
  test esi,esi                                   # If nothing exists, return BAD
  jnz _X1B.MiracleSeekReexamine2                 # 
  add esp,0x04                                   # Correct esp before returning BAD
  jmp _X1B.ReturnBad                             # 
_X1B.MiracleSeekReexamine2:                      # ----------------------------------------
  mov eax,0x00492f37                             # Not -1, so call RNG
  mov ebp,edx                                    # TMP Storage
  call eax                                       # 
  cdq                                            # 
  idiv esi                                       # Get modulus
  mov DWORD PTR [esp],edx                        # Store modulus as new position
  mov edx,ebp                                    # Restore edx
  xor eax,eax                                    # Try again
  xor esi,esi                                    # 
  mov edi,DWORD PTR [edx+0x08]                   # 
  jmp _X1B.MiracleSeekLoop                       # 
_X1B.MiracleSearch:                              # ========================================
  mov edi,DWORD PTR [edx+0x08]                   # InfoStorage in edi
  mov ecx,DWORD PTR [edx+0x0c]                   # For comparison
  xor eax,eax                                    # Precaution
  cmp DWORD PTR [edx+0x10],0x00                  # If non-zero, this is actually the ID
  jne _X1B.MiracleSearchLoop                     # Just skip to the actual search loop
  mov eax,ecx                                    # Store
  mov ecx,DWORD PTR [ebx+0x08]                   # Maximum
  mov esi,DWORD PTR [ebx+0x18]                   # I only need the AnimList in this case
_X1B.MiracleSearchPreinitLoop:                   # ----------------------------------------
  cmp DWORD PTR [esi+0x0c],eax                   # This is the ID
  je _X1B.MiracleSearchInit                      # 
  dec ecx                                        # Loop until we run out or until we find it
  lea esi,[esi+0x10]                             # 
  jg _X1B.MiracleSearchPreinitLoop               # 
  jmp _X1B.ReturnBad                             # Logically
_X1B.MiracleSearchInit:                          # ----------------------------------------
  neg ecx                                        # Max - ecx, right?
  add ecx,DWORD PTR [ebx+0x08]                   # 
  xor eax,eax                                    # Position reset
_X1B.MiracleSearchLoop:                          # ----------------------------------------
  cmp DWORD PTR [edi],ecx                        # Well we found it
  je _X1B.Return                                 # 
  cmp DWORD PTR [edi],0xffffffff                 # Well it's the end
  je _X1B.ReturnBad                              # 
  inc eax                                        # Keep going 
  lea edi,[edi+0x14]                             # 
  cmp ax,0x1000                                  # Hard end
  jb _X1B.MiracleSearchLoop                      # 
_X1B.ReturnBad:                                  # ========================================
  mov eax,0xffffffff                             # -1 is bad
_X1B.Return:                                     # ----------------------------------------
  mov DWORD PTR [esp+0x64],eax                   # Return properly
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.FetchClsn (ID = 28)
--------------------------------------------
Fetch Clsn data for the anim data specified in Param0.
If Param1 is non-zero, the AnimElemTime(1) used in calculations will be decremented by 1.
Param2 determines the behavior of this function.
Param2 = 0:
  Only perform operations on the current elem
  Params 0~1 will return non-zero values if Clsn1/Clsn2 are found, respectively
Param2 = 1:
  Continue execution until an elem with Clsn1/Clsn2 is found
  Params 0~1 will return the pointers to the elements in question
  Will loop to the first elem possible
  Params 2~3 will be set to the delay until this elem is reached
Param2 = X:
  Continue execution until an elem WITHOUT Clsn1/Clsn2 is found
  Identical syntax to Param2=1
  ...not implemented as of right now, though
*/
asm(R"(.intel_syntax noprefix
_X1C:                                            # ========================================
  mov esi,DWORD PTR [edx]                        # Grab target to scan
  mov ecx,DWORD PTR [esi+0x1c]                   # Fetch current AnimElemTime(1) due to a desync
  xor eax,eax                                    # 
  cmp DWORD PTR [edx+0x04],0x00                  # Decrement
  setne al                                       # 
  sub ecx,eax                                    # 
  mov edi,DWORD PTR [esi+0x0c]                   # Fetch Anim ID
  mov ebx,DWORD PTR [esi]                        # Move into Anim databank
  imul edi,edi,0x14                              # Multiply edi by 0x14 for adjustment purposes
  mov esi,DWORD PTR [ebx]                        # 
  mov ebx,DWORD PTR [esi+0x14]                   # 
  add ebx,edi                                    # 
  mov esi,DWORD PTR [ebx]                        # Fetch the desired animation
  mov edi,DWORD PTR [esi+0x0c]                   # Hold onto the animelem count for later
  mov eax,DWORD PTR [esi+0x14]                   # 's elements
_X1C.ElemGet:                                    # ========================================
  dec edi                                        # The last one possible?
  lea eax,[eax+0x30]                             # Adjust to the next AnimElem
  jng _X1C.ParseClsn                             # 
  cmp DWORD PTR [eax],ecx                        # If this one occurs later, end the loop
  jng _X1C.ElemGet                               # 
_X1C.ParseClsn:                                  # ========================================
  sub eax,0x30                                   # Go back 1
  xor ebp,ebp                                    # For storage
  cmp DWORD PTR [eax+0x28],0x00                  # If there's a Clsn1 HitBox, place it as an output
  cmovne ebp,eax                                 # 
  mov DWORD PTR [edx],ebp                        # 
  xor ebp,ebp                                    # 
  cmp DWORD PTR [eax+0x2c],0x00                  # If there's a Clsn2 Hitbox, place it as an output
  cmovne ebp,eax                                 # 
  mov DWORD PTR [edx+0x04],ebp                   # 
  cmp DWORD PTR [edx+0x08],0x00                  # Mode switch
  jz _X1C.Return                                 # 
_X1C.SearchClsn:                                 # ----------------------------------------
  xor ebp,ebp                                    # 
  mov DWORD PTR [edx+0x08],ebp                   # Default to 0 as idc
  mov DWORD PTR [edx+0x0c],ebp                   # 
  dec edi                                        # The last one possible?
  lea eax,[eax+0x30]                             # Adjust to the next AnimElem
  jg _X1C.SearchClsnParseC1                      # 
_X1C.SearchClsnResetCheck:                       # ----------------------------------------
  test esi,esi                                   # If this is set to 0, return (we've already looped once)
  jz _X1C.ReturnC1                               # Set arguments as appropriate and return
  cmp DWORD PTR [ebx+0x08],0xffffffff            # A special condition that never loops under any circumstance (all others loop)
  je _X1C.ReturnC1                               # 
  sub ecx,DWORD PTR [ebx+0x08]                   # Account for differences
  jl _X1C.SearchClsnCont                         # Cap this value at a 1F delay
  mov ecx,0xffffffff                             # 
_X1C.SearchClsnCont:                             # ----------------------------------------
  add ecx,DWORD PTR [ebx+0x0c]                   # 
  mov edi,DWORD PTR [ebx+0x10]                   # Loop ElemID here
  mov eax,edi                                    # Multiply by 0x10
  shl eax,0x04                                   # 
  add eax,DWORD PTR [esi+0x14]                   # Reset eax to the correct location
  neg edi                                        # Get remaining elemCount
  add edi,DWORD PTR [esi+0x0c]                   # 
  xor esi,esi                                    # I'm not using it past this point, so use this as the flag
_X1C.SearchClsnParseC1:                          # ----------------------------------------
  cmp DWORD PTR [edx],0x00                       # Already exists
  je _X1C.SearchClsnParseC2                      # 
  mov ebp,DWORD PTR [eax+0x28]                   # 
  test ebp,ebp                                   # If there's a Clsn1 HitBox, place it as an output
  jz _X1C.SearchClsnParseC2                      # 
  mov DWORD PTR [edx],ebp                        # Store
  mov ebp,DWORD PTR [eax]                        # Fetch AnimElemTime
  sub ebp,ecx                                    # Incorporate the proper delay
  mov DWORD PTR [edx+0x08],ebp                   # Store
  cmp DWORD PTR [edx+0x04],0x00                  # C2 already found, return
  jne _X1C.Return                                # 
_X1C.SearchClsnParseC2:                          # ----------------------------------------
  cmp DWORD PTR [edx+0x04],0x00                  # Already exists
  je _X1C.SearchClsnNext                         # 
  mov ebp,DWORD PTR [eax+0x2c]                   # 
  test ebp,ebp                                   # If there's a Clsn2 HitBox, place it as an output
  jz _X1C.SearchClsnNext                         # 
  mov DWORD PTR [edx+0x04],ebp                   # Store
  mov ebp,DWORD PTR [eax]                        # Fetch AnimElemTime
  sub ebp,ecx                                    # Incorporate the proper delay
  mov DWORD PTR [edx+0x0c],ebp                   # Store
  cmp DWORD PTR [edx],0x00                       # C1 already found, return
  jne _X1C.Return                                # 
_X1C.SearchClsnNext:                             # ----------------------------------------
  dec edi                                        # The last one possible?
  lea eax,[eax+0x30]                             # Adjust to the next AnimElem
  jng _X1C.SearchClsnResetCheck                  # Exceeded last elem; loop?
  cmp edi,0x01                                   # This is the last elem, no need for any special checks
  mov ebp,DWORD PTR [eax]                        # Hm...
  je _X1C.SearchClsnValidElem                    # 
_X1C.SearchClsnNextValid:                        # ----------------------------------------
  cmp DWORD PTR [eax+0x30],ebp                   # If this doesn't make sense, then we have to keep looping
  cmovg ebp,DWORD PTR [eax]                      # If it makes sense, we can continue with this animelemtime
  jg _X1C.SearchClsnValidElem                    # 
  dec edi                                        # Similar adjustment to the above
  lea eax,[eax+0x30]                             # 
  cmp edi,0x01                                   # Keep checking
  ja _X1C.SearchClsnNextValid                    # 
_X1C.SearchClsnValidElem:                        # ----------------------------------------
  cmp DWORD PTR [ebx+0x08],ebp                   # We have a chance to loop, so do so
  jng _X1C.SearchClsnResetCheck                  # 
  jmp _X1C.SearchClsnParseC1                     # Otherwise, check C1 as usual
_X1C.ReturnC1:                                   # ========================================
  cmp DWORD PTR [edx],0x00                       # No Clsn1, return -1
  jne _X1C.ReturnC2                              # 
  mov DWORD PTR [edx+0x08],0xffffffff            # 
_X1C.ReturnC2:                                   # ----------------------------------------
  cmp DWORD PTR [edx+0x04],0x00                  # No Clsn2, return -1
  jne _X1C.Return                                # 
  mov DWORD PTR [edx+0x0c],0xffffffff            # 
_X1C.Return:                                     # ----------------------------------------
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.AnimInfo (ID = 29)
--------------------------------------------
Parse the potential Clsn information found in the designated player (Param0)
Param1 = 0: 
  Check for existence of both Clsn1 and Clsn2 hitboxes.
Param1 = X:
  Simply return the AnimElemNo
Check to see if the designated player (specified by Param0) currently has a Clsn1/Clsn2 hitbox.
If Param1 is 1 OR 2, it will return a pointer to the specified hitboxes.
If Param1 is less than 0, it will instead return the number of the element in question.
Dwarf of PlayerInfoAcquire for the sake of Exploration that has since been repurposed for dynamic offset purposes (B_Saki)
*/
asm(R"(.intel_syntax noprefix
_X1D:                                            # ========================================
  mov ebx,DWORD PTR [edx]                        # Grab target to scan
  mov ebp,DWORD PTR [edx+0x04]                   # Store for later
  mov esi,DWORD PTR [ebx+0x000013bc]             # Scout for Target,AnimElemNo(0)
  mov ecx,DWORD PTR [esi+0x1c]                   # Fetch target's current AnimElemTime(1) due to a desync
  mov edi,DWORD PTR [ebx+0x08]                   # Fetch PlayerNo
  cmp DWORD PTR [eax+edi*4+0x0000ba64],0x00      # If it looks like the target didn't move this frame, decrement by 1
  jne _X1D.SkipDecrement                         # (The opponent's animation won't update if paused, but animelemtime will)
  sub ecx,0x01                                   # 
_X1D.SkipDecrement:                              # ========================================
  mov edi,DWORD PTR [esi+0x0c]                   # Fetch Anim ID
  mov edx,DWORD PTR [esi]                        # Move into Anim databank
  imul edi,0x14                                  # Multiply edi by 0x14 for adjustment purposes
  mov edx,DWORD PTR [edx]                        # 
  mov edx,DWORD PTR [edx+0x14]                   # 
  lea edx,DWORD PTR [edx+edi]                    # 
  mov esi,DWORD PTR [edx]                        # Fetch the desired animation
  mov edi,DWORD PTR [esi+0x0c]                   # Hold onto the animelem count for later
  xor eax,eax                                    # (Before we forget)
  mov edx,DWORD PTR [esi+0x14]                   # 's elements
_X1D.ElemGet:                                    # ========================================
  add edx,0x30                                   # Adjust to the next AnimElem
  sub edi,0x01                                   # The last one possible
  jng _X1D.ParseClsn                             # 
  cmp DWORD PTR [edx],ecx                        # If this one occurs later, end the loop
  jng _X1D.ElemGet                               # 
_X1D.ParseClsn:                                  # ========================================
  sub edx,0x30                                   # Go back 1
  neg edi                                        # Negate to directly fetch the elemno
  add edi,DWORD PTR [esi+0x0c]                   # 
  test ebp,ebp                                   # Return the elemno
  cmovnz eax,edi                                 # 
  jnz _X1D.ReturnValue                           # 
  cmp DWORD PTR [edx+0x28],0x00                  # If there's a Clsn1 HitBox, set the appropriate flag
  setne al                                       # 
  cmp DWORD PTR [edx+0x2c],0x00                  # If there's a Clsn2 HitBox, set the appropriate flag
  je _X1D.ReturnValue                            # 
  or al,0x02                                     # Set the appropriate flag
_X1D.ReturnValue:                                # ========================================
  mov DWORD PTR [esp+0x64],eax                   # literally
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.ExpExecute (ID = 34)
--------------------------------------------
If Param1 ≥ 0, executes the StateDef specified in Param0 remotely.
If Param1 is non-zero, exploration should be finalized, and Def -244/-333 should be restored (Param1 = Address)
If Param2 is non-zero, the custom state pointer should be used if available, not the primary state pointer.
If Param3 is non-zero, the player clone located in the 2nd sound's data will execute this code instead.
    The argument should be a pointer to the particular player's information pointer.
This used to actually be relevant in exploration, hence the name, but I don't use it anymore
*/
asm(R"(.intel_syntax noprefix
_X22:                                            # ========================================
  xor eax,eax                                    # XOR value: -1
  not eax                                        # 
  cmp DWORD PTR [edx+0x04],0x00                  # If < 0, only finalize exploration
  jl _X22.PopExpPrime                            # 
  mov eax,DWORD PTR [edx+0x08]                   # Should we use custom states instead?
  xor al,0x07                                    # Set eax to 0x07/0x06 (Custom State NG/OK, Own State OK, Enemy State OK)
  push eax                                       # Push appropriate flags
  push DWORD PTR [edx]                           # Push Execution StateDef
  cmp DWORD PTR [edx+0x0c],0x00                  # If non-zero, use the player clone as appropriate
  mov esi,ebx                                    # Backup ebx
  jz _X22.UseSelf                                # 
  mov ebx,DWORD PTR [edx+0x0c]                   # Assume Information Pointer
  mov eax,DWORD PTR [ebx+0x000003d4]             # Enter SND data to get player clone
  mov ecx,DWORD PTR [eax]                        # 
  mov ebx,DWORD PTR [ecx+0x14]                   # 
  mov eax,DWORD PTR [ebx+0x04]                   # 2nd SND
  mov ebx,DWORD PTR [eax+0x20]                   # Located at raw data
_X22.UseSelf:                                    # ----------------------------------------
  mov eax,0x0047f7d0                             # Set eax to 0x0047f7d0
  push ebx                                       # Push execution address
  mov edi,DWORD PTR [edx+0x04]                   # Store the restoration address for later (edi gets auto-backed up)
  call eax                                       # Call Negative State Handler
  add esp,0x0c                                   # Re-adjust esp
  xor eax,eax                                    # XOR value: 0
  test edi,edi                                   # If non-zero, finalize exploration
  jnz _X22.PopExp                                # 
  ret                                            # 
_X22.PopExpPrime:                                # ========================================
  mov edi,DWORD PTR [edx+0x04]                   # Destination address = Def -EXP 
_X22.PopExp:                                     # ----------------------------------------
  mov ebx,esi                                    # Restore from backup
  xor edi,eax                                    # Account for the < 0 case
  lea esi,[edi+0x000000a8]                       # Adjust to the appropriate backup StateDef (+0x04)
  add edi,0x04                                   # (same deal here)
  xor ecx,ecx                                    # Set ecx to 0xa0/4
  mov cl,0x28                                    # 
  rep movsd                                      # Restore header
  mov ebx,0xffffff5c                             # Define -0xa4
  mov eax,DWORD PTR [edi+ebx]                    # Adjust accordingly to get our destinations
  mov edx,DWORD PTR [esi+ebx]                    # 
  mov edi,DWORD PTR [eax+0x14]                   # Fetch state code data to restore
  mov ecx,DWORD PTR [edx+0x14]                   # Fetch trigger data to restore from
  mov ebx,DWORD PTR [ecx]                        # 
  mov esi,DWORD PTR [ebx+0x14]                   # 
  xor edx,edx                                    # Set edx to 512/2 since we'll be affecting 512 StCtrls, 2 at a time
  mov dh,0x01                                    # 
  xor ecx,ecx                                    # Define ecx as 0, eax as 0 (hmmm today i will store 0 forever [clueless])
  xor eax,eax                                    # 
_X22.PopLoop:                                    # ========================================
  movsd                                          # Store trigger 2i
  mov cl,0x18                                    # Set to 0x60/4 for rep purposes
  rep stosd                                      # Store 0 everywhere
  mov BYTE PTR [edi-0x5c],0x01                   # We WILL need to set Trigger# to 1 though
  mov DWORD PTR [esi-0x04],eax                   # Store 0 here as well
  movsd                                          # Store trigger 2i + 1
  mov cl,0x18                                    # Repeat procedure
  rep stosd                                      # 
  mov BYTE PTR [edi-0x5c],0x01                   # 
  mov DWORD PTR [esi-0x04],eax                   # 
  add esi,0x04                                   # Skip over Oper2
  sub edx,0x01                                   # Loop until all 512 Triggers are passed through the system
  jg _X22.PopLoop                                # 
  ret                                            #
  .balign 0x10, 0x00
)");

/*
---------------
.ExplMapSeek (ID = 37)
--------------------------------------------
Searches for a state corresponding to our exploration data, passed as Param0
Param1: State Code
Param2: StateID
Param3: Flags
      &1: Param2 is StateNo, not StateID
          Note that, if this setting is enabled and we aren't able to find a state, we'll search for ~StateNo in the map
      &2: Abandon after finding StateID
          Only usable if &1 is enabled; this will simply return the StateID of the state (helps with searching)
      &4: Allow Blanks
          If we can't find the correct state, but we reach the end, should we just return that position?
*/
asm(R"(.intel_syntax noprefix
_X25:                                            # ========================================
  mov edi,DWORD PTR [edx]                        # Fetch InfoStorage
  bt DWORD PTR [edx+0x0c],0x00                   # Do we need to search for StateID?
  mov ebx,DWORD PTR [edx+0x08]                   # Grab StateID anyway
  jnc _X25.StateIDFound                          # 
_X25.StateIDSeek:                                # ========================================
  mov esi,DWORD PTR [edx+0x04]                   # Fetch StateCode
  mov eax,DWORD PTR [esi]                        # 
  mov ebp,DWORD PTR [eax+0x0c]                   # Prep for looping
  mov ecx,ebp                                    # 
  mov esi,DWORD PTR [eax+0x18]                   # Enter DefList
_X25.StateIDSeek_Loop:                           # ----------------------------------------
  cmp DWORD PTR [esi+0x08],ebx                   # Does it match?
  je _X25.StateIDSeek_End                        # 
  sub ecx,0x01                                   # Keep searching if no
  lea esi,[esi+0x10]                             # 
  jnz _X25.StateIDSeek_Loop                      # Until we run out
  bt DWORD PTR [edx+0x0c],0x01                   # Did we want to return StateID?
  jc _X25.ReturnBad                              # Return -1 if we did
  not ebx                                        # If we aren't returning, search for ~StateNo
  jmp _X25.StateIDFound                          # 
_X25.StateIDSeek_End:                            # ----------------------------------------
  sub ebp,ecx                                    # Fetch the actual ID
  bt DWORD PTR [edx+0x0c],0x01                   # Did we want to return StateID?
  mov ebx,ebp                                    # 
  cmovc eax,ebp                                  # If so, return it
  jc _X25.Return                                 # 
_X25.StateIDFound:                               # ========================================
  xor eax,eax                                    # Set count to 0 and begin
_X25.MapScan_Loop:                               # ----------------------------------------
  cmp DWORD PTR [edi],ebx                        # Does it match?
  je _X25.Return                                 # If it does, return our position
  cmp DWORD PTR [edi],0xffffffff                 # Did we run out of space?
  jne _X25.MapScan_Next                          # If we didn't, continue
  bt DWORD PTR [edx+0x0c],0x02                   # Are we fine with returning the position if it's -1?
  jc _X25.Return                                 # 
  jmp _X25.ReturnBad                             # If we aren't, return BAD
_X25.MapScan_Next:                               # ----------------------------------------
  inc eax                                        # Next position
  lea edi,[edi+0x60]                             # 
  cmp ax,0x0400                                  # 1024 entries in the map at max
  jb _X25.MapScan_Loop                           # If we exceed, return BAD
_X25.ReturnBad:                                  # ========================================
  mov eax,0xffffffff                             # -1 is bad
_X25.Return:                                     # ----------------------------------------
  mov DWORD PTR [esp+0x64],eax                   # Return value
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
---------------
.NextPlayer (ID = 38)
--------------------------------------------
Will fetch the next player's PlayerNo and return it.
Next isn't just a +1 thing, but is instead checking to see who SHOULD execute code next.
Param0 = Starting PlayerNo (NOTE: 0-indexed)
Param1 = Flags
  &1: MT=A Sweep
  &2: Allow Self
*/
asm(R"(.intel_syntax noprefix
_X26:                                            # ========================================
  mov ebp,DWORD PTR [edx+0x04]                   # Flags
  mov ecx,DWORD PTR [edx]                        # PlayerNo
  mov esi,DWORD PTR [ebx]                        # Name Pointer
  lea edx,[ecx-0x01]                             # Loop Ending Point
  mov ebx,DWORD PTR [eax+0x0000b3fc]             # GameTime Fetch
  add eax,0x0000b754                             # Player Basis
  cmp ecx,0x3e                                   # Set to 0 if out of range
  jna _X26.FPLoopStart                           # 
  xor ecx,ecx                                    # 
_X26.FPLoopStart:                                # ========================================
  lea edi,[eax+ecx*4]                            # Align to Player
  cmp DWORD PTR [edi+0x00000314],0x00            # If the player doesn't move this frame, skip it
  jz _X26.FPLoopNext                             # 
  mov edi,DWORD PTR [edi]                        # Actually fetch player
  test edi,edi                                   # Blatant
  jz _X26.FPLoopNext                             # 
  cmp DWORD PTR [edi+0x00000158],0x00            # It's not a player, so we don't care
  je _X26.FPLoopNext                             # 
  cmp DWORD PTR [edi+0x0000015c],0x00            # It's frozen, so we don't care
  jne _X26.FPLoopNext                            # 
  test ebp,0x02                                  # Self is allowed, continue
  jnz _X26.FPLoopAllowed                         # 
  cmp DWORD PTR [edi],esi                        # If the name pointer matches, skip
  je _X26.FPLoopNext                             # 
_X26.FPLoopAllowed:                              # ----------------------------------------
  test ebp,0x01                                  # MT=A Switch
  jnz _X26.FPLoopMTA                             # 
  cmp DWORD PTR [edi+0x00000e2c],ebx             # ActionFlag=GameTime: Skip
  je _X26.FPLoopNext                             # 
  jmp _X26.Return                                # Otherwise, return it
_X26.FPLoopMTA:                                  # ----------------------------------------
  cmp DWORD PTR [edi+0x00000e04],0x01            # MT needs to actually be A
  je _X26.Return                                 # If so, return it
_X26.FPLoopNext:                                 # ========================================
  inc ecx                                        # 
  cmp ecx,edx                                    # Ending point reached
  jne _X26.FPLoopNext2                           # 
  mov ecx,0xffffffff                             # Return -1
_X26.Return:                                     # ----------------------------------------
  mov DWORD PTR [esp+0x64],ecx                   # Store return value
  ret                                            # 
_X26.FPLoopNext2:                                # ----------------------------------------
  cmp ecx,0x3e                                   # Continue until 63 is reached, which is OoB
  jna _X26.FPLoopStart                           # 
  and ebp,0xfffffffe                             # Clear MT=A and continue until we reach the endpoint
  xor ecx,ecx                                    # Reset the counter (v important)
  jmp _X26.FPLoopStart                           # 
  .balign 0x10, 0x00
)");

/*
----------------
.P1ProjHandler (ID = 39)
--------------------------------------------
Searches for all projectiles owned by the enemy (specified in Param0) with an ID of Param1
If it has the potential to hit something, returns the position of this projectile. Otherwise, returns -1
Such that things can be looped efficiently, Param2 can be used to specify the starting index for searching
*/
asm(R"(.intel_syntax noprefix
_X27:                                            # ========================================
  mov ebx,DWORD PTR [edx]                        # Fetch target
  mov ecx,DWORD PTR [ebx+0x0000021c]             # Obtain Projectile Databank Pointer
  mov eax,DWORD PTR [ecx]                        # 
  mov ecx,DWORD PTR [eax+0x28]                   # Fetch the highest active element while we're here
  inc ecx                                        # Surprise tool
  mov esi,DWORD PTR [eax+0x14]                   # Databank
  mov edi,DWORD PTR [eax+0x18]                   # Existence List
  mov ebp,DWORD PTR [edx+0x08]                   # Initial index
  sub ecx,ebp                                    # Adjust appropriately
  mov ebx,ecx                                    # Store for later
  jng _X27.ReturnBad                             # Just in case it's the last one
  mov eax,DWORD PTR [edx+0x04]                   # Fetch ProjID
  mov edx,ebp                                    # 
  shl edx,0x02                                   # Multiply by 4
  lea edi,[edi+edx*4]                            # Adjust to first array position
  mov edx,0x000002dc                             # Fuck it proj
  imul edx,ebp                                   # 
  lea esi,[esi+edx*1]                            # Adjust to first dex position 
_X27.ProjLoop:                                   # ========================================
  cmp DWORD PTR [edi],0x00                       # Exists?
  je _X27.ProjSkip                               # 
  cmp DWORD PTR [esi],eax                        # ID matches?
  jne _X27.ProjSkip                              # 
  cmp DWORD PTR [esi+0x1c],0x00                  # Still has hits on the board?
  jng _X27.ProjSkip                              # 
  cmp DWORD PTR [esi+0x2c],0x00                  # Not currently in miss processing?
  jne _X27.ProjSkip                              # 
  mov edx,DWORD PTR [esi+0x000000a4]             # Prep for attr
  test dl,0x07                                   # 
  jz _X27.ProjSkip                               # 
  test dx,0x0ff8                                 # 
  jz _X27.ProjSkip                               # 
  test DWORD PTR [esi+0x00000110],0x5c           # HitFlag includes a valid statetype
  jz _X27.ProjSkip                               # 
  mov edx,DWORD PTR [esi+0x00000294]             # AnimElemNo Fetch (checked empirically and seems like it has no problems)
  mov edx,DWORD PTR [edx+0x28]                   # Do you have C1?
  test edx,edx                                   # 
  jz _X27.ProjSkip                               # 
  cmp DWORD PTR [edx+0x04],0x00                  # Safety
  je _X27.ProjSkip                               # 
  mov eax,ebp                                    # InitialIndex + InitialECX - FinalECX
  add eax,ebx                                    # 
  sub eax,ecx                                    # This will get the current position
  jmp _X27.Return                                # 
_X27.ProjSkip:                                   # ----------------------------------------
  lea esi,[esi+0x000002dc]                       # Next Proj
  dec ecx                                        # Subtract until we run out
  lea edi,[edi+0x10]                             # Next array index
  jg _X27.ProjLoop                               # 
_X27.ReturnBad:                                  # ========================================
  mov eax,0xffffffff                             # -1 bad!
_X27.Return:                                     # ----------------------------------------
  mov DWORD PTR [esp+0x64],eax                   # Return the value
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.SM64DS (ID = 40)
--------------------------------------------
Has different functionality depending on Param0.
Param0 >= 0:
    Will ensure that the next (Param0 + 1)th time Random is called, it will return a desired value based on the seed specified by Param1.
    With proper seeding you can get insanely accurate values!
    In THE☆ORY possible to do manually with enough knowledge on what values are returned from random as it's an LCRNG
    Used only for stupid circumstances (CORESYSTEMS, dealing with root deleter, etc.)
Param0 < 0:
    Will ensure that starting on the next (~Param0 + 1)th time Random is called, it will return the pattern specified by all subsequent params.
    This is determined on a rand%(paramA&65535)%(paramA>>16&65535)?(paramB&65535) basis, where ? is determined by paramB&-65536.
        0x0000: >
        0x0001: =
        0x0002: <
        0x0100: <=
        0x0101: !=
        0x0102: >=
        (technically this is encoded as a, e, b, na, ne, nb, but since we're dealing with unsigned words it's equivalent)
    This is possible to perform by manual stepthrough.
*/

asm(R"(.intel_syntax noprefix
_X28:                                            # ========================================
  mov esi,0x00000000                             # Do we have a reverse adder?
  test esi,esi                                   #
  mov edi,0x00000000                             #
  jne _X28.RevDerived                            #
  push ecx                                       # Backup codepoint
  mov esi,DWORD PTR [0x00492f41]                 # For the sake of inter-version compat...
  mov ecx,esi                                    # Get nib position from lowest nib
  and ecx,0x0f                                   # 
  shr cl                                         # >>1 to get the intended nib (even numbers will produce invalid results but good LCRNG does not use even numbers here)
  mov ebx,0xf5397db1                             # GP matrix translation
  shl cl,0x02                                    # *4 to get the shr value for nib fetch
  shr ebx,cl                                     # fetch nib
  and ebx,0x0f                                   # 
  mov ch,bl                                      # Store initial nib
  xor edi,edi                                    # Prep
_X28.MMILoop:                                    # ----------------------------------------
  mov ebx,edi                                    # Work is shown below after a manual iteration
  neg ebx                                        # -This+1
  inc ebx                                        #
  shr ebx,cl                                     # Get nib
  xchg ch,cl                                     # the only reasonable method for this that i could find that doesn't push/pop
  imul ebx,ecx                                   # The fact that there's a ch byte won't matter due to the below AND
  xchg ch,cl                                     # 
  and ebx,0x0f                                   # 
  imul ebx,esi                                   # Multiply and add
  shl ebx,cl                                     # Readjust
  add cl,0x04                                    # Next nib
  lea edi,[edi+ebx*1]                            # Combine
  and cl,0x1c                                    # Repeat 7 times to get the MMI
  jnz _X28.MMILoop                               #
  mov ecx,DWORD PTR [0x00492f47]                 # Get subtraction coefficient
  neg ecx                                        #
  imul edi,ecx                                   # Get reverse multiplier by performing a simple mul on MMI
  pop ecx                                        # Self-modify for future iterations
  mov DWORD PTR [ecx+0x01],esi                   #
  mov DWORD PTR [ecx+0x08],edi                   #
_X28.RevDerived:                                 # ----------------------------------------
  mov ebx,edx                                    # Environment-specific problem avoidance
  mov eax,0x004988bf                             # _getptd_noexit for Tls
  call eax                                       # 
  mov ebp,eax                                    # Store for later
  cmp DWORD PTR [ebx],0x00                       # If < 0, switch modes
  cmovnl eax,DWORD PTR [ebx+0x04]                # Seed in param1 if >= 0
  jnl _X28.SeedFound                             #
  mov ecx,DWORD PTR [ebp+0x14]                   # Initialize our seed
  push esi                                       #
  mov esi,DWORD PTR [0x00492f41]                 # Borrowing
  push edi                                       #
  mov edi,DWORD PTR [0x00492f47]                 # Borrowing
  push ebx                                       # Need to back up, will be operating under this pretense
  add ebx,0x04                                   # Get to first set
  push ecx                                       # Need to back up for restoration purposes
_X28.DerivationLoop:                             # ----------------------------------------
  imul ecx,esi                                   # Emulate RNG
  add ecx,edi                                    # 
  mov eax,ecx                                    #
  shr eax,0x10                                   #
  cdq                                            # Prepare for div
  and eax,0x7fff                                 #
  div WORD PTR [ebx]                             # valid since eax==ax
  cmp WORD PTR [ebx+0x02],0x00                   # Second modulus
  je _X28.DerivationSkipMod                      #
  mov ax,dx                                      # Repeat
  cdq                                            #
  div WORD PTR [ebx+0x02]                        # valid since eax==ax
_X28.DerivationSkipMod:                          # ----------------------------------------
  cmp dx,WORD PTR [ebx+0x04]                     # Perform comparison
  sete al                                        # Transfer important flags to al
  setb ah                                        #
  shl ah                                         #
  or al,ah                                       # (0000:>|0001:=|0102:<)
  cmp al,BYTE PTR [ebx+0x06]                     # Check if matching
  sete al                                        # If it does, bind
  xor al,BYTE PTR [ebx+0x07]                     # BUT. If &0x01000000, invert the result.
  jnz _X28.DerivationReset                       # If everything matches, keep going.
  add ebx,0x08                                   #
  cmp DWORD PTR [ebx],0x00                       # Done?
  jnz _X28.DerivationLoop                        # If not, continue
  mov eax,ecx                                    # This is our seed
  pop ecx                                        # Fix stack/refetch MMI and reverse adder
  pop ebx                                        #
  pop edi                                        #
  pop esi                                        #
  jmp _X28.SeedFound                             #
_X28.DerivationReset:                            # ----------------------------------------
  pop ecx                                        # Step once
  imul ecx,esi                                   # Emulate RNG
  pop ebx                                        # Reset placement
  add ecx,edi                                    # 
  push ebx                                       # Re-backup
  push ecx                                       # 
  jmp _X28.DerivationLoop                        # Resume
_X28.SeedFound:                                  # ----------------------------------------
  mov ecx,DWORD PTR [ebx]                        # Fetch iteration count
  mov edx,ecx                                    # Prepare 
  sar edx,0x1f                                   # Extend sign bit 
  xor ecx,edx                                    # One's complement if negative, straight if positive
  jz _X28.Return                                 # If zero, it'll just set the seed to this
_X28.Backstep:                                   # ----------------------------------------
  mul esi                                        # Apply MMI
  add eax,edi                                    # Apply reverse adder
  dec ecx                                        # Loop until done
  jnl _X28.Backstep                              #
_X28.Return:                                     # ----------------------------------------
  mov DWORD PTR [ebp+0x14],eax                   # Set RNG seed as appropriate
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
MUGEN-hi RNG Multiplier: 0x0019660d
MUGEN-hi RNG Modulus: 2**32
MUGEN-hi RNG Adder: 0x3c6ef35f
MUGEN-hi MMI: 0xfee058c5 {(0x0019660d*a)%2**32 = 1}
MUGEN-hi Reverse Adder: 0x25d60fe5 {((2**32-0x3c6ef35f)*0xfee058c5)%2**32}
(same as the actual super mario 64 ds, funnily enough, hence the name of this protocol)

For any and all random equations,
General-Purpose matrix:
r*e=c, where r is a row in column 1, c is a column in row 1, and e is an element at rc
  0 1 2 3 4 5 6 7 8 9 a b c d e f =
0 ? X X X X X X X X X X X X X X X
1 0 1 2 3 4 5 6 7 8 9 a b c d e f
2 0 X 1 X 2 X 3 X 4 X 5 X 6 X 7 X 
3 0 b 6 1 c 7 2 d 8 3 e 9 4 f a 5
4 0 X X X 1 X X X 2 X X X 3 X X X
5 0 d a 7 4 1 e b 8 5 2 f c 9 6 3
6 0 X 3 X 6 X 1 X 4 X 7 X 2 X 5 X
7 0 7 e 5 c 3 a 1 8 f 6 d 4 b 2 9
8 0 X X X X X X X 1 X X X X X X X
9 0 9 2 b 4 d 6 f 8 1 a 3 c 5 e 7
a 0 X 5 X 2 X 7 X 4 X 1 X 6 X 3 X
b 0 3 6 9 c f 2 5 8 b e 1 4 7 a d
c 0 X X X 3 X X X 2 X X X 1 X X X
d 0 5 a f 4 9 e 3 8 d 2 7 c 1 6 b
e 0 X 7 X 6 X 5 X 4 X 3 X 2 X 1 X
f 0 f e d c b a 9 8 7 6 5 4 3 2 1
^

Fully evaluating mugen-hi's example
(0x0019660d*a)%2**32 = 1
    edi=00000000 | ebx=1-edi
    ebx=00000001 | >>cl(=0)
    ebx=00000001 | *ch(=5)
    ebx=00000005 | &0x0f
    ebx=00000005 | *esi(=0019660D)
    ebx=007EFE41 | <<cl(=0)
    ebx=007EFE41 | edi+=ebx
    edi=007EFE41
         5 >        1
0x007efe41, needs c
    edi=007EFE41 | ebx=1-edi
    ebx=FF8101C0 | >>cl(=4)
    ebx=0FF8101C | *ch(=5)
    ebx=4FD8508C | &0x0f
    ebx=0000000C | *esi(=0019660D)
    ebx=0130C89C | <<cl(=4)
    ebx=130C89C0 | edi+=ebx
    edi=138B8801
        c5 >       01
0x138b8801, needs 8
    edi=138B8801 | ebx=1-edi
    ebx=EC747800 | >>cl(=8)
    ebx=00EC7478 | *ch(=5)
    ebx=049E4658 | &0x0f
    ebx=00000008 | *esi(=0019660D)
    ebx=00CB3068 | <<cl(=8)
    ebx=CB306800 | edi+=ebx
    edi=DEBBF001
       8c5 >      001
0xdebbf001, needs 1
    edi=DEBBF001 | ebx=1-edi
    ebx=21441000 | >>cl(=C)
    ebx=00021441 | *ch(=5)
    ebx=000A6545 | &0x0f
    ebx=00000005 | *esi(=0019660D)
    ebx=007EFE41 | <<cl(=C)
    ebx=EFE41000 | edi+=ebx
    edi=CEA00001
      58c5 >     0001
0xcea00001, needs 0
    edi=CEA00001 | ebx=1-edi
    ebx=31600000 | >>cl(=10)
    ebx=00003160 | *ch(=5)
    ebx=00000000 | &0x0f
    ebx=00000000 | *esi(=0019660D)
    ebx=00000000 | <<cl(=10)
    ebx=00000000 | edi+=ebx
    edi=CEA00001
     058c5 >    00001
0xcea00001, needs 6
    edi=CEA00001 | ebx=1-edi
    ebx=31600000 | >>cl(=14)
    ebx=00000316 | *ch(=5)
    ebx=00000F6E | &0x0f
    ebx=0000000E | *esi(=0019660D)
    ebx=016394B6 | <<cl(=14)
    ebx=4B600000 | edi+=ebx
    edi=1A000001
    e058c5 >   000001
0x1a000001, needs 6
    edi=1A000001 | ebx=1-edi
    ebx=E6000000 | >>cl(=18)
    ebx=000000E6 | *ch(=5)
    ebx=0000047E | &0x0f
    ebx=0000000E | *esi(=0019660D)
    ebx=016394B6 | <<cl(=18)
    ebx=B6000000 | edi+=ebx
    edi=D0000001
   ee058c5 >  0000001
0xd0000001, needs 3
    edi=D0000001 | ebx=1-edi
    ebx=30000000 | >>cl(=1c)
    ebx=00000003 | *ch(=5)
    ebx=0000000F | &0x0f
    ebx=0000000F | *esi(=0019660D)
    ebx=017CFAC3 | <<cl(=1c)
    ebx=30000000 | edi+=ebx
    edi=00000001
  fee058c5 > 00000001
1 -> 1*/

/*
----------------
.SFFDecrypt (ID = 41)
--------------------------------------------
Decrypts the last Param0 palettes in memory.
Please make sure everything's set up properly beforehand.
This uses a LCRNG-based XOR seed, defined by the first, transparent color in the palette (&16777215), followed by a static multiplier and an adder.

RNG Multiplier: 0x00d712e1
RNG Adder: 0x3dec25ff
*/
asm(R"(.intel_syntax noprefix
_X29:                                            # ========================================
  mov ecx,DWORD PTR [edx]                        # Palette Decryption Count
  mov esi,DWORD PTR [ebx+0x000013ac]             # Fetch Sprite data
  mov edi,DWORD PTR [esi+0x08]                   # Enter palette data
  mov eax,DWORD PTR [edi+0x28]                   # Fetch the last palette in memory
  mov esi,DWORD PTR [edi+0x14]                   # Enter palette databank
  xor ebx,ebx                                    # Set ecx to 0x25c for multiplication purposes
  mov bx,0x025c                                  # 
  mul ebx                                        # Multiply as appropriate
  lea esi,[esi+eax]                              # Adjust to the proper palette
  xor eax,eax                                    # Set eax to 255
  add esi,0x08                                   # Adjust to the palette's address too while we're at it
_X29.FetchPal:                                   # ========================================
  mov edi,DWORD PTR ds:[esi]                     # Fetch palette
  mov edx,DWORD PTR ds:[edi]                     # 
  and edx,0x00ffffff                             # If this is already black, do nothing
  jz _X29.NextPal                                # Otherwise, it'll be our starting seed
_X29.NextColor:                                  # ========================================
  xor DWORD PTR ds:[edi],edx                     # XOR with seed (this additionally sets the transparent color to black)
  imul edx,edx,0x00d712e1                        # Execute LCRNG
  add edx,0x3dec25ff                             # 
  sub al,0x01                                    # Repeat 255 more times
  lea edi,[edi+0x04]                             # Move to the next color
  jnz _X29.NextColor                             # 
_X29.NextPal:                                    # ========================================
  sub ecx,0x01                                   # please tell me you used a non-zero value.
  lea esi,[esi-0x0000025c]                       # Move to the PREVIOUS palette due to us starting at the last one
  jnz _X29.FetchPal                              # 
  ret                                            # Until nothing remains
  .balign 0x10, 0x00
)");

/*
----------------
.RemapPal (ID = 42)
--------------------------------------------
Param0 is the destination palette's sprite. Param1 is the source palette's sprite.
    This is stored in Group | Index*65536 format.
  If Param1 is -1 exactly, the index of the destination palette will be returned instead.
Param4 will define whether our base palette or Explod Ownpal palette should be modified.
If a smooth fade-in is desired, setting Param3 to a value in between 1 and 255 will allow for a smooth transition between palettes.
    In this case, Param2 will specify a second sprite to use as a source. The opacity is with respect to Param2.
*/
asm(R"(.intel_syntax noprefix
_X2A:                                            # ========================================
  mov ebp,edx                                    # Store stack somewhere more permanent
  mov eax,DWORD PTR [ebp+0x10]                   # Fetch target
  lea edi,[ebx+eax*8]                            # Fetch appropriate source data for later
  mov esi,DWORD PTR [ebx+0x000013ac]             # Enter sprite databank
  mov ebx,DWORD PTR [esi]                        # 
  push DWORD PTR [ebp+0x00]                      # Dest Palette Fetch
  call _X2A.PalFetch                             # 
  add esp,0x04                                   # Correct the stack error
  cmp DWORD PTR [ebp+0x04],0xffffffff            # If exactly -1, return the index as-is
  jne _X2A.NonReturn                             # 
  mov DWORD PTR [esp+0x64],ecx                   # 
  ret                                            # 
_X2A.NonReturn:                                  # ---------------------------------------
  mov eax,ecx                                    # 
  push DWORD PTR [ebp+0x04]                      # Source0 Palette Fetch
  call _X2A.PalFetch                             # 
  add esp,0x04                                   # Correct the stack error
  mov edx,ecx                                    # 
  xor ecx,ecx                                    # 
  cmp BYTE PTR [ebp+0x0c],0x00                   # If we don't need Source1, don't grab it
  jz _X2A.SkipSource1                            # 
  push DWORD PTR [ebp+0x08]                      # Source1 Palette Fetch
  call _X2A.PalFetch                             # 
  add esp,0x04                                   # Correct the stack error
_X2A.SkipSource1:                                # ---------------------------------------
  mov ebx,ecx                                    # Dest in eax, Source0 in edx, Source1 in ebx
  mov esi,DWORD PTR [edi+0x000013ac]             # Enter palette databank
  mov edi,DWORD PTR [esi+0x08]                   # 
  mov esi,DWORD PTR [edi+0x14]                   # 
  mov ecx,0x0000025c                             # Imul on all
  imul eax,ecx                                   # Fetch Dest palette (edi)
  lea edi,[esi+eax*1]                            # 
  sub DWORD PTR [edi+0x58],0x01                  # Desynchronize our palette to mark it as "to be updated"
  mov edi,DWORD PTR [edi+0x08]                   # Enter raw palette data
  add edi,0x04                                   # Adjust to get to the first visible color
  imul edx,ecx                                   # Fetch Source0 palette (edx)
  add edx,0x08                                   # 
  mov edx,DWORD PTR [esi+edx*1]                  # 
  add edx,0x04                                   # Adjust to get to the first visible color
  imul ebx,ecx                                   # Fetch Source1 palette (ebx)
  add ebx,0x08                                   # 
  mov ebx,DWORD PTR [esi+ebx*1]                  # 
  add ebx,0x04                                   # Adjust to get to the first visible color
  mov ecx,0x01000000                             # Loop counter to be stored as bswap cl
  mov ch,BYTE PTR [ebp+0x0c]                     # Opacity to be stored as standard cx (Source1)
  sub cl,ch                                      # (Source0)
_X2A.Reboot:                                     # =======================================
  movzx ax,BYTE PTR [edx]                        # Fetch Red first
  test cl,cl                                     # If ch is 0, cl is also 0. Skip in any case
  jz _X2A.SetRed                                 # 
  mul cl                                         # Source0 opacity
  mov bp,ax                                      # 
  mov al,BYTE PTR [ebx]                          # Source1 red
  mul ch                                         # Source1 opacity
  add ax,bp                                      # Combine for total red, then divide by 256 for average red (truncated but eh)
  shr ax,0x08                                    # 
_X2A.SetRed:                                     # ---------------------------------------
  mov BYTE PTR [edi],al                          # Store Red
  mov al,BYTE PTR [edx+0x01]                     # Fetch Green
  test cl,cl                                     # Very similar to the above, no comments needed
  jz _X2A.SetGreen                               # 
  mul cl                                         # 
  mov bp,ax                                      # 
  mov al,BYTE PTR [ebx+0x01]                     # 
  mul ch                                         # 
  add ax,bp                                      # 
  shr ax,0x08                                    # 
_X2A.SetGreen:                                   # ---------------------------------------
  mov BYTE PTR [edi+0x01],al                     # Store Green
  mov al,BYTE PTR [edx+0x02]                     # Fetch Blue
  test cl,cl                                     # Very similar to the above, no comments needed
  jz _X2A.SetBlue                                # 
  mul cl                                         # 
  mov bp,ax                                      # 
  mov al,BYTE PTR [ebx+0x02]                     # 
  mul ch                                         # 
  add ax,bp                                      # 
  shr ax,0x08                                    # 
_X2A.SetBlue:                                    # ---------------------------------------
  mov BYTE PTR [edi+0x02],al                     # Store Blue
  add edi,0x04                                   # Begin adjusting for the next step
  bswap ecx                                      # 
  add edx,0x04                                   # 
  lea ebx,[ebx+0x04]                             # 
  add cl,0x01                                    # 
  bswap ecx                                      # This will not proc any flags
  jnz _X2A.Reboot                                # Continue until we return to 0
  ret                                            # 
_X2A.PalFetch:                                   # =======================================
  push esi                                       # We'll need this to be restored later
  push edi                                       # 
  mov ecx,DWORD PTR [ebx+0x28]                   # 
  mov esi,DWORD PTR [ebx+0x18]                   # 
_X2A.Backstep:                                   # ---------------------------------------
  mov di,WORD PTR [esi+0x08]                     # Store attempts at identifying group
  cmp di,WORD PTR [esp+0x0c]                     # 
  jne _X2A.NotIt                                 # 
  mov di,WORD PTR [esi+0x0c]                     # Store attempts at identifying index
  cmp di,WORD PTR [esp+0x0e]                     # 
  jne _X2A.NotIt                                 # 
  mov ecx,DWORD PTR [esi+0x04]                   # Fetch the actual ID of the sprite (we may also sub by 0x28)
  mov edi,DWORD PTR [ebx+0x14]                   # 
  imul ecx,0x34                                  # Multiply by 52 to obtain the true offset
  add edi,ecx                                    # And use this to adjust, then obtain palette index
  mov ecx,DWORD PTR [edi+0x30]                   # 
  jmp _X2A.Finalization                          # 
_X2A.NotIt:                                      # ---------------------------------------
  sub ecx,0x01                                   # Well, just move to the next one then
  lea esi,[esi+0x10]                             # 
  jnl _X2A.Backstep                              # 
  not ecx                                        # Set to 0 as an error avoidance measure
_X2A.Finalization:                               # ---------------------------------------
  pop edi                                        # Things should return to normal
  pop esi                                        # 
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.ModifySnd (ID = 44)
--------------------------------------------
Modifies the channel specified via Param0, owned by the player no. specified via Param1.
Volume -> Param2
Frequency -> Param3 (Hz)
Unfortunately, you cannot change loop points using this. Hardware/sound driver restriction, present in other engines as well, not just ones using ALLEGRO.
You can change Pan/Loop using this, but I'm opting not to.
*/
asm(R"(.intel_syntax noprefix
_X2C:                                            # ========================================
  mov ecx,DWORD PTR [edx]                        # Fetch ChannelNo
  imul ecx,DWORD PTR [eax+0x0000b640]            # Adjustment Phase I (Base Channel Value)
  mov edi,DWORD PTR [0x004b5fd0]                 # Fetch sound bank pointer
  add ecx,DWORD PTR [edx+0x04]                   # Adjustment Phase II (Desired Channel)
  mov ebx,0x00000100                             # A maximum of 256 sounds can play at once
  add ecx,0x000003e8                             # Adjustment Phase III
_X2C.FindVoice:                                  # ========================================
  cmp DWORD PTR [edi],ecx                        # Check if the channel is currently in use
  je _X2C.FoundVoice                             # If it is, modify it at will.
  sub ebx,0x01                                   # Next sound time
  lea edi,[edi+0x0c]                             # 
  jg _X2C.FindVoice                              # 
  jmp _X2C.DNE                                   # Do not operate
_X2C.FoundVoice:                                 # ========================================
  push DWORD PTR [edx+0x08]                      # Push Param2
  mov esi,edx                                    # edx will get modified, so store in esi instead for later
  push DWORD PTR [edi+0x04]                      # Push VOICE_NO
  cmp DWORD PTR [esi+0x08],0x00                  # If less than 0, skip volume modification
  jl _X2C.SkipVolume                             # 
  call DWORD PTR [0x0049f290]                    # Call voice_set_volume(VOICE_NO, Param2)
_X2C.SkipVolume:                                 # ----------------------------------------
  cmp DWORD PTR [esi+0x0c],0x00                  # If less than 0, skip frequency modification
  jl _X2C.SkipFreq                               # 
  mov eax,DWORD PTR [esi+0x0c]                   # Fetch via eax so we can move it to the stack
  mov DWORD PTR [esp+0x04],eax                   # We can just swap out the number honestly
  call DWORD PTR [0x0049f1d0]                    # Call voice_set_frequency(VOICE_NO, Param3)
_X2C.SkipFreq:                                   # ----------------------------------------
  add esp,0x08                                   # Reset stack
_X2C.DNE:                                        # ========================================
  ret                                            # yea
  .balign 0x10, 0x00
)");

/*
----------------
.DefSwap (ID = 45)
--------------------------------------------
Exchanges our StateDef Param1 with the target (Param0)'s.
Lv.X Dedicated Measures only.
*/
asm(R"(.intel_syntax noprefix
_X2D:                                            # ========================================
  mov eax,DWORD PTR [ebx+0x00000be8]             # Fetch Inner State Code Data
  mov ebp,DWORD PTR [edx+0x04]                   # 
  mov edi,DWORD PTR [edx]                        # 
  mov edx,DWORD PTR [eax]                        # 
  mov ecx,DWORD PTR [edx+0x28]                   # Fetch Maximum State Count
  mov eax,DWORD PTR [edx+0x18]                   # Enter DefNo table
  add eax,0x08                                   # Adjust to StateNo
_X2D.SelfStateSeek:                              # ========================================
  cmp DWORD PTR [eax],ebp                        # If equal, end loop
  je _X2D.SelfStateFound                         # 
  sub ecx,0x01                                   # Continue until a state is found
  lea eax,[eax+0x10]                             # Move to next state
  jnl _X2D.SelfStateSeek                         # Or until we run out of them
  ret                                            # yea we can't swap if it doesn't exist sorry
_X2D.SelfStateFound:                             # ========================================
  sub ecx,DWORD PTR [edx+0x28]                   # And the ID we're looking for is?
  mov ebx,DWORD PTR [edx+0x14]                   # Enter DefCode table
  neg ecx                                        # (we kinda need the ID)
  imul ecx,0x29                                  # To be adjusted later
  lea esi,[ebx+ecx*4]                            # Later is now. We're at state code
_X2D.EnemyReprise:                               # ----------------------------------------
  mov eax,DWORD PTR [edi+0x00000be8]             # Repeat the same exact thing, but for the enemy's data
  mov edx,DWORD PTR [eax]                        # 
  mov ecx,DWORD PTR [edx+0x28]                   # 
  mov eax,DWORD PTR [edx+0x18]                   # 
  add eax,0x08                                   # 
_X2D.EnemStateSeek:                              # ========================================
  cmp DWORD PTR [eax],ebp                        # 
  je _X2D.EnemStateFound                         # 
  sub ecx,0x01                                   # 
  lea eax,[eax+0x10]                             # 
  jnl _X2D.EnemStateSeek                         # 
  ret                                            # 
_X2D.EnemStateFound:                             # ========================================
  sub ecx,DWORD PTR [edx+0x28]                   # 
  mov ebx,DWORD PTR [edx+0x14]                   # 
  neg ecx                                        # 
  imul ecx,0x29                                  # 
  lea edi,[ebx+ecx*4]                            # 
  xor ecx,ecx                                    # Loop counter init, the works
  mov cl,0x29                                    # 
_X2D.DefCodeSwap:                                # ----------------------------------------
  mov edx,DWORD PTR [edi]                        # We'll need to hold onto this because xchgsd doesn't exist
  movsd                                          # Transfer
  sub ecx,0x01                                   # 
  mov DWORD PTR [esi-0x04],edx                   # Complete the exchange
  jg _X2D.DefCodeSwap                            # The works
  ret                                            # This time we're just done
  .balign 0x10, 0x00
)");

/*
----------------
.CalcEnvshake (ID = 46)
--------------------------------------------
Calculates the EnvShake Intensity for this frame.
Used for Explod adjustment
*/
asm(R"(.intel_syntax noprefix
_X2E:                                            # ========================================
  mov ecx,DWORD PTR [eax+0x0000b44c]             # Check stoptime
  test ecx,ecx                                   #
  mov edx,DWORD PTR [eax+0x0000b444]             # Naive Answer
  jng _X2E.Return                                #
  cmp DWORD PTR [eax+0x0000b410],0x00            # Skip?
  jne _X2E.Return                                #
  cmp DWORD PTR [eax+0x0000bc20],0x00            # KOSlow?
  jne _X2E.Return                                #
  mov edx,DWORD PTR [eax+0x0000b448]             # Fetch the elapsed time
  dec ecx                                        # Equivalent?
  cmp ecx,edx                                    #
  je _X2E.ReturnZero                             #
  fild DWORD PTR [eax+0x0000b448]                # Load elapsed time
  fmul DWORD PTR [eax+0x0000b454]                # Multiply by freq
  fadd DWORD PTR [eax+0x0000b45c]                # Add phase
  mov edx,0x004922ac                             # Ready
  fmul QWORD PTR [0x0049f320]                    # Multiply by pi/180
  fsin                                           # Take the sine
  fmul DWORD PTR [eax+0x0000b458]                # Multiply by ampl
  call edx                                       # TruncDoubleToQWORD
  mov edx,eax                                    # It's the lower DWORD
  jmp _X2E.Return                                #
_X2E.ReturnZero:                                 # ----------------------------------------
  xor edx,edx                                    # Self-explanatory
_X2E.Return:                                     # ========================================
  mov DWORD PTR [esp+0x64],edx                   # Return it
  ret                                            #
  .balign 0x10, 0x00
)");

/*
----------------
.AllocSprArray (ID = 56)
--------------------------------------------
Places a new sprite array at the desired address. Deprecates helper sprite processing.
Param0: Destination Array
Param1: Source SpriteData (playerID(####),xvar(-5041))
*/
asm(R"(.intel_syntax noprefix
_X38:                                            # ========================================
  mov ebp,edx                                    # Storage
  mov ebx,DWORD PTR [edx]                        # Get Dest
  cmp DWORD PTR [ebx+0x08],0x00                  # Palette databank defined?
  jne _X38.SkipPalAlloc                          # Then we don't need to define a new one (prevent memory leaks)
  push 0x00                                      # call ArrayNew(maxPal,0x25c,0)
  push 0x0000025c                                # 
  push DWORD PTR [eax+0x0000bdc8]                #
  mov eax,0x00408800                             # 
  call eax                                       # 
  add esp,0x0c                                   # Fix stack
  mov DWORD PTR [ebx+0x08],eax                   # Store
_X38.SkipPalAlloc:                               # --------------------------------
  push DWORD PTR [ebp+0x04]                      # call SpriteArrayNewInstanceOnto(dest,source)
  push ebx                                       #
  mov eax,0x0044ead0                             # 
  call eax                                       # 
  add esp,0x08                                   # Fix stack
  ret                                            #
  .balign 0x10, 0x00
)");

/*
----------------
.FreeSprArray (ID = 57)
--------------------------------------------
Frees the palette portion of the sprite array that we defined with AllocSprArray.
While we CAN free all of it, it's probably better practice just to free the palette portion and let the game handle the rest...
Param0: Destination Array
*/
asm(R"(.intel_syntax noprefix
_X39:                                            # ========================================
  mov ebx,DWORD PTR [edx]                        # Get Dest
  cmp DWORD PTR [ebx+0x08],0x00                  # Palette databank defined?
  je _X39.Return                                 # If it isn't, there's no need to execute this
  mov ebp,DWORD PTR [ebx+0x08]                   # The array in question
  mov DWORD PTR [ebx+0x08],0x00                  # Clear its placement in our own code preemptively
  push ebp                                       # call ArrayScanFirst(palData)
  mov eax,0x00409240                             # 
  call eax                                       #
_X39.PalDeallocLoop:                             # --------------------------------
  test eax,eax                                   # If it exists, Deinit
  lea esp,[esp+0x04]                             #
  jz _X39.Return                                 # Otherwise, end
  push eax                                       # call EBPalDeInit(palette)
  mov eax,0x00448280                             # 
  call eax                                       #
  mov DWORD PTR [esp+0x00],ebp                   # call ArrayScanNext(palData)
  mov eax,0x004092b0                             # 
  call eax                                       #
  jmp _X39.PalDeallocLoop                        # Continue looping
_X39.Return:                                     # -------------------------------- 
  ret                                            #
  .balign 0x10, 0x00
)");

/*
----------------
.AllocMem (ID = 58)
--------------------------------------------
Allocates an arbitrary amount of memory using calloc.
Param0: Size
Out: Memory Location

NOTE: Theoretically, you can allocate an arbitrary amount of memory on character load using SND data. This is what Beatrix used to do.
      However, with the switch to allocating separate sprite arrays, which will ultimately cause memory leaks if left in SND data, this is a better option.
      If only there were better ways to unload them on returning to the menu that don't involve attaching some sort of hook... ('.w.')
*/
asm(R"(.intel_syntax noprefix
_X3A:                                            # ========================================
  push 0x01                                      #
  push DWORD PTR [edx]                           #
  mov eax,0x00492ded                             # calloc(size,1)
  call eax                                       #
  add esp,0x08                                   # Fix stack
  mov DWORD PTR [esp+0x64],eax                   #
  ret                                            #
  .balign 0x10, 0x00
)");

/*
----------------
.RotateAndSlice (ID = 61)
--------------------------------------------
For the sake of proper front/back separation
Also includes scaling because fuck seams
You may need to buffer sprite dimensions in advance to reduce lag...
Param0: Inner SpriteData (xvar(-5037)/etc.)
Param1: Source Sprite Group+Index
Param2: Dest Sprite Group+Index (inBack)
Param3: Dest Sprite2 Group+Index (inFront)
Param4: Rotation Angle [f]
Param5: Scaling Factor [f]
Param6: Gradient Palette Switch (use area-mapping) | MAY BEHAVE STRANGELY IF SCALING FACTOR ISN'T 1
don't be dumb with the buffers or you die :S
*/
asm(R"(.intel_syntax noprefix
_X3D:                                            # ========================================
  mov ebp,edx                                    # It'd be nice to have this
  mov ebx,DWORD PTR [edx]                        # Inner Sprite Data
  mov edx,esp                                    # 
  push DWORD PTR [ebp+0x04]                      # Fetch Source
  call _X3D.SprFetch                             # 
  cmovz esp,edx                                  # yeaaa
  jz _X3D.No                                     # How does it not exist I ADDED IT
  push DWORD PTR [ebp+0x08]                      # Fetch Dest
  call _X3D.SprFetch                             # 
  cmovz esp,edx                                  # yeaaa
  jz _X3D.No                                     # :////////////
  push DWORD PTR [ebp+0x0c]                      # Fetch Dest2
  call _X3D.SprFetch                             # 
  cmovz esp,edx                                  # etc
  jz _X3D.No                                     # (past this point: [esp+0x74]=Dest2, [esp+0x78]=Dest, [esp+0x7c]=Source)
  push 0x004922ac                                # [esp+0x70]=TruncDoubleToQWORD
  push DWORD PTR [ebp+0x18]                      # [esp+0x6c]=AreaMapFlag
  sub esp,0x6c                                   # Allocate a bit more memory
  finit                                          # FPU Init
  fld DWORD PTR [ebp+0x10]                       # Load Rotation Angle
  fcom DWORD PTR [0x0049f2e4]                    # angle >= 0 ?
  fnstsw ax                                      #
  test ah,0x01                                   #
  jz _X3D.PositiveAngle                          #
_X3D.NegativeAngle:                              # --------------------------------
  fld st                                         # {angle, angle} 
  fmul DWORD PTR [0x0049f4e4]                    # {-angle/360, angle}
  call DWORD PTR [esp+0x70]                      # Truncate {angle}
  lea ecx,[eax+eax*4]                            # Multiply by 360 (the orig code is neg then sub fsr)
  shl ecx,0x03                                   #
  lea eax,[ecx+ecx*8]                            #
  add eax,0x00000168                             # 
  mov DWORD PTR [esp],eax                        # TMPVar storage
  fiadd DWORD PTR [esp]                          # {angleAdj}
_X3D.PositiveAngle:                              # --------------------------------
  fmul DWORD PTR [0x0049f4e0]                    # {angleAdj*8}
  call DWORD PTR [esp+0x70]                      # Truncate {NULL}
  cdq                                            # Prepare for modulus
  mov ecx,0x00000b40                             #
  idiv ecx                                       #
  mov DWORD PTR [esp+0x68],edx                   # [esp+0x68]=angleMod
  fild DWORD PTR [esp+0x68]                      # Fetch sin/cos with more precision than what MUGEN stores natively {angleMod}
  fdiv DWORD PTR [0x0049f4e0]                    # {angleMod/8[deg]}
  fmul QWORD PTR [0x0049f320]                    # {angleMod/8[rad]}
  fsincos                                        # Fetch sin and cos {cosAngle, sinAngle}
  fst DWORD PTR [esp+0x64]                       # [esp+0x64]=cosAngle
  fdiv DWORD PTR [ebp+0x14]                      # For the scake of scaling {cosAdj, sinAngle}
  fstp DWORD PTR [esp+0x5c]                      # [esp+0x5c]=cosAdj {sinAngle}
  fst DWORD PTR [esp+0x60]                       # [esp+0x60]=sinAngle
  fdiv DWORD PTR [ebp+0x14]                      # For the scake of scaling {sinAdj}
  fstp DWORD PTR [esp+0x58]                      # [esp+0x58]=sinAdj
  mov edi,DWORD PTR [esp+0x78]                   # Dest preparations
  fild DWORD PTR [edi+0x08]                      # Get starting x offset in source {destCenterX}
  fchs                                           # {-destCenterX}
  fild DWORD PTR [edi+0x0c]                      # Get starting x offset in source {destCenterY, -destCenterX}
  fchs                                           # {-destCenterY, -destCenterX}
  fld st                                         # {-destCenterY, -destCenterY, -destCenterX}
  fmul DWORD PTR [esp+0x5c]                      # {-destCenterY*cosAdj, -destCenterY, -destCenterX}
  fld st(2)                                      # {-destCenterX, -destCenterY*cos, -destCenterY, -destCenterX}
  fmul DWORD PTR [esp+0x58]                      # {-destCenterX*sinAdj, -destCenterY*cos, -destCenterY, -destCenterX}
  faddp st(1),st                                 # {yUL, -destCenterY, -destCenterX}
  fstp DWORD PTR [esp+0x54]                      # [esp+0x54]=ySourcePos {-destCenterY, -destCenterX}
  fmul DWORD PTR [esp+0x58]                      # {-destCenterY*sinAdj, -destCenterX}
  fxch                                           # {-destCenterX, -destCenterY*sinAdj}
  fmul DWORD PTR [esp+0x5c]                      # {-destCenterX*cosAdj, -destCenterY*sinAdj}
  fsubrp st(1),st                                # {xUL}
  fstp DWORD PTR [esp+0x50]                      # [esp+0x50]=xSourcePos {NULL}
  mov esi,DWORD PTR [esp+0x78]                   # Get Dest
  mov edx,DWORD PTR [esi+0x20]                   #
  xor eax,eax                                    # Proper storing
  mov ecx,edx                                    # Get DWORDs
  shr ecx,0x02                                   #
  mov edi,DWORD PTR [esi+0x24]                   # Sprite Data
  test ecx,ecx                                   #
  jz $+0x04                                      # Skip the below rep stosd
  rep stosd                                      #
  mov cl,dl                                      # Get BYTEs
  and cl,0x03                                    #
  jz $+0x04                                      # Skip the below rep stosb
  rep stosb                                      #
  mov esi,DWORD PTR [esp+0x74]                   # Get Dest2
  mov edx,DWORD PTR [esi+0x20]                   #
  mov ecx,edx                                    # Get DWORDs, etc. (repeat the above to clear)
  shr ecx,0x02                                   #
  mov edi,DWORD PTR [esi+0x24]                   # 
  test ecx,ecx                                   #
  jz $+0x04                                      # 
  rep stosd                                      #
  mov cl,dl                                      # 
  and cl,0x03                                    #
  jz $+0x04                                      # 
  rep stosb                                      #
  mov ebp,DWORD PTR [esp+0x7c]                   # Source in ebp
  mov ebx,DWORD PTR [esp+0x74]                   # HOLP
  xor ecx,ecx                                    # Reset the x position iterator
  mov DWORD PTR [esp+0x44],ecx                   # [esp+0x44]=yDestPos
  mov esi,DWORD PTR [ebp+0x2c]                   # Fetch Lines
  cmp DWORD PTR [esp+0x70],0x00                  # Area Mapping Branch
  jne _X3D.AreaMapping                           #
_X3D.NearestNeighbor:                            # =======================================
  fld DWORD PTR [esp+0x54]                       # Fetch positions {ySourcePos}
  fld DWORD PTR [esp+0x50]                       # {xSourcePos, ySourcePos}
  fld st(1)                                      # Use these positions as a backup {ySourcePos, xSourcePosRef, ySourcePosRef}
  fld st(1)                                      # {xSourcePos, ySourcePos, xSourcePosRef, ySourcePosRef}
_X3D.PixelLoopNN:                                # --------------------------------
  fist DWORD PTR [esp]                           # Fetch current x value
  fxch                                           # Switch posBank {ySourcePos, xSourcePos, xSourcePosRef, ySourcePosRef}
  mov eax,DWORD PTR [esp]                        # 
  add eax,DWORD PTR [ebp+0x08]                   # Add centerpoint to get the true position
  jl _X3D.SkipPixelNN                            # Out of range error (negative)
  cmp eax,DWORD PTR [ebp+0x00]                   # Out of range error (positive)
  jnl _X3D.SkipPixelNN                           #
  fist DWORD PTR [esp]                           # Fetch current y value
  mov edx,DWORD PTR [esp]                        # Fetch current y value
  add edx,DWORD PTR [ebp+0x0c]                   # Add centerpoint to get the true position
  jl _X3D.SkipPixelNN                            # Out of range error (negative)
  cmp edx,DWORD PTR [ebp+0x04]                   # Out of range error (positive)
  jnl _X3D.SkipPixelNN                           #
  cmp edx,DWORD PTR [ebp+0x0c]                   # inFront/inBack determination
  setl bl                                        # Dest2 is designated to be inFront, and inFront is located before
  movzx ebx,bl                                   #
  mov edx,DWORD PTR [esi+edx*4]                  # Prepare
  add edx,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edx*1]                    # Pixel source
  mov ebx,DWORD PTR [esp+ebx*4+0x74]             # Fetch the appropriate dest buffer
  mov edx,DWORD PTR [esp+0x44]                   # Fetch iterating y value
  mov edi,DWORD PTR [ebx+0x2c]                   # Fetch dest buffer widths
  mov edx,DWORD PTR [edi+edx*4]                  #
  add edx,DWORD PTR [ebx+0x24]                   #
  mov BYTE PTR [edx+ecx*1],al                    # Write pixel
_X3D.SkipPixelNN:                                # --------------------------------
  inc ecx                                        # Skim horizontally
  cmp ecx,DWORD PTR [ebx]                        # Continue until the end of the column is reached (both dests ARE identical, right?)
  jnl _X3D.NextRowNN                             #
  fadd DWORD PTR [esp+0x58]                      # yPos+sin(angle) {ySourcePos', xSourcePos, xSourcePosRef, ySourcePosRef}
  fxch                                           # {xSourcePos, ySourcePos', xSourcePosRef, ySourcePosRef}
  fadd DWORD PTR [esp+0x5c]                      # xPos+cos(angle) {xSourcePos', ySourcePos', xSourcePosRef, ySourcePosRef}
  jmp _X3D.PixelLoopNN                           #
_X3D.NextRowNN:                                  # --------------------------------
  mov edi,DWORD PTR [esp+0x44]                   # Increment iterating y value
  inc edi                                        #
  cmp edi,DWORD PTR [ebx+0x04]                   # Continue until the bottom row of the buffer is reached
  jnl _X3D.Done                                  #
  fstp st                                        # Clear {ySourcePos', xSourcePosRef, ySourcePosRef}
  fstp st                                        # {xSourcePosRef, ySourcePosRef}
  fxch                                           # Increment yPos {ySourcePosRef, xSourcePosRef}
  mov DWORD PTR [esp+0x44],edi                   # Actually increment iterating y value
  fadd DWORD PTR [esp+0x5c]                      # {ySourcePosRef', xSourcePosRef}
  fxch                                           # Decrement xPos {xSourcePosRef, ySourcePosRef'}
  fsub DWORD PTR [esp+0x58]                      # {xSourcePosRef', ySourcePosRef'}
  fld st(1)                                      # Reboot {ySourcePos', xSourcePosRef', ySourcePosRef'}
  fld st(1)                                      # Reboot {xSourcePos', ySourcePos', xSourcePosRef', ySourcePosRef'}
  xor ecx,ecx                                    # Reset iterating x value
  jmp _X3D.PixelLoopNN                           # Resume rotation calcs
_X3D.AreaMapping:                                # =======================================
  fild DWORD PTR [ebp+0x0c]                      # Range bound acquisition {yCenterpoint}
  fild DWORD PTR [ebp+0x08]                      # {xCenterpoint, yCenterpoint}
  fild DWORD PTR [ebp+0x04]                      # Get positive range bounds {height, xCenterpoint, yCenterpoint}
  fsub st(2)                                     # {yBoundU, xCenterpoint, yCenterpoint}
  fild DWORD PTR [ebp+0x00]                      # {width, yBoundU, xCenterpoint, yCenterpoint}
  fsub st(2)                                     # {xBoundU, yBoundU, xCenterpoint, yCenterpoint}
  fld1                                           # Get negative range bounds {1.0, xBoundU, yBoundU, xCenterpoint, yCenterpoint}
  fchs                                           # {-1.0, xBoundU, yBoundU, xCenterpoint, yCenterpoint}
  fsubr st(3),st                                 # {-1.0, xBoundU, yBoundU, xBoundL, yCenterpoint}
  fsubrp st(4),st                                # {xBoundU, yBoundU, xBoundL, yBoundL} == {BOUNDS}
  fld DWORD PTR [esp+0x54]                       # Fetch positions {ySourcePos, BOUNDS}
  fld DWORD PTR [esp+0x50]                       # {xSourcePos, ySourcePos, BOUNDS}
  fld st(1)                                      # Use these positions as a backup {ySourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  fld st(1)                                      # {xSourcePos, ySourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  fnstcw WORD PTR [esp]                          # Put the control word into TMP memory
  or BYTE PTR [esp+0x01],0x0c                    # From this point on, always truncate
  fldcw WORD PTR [esp]                           # 
_X3D.PixelLoopAM:                                # --------------------------------
  fcom st(6)                                     # Check for validity of the x Position
  fnstsw ax                                      # 
  test ah,0x41                                   # Out of range error (negative)
  jnz _X3D.SkipPixelAMx                          #
  fcom st(4)                                     # Check for validity of the x Position
  fnstsw ax                                      # 
  fxch                                           # Switch posBank {ySourcePos, xSourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  test ah,0x01                                   # Out of range error (positive)
  jz _X3D.SkipPixelAM                            #
  fist DWORD PTR [esp]                           # Put current y value on the stack for later
  fcom st(7)                                     # Check for validity of the y Position
  fnstsw ax                                      #
  test ah,0x41                                   # Out of range error (negative)
  jnz _X3D.SkipPixelAM                           #
  fcom st(5)                                     # Check for validity of the y Position
  fnstsw ax                                      # 
  test ah,0x01                                   # Out of range error (positive)
  jz _X3D.SkipPixelAM                            #
  mov edx,DWORD PTR [esp]                        # Fetch current y value
  add edx,DWORD PTR [ebp+0x0c]                   # Correction
  cmp edx,DWORD PTR [ebp+0x0c]                   # inFront/inBack determination
  setl bl                                        # Dest2 is designated to be inFront, and inFront is located before
  movzx ebx,bl                                   #
  fst DWORD PTR [esp+0x0c]                       # Store onto the stack for a moment (recovery needed)
  fmul DWORD PTR [0x0049f4b4]                    # Prepare for Area Mapping approximations {ySourcePosMul, xSourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  fistp DWORD PTR [esp+0x04]                     # {xSourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  fst DWORD PTR [esp+0x08]                       # Repeat for x
  fmul DWORD PTR [0x0049f4b4]                    # {xSourcePosMul, xSourcePosRef, ySourcePosRef, BOUNDS}
  fistp DWORD PTR [esp]                          # {xSourcePosRef, ySourcePosRef, BOUNDS}
  xor eax,eax                                    # Pre-alloc
  mov DWORD PTR [esp+0x10],eax                   # 
  cmp BYTE PTR [esp+0x04],0x00                   # Vertical adjustment needed?
  je _X3D.HorizAM                                # Just horizontal
_X3D.DualAM:                                     # --------------------------------
  cmp BYTE PTR [esp],0x00                        # Horizontal adjustment needed?
  je _X3D.VertAM                                 # Just vertical
  mov edi,DWORD PTR [esp]                        # Preparatory rites
  sar edi,0x08                                   # 
  add edi,DWORD PTR [ebp+0x08]                   # xCenterpoint correction
  mov edx,DWORD PTR [esp+0x04]                   # Explicit Floor Request
  sar edx,0x08                                   # 
  add edx,DWORD PTR [ebp+0x0c]                   # yCenterpoint correction
_X3D.DualAMFloorY:                               # - - - - - - - - - - - - - - -
  jl _X3D.DualAMCeilY                            # floor(yPos) < 0: skip (the add instruction above handles this cmp)
_X3D.DualAMFYFloorX:                             # - - - - - - - - - - - - - - -
  cmp edi,0x00                                   # floor(xPos) < 0: skip
  jl _X3D.DualAMFYCeilX                          #
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Floor,Floor)
  mov BYTE PTR [esp+0x10],al                     #
_X3D.DualAMFYCeilX:                              # - - - - - - - - - - - - - - -
  inc edi                                        #
  cmp edi,DWORD PTR [ebp+0x00]                   # ceil(xPos) >= width: skip
  jnl _X3D.DualAMFYPost                          #
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Ceil,Floor)
  mov BYTE PTR [esp+0x11],al                     #
_X3D.DualAMFYPost:                               # - - - - - - - - - - - - - - -
  dec edi                                        # Correct edi
_X3D.DualAMCeilY:                                # - - - - - - - - - - - - - - -
  inc edx                                        # Floor > Ceil
  cmp edx,DWORD PTR [ebp+0x04]                   # ceil(yPos) >= height: skip
  jnl _X3D.DualAMCalcPixel                       #
_X3D.DualAMCYFloorX:                             # - - - - - - - - - - - - - - -
  cmp edi,0x00                                   # floor(xPos) < 0: skip
  jl _X3D.DualAMCYCeilX                          #
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Floor,Ceil)
  mov BYTE PTR [esp+0x12],al                     #
_X3D.DualAMCYCeilX:                              # - - - - - - - - - - - - - - -
  inc edi                                        #
  cmp edi,DWORD PTR [ebp+0x00]                   # ceil(xPos) >= width: skip
  jnl _X3D.DualAMCalcPixel                       #
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Ceil,Ceil)
  mov BYTE PTR [esp+0x13],al                     #
_X3D.DualAMCalcPixel:                            # - - - - - - - - - - - - - - -
  movzx eax,BYTE PTR [esp+0x00]                  # xSubPixel (invert for floor)
  not al                                         # 
  mov ah,BYTE PTR [esp+0x04]                     # ySubPixel (invert for floor)
  not ah                                         #
  mul ah                                         # Fetch multiplier
  movzx edx,BYTE PTR [esp+0x10]                  # pixelFXFY
  mul edx                                        # pixelAdj in eax
  mov edi,eax                                    # Output Storage
  movzx eax,BYTE PTR [esp+0x04]                  # ySubPixel (invert for floor)
  not al                                         #
  mul BYTE PTR [esp+0x00]                        # No inversion on x needed, so skip some steps
  movzx edx,BYTE PTR [esp+0x11]                  # pixelCXFY
  mul edx                                        # 
  add edi,eax                                    # Add to output storage
  movzx eax,BYTE PTR [esp+0x00]                  # xSubPixel (invert for floor)
  not al                                         #
  mul BYTE PTR [esp+0x04]                        # No inversion on y needed, so skip some steps
  movzx edx,BYTE PTR [esp+0x12]                  # pixelFXCY
  mul edx                                        # 
  add edi,eax                                    # Add to output storage
  movzx eax,BYTE PTR [esp+0x00]                  # xSubPixel (no inversion for ceil)
  mul BYTE PTR [esp+0x04]                        # No inversion on y needed, so skip some steps
  movzx edx,BYTE PTR [esp+0x13]                  # pixelCXCY
  mul edx                                        # 
  add eax,edi                                    # This is the true output
  shr eax,0x10                                   # Account for *65536 multiplication
  jmp _X3D.WritePixelAM                          #
_X3D.HorizAM:                                    # --------------------------------
  cmp BYTE PTR [esp],0x00                        # Horizontal adjustment needed?
  je _X3D.NaiveAM                                # Naive
  mov edi,DWORD PTR [esp]                        # Preparatory rites (since edx doesn't have a subpixel, it's fine as-is)
  sar edi,0x08                                   # 
  add edi,DWORD PTR [ebp+0x08]                   # xCenterpoint Correction
_X3D.HorizAMFloorX:                              # - - - - - - - - - - - - - - -
  jl _X3D.HorizAMCeilX                           # floor(xPos) < 0: skip (above add)
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Floor)
  mov BYTE PTR [esp+0x10],al                     #
_X3D.HorizAMCeilX:                               # - - - - - - - - - - - - - - -
  inc edi                                        #
  cmp edi,DWORD PTR [ebp+0x00]                   # ceil(xPos) >= width: skip
  jnl _X3D.SingleAMCalcPixel                     #
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Ceil)
  mov BYTE PTR [esp+0x11],al                     #
  jmp _X3D.SingleAMCalcPixel                     # Conglomerate
_X3D.VertAM:                                     # --------------------------------
  mov edi,DWORD PTR [esp]                        # Preparatory rites
  sar edi,0x08                                   # 
  add edi,DWORD PTR [ebp+0x08]                   # xCenterpoint Correction
  mov edx,DWORD PTR [esp+0x04]                   # Explicit Floor Request
  mov DWORD PTR [esp],edx                        # Code compression
  sar edx,0x08                                   # 
  add edx,DWORD PTR [ebp+0x0c]                   # yCenterpoint Correction
_X3D.VertAMFloorY:                               # - - - - - - - - - - - - - - -
  jl _X3D.VertAMCeilY                            # floor(yPos) < 0: skip (etc.)
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Floor)
  mov BYTE PTR [esp+0x10],al                     #
_X3D.VertAMCeilY:                                # - - - - - - - - - - - - - - -
  inc edx                                        #
  cmp edx,DWORD PTR [ebp+0x04]                   # ceil(yPos) >= height: skip
  jnl _X3D.SingleAMCalcPixel                     #
  mov eax,DWORD PTR [esi+edx*4]                  # Prepare
  add eax,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edi*1]                    # Pixel source (Ceil)
  mov BYTE PTR [esp+0x11],al                     #
_X3D.SingleAMCalcPixel:                          # - - - - - - - - - - - - - - -
  movzx eax,BYTE PTR [esp+0x00]                  # SubPixel (invert for floor)
  not al                                         # 
  mul BYTE PTR [esp+0x10]                        # pixelF
  mov edx,eax                                    # TMP storage
  movzx eax,BYTE PTR [esp+0x00]                  # SubPixel (no inversion for ceil)
  mul BYTE PTR [esp+0x11]                        # pixelC
  add eax,edx                                    # This is the true output
  shr eax,0x08                                   # Account for *256 multiplication
  jmp _X3D.WritePixelAM                          #
_X3D.NaiveAM:                                    # --------------------------------
  mov eax,DWORD PTR [esp]                        # xPos
  sar eax,0x08                                   # Correction
  add eax,DWORD PTR [ebp+0x08]                   # (yPos was obtained previously)
  mov edx,DWORD PTR [esi+edx*4]                  # Prepare
  add edx,DWORD PTR [ebp+0x24]                   # SpriteData addition
  mov al,BYTE PTR [eax+edx*1]                    # Pixel source
_X3D.WritePixelAM:                               # --------------------------------
  mov ebx,DWORD PTR [esp+ebx*4+0x74]             # Fetch the appropriate dest buffer
  mov edx,DWORD PTR [esp+0x44]                   # Fetch iterating y value
  mov edi,DWORD PTR [ebx+0x2c]                   # Fetch dest buffer widths
  mov edx,DWORD PTR [edi+edx*4]                  #
  add edx,DWORD PTR [ebx+0x24]                   #
  mov BYTE PTR [edx+ecx*1],al                    # Write pixel
  fld DWORD PTR [esp+0x08]                       # Restore from backup {xSourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  fld DWORD PTR [esp+0x0c]                       # {ySourcePos, xSourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  jmp _X3D.SkipPixelAM                           # Avoid the sync test
_X3D.SkipPixelAMx:                               # --------------------------------
  fxch                                           # Sync
_X3D.SkipPixelAM:                                # --------------------------------
  inc ecx                                        # Skim horizontally
  cmp ecx,DWORD PTR [ebx]                        # Continue until the end of the column is reached (both dests ARE identical, right?)
  jnl _X3D.NextRowAM                             #
  fadd DWORD PTR [esp+0x58]                      # yPos+sin(angle) {ySourcePos', xSourcePos, xSourcePosRef, ySourcePosRef, BOUNDS}
  fxch                                           # {xSourcePos, ySourcePos', xSourcePosRef, ySourcePosRef, BOUNDS}
  fadd DWORD PTR [esp+0x5c]                      # xPos+cos(angle) {xSourcePos', ySourcePos', xSourcePosRef, ySourcePosRef, BOUNDS}
  jmp _X3D.PixelLoopAM                           #
_X3D.NextRowAM:                                  # --------------------------------
  mov edi,DWORD PTR [esp+0x44]                   # Increment iterating y value
  inc edi                                        #
  cmp edi,DWORD PTR [ebx+0x04]                   # Continue until the bottom row of the buffer is reached
  jnl _X3D.Done                                  #
  fstp st                                        # Clear {ySourcePos', xSourcePosRef, ySourcePosRef, BOUNDS}
  fstp st                                        # {xSourcePosRef, ySourcePosRef, BOUNDS}
  fxch                                           # Increment yPos {ySourcePosRef, xSourcePosRef, BOUNDS}
  mov DWORD PTR [esp+0x44],edi                   # Actually increment iterating y value
  fadd DWORD PTR [esp+0x5c]                      # {ySourcePosRef', xSourcePosRef, BOUNDS}
  fxch                                           # Decrement xPos {xSourcePosRef, ySourcePosRef', BOUNDS}
  fsub DWORD PTR [esp+0x58]                      # {xSourcePosRef', ySourcePosRef', BOUNDS}
  fld st(1)                                      # Reboot {ySourcePos', xSourcePosRef', ySourcePosRef', BOUNDS}
  fld st(1)                                      # Reboot {xSourcePos', ySourcePos', xSourcePosRef', ySourcePosRef', BOUNDS}
  xor ecx,ecx                                    # Reset iterating x value
  jmp _X3D.PixelLoopAM                           # Resume rotation calcs
_X3D.Done:                                       # =======================================
  sub esp,0xffffff80                             # Revert stack to its original state
  finit                                          # Reset FPU
_X3D.No:                                         # --------------------------------
  ret                                            #
  
_X3D.SprFetch:                                   # =======================================
  push esi                                       # We'll need this to be restored later
  push edi                                       # 
  mov ecx,DWORD PTR [ebx+0x28]                   # 
  mov esi,DWORD PTR [ebx+0x18]                   # 
_X3D.SprFetch_Backstep:                          # ---------------------------------------
  mov di,WORD PTR [esi+0x08]                     # Store attempts at identifying group
  cmp di,WORD PTR [esp+0x0c]                     # 
  jne _X3D.SprFetch_NotIt                        # 
  mov di,WORD PTR [esi+0x0c]                     # Store attempts at identifying index
  cmp di,WORD PTR [esp+0x0e]                     # 
  jne _X3D.SprFetch_NotIt                        # 
  mov ecx,DWORD PTR [esi+0x04]                   # Fetch the actual ID of the sprite (we may also sub by 0x28)
  mov edi,DWORD PTR [ebx+0x14]                   # 
  imul ecx,DWORD PTR [ebx+0x04]                  # Multiply by 52/Length to obtain the true offset
  add ecx,edi                                    # And use this to adjust
  jmp _X3D.SprFetch_Finalization                 # 
_X3D.SprFetch_NotIt:                             # ---------------------------------------
  sub ecx,0x01                                   # Well, just move to the next one then
  lea esi,[esi+0x10]                             # 
  jnl _X3D.SprFetch_Backstep                     # 
  xor ecx,ecx                                    # Set to 0 as an error avoidance measure
_X3D.SprFetch_Finalization:                      # ---------------------------------------
  pop edi                                        # Things should return to normal
  pop esi                                        # 
  mov DWORD PTR [esp+0x04],ecx                   # Store the address of this sprite as the return value
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.Pseudomask (ID = 63)
--------------------------------------------
I tried the variable method for this and I cried, this is the only other solution
Param0: Sprite ID to use as a source for copying
Param1: Sprite ID to use as a dest for copying
Param2: FadeOut Pixel Start
Param3: FadeIn Pixel Start
Param4: Flags
    &1: LoopOK
    &2: 2xScale Flag (for things like the cutin)
    &4: CopyOnly
don't be dumb with the buffers or you die :S
*/
asm(R"(.intel_syntax noprefix
_X3F:                                            # ========================================
  mov edi,DWORD PTR [ebx+0x000013ac]             # Sprite data
  mov ebx,DWORD PTR [edi]                        # Inner Sprite Data
  push DWORD PTR [edx]                           # Fetch Source
  call _X3F.SprFetch                             # 
  lea esp,[esp+0x04]                             # yeaaa
  jz _X3F.No                                     # How does it not exist I ADDED IT
  mov esi,DWORD PTR [ecx+0x24]                   # Sprite data goes here
  push DWORD PTR [edx+0x04]                      # Fetch Dest
  call _X3F.SprFetch                             # 
  lea esp,[esp+0x04]                             # yeaaa
  jz _X3F.No                                     # :////////////
  mov ebp,edx                                    # Yea let's just store this now?
  mov edi,DWORD PTR [ecx+0x24]                   # Sprite data goes here
  push DWORD PTR [ecx+0x04]                      # Let's not be stupid, keep this for later
  mov edx,DWORD PTR [ecx]                        # I feel that this is just a good idea
  mov ebx,DWORD PTR [ecx+0x20]                   # Need to adjust later
  mov ecx,ebx                                    # Store for rep
  rep movsb                                      # weeeeeee
  test DWORD PTR [ebp+0x10],0x04                 # we're just passing through
  jnz _X3F.Donezo                                # 
  sub edi,ebx                                    # (esi isn't needed for address purposes)
  mov esi,DWORD PTR [ebp+0x08]                   # Fetch FadeOut pixel
  test DWORD PTR [ebp+0x10],0x02                 # If not x2 scale, set flag (will be adjusted to 0x0a if not x2, 0x05 if x2)
  setz cl                                        # 
  lea ecx,[ecx+ecx*4+0x05]                       # Iteration adjustment to be stored in cl
  mov ch,0xfa                                    # Store 250 - cl in ch as a starting point
  sub ch,cl                                      # 
  jmp _X3F.TimeForDieKai                         # Skip overhead
_X3F.Overhead:                                   # ========================================
  add esi,0x01                                   # Increment
  bswap ecx                                      # Invert to fetch flag
  cmp esi,edx                                    # We've achieved width!
  jl _X3F.NotYetTho                              # 
  test DWORD PTR [ebp+0x10],0x01                 # If we don't loop, exit prematurely
  jz _X3F.Donezo                                 # 
  xor esi,esi                                    # Back to 0 you go then
_X3F.NotYetTho:                                  # ----------------------------------------
  cmp cl,0x01                                    # Switch case lite
  bswap ecx                                      # 
  jg _X3F.FadeIn                                 # cl = 0x02
  je _X3F.DoNothing                              # cl = 0x01
_X3F.FadeOut:                                    # ----------------------------------------
  sub ch,cl                                      # Decrement opacity
  jnz _X3F.TimeForDieKai                         # Coctinue
  add ecx,0x01000000                             # DoNothing Step reached
_X3F.DoNothing:                                  # ----------------------------------------
  cmp esi,DWORD PTR [ebp+0x0c]                   # Wait until this pixel is reached
  jne _X3F.TimeForDieKai                         # Coctinue
  add ecx,0x01000000                             # FadeIn Step reached
_X3F.FadeIn:                                     # ----------------------------------------
  add ch,cl                                      # Increment opacity
  cmp ch,0xfa                                    # Return to sender
  jae _X3F.Donezo                                # 
_X3F.TimeForDieKai:                              # ========================================
  mov ebx,DWORD PTR [esp]                        # Fetch y width
  push esi                                       # I don't really think there's any better way to do this
_X3F.Columns:                                    # --------------------------------
  movzx ax,BYTE PTR [edi+esi*1]                  # Alright now we do some multipliers
  mul ch                                         # hahahaha it spells mulch xD
  mov BYTE PTR [edi+esi*1],ah                    # I'm losing it here
  sub ebx,0x01                                   # Iterate until Done
  lea esi,[esi+edx*1]                            # (I knew it was a good idea)
  jg _X3F.Columns                                # 
  pop esi                                        # Still don't think there's any better way
  jmp _X3F.Overhead                              # Everything in terms of incrementation/loop cases is handled here instead
_X3F.Donezo:                                     # --------------------------------
  add esp,0x04                                   # We used like 1 push I think
_X3F.No:                                         # --------------------------------
  ret                                            # Return as we deem necessary
_X3F.SprFetch:                                   # =======================================
  push esi                                       # We'll need this to be restored later
  push edi                                       # 
  mov ecx,DWORD PTR [ebx+0x28]                   # 
  mov esi,DWORD PTR [ebx+0x18]                   # 
_X3F.Backstep:                                   # ---------------------------------------
  mov di,WORD PTR [esi+0x08]                     # Store attempts at identifying group
  cmp di,WORD PTR [esp+0x0c]                     # 
  jne _X3F.NotIt                                 # 
  mov di,WORD PTR [esi+0x0c]                     # Store attempts at identifying index
  cmp di,WORD PTR [esp+0x0e]                     # 
  jne _X3F.NotIt                                 # 
  mov ecx,DWORD PTR [esi+0x04]                   # Fetch the actual ID of the sprite (we may also sub by 0x28)
  mov edi,DWORD PTR [ebx+0x14]                   # 
  imul ecx,0x34                                  # Multiply by 52 to obtain the true offset
  add ecx,edi                                    # And use this to adjust
  jmp _X3F.Finalization                          # 
_X3F.NotIt:                                      # ---------------------------------------
  sub ecx,0x01                                   # Well, just move to the next one then
  lea esi,[esi+0x10]                             # 
  jnl _X3F.Backstep                              # 
  not ecx                                        # Set to 0 as an error avoidance measure
_X3F.Finalization:                               # ---------------------------------------
  pop edi                                        # Things should return to normal
  pop esi                                        # 
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.TriggerJmp (ID = 64)
--------------------------------------------
A pseudo-while loop that does all of the while looping almost automatically
Place within a trigger.
Param0: TriggerX[i]
Param1: Loop Condition (if TRUE, loop)
Protip: to pass 0:
    xvar(512) := # && 0 || xvar(513) := COND ^^ exec(64)
        to pass 1:
    xvar(512) := # && xvar(513) := COND || exec(64)
*/
asm(R"(.intel_syntax noprefix
_X40:                                            # ========================================
  cmp DWORD PTR [edx+0x04],0x00                  # If non-zero we should loop (so if the condition is false, it'll stop)
  jz _X40.YeaWeGood                              # 
  movzx eax,WORD PTR [edx]                       # Fetch Trigger WORDs
  movzx edx,WORD PTR [edx+0x02]                  # 
  mov DWORD PTR [esp+0x74],eax                   # Assign them as appropriate
  mov DWORD PTR [esp+0x78],edx                   # 
_X40.YeaWeGood:                                  # 
  ret                                            # 
  .balign 0x10, 0x00
)");

/*
----------------
.StConJmp (ID = 65)
--------------------------------------------
Another pseudo-while loop that does all of the while looping almost automatically
Place within a state controller's param.
Param0: StCon[i]
Protip: to pass 0:
    xvar(512) := # && 0 || xvar(513) := COND ^^ exec(64)
        to pass 1:
    xvar(512) := # && xvar(513) := COND || exec(64)

asm(R"(.intel_syntax noprefix
_X41:                                            # ========================================
  cmp DWORD PTR [edx+0x04],0x00                  # If non-zero we should loop (so if the condition is false, it'll stop)
  jz _X40.YeaWeGood                              # 
  movzx eax,WORD PTR [edx]                       # Fetch Trigger WORDs
  movzx edx,WORD PTR [edx+0x02]                  # 
  mov DWORD PTR [esp+0x74],eax                   # Assign them as appropriate
  mov DWORD PTR [esp+0x78],edx                   # 
_X41.YeaWeGood:                                  # 
  ret                                            # 
  .balign 0x10, 0x00
)");
*/


////////////////////////////////////////////////////////////////////////////////////////////////
// VARIABLE INFORMATION BLOCK
////////////////////////////////////////////////////////////////////////////////////////////////

/*
----------------
.VarSetup (ID = 128)
--------------------------------------------
Handles everything and anything related to variable information acquisition as a whole.
This includes the following:
  - GameTime Formula Acquisition
  - Life/Damage-Influenced Variable Acquisition
  - Invulnerability Flag Acquisition (not implemented, but planned)
  - StateNo/PrevStateNo/StateTime Variable Acquisition
This does NOT include Helper ID variables, even though the source code has support for it baked in.
  This functionality in particular is handled separately by the brain due to redirect processing

Param0 = Reference Address
Param1 = Player Data Storage (beatrix,xvar(500)+32768*P)
Param2 = The recorded life of the opponent, after processing
Param3 = The recorded change in life of the opponent, after processing
Param4 = Scraping Damage
Param5 = Misc. Flags
      &1: Scraping In Progress
      &2: Life Change Detected
      &4: Invuln Release Detected (last frame)
      &8: Invuln Grant Detected (last frame)
Param6 = Whether -3 was read this frame or not
*/
asm(R"(.intel_syntax noprefix
_X80:                                                     # ========================================
  mov    eax,0x00000000                                   # Check for initialization
  test   eax,eax                                          #
  jnz    _X80_HANDLER                                     #
  mov    DWORD PTR [ecx+0x01],ecx                         #
  sub    ecx,offset _X80                                  # Get absolute position
# lea    eax,[ecx+_DCONST_1]                              # -O3 specific adjustments
# mov    DWORD PTR [ecx+__varSetup_Problem0+0x21],eax     #
# mov    DWORD PTR [ecx+__varSetup_Problem1+0x21],eax     #
# mov    DWORD PTR [ecx+__varSetup_Problem2+0x16],eax     #
# lea    eax,[ecx+_DCONST_Neg1]                           #
# mov    DWORD PTR [ecx+__varSetup_Problem0+0x28],eax     #
# mov    DWORD PTR [ecx+__varSetup_Problem1+0x28],eax     #
# mov    DWORD PTR [ecx+__varSetup_Problem2+0x1d],eax     #
  add    DWORD PTR [ecx+_errorRounding+0x0b],ecx          # General adjustments
  add    DWORD PTR [ecx+_errorRounding+0x27],ecx          #
  add    DWORD PTR [ecx+_errorRounding+0x43],ecx          #
  add    DWORD PTR [ecx+_round+0x33],ecx                  #
  add    DWORD PTR [ecx+_round+0x41],ecx                  #
  add    DWORD PTR [ecx+_round_neg+0x26],ecx              #
  add    DWORD PTR [ecx+_round_neg+0x34],ecx              #
  add    DWORD PTR [ecx+_log2+0x17],ecx                   #
_X80_HANDLER:                                             # ---------------------------------------
  push   DWORD PTR [edx+0x0c]                             # Stack setup
  push   DWORD PTR [edx+0x08]                             #
  push   ebx                                              #
  push   DWORD PTR [edx+0x04]                             #
  push   DWORD PTR [edx+0x00]                             #
  finit                                                   #
  call   _varSetup                                        # Call C++ Function
  add    esp,0x14                                         # Repair stack
  finit                                                   #
  ret                                                     #
  .balign 0x10, 0x00
)");

/*
----------------
.VarTamper (ID = 129)
--------------------------------------------
Handles full-area variable tampering based on the protocols provided in tamperInfo.
Automatically handles self/parent discrimination so you don't have to worry about copying and pasting certain parts of it wrong...
Param0 = Tampering Protocol Information
Param1 = Variable Information Storage
Param2 = GameTime
Param3 = Tampering Flags Location
Param4 = Target,isParent
Param5 = Ignore Standard Vars (Non-CT, in case CT already set them)
Param6 = Tampering Execution Flag
Param7 = Restitution Execution Flag
*/
asm(R"(.intel_syntax noprefix
_X81:                                                     # ========================================
  mov    eax,0x00000000                                   # Check for initialization
  test   eax,eax                                          #
  jnz    _X81_HANDLER                                     #
  mov    DWORD PTR [ecx+0x01],ecx                         #
  sub    ecx,offset _X81                                  # Get absolute position
  lea    eax,[ecx+_X81_POINTERMAP]                        # We need to MANUALLY SPECIFY the jmp table
  mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Float_AdjAddRandom-0x04],eax      # -Os
  add    eax,0x14                                         # prev=5 cases
  mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Float_AdjAddRandVar-0x04],eax
  add    eax,0x18                                         # prev=6 cases
  mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Float_AddAdj-0x04],eax
  add    eax,0x18                                         # prev=6 cases
  mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Int_AdjAddRandom-0x04],eax
  add    eax,0x14                                         # prev=5 cases
  mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Int_AdjAddRandVar-0x04],eax
  add    eax,0x18                                         # prev=6 cases
  mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Int_AddAdj-0x04],eax
# mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Float_XorAdj-0x11],eax            # -O3
# add    eax,0x14                                         # prev=5 cases
# mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Float_AdjDivRandom-0x0d],eax
# add    eax,0x18                                         # prev=6 cases
# mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Float_AndAdj-0x0f],eax
# add    eax,0x18                                         # prev=6 cases
# mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Int_AdjVarSetup-0x0d],eax
# add    eax,0x14                                         # prev=5 cases
# mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Int_AdjAddRandVar-0x0d],eax
# add    eax,0x18                                         # prev=6 cases
# mov    DWORD PTR [ecx+__varTamper_TamperingLoop_Int_XorAdj-0x0d],eax
  mov    eax,0x00000021                                   #
_X81_PREHANDLER_Loop_POINTERMAP:                          # ---------------------------------------
  add    DWORD PTR [ecx+eax*4+_X81_POINTERMAP],ecx        #
  dec    eax                                              #
  jns    _X81_PREHANDLER_Loop_POINTERMAP                  #
_X81_HANDLER:                                             # ---------------------------------------
  push   DWORD PTR [edx+0x1c]                             # Stack Setup 
  push   DWORD PTR [edx+0x18]                             #
  push   DWORD PTR [edx+0x14]                             #
  push   DWORD PTR [edx+0x10]                             #
  push   DWORD PTR [edx+0x0c]                             #
  push   DWORD PTR [edx+0x08]                             #
  push   DWORD PTR [edx+0x04]                             #
  push   DWORD PTR [edx]                                  #
  push   ebx                                              #
  finit                                                   #
  call   _varTamper                                       # Call C++ Function
  add    esp,0x24                                         # Repair stack
  finit                                                   #
  ret                                                     #
_X81_POINTERMAP:                                          #
  .int __varTamper_TamperingLoop_Float_AdjAddRandom
  .int __varTamper_TamperingLoop_Float_AdjMulRandom
  .int __varTamper_TamperingLoop_Float_AdjDivRandom
  .int __varTamper_TamperingLoop_Float_AdjVarSetup-0x02
  .int __varTamper_TamperingLoop_Float_AdjRootVarSetup
  .int __varTamper_TamperingLoop_Float_AdjAddRandVar
  .int __varTamper_TamperingLoop_Float_AdjAddRandFVar
  .int __varTamper_TamperingLoop_Float_AdjAddRandSysVar
  .int __varTamper_TamperingLoop_Float_AdjAddRandSysFVar
  .int __varTamper_TamperingLoop_Float_AdjAddRandAnyVar
  .int __varTamper_TamperingLoop_Float_AdjAddSyncVar
  .int __varTamper_TamperingLoop_Float_AddAdj
  .int __varTamper_TamperingLoop_Float_MulAdj
  .int __varTamper_TamperingLoop_Float_DivAdj
  .int __varTamper_TamperingLoop_Float_OrAdj
  .int __varTamper_TamperingLoop_Float_AndAdj
  .int __varTamper_TamperingLoop_Float_XorAdj
  .int __varTamper_TamperingLoop_Int_AdjAddRandom
  .int __varTamper_TamperingLoop_Int_AdjMulRandom
  .int __varTamper_TamperingLoop_Int_AdjDivRandom
  .int __varTamper_TamperingLoop_Int_AdjVarSetup-0x03
  .int __varTamper_TamperingLoop_Int_AdjRootVarSetup
  .int __varTamper_TamperingLoop_Int_AdjAddRandVar
  .int __varTamper_TamperingLoop_Int_AdjAddRandFVar
  .int __varTamper_TamperingLoop_Int_AdjAddRandSysVar
  .int __varTamper_TamperingLoop_Int_AdjAddRandSysFVar
  .int __varTamper_TamperingLoop_Int_AdjAddRandAnyVar
  .int __varTamper_TamperingLoop_Int_AdjAddSyncVar
  .int __varTamper_TamperingLoop_Int_AddAdj
  .int __varTamper_TamperingLoop_Int_MulAdj
  .int __varTamper_TamperingLoop_Int_DivAdj
  .int __varTamper_TamperingLoop_Int_OrAdj
  .int __varTamper_TamperingLoop_Int_AndAdj
  .int __varTamper_TamperingLoop_Int_XorAdj
  .balign 0x10, 0x00
)");

/*
----------------
.FormulaFetch (ID = 130)
--------------------------------------------
An interface to the formula fetching functions of .VarSetup that simply returns its output
Please note that this always will return an int even if you're using a float input
Param0 = Target
Param1 = Data Storage
Param2 = GameTime
Param3 = Variable ID
*/
asm(R"(.intel_syntax noprefix
_X82:                                                     # ========================================
  finit                                                   # Usual Precaution
  push   0x00000000                                       # int dummySub=0
  push   DWORD PTR [edx+0x08]                             # int gt=Param2
  mov    eax,DWORD PTR [edx+0x04]                         #
  push   eax                                              # int* dataStorage=Param1
  mov    ecx,DWORD PTR [edx+0x0c]                         # Get the variable to adjust to
  mov    ebx,ecx                                          #
  shl    ebx,0x07                                         #
  lea    eax,[eax+ebx*1+0x00004000]                       # Adjust to formInfo
  push   eax                                              # int* formula=Param3FormInfo
  cmp    ecx,0x3c                                         # If var, process int
  push   DWORD PTR [edx]                                  # int* target=Param0
  jb     _X82_ProcessInt                                  #
  cmp    ecx,0x64                                         # If fvar, process float
  jb     _X82_ProcessFloat                                #
  cmp    ecx,0x69                                         # If sysvar, process int; otherwise, process float
  jb     _X82_ProcessInt                                  #
_X82_ProcessFloat:                                        # ---------------------------------------
  call   _assembleFormulaF                                # Call C++ Function
  fstp   DWORD PTR [esp+0x78]                             # Returns float, store as an int
  jmp    _X82_Return                                      #
_X82_ProcessInt:                                          # ---------------------------------------
  call   _assembleFormulaI                                # Call C++ Function
  mov    DWORD PTR [esp+0x78],eax                         # Returns int
_X82_Return:                                              # ---------------------------------------
  add    esp,0x14                                         # Repair stack
  finit                                                   #
  ret                                                     #
  .balign 0x10, 0x00
)");

/*
----------------
.NormalcyPenetration (ID = 131)
--------------------------------------------
Through specifying certain params, will arbitrarily select and penetrate the formulae obtained with .VarSetup.

Param0 = Reference Address
      Note that the reference's variables will be written to.
      State/Prev will also be written temporarily, but will be restored after
Param1 = Player Data Storage (beatrix,xvar(500)+32768*P)
Param2 = Remote Substitution Source
Param3 = Reference Formula Storage
      Note that this is independent of Param1.
Param4 = Fallback Formula Storage
      If a formula doesn't exist in Param3, but DOES exist in Param4, call it
Param5 = Penetration Flags
      &15: GameTime Defense Penetration Methodology
         =01: Standard
         =02: c*gametime+d => c+gametime+d (Oniwarud 3rd)
         =03: c*gametime+d => c*gametime*d (Dream Eater...?)
      &16: State Variable Tampering
      &32: PrevState Variable Tampering
      &64: Time Variable Tampering
      &128: Root Reference
      &768: Unknown Float Formula Penetration Methodology
         =256: Substitute Float with QNaN (too many edits)
         =512: Substitute Float with Infinity (Blocking-Heart)
         =768: Substitute Float with -Infinity
      &3072: Unknown Formula Penetration Methodology
         =1024: Substitute with SourceVar (-3 Infiltration, MT=A Helper > Body Substitution, etc.)
      &12288: Life Variable Tampering Methodology
         =04096: Exact
         =08192: AddToCurr
         =12288: AddToInit
      < 0: target,isParent (Crosstalk>Helper, BodyHelper>Root)
int flags, int gametime, int stateno, int prevstateno, int statetime, int life, int damage
Param6 = GameTime (GameTime Formula)
Param7 = Desired StateNo (GameTime Formula/StateNo Variables)
Param8 = Desired PrevStateNo (GameTime Formula/PrevStateNo Variables)
Param9 = Desired StateTime (Time Variables)
Param10 = Desired Life
Param11 = Desired Damage
*/
asm(R"(.intel_syntax noprefix
_X83:                                                     # ========================================
  push   DWORD PTR [edx+0x2c]                             # Setup Stack
  push   DWORD PTR [edx+0x28]                             #
  push   DWORD PTR [edx+0x24]                             #
  push   DWORD PTR [edx+0x20]                             #
  push   DWORD PTR [edx+0x1c]                             #
  push   DWORD PTR [edx+0x18]                             #
  push   DWORD PTR [edx+0x14]                             #
  push   DWORD PTR [edx+0x10]                             #
  push   DWORD PTR [edx+0x0c]                             #
  push   DWORD PTR [edx+0x08]                             #
  push   DWORD PTR [edx+0x04]                             #
  push   DWORD PTR [edx]                                  #
  call   _normalcyPenetration                             # Call C++ Function
  add    esp,0x30                                         # Repair Stack
  ret                                                     #
  .balign 0x10, 0x00
)");

extern "C" float bindToFloat (int* target, int* formulaAdj, int* dataStorage, int dummySub) {
    float* dataStorageF = (float*)(dataStorage); float* formulaAdjF = (float*)(formulaAdj);
    dword info = formulaAdj[12]; dword suppl = formulaAdj[4]; float outF = formulaAdjF[0];
    if (!(info&0x00000003)) { return outF; } //No operation
    if (info&0x00000010) { //isFloat
        float sF;
        if (info&0x00000008) { //Offset Flag
            sF = (dummySub ? *(float*)(dummySub) : (suppl&0x007f0000 ? //Only used in the context of stateno/etc. [variables use raw references]
                dataStorageF[32*((suppl&0x007f0000)>>16)+26] : 
                *(float*)((int)(suppl&0x00008000 ? *(int**)(target+2441) : (suppl&0x00004000 ? *(int**)(target+2440) : target)) + (suppl&0x00003fff))
            ));
            sF *= ((int)(suppl)<0 ? -1 : 1);
        } else {
            sF = *(float*)(&suppl);
        }
        switch (info&0x00000003) {
            case 0x03:
                if (sF == 0) { //Division by 0 results in 0 (even though it should never happen)
                    outF = 0;
                } else {
                    outF /= sF;
                }
                break;
            case 0x02:
                outF *= sF;
                break;
            default:
                outF += sF;
        }
    } else {
        int sI;
        if (info&0x00000008) { //Offset Flag
            sI = (dummySub ? *(int*)(dummySub) : (suppl&0x007f0000 ? //Only used in the context of stateno/etc. [variables use raw references]
                dataStorage[32*((suppl&0x007f0000)>>16)+26] : 
                *(int*)((int)(suppl&0x00008000 ? *(int**)(target+2441) : (suppl&0x00004000 ? *(int**)(target+2440) : target)) + (suppl&0x00003fff))
            ));
            sI *= ((int)(suppl)<0 ? -1 : 1);
        } else {
            sI = suppl;
        }
        switch (info&0x00000003) {
            case 0x03:
                if (sI == 0) { //Division by 0 results in 0 (even though it should never happen)
                    outF = 0;
                } else {
                    outF /= sI;
                }
                break;
            case 0x02:
                outF *= sI;
                break;
            default:
                outF += sI;
        }
    }
    return outF;
}
extern "C" int bindToInt (int* target, int* formulaAdj, int* dataStorage, int dummySub) {
    float* dataStorageF = (float*)(dataStorage);
    dword info = formulaAdj[12]; dword suppl = formulaAdj[4]; int outI = formulaAdj[0];
    int sI;
    if (!(info&0x0000003)) { return outI; } //No operation
    if (info&0x00000010) { //isFloat
        float sF; 
        if (info&0x00000008) { //Offset Flag
            sF = (dummySub ? *(float*)(dummySub) : (suppl&0x007f0000 ? //Only used in the context of stateno/etc. [variables use raw references]
                dataStorageF[32*((suppl&0x007f0000)>>16)+26] : 
                *(float*)((int)(suppl&0x00008000 ? *(int**)(target+2441) : (suppl&0x00004000 ? *(int**)(target+2440) : target)) + (suppl&0x00003fff))
            ));
            sF *= ((int)(suppl)<0 ? -1 : 1);
        } else {
            sF = *(float*)(&suppl);
        }
        sI = (int)(info&0x00000020 ? ceil(sF) : floor(sF)); //Typecast
    } else {
        if (info&0x00000008) { //Offset Flag
            sI = (dummySub ? *(int*)(dummySub) : (suppl&0x007f0000 ? //Only used in the context of stateno/etc. [variables use raw references]
                dataStorage[32*((suppl&0x007f0000)>>16)+26] : 
                *(int*)((int)(suppl&0x00008000 ? *(int**)(target+2441) : (suppl&0x00004000 ? *(int**)(target+2440) : target)) + (suppl&0x00003fff))
            ));
            sI *= ((int)(suppl)<0 ? -1 : 1);
        } else {
            sI = suppl;
        }
    }
    switch (info&0x00000003) { //Both guarantee that sI is a thing, so we can keep it outside the ifelse
        case 0x03:
            if (sI == 0) { //Division by 0 results in 0 (even though it should never happen)
                outI = 0;
            } else if (!(outI == -2147483648 && sI == -1)) { //Crash avoidance (even though it should never happen)
                outI /= sI;
            }
            break;
        case 0x02:
            outI *= sI;
            break;
        default:
            outI += sI;
    }
    return outI;
}
//For the below, if you wish to pass a formula that can be modified freely, it may be in your best interest to pass a 16-long array with the expected format.
//{aX, bX, cX, dX, aSuppl, bSuppl, cSuppl, dSuppl, am, bm, cm, dm, aInfo, bInfo, cInfo, dInfo}
//formula != formStorage+32*v
extern "C" float assembleFormulaF (int* target, int* formula, int* dataStorage, int gt, int dummySub=0) {
    int gtS = gt*gt; int gtC = gt*gt*gt; int gtinv = 1-2*(gt&1); //gametimes
    int FPU; //Precaution
    asm(".intel_syntax noprefix\n"       //Set FPU to 32-bit mode
        "fnstcw WORD PTR [eax];"
        "fnstcw WORD PTR [eax+0x02];"
        "and BYTE PTR [eax+0x01],0xfc;"
        "fldcw WORD PTR [eax];"
        :
        : "a"(&FPU)
    );
    int aI = 0; float& aF = (float&)(aI);
    int aIT = 0; float& aFT = (float&)(aIT); bool aFloat = false;
    if (formula[12] < 0 && formula[12]&0x00000004) {
        aFloat = true;
        aF = bindToFloat(target, formula, dataStorage, dummySub); //Modulus isn't supported, so it's left out for the time being
        switch (formula[12]&0x00000300) {
            case 0x00000000:
                aFT = aF*gtC;
                break;
            case 0x00000100:
                aFT = aF*gtS*gt;
                break;
            case 0x00000200:
                aFT = aF*gt*gtS;
                break;
            case 0x00000300:
                aFT = aF*gt*gt*gt;
                break;
        }
    } else if (formula[12] < 0) {
        aI = bindToInt(target, formula, dataStorage, dummySub);
        aIT = aI*gtC;
    }
    int bI = 0; float& bF = (float&)(bI);
    int bIT = 0; float& bFT = (float&)(bIT); bool bFloat = false;
    if (formula[13] < 0 && formula[13]&0x00000004) {
        bFloat = true;
        bF = bindToFloat(target, formula+1, dataStorage, dummySub);
        if (formula[13]&0x00000080) { bF *= gtinv; } //Inversion supported
        switch (formula[13]&0x00000100) {
            case 0x00000000:
                bFT = bF*gtS;
                break;
            case 0x00000100:
                bFT = bF*gt*gt;
                break;
        }
    } else if (formula[13] < 0) {
        bI = bindToInt(target, formula+1, dataStorage, dummySub);
        if (formula[13]&0x00000080) { bI *= gtinv; } //Inversion supported
        bIT = bI*gtS;
    }
    int cI = 0; float& cF = (float&)(cI);
    int cIT = 0; float& cFT = (float&)(cIT); bool cFloat = false;
    if (true && formula[14]&0x00000004) {
        cFloat = true;
        cF = bindToFloat(target, formula+2, dataStorage, dummySub);
        if (formula[14]&0x00000080) { cF *= gtinv; } //Inversion supported
        cFT = cF*((formula[10]>1||formula[10]<-1) ? gt%formula[10] : gt); //Modulus supported
    } else if (true) {
        cI = bindToInt(target, formula+2, dataStorage, dummySub);
        if (formula[14]&0x00000080) { cI *= gtinv; } //Inversion supported
        cIT = cI*((formula[10]>1||formula[10]<-1) ? gt%formula[10] : gt); //Modulus supported
    }
    int dI = 0; float& dF = (float&)(dI);
    int dIT = 0; float& dFT = (float&)(dIT); bool dFloat = false;
    if (true && formula[15]&0x00000004) {
        dFloat = true;
        dF = bindToFloat(target, formula+3, dataStorage, dummySub);
        if (formula[15]&0x00000080) { dF *= gtinv; } //Inversion supported
        dFT = dF + (formula[11]>1 ? gt%formula[11] : (formula[11]<-1 ? -(gt%-formula[11]) : 0)); //Modulus add/sub supported
    } else if (true) {
        dI = bindToInt(target, formula+3, dataStorage, dummySub);
        if (formula[15]&0x00000080) { dI *= gtinv; } //Inversion supported
        dIT = dI + (formula[11]>1 ? gt%formula[11] : (formula[11]<-1 ? -(gt%-formula[11]) : 0)); //Modulus add/sub supported
    }
    //Sum is more complicated as float is strange
    int out = 0; float& outF = (float&)(out); bool outFloat = false;
    int params[4] = {aIT,bIT,cIT,dIT}; float* paramsF = (float*)(params);
    bool paramLogic[4] = {aFloat,bFloat,cFloat,dFloat};
    { //Local scope
        char i = 0; char p = 0;
        do {
            if (formula[12+i] < 0 && (formula[12+i]&0x00001800)>>11 == p) { //Found the correct priority, add
                if (outFloat) { //if it's already float, add directly to the float value
                    outF += (paramLogic[i] ? paramsF[i] : params[i]);
                } else if (paramLogic[i]) { //if the param is a float, convert to float
                    outF = out + paramsF[i];
                    outFloat = true;
                } else { //int + int, so add directly to the int
                    out += params[i];
                }
                i = 0; p++;
            } else { //Otherwise skip
                i++;
            }
        } while (i < 4 && p < 4);
    }
    if (!outFloat) { outF = out; } //If this still isn't a float, it will be truncated as if it was one
    asm(".intel_syntax noprefix\n"       //Revert FPU to 80-bit mode
        "fldcw WORD PTR [edx+0x02];"
        :
        : "d"(&FPU)
    );
    return outF;
}
extern "C" int assembleFormulaI (int* target, int* formula, int* dataStorage, int gt, int dummySub=0) {
    int gtS = gt*gt; int gtC = gt*gt*gt; int gtinv = 1-2*(gt&1); //gametimes
    int FPU; //Precaution
    asm(".intel_syntax noprefix\n"       //Set FPU to 32-bit mode
        "fnstcw WORD PTR [eax];"
        "fnstcw WORD PTR [eax+0x02];"
        "and BYTE PTR [eax+0x01],0xfc;"
        "fldcw WORD PTR [eax];"
        :
        : "a"(&FPU)
    );
    int aI = 0; int aIT = 0;
    if (formula[12] < 0) {
        aI = bindToInt(target, formula, dataStorage, dummySub);
        aIT = aI*gtC;
    }
    int bI = 0; int bIT = 0;
    if (formula[13] < 0) {
        bI = bindToInt(target, formula+1, dataStorage, dummySub);
        if (formula[13]&0x00000080) { bI *= gtinv; } //Inversion supported
        bIT = bI*gtS;
    }
    int cI = 0; int cIT = 0;
    if (true) {
        cI = bindToInt(target, formula+2, dataStorage, dummySub);
        if (formula[14]&0x00000080) { cI *= gtinv; } //Inversion supported
        cIT = cI*((formula[10]>1||formula[10]<-1) ? gt%formula[10] : gt); //Modulus supported
    }
    int dI = 0; int dIT = 0;
    if (true) {
        dI = bindToInt(target, formula+3, dataStorage, dummySub);
        if (formula[15]&0x00000080) { dI *= gtinv; } //Inversion supported
        dIT = dI + (formula[11]>1 ? gt%formula[11] : (formula[11]<-1 ? -(gt%-formula[11]) : 0)); //Modulus add/sub supported
    }
    //Sum is simple because it's int
    int out = aIT+bIT+cIT+dIT;
    asm(".intel_syntax noprefix\n"       //Revert FPU to 80-bit mode
        "fldcw WORD PTR [edx+0x02];"
        :
        : "d"(&FPU)
    );
    return out;
}

/*void scrapingParser (int* scrapingInfo) {
    //Flags are interference flags
    asm("__scrapingParser:");
    //Double-tap prevention
    int* MUGEN = *(int**)(0x004b5b4c);
    int gametime = MUGEN[0x2cff];
    if (gametime == scrapingInfo[1023]) { return; } //DO NOT DOUBLE TAP
    //Things I'm going to use relatively frequently
    int* patternBlock = scrapingInfo; 
    int* supplBlock = scrapingInfo+4*1024; 
    char vr = 0;
    int flagsDVL = scrapingInfo[7*1024+1023]; //damageVar > lifeVar > lifeset
    int flagsVL = scrapingInfo[7*1024+1022]; //lifeVar > lifeset / damageVar > lifeVar
    int flagsL = scrapingInfo[7*1024+1021]; //lifeset / varset
    do {
        asm("__scrapingParser_Loop:");
        if (!(supplBlock[255]&~0x00000003)) { continue; } //Need info storage of some sort to properly handle it
        float* patternBlockF = (float*)(patternBlock); float* supplBlockF = (float*)(supplBlock);
        int* rawDataBlock = patternBlock+500; float* rawDataBlockF = (float*)(rawDataBlock);
        int* infoPointer = (int*)(supplBlock[255]&~0x00000003); float* infoPointerF = (float*)(infoPointer);
        int* dataPointer = infoPointer-4096; float* dataPointerF = (float*)(dataPointer);
        bool isFloat = supplBlock[255]&0x00000001;
        //To get the estimated life value, use slope*(final - value) [slope is inverted for life synchronization]
        //for instance, a variable that goes down from 9000 to 8000 should return 500 at 8500 (-(8000 - 8500))
        //a variable of 123000-life should return 500 if the value is 122500 (+(123000 - 122500))
        int dataCast; float& dataCastF = (float&)(dataCast);
        if (isFloat) {
            asm("__scrapingParser_Float:");
            if (rawDataBlock[40]) { //If at least 1 delta has been recorded, increment the delay timer
                patternBlock[1020] += 1;
            }
            float dataCast = (infoPointerF[1]-dataPointerF[20])/((vr != 0) != !!(supplBlock[0]&0x40000000) ? infoPointerF[8] : -infoPointerF[8]);
            //Life-related values cannot be 0; this also shouldn't be NaN
            if (!vr && !dataCast || dataCast != dataCast) { continue; }
            if (!patternBlock[1023]) { //Initialize
                asm("__scrapingParser_FloatInit:");
                rawDataBlockF[0] = dataCast;
                rawDataBlockF[10] = dataCast;
                patternBlock[1023] = gametime;
                continue;
            }
            asm("__scrapingParser_FloatValid:");
            float del = dataCast-rawDataBlockF[0]; //NOTE: we handle inversion up above as well; -del is essentially damage
            float dmg = -del;
            if (!del) { //Stasis processing
                asm("__scrapingParser_FloatStasis:");
                continue;
            }
            asm("__scrapingParser_FloatNonStasis:");
            if (dataCast < patternBlockF[1021]) { patternBlockF[1021] = dataCast; } //Global minima
            if (dataCast > patternBlockF[1022]) { patternBlockF[1022] = dataCast; } //Global maxima
            memcpyrev(rawDataBlock,rawDataBlock+1,36); //Feedtape
            memcpyrev(rawDataBlock+40,rawDataBlock+41,36);
            memcpyrev(rawDataBlock+80,rawDataBlock+81,36);
            memcpyrev(rawDataBlock+120,rawDataBlock+121,36);
            //Write data
            rawDataBlockF[0] = dataCast;
            rawDataBlockF[40] = del;
            rawDataBlock[80] = patternBlock[1020]; patternBlock[1020] = 0; //Reset delay timer
            //Write the appropriate set of flags
            if (!(supplBlock[254]&0x03)) { //Determine them empirically first
                char delayTime = 1;
                if (!vr && flagsDVL&~(flagsVL|flagsL) && !(~flagsDVL&(flagsVL|flagsL))) { //life special case: a bit exists in DVL that doesn't exist in either
                    delayTime = 3;
                } else if (flagsVL&~flagsL && !(~flagsVL&flagsL)) { //a bit exists in VL that doesn't exist in L
                    delayTime = 2;
                }
                supplBlock[254] |= delayTime;
            }
            int flagsW = ((supplBlock[254]&0x03) == 1 ? flagsL : ((supplBlock[254]&0x03) == 2 ? flagsVL : flagsDVL));
            int lifePattern = supplBlock[0];
            rawDataBlock[120] = flagsW;
            if (!(flagsW&0x0000ffff)) { //Non-Interference
                asm("__scrapingParser_FloatNonInterference:");
                memcpyrev(rawDataBlock+10,rawDataBlock+11,36); //Feedtape
                memcpyrev(rawDataBlock+50,rawDataBlock+51,36);
                memcpyrev(rawDataBlock+90,rawDataBlock+91,36);
                memcpyrev(rawDataBlock+130,rawDataBlock+131,36);
                //Write data
                rawDataBlockF[10] = dataCast;
                rawDataBlockF[50] = del;
                rawDataBlock[90] = rawDataBlock[80];
                rawDataBlock[130] = flagsW;
                //Mean and standard deviation
                float meanData = 0; float stdevData = 0;
                char dt = 0;
                do {
                    meanData += rawDataBlockF[10+dt];
                } while (dt < 9 && rawDataBlockF[50+dt] && ++dt); //If a delta existed here, it's a valid candidate; don't exceed max
                meanData /= dt+1;
                {
                    char d = 0;
                    do {
                        stdevData += (rawDataBlockF[d] - meanData)*(rawDataBlockF[d] - meanData);
                    } while (++d <= dt);
                    stdevData = sqrt(stdevData/dt);
                }
                supplBlockF[4] = meanData; supplBlockF[5] = stdevData;
                if (lifePattern&0x80000000) { continue; } //Discarded, terminate
                asm("__scrapingParser_FloatLifePatternParse:");
                //Determine the current pattern
                //If it swapped polarity twice, it's most likely RNG (note that each isolated instance of an outlier is 2 polarity shifts)
                char lifePatternCurr = 3;
                float initialData; char di;
                {
                    char d = 0; //Last index
                    char decCount = 0; char outlierCount = 0;
                    do { //Fetch dec/outlier count
                        decCount += (rawDataBlockF[50+d]<0);
                        outlierCount += ((rawDataBlockF[50+d]<0) != ((d!=dt-1?rawDataBlockF[51+d]:rawDataBlockF[50])<0));
                    } while (++d < dt); //Delta must exist
                    if (outlierCount < 4) { //Adjust
                        lifePatternCurr = 1 + (decCount<=dt/2);
                        do { //Fetch the initial value responsible for this delta (10+d is +1 because initial)
                            if ((rawDataBlockF[10+d]>0) == (lifePatternCurr==2)) { break; }
                        } while (--d > 0);
                    }
                    initialData = rawDataBlockF[10+d];
                    di = d;
                }
                if (dt == 9) { //9 deltas
                    //If we don't have a pattern currently defined, define this as the initial pattern
                    if (!(lifePattern&0x00000007)) {
                        lifePattern |= lifePatternCurr;
                    //If we do and it matches, keep it as such
                    } else if ((lifePattern&0x00000007) == lifePatternCurr) {
                        if (lifePattern&0x00000040) { //Clear the alt pattern flag, check for reversion (stored on significant differences)
                            if ((initialData < supplBlockF[36]*0.9 || initialData > supplBlockF[36]*1.1) &&
                                (!(lifePattern&0x00000080) || initialData < supplBlockF[37]*0.9 || initialData > supplBlockF[37]*1.1)
                            ) { //Store reversion info
                                supplBlockF[37] = initialData;
                                lifePattern |= 0x00000080;
                            }
                            lifePattern &= ~0x00000040;
                        }
                    //If it matches the alt pattern, keep it as the alt pattern
                    } else if ((lifePattern>>3&0x00000007) == lifePatternCurr) {
                        if (!(lifePattern&0x00000040)) { //Set the alt pattern flag, write the new shift
                            lifePattern |= 0x00000040;
                        }
                    //Otherwise, define it as the new alt pattern and clear reversion
                    } else {
                        //If there's a significant enough difference, update
                        if (!(lifePattern>>3&0x00000007) || (initialData < supplBlockF[36]*0.9 || initialData > supplBlockF[36]*1.1) &&
                            (!(lifePattern&0x00000080) || initialData < supplBlockF[37]*0.9 || initialData > supplBlockF[37]*1.1)
                        ) { //Store reversion info
                            supplBlockF[36] = initialData;
                        }
                        lifePattern = lifePattern&~(0x00000007<<3|0x00000080) | lifePatternCurr<<3|0x00000040;
                    }
                    supplBlock[0] = lifePattern;
                    //During random, if the standard deviation is greater than 100, DQ
                    //Alternatively, upon a sudden polarity shift, if the standard deviation suddenly jumps to 25% of the delta, DQ
                        //This is only possible if gametime%X or similar
                    if (lifePatternCurr == 3 && stdevData > 100 || lifePatternCurr != 3 && (lifePatternCurr==2)!=(del>0) && stdevData >= 0.25*abs(del)) {
                        supplBlock[0] |= 0x80000000;
                        continue;
                    }
                }
                //Supplemental information that could be useful to just have
                asm("__scrapingParser_FloatLifePatternSuppl:");
                float delMin = supplBlockF[24+2*!!(lifePattern&0x00000040)];
                float delMax = supplBlockF[25+2*!!(lifePattern&0x00000040)];
                if (lifePatternCurr != 3 && (del < delMin || del > delMax)) { //Passive Suspicion Raising
                    short sus = supplBlock[0]>>16;
                    short nonsus = (supplBlock[1]|supplBlock[2])>>16;
                    float delMod = del - (del > delMax ? delMax : delMin);
                    //GetHit Vampirism/Drainage
                    if (!(nonsus&0x0003) && rawDataBlock[130]>>23&0x03) {
                        if (delMod > 0) { //Vampirism
                            if (sus&0x0002) { //Contradiction
                                supplBlock[2] |= 0x0003<<16;
                            } else if (sus&0x0001) { //Confirmation
                                supplBlock[1] |= 0x0001<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0001<<16;
                            }
                        } else { //Drainage
                            if (sus&0x0001) { //Contradiction
                                supplBlock[2] |= 0x0003<<16;
                            } else if (sus&0x0002) { //Confirmation
                                supplBlock[1] |= 0x0002<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0002<<16;
                            }
                        }
                    }
                    //HitDefAttr Regeneration/Drainage
                    if (!(nonsus&0x000c) && rawDataBlock[130]&0x02000000) {
                        if (delMod > 0) { //Regeneration
                            if (sus&0x0008) { //Contradiction
                                supplBlock[2] |= 0x000c<<16;
                            } else if (sus&0x0004) { //Confirmation
                                supplBlock[1] |= 0x0004<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0004<<16;
                            }
                        } else { //Drainage
                            if (sus&0x0004) { //Contradiction
                                supplBlock[2] |= 0x000c<<16;
                            } else if (sus&0x0008) { //Confirmation
                                supplBlock[1] |= 0x0008<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0008<<16;
                            }
                        }
                    }
                    //MT=A Regeneration/Drainage
                    if (!(nonsus&0x0030) && (rawDataBlock[130]>>21&0x03) == 1) {
                        if (delMod > 0) { //Regeneration
                            if (sus&0x0020) { //Contradiction
                                supplBlock[2] |= 0x0030<<16;
                            } else if (sus&0x0010) { //Confirmation
                                supplBlock[1] |= 0x0010<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0010<<16;
                            }
                        } else { //Drainage
                            if (sus&0x0010) { //Contradiction
                                supplBlock[2] |= 0x0030<<16;
                            } else if (sus&0x0020) { //Confirmation
                                supplBlock[1] |= 0x0020<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0020<<16;
                            }
                        }
                    }
                    //MT=H Regeneration/Drainage
                    if (!(nonsus&0x00c0) && (rawDataBlock[130]>>21&0x03) == 2) {
                        if (delMod > 0) { //Regeneration
                            if (sus&0x0080) { //Contradiction
                                supplBlock[2] |= 0x00c0<<16;
                            } else if (sus&0x0040) { //Confirmation
                                supplBlock[1] |= 0x0040<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0040<<16;
                            }
                        } else { //Drainage
                            if (sus&0x0040) { //Contradiction
                                supplBlock[2] |= 0x00c0<<16;
                            } else if (sus&0x0080) { //Confirmation
                                supplBlock[1] |= 0x0080<<16;
                            } else { //Suspicion
                                supplBlock[0] |= 0x0080<<16;
                            }
                        }
                    }
                    //Piggyback Recovery
                    if (!(nonsus&0x0100) && (rawDataBlock[130]>>16&0x03) == 2) {
                        if (delMod <= 0) { //Contradiction
                            supplBlock[2] |= 0x0100<<16;
                        } else if (sus&0x0100) { //Confirmation
                            supplBlock[1] |= 0x0100<<16;
                        } else { //Suspicion
                            supplBlock[0] |= 0x0100<<16;
                        }
                    }
                    //LifeAdd Vampirism
                    if (!(nonsus&0x0200) && (rawDataBlock[130]>>16&0x03) == 1) {
                        if (delMod <= 0) { //Contradiction
                            supplBlock[2] |= 0x0200<<16;
                        } else if (sus&0x0200) { //Confirmation
                            supplBlock[1] |= 0x0200<<16;
                        } else { //Suspicion
                            supplBlock[0] |= 0x0200<<16;
                        }
                    }
                    //Damage Absorption cannot be proc'd here; check interference
                    //if (!(nonsus&0x0400) && 00000000) { }
                    //MoveReversed Vampirism/Drainage cannot be proc'd here; check interference
                    //if (!(nonsus&0x0800) && rawDataBlock[130]&0x00000800) { }
                }
            } else { //Potential Scraping

            }
        } else {
            asm("__scrapingParser_Int:");
            dataCast = (infoPointer[1]-dataPointer[20])/((vr != 0) != !!(supplBlock[333]&0x01000000) ? infoPointer[8] : -infoPointer[8]);
            //Life-related values cannot be 0
            if (!vr && !dataCast) { continue; }
            int del = dataCast-rawDataBlock[0];
            if (!del) { //Stasis processing
                continue;
            }
        }
    } while (patternBlock += 1024, supplBlock += 256, ++vr < 4);
    asm("__scrapingParser_End:");
    scrapingInfo[1023] = gametime; //Doubletap prevention
    return;
}*/

//THERE IS AN OOPSIE SOMEWHERE HERE THAT CAUSES FUNCTIONS TO RETURN TO INVALID LOCATIONS :(
//There's seemingly a hole that causes an errant return without an add esp,0x10 (or the offset is just wrong)
//I THINK it's fixed by adding the math error suppressor, though, but still, scrutinize if needed
extern "C" void varSetup (int* target, int* dataStorage, int* self, int flags, bool minusThree) { //exec(128)
    //Format of flags:
        //&1: Scraping Hit (this frame)
        //&2: Life Change Detected
        //&16: Invuln Release Detected
        //&32: Invuln Grant Detected
        //&256: Allow Tampered Variables
    asm("__varSetup:");
    //Double-tap prevention
    int* MUGEN = *(int**)(0x004b5b4c);
    int gametime = *(MUGEN+0x2cff);
    if (gametime == dataStorage[32*127+20]) { return; } //DO NOT DOUBLE TAP
    //Things I'm going to use relatively frequently
    float* dataStorageF = (float*)(dataStorage); //typecasts
    int* formStorage = dataStorage+4096; float* formStorageF = (float*)(formStorage);
    int* infoStorage = formStorage+4064; int* scrapingInfo = infoStorage-32; int* tamperedWith = infoStorage+12;
    bool isHelper = target[7]; //isHelper flag
    int* bea = *(int**)(self+2441);
    int* parent = *(int**)(target+2440); int* root = *(int**)(target+2441);
    int stateno = target[765]; int prevstateno = target[766]; int statetime = target[895];
    int lifemax = (isHelper ? root[89] : target[89]);
    int roundstate = *(MUGEN+0x2f0c);
    int RSTime = *(MUGEN+0x2f0f); //roundstate timer
    int matchTime = (roundstate<2||RSTime<=*(MUGEN+0x2685) ? 0 : RSTime-*(MUGEN+0x2685)); //in-game timer
    bool matchStarted = (matchTime > 0 && roundstate == 2);
    int* var = target+912; float* fvar = (float*)(var); //note that fvar isn't aligned with actual fvars
    //Flags
    int flagsO = infoStorage[11];
    bool scrapingHit = flags&1; bool scrapingHitO = flagsO&1;
    bool lifeChange = flags&2; bool lifeChangeO = flagsO&2;
    bool invulnDel = flags&16; bool invulnDelO = flagsO&16;
    bool invulnGrant = flags&32; bool invulnGrantO = flagsO&32;
    bool allowTamperScout = flags&256;
    bool firstRun = !dataStorage[32*127+22]; //gametime[0] == 0 means that nothing has been executed yet (or it was freshly reset)
    //Misc. Information (Initally stored outside of the loop in premonition; account for it)
    int life = dataStorage[32*113+25]; int lifeO = dataStorage[32*113+20];
    int dL = dataStorage[32*114+25]; int dLO = dataStorage[32*114+20];
    int beaLife = dataStorage[32*115+25]; int beaLifeO = dataStorage[32*115+20];
    int beaState = dataStorage[32*116+25]; int beaStateO = dataStorage[32*116+20];
    int damageS = dataStorage[32*117+25]; int damageSO = dataStorage[32*117+20];
    int fallDamageS = dataStorage[32*118+25]; int fallDamageSO = dataStorage[32*118+20];
    bool globDamaged = (damageS || fallDamageS);
    int damage = dataStorage[32*119+25]; int damageO = dataStorage[32*119+20];
    int fallDamage = dataStorage[32*120+25]; int fallDamageO = dataStorage[32*120+20]; 
    //GameTime information setup (for formula acquisition)
    int gt[5] = {dataStorage[32*127+3], dataStorage[32*127+2], dataStorage[32*127+1], dataStorage[32*127+0], gametime};
    int gtinv[5] = {1-2*(gt[0]&1), 1-2*(gt[1]&1), 1-2*(gt[2]&1), 1-2*(gt[3]&1), 1-2*(gametime&1)};
    int del32 = gt[3] - gt[2]; int del31 = gt[3] - gt[1]; int del21 = gt[2] - gt[1]; 
    int del30 = gt[3] - gt[0]; int del20 = gt[2] - gt[0]; int del10 = gt[1] - gt[0]; 
    //Quadratic convenience factors
    int gt4S = gt[4]*gt[4]; int gt3S = gt[3]*gt[3]; int gt0S = gt[0]*gt[0];
    int sum32 = gt[3] + gt[2];
    int delprod321 = del32*del31*del21;
    //Cubic convenience factors
    int gt4C = gt4S*gt[4]; int gt3C = gt3S*gt[3];
    int delprod210 = del21*del20*del10; int delprod310 = del31*del30*del10; int delprod320 = del32*del30*del20;
    int delprod3210 = delprod321*del30*del20*del10;
    int sum321 = sum32 + gt[1]; int prod32 = gt[2]*sum32 + gt3S;
    //Inversion convenience factors
    char invInd[2] = {-1,-1};
    char invNInd[3] = {-1,-1,-1};
    asm("__varSetup_GTInv_Acquire:");
    if (gt[2]) { //Only iterate if we can perform a checksum properly
        char i = 3; char j = 4; //i should be signed
        do {
            asm("__varSetup_GTInv_Loop:");
            if (gt[i] && (gt[i]^gt[j])&1) {
                invInd[0] = i; invInd[1] = j; //Unfortunately i can't assign with {} here
                invNInd[0] = (i<1)*(1+(j<2)); invNInd[1] = 1+(i<2)*(1+(j<3)); invNInd[2] = 2+(i<3)*(1+(j<4));
                //0,1
                //1*(1+1)=2, 1+1*(1+1)=3, 2+1*(1+1)=4
                //0,3
                //1*(1+0)=1, 1+1*(1+0)=2, 2+1*(1+1)=4
                //1,4
                //0*(1+0)=0, 1+1*(1+0)=2, 2+1*(1+0)=3
                break;
            }
            asm("__varSetup_GTInv_Next:");
            if (--i < 0) { i = (--j) - 1; }
        } while (j > 0);
    }
    char invIndQ[3] = {-1,-1,-1};
    char invNIndQ[2] = {-1,-1};
    int deli10Q = 0; int deli20Q = 0; int deli21Q = 0;
    int sumi21Q = 0; int sumi20Q = 0; int gti2SQ = 0;
    asm("__varSetup_GTInvQ_Acquire:");
    if (gt[1]) { //Only iterate if we can perform a checksum properly
        char i = 2; char j = 3; char k = 4; //should be signed
        do {
            asm("__varSetup_GTInvQ_Loop:");
            if (gt[i] && (gt[i]^gt[j])&1 && (gt[j]^gt[k])&1) { //It would be best to have a predictable pattern in this case
                invIndQ[0] = i; invIndQ[1] = j; invIndQ[2] = k; //Unfortunately i can't assign with {} here
                invNIndQ[0] = (i<1)*(1+(j<2)*(1+(k<3))); invNIndQ[1] = 1+(i<2)*(1+(j<3)*(1+(k<4)));
                deli10Q = gt[j]-gt[i]; deli20Q = gt[k]-gt[i]; deli21Q = gt[k]-gt[j];
                sumi21Q = gt[k]+gt[j]; sumi20Q = gt[k]+gt[i]; gti2SQ = gt[k]*gt[k];
                //0,1,2 | need 3,4
                //1*(1+1*(1+1))=3, 1+1*(1+1*(1+1))=4
                //1,3,4 | need 0,2
                //0*(1+0*(1+0))=0, 1+1*(1+0*(1+0))=2
                //1,2,3 | need 0,4
                //0*(1+0*(1+0))=0, 1+1*(1+1*(1+1))=4
                break;
            }
            asm("__varSetup_GTInvQ_Next:");
            if (--i < 0) {
                if (--j < 1) {
                    j = (--k) - 1;
                }
                i = j - 1;
            }
        } while (k > 1);
    }

    if (!firstRun) { for (char v = 109; v >= 0; v--) {
        asm("__varSetup_MainLoop_Overhead:");
        //Buffer Init
        bool isFloat = (v >= 60 && v < 100 || v >= 105);
        dword varInfo = formStorage[32*v+30];
        dword varCore = formStorage[32*v+31];
        byte type = varCore>>8&0x00000007;
        byte suplFlags = varCore>>4&0x0000000f;
        byte subtype = varCore&0x0000000f;
        bool changed; bool increased; bool decreased; bool isNaN = false;
        //Variable Init
        int vI; int vIO; int dvI;
        //float vF; float vFO; float dvF; //...actually, couldn't I save memory on a microscopic level by doing this? vI and vF will never be used in the same spot in practice
        float& vF = (float&)(vI); float& vFO = (float&)(vIO); float& dvF = (float&)(dvI);
        bool processed = false;
        if (isFloat) {
            //////////////////////////////////////////
            //      1F DELAY SCOPE (PREVFRAME)      //
            //////////////////////////////////////////
            //Declare local scope using do/while such that I can break to finalization using continue/etc.
            asm("__varSetup_FloatLoop_PrevScope:");
            if (dataStorage[32*127+21]) { do { //please only execute if we've seen 2 values | do/while(false) for continue/break functionality
                bool isTampered = tamperedWith[4+v/30]&(1<<v%30);
                if (isTampered && !allowTamperScout) { continue; } //yea let's acquire information on something that we tampered, very smart idea
                vI = dataStorage[32*v+20]; vIO = dataStorage[32*v+21]; dvF = vF - vFO; //The vI/vIO set also adjusts vF/vFO
                increased = (dvF>0); decreased = (dvF<0); changed = (increased||decreased); isNaN = (dvF!=dvF); //dvF!=dvF covers both vF and vFO
                //Any stuff that is done regardless of locking status will be placed here
                //Past this point, there's no new information to find if the variable is locked
                if (varCore&0x00000800) { continue; }
                /////////////////////////////
                //Root,StateNo Synchronization
                /////////////////////////////
                asm("__varSetup_FloatLoop_Prev_RootStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (isHelper && !(varInfo&0x00000100) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvF) == abs(root[765] - infoStorage[5])) {
                        float cF = (__builtin_expect(decreased != (root[765]-infoStorage[5]<0), 0) ? -1 : 1); //Oftentimes, this is actually aligned
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*(int)(root[765]);
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        processed = true; continue;
                    //If it otherwise changed, invalid
                    } else if (changed || infoStorage[5] != root[765]) {
                        varInfo |= 0x00000100;
                    //But if it didn't change at all and the opponent's statetime is 1 with identical magnitude
                    //ALSO assume that it's a stateno variable
                    } else if (root[895] == 1 && root[765] && abs(vF) == root[765]) {
                        formStorageF[32*v+0] = (__builtin_expect((vF<0) != (root[765]<0), 0) ? -1 : 1);
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        processed = true; continue;
                    }
                }
                asm("__varSetup_FloatLoop_Prev_RootPrevStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (isHelper && !(varInfo&0x00000200) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a prevstateno variable
                    if (changed && abs(dvF) == abs(root[766] - infoStorage[6])) {
                        float cF = (__builtin_expect(decreased != (root[766]-infoStorage[6]<0), 0) ? -1 : 1);
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*(int)(root[766]);
                        varCore = varCore&~0x000007ff | 0x00000a07;
                        processed = true; continue;
                    //If it otherwise changed, invalid
                    } else if (changed || infoStorage[6] != root[766]) {
                        varInfo |= 0x00000200;
                    }
                }
                /////////////////////////////
                //Aftermath-type Enemy Life Synchronization
                /////////////////////////////
                asm("__varSetup_FloatLoop_Prev_AftermathSync:");
                //If the type is an aftermath sync variable, lock it contently if we see that it's a stable synchronization
                if (type == 0x03 && subtype == 0x04 && suplFlags&0x02) {
                    if (life == vF*formStorageF[32*v+2] + formStorageF[32*v+3]) {
                        if ((++formStorage[32*v+4]) >= 100) varCore |= 0x00000800;
                    //If OoR but life is still a reasonable value, abandon completely
                    } else if (life > 1 && life < lifemax-1) {
                        varCore &= ~0x00000fff;
                        if (suplFlags&0x04) { //It's fine; we can make the swap back to damage-influenced
                            varCore |= 0x0000030f;
                        } else { //Scraping-independent variable; eliminate
                            varCore |= 0x10000000;
                        }
                    }
                    processed = true; continue;
                }
                //If we haven't disproven it as a scraping variable and it's not NaN
                if (!(varCore&0x90000000) && !isNaN && //ScrapingDNE | LastFrameFailure
                //the type is either unknown or a damage-influenced variable
                    (!type || type == 0x03 && subtype == 0x0f) && //type==NULL,Scraping[DI]
                //a life differential exists
                    dL &&
                //the differential happens to be equivalent
                    abs(dL) == abs(dvF)
                //Assume it's life-synchronized
                ) {
                    varCore = (varCore&~0x00000fff|0x00000324|0x40*(type==0x03));
                    formStorage[32*v+0] = dataStorage[32*v+22];
                    //life = slope*var + const
                    //life = 9000 - sysvar(1) | slope = -1, const = 9000 (startVal = 8000, nextVal = 8010; const = startVal+life)
                    //life = sysvar(1) - 4000 | slope = 1, const = -4000 (startVal = 5000, nextVal = 4995; const = -startVal+life)
                    formStorageF[32*v+1] = formStorageF[32*v+0] + lifemax*(1-2*decreased);
                    formStorageF[32*v+2] = (decreased!=(dL<0) ? -1 : 1); //Slope = +1/-1
                    formStorageF[32*v+3] = -vFO*formStorageF[32*v+2] + lifeO;
                    formStorage[32*v+4] = 0; //Begin Timer
                    processed = true; continue; //Using continues as breaks for my mentality
                }
                /////////////////////////////
                //No Correlation (Scraping)
                /////////////////////////////
                asm("__varSetup_FloatLoop_Prev_NonScraping:");
                //If we haven't disproven it as a scraping variable
                if (!(varCore&0x90000000) && //ScrapingDNE | LastFrameFailure
                //So long as we're not currently investigating its potential as a scraping variable
                    type != 0x03 && 
                //If the match started and the variable changed somehow (or it's NaN)
                    matchStarted && (changed || isNaN) &&
                //It doesn't seem to be tied to life whatsoever, or it's a game timer variable of some type (or, again, it's NaN)
                //(Note that all time inquiries are technically 1 frame behind)
                    (!scrapingHit || isNaN || 
                        abs(dvF) == 1 && (abs(vF) >= RSTime - 2 && abs(vF) <= RSTime - 1 || matchTime >= 5 && abs(vF) >= matchTime - 2 && abs(vF) <= matchTime - 1)
                    )
                //Assume it's unimportant and seal scraping processing
                ) {
                    varCore = (varCore&~0x00000fff|0x10000000);
                }
            } while (false); }
            //Feedtapes of potential importance to place AFTER prevF handling
            //asm("__varSetup_FloatLoop_Feedtape:");

            //////////////////////////////////////////
            //       MAIN/CURRENT FRAME SCOPE       //
            //////////////////////////////////////////
            //Local scope for identical reasons as the above
            //Resetting isNaN doesn't matter as for ints it's false anyway
            asm("__varSetup_FloatLoop_CurrScope:");
            if (!processed) { do { //for continue/break functionality
                bool isTampered = tamperedWith[v/30]&(1<<v%30);
                if (isTampered && !allowTamperScout) continue; //yea let's acquire information on something that we tampered, very smart idea
                vIO = dataStorage[32*v+20]; vI = var[v]; dvF = vF - vFO; //The vI/vIO set also adjusts vF/vFO
                increased = (dvF>0); decreased = (dvF<0); changed = (increased||decreased); isNaN = (dvF!=dvF); //dvF!=dvF covers both vF and vFO
                dataStorage[32*v+31-changed] += 1;
                dataStorage[32*v+29] = (changed ? dataStorage[32*v+29]+1 : 0);
                /////////////////////////////
                //Main Information Unlock processing
                /////////////////////////////
                asm("__varSetup_FloatLoop_Curr_Unlocks:");
                //Force all gametime variables to unlock and nullification to be reset if this is proc'd
                if (formStorage[32*v+28]&1) {
                    varCore &= ~(0x40000000|0x00000800*(type==0x01));
                //Under certain conditions, automatically unlock for re-evaluation
                } else if (minusThree && type == 0x01 && varCore&0x00000800) {
                    asm("__varSetup_FloatLoop_Curr_UnlocksGT:");
                    //Float/Int Mismatch Unlock
                    if ((formStorage[32*v+8]&0x80000400) == 0x80000000 && gt[4] >= 1291 ||
                        (formStorage[32*v+9]&0x80000400) == 0x80000000 && gt[4] >= 46341
                    ) {
                        varCore &= ~0x00000800;
                    //Float Magnitude Shift Unlock (Precaution for more erratic formulae)
                    } else if ((formStorage[32*v+12]|formStorage[32*v+13]|formStorage[32*v+14]|formStorage[32*v+15])&0x00000004 &&
                        vF && vFO && ((vF>0) != (vFO>0) || floor(log2(abs(vF))) != floor(log2(abs(vFO))))
                    ) {
                        varCore &= ~0x00000800;
                    //StateNo Suspicion Unlock
                    } else if (dataStorage[32*110+0] != dataStorage[32*110+26] &&
                        (formStorage[32*v+13]|formStorage[32*v+14]|formStorage[32*v+15])&0x00010000
                    ) {
                        varCore &= ~0x00000800;
                    //PrevStateNo Suspicion Unlock
                    } else if (dataStorage[32*111+0] != dataStorage[32*111+26] &&
                        (formStorage[32*v+13]|formStorage[32*v+14]|formStorage[32*v+15])&0x00020000
                    ) {
                        varCore &= ~0x00000800;
                    //StateTime Suspicion Unlock
                    } else if ((dword)(dataStorage[32*112+26]) < 2 &&
                        formStorage[32*v+14]&0x00040000 &&
                        (formStorage[32*v+12]|formStorage[32*v+13]) >= 0
                    ) {
                        varCore &= ~0x00000800;
                    //Modulus Unlock
                    } else if (formStorage[32*v+17] < 256 &&
                        (formStorage[32*v+12]|formStorage[32*v+13]) >= 0
                    ) {
                        varCore &= ~0x00000800;
                    }
                }
                //If a damage-influenced variable has exceeded its range, redefine the boundaries or nullify
                if (type == 0x03 && subtype == 0x0f) {
                    float idiotBuffer;
                    asm volatile(R"(.intel_syntax noprefix
                          push 0x05
                          fild DWORD PTR [esp]
                          add esp,0x04
                        )"
                        : "=t"(idiotBuffer) //idiotBuffer=5
                    );
                    //use transitive property of float/int to save processing time
                    if (formStorage[32*v+2]<0 && vF < formStorageF[32*v+1]) {
                        asm("__varSetup_FloatLoop_Curr_UnlocksDI_Desc:");
                        //Scraping Nullification if the value is completely and totally out of range
                        if (abs(vF) > abs(idiotBuffer*formStorageF[32*v+0]) && abs(vF) > abs(idiotBuffer*(formStorageF[32*v+0]-lifemax))) {
                            varCore = varCore&~0x00000fff|0x10000000;
                            memset(formStorage+32*v,0,96);
                        //Set to 0 if > 0
                        } else if (formStorageF[32*v+1] > 0) {
                            formStorageF[32*v+1] = 0;
                        //Set to init-lifemax if > than that
                        } else if (formStorageF[32*v+1] > formStorageF[32*v+0]-lifemax) {
                            formStorageF[32*v+1] = formStorageF[32*v+0]-lifemax;
                        //Set to negative if the initial value was positive
                        } else if (formStorageF[32*v+1] > -formStorageF[32*v+0] && formStorageF[32*v+0] > 0) {
                            formStorageF[32*v+1] = -formStorageF[32*v+0];
                        }
                    } else if (formStorage[32*v+2]>0 && vF > formStorageF[32*v+1]) {
                        asm("__varSetup_FloatLoop_Curr_UnlocksDI_Asc:");
                        //Scraping Nullification if the value is completely and totally out of range
                        if (abs(vF) > abs(idiotBuffer*formStorageF[32*v+0]) && abs(vF) > abs(idiotBuffer*(formStorageF[32*v+0]+lifemax))) {
                            varCore = varCore&~0x00000fff|0x10000000;
                            memset(formStorage+32*v,0,96);
                        //Set to 0 if < 0
                        } else if (formStorageF[32*v+1] < 0) {
                            formStorageF[32*v+1] = 0;
                        //Set to init+lifemax if < than that
                        } else if (formStorageF[32*v+1] < formStorageF[32*v+0]+lifemax) {
                            formStorageF[32*v+1] = formStorageF[32*v+0]+lifemax;
                        //Set to positive if the initial value was negative
                        } else if (formStorageF[32*v+1] < -formStorageF[32*v+0] && formStorageF[32*v+0] < 0) {
                            formStorageF[32*v+1] = -formStorageF[32*v+0];
                        }
                    }
                }
                //Past this point, there's no new information to find if the variable is locked
                if (varCore&0x00000800) continue;
                //If RS=2 hasn't been checked, assume that it COULD be a gametime variable even if we already proved it false
                if ((varCore&0x40001000) == 0x40000000 && roundstate > 1 && RSTime > 0) varCore &= ~0x40000000;
                //If this variable, regardless of whether we can obtain a formula or not, is synchronized with the root's identical variable in an anomalous fashion,
                //Then perhaps we should mark it (only valid if RS<2 due to potential complications)
                //In other words, if it either matches up with the root's exactly or matches up with the root's from the NEXT frame, then it's anomalous
                asm("__varSetup_FloatLoop_Curr_AnomalySync:");
                if (isHelper && dataStorage[32*v+30] >= 5*dataStorage[32*v+31] && minusThree && gt[0] && //Must constantly change, must have -3
                    (dataStorage[32*v+3] == dataStorage[32*v+11] || dataStorage[32*v+3] == dataStorage[32*v+10]) &&
                    (dataStorage[32*v+2] == dataStorage[32*v+10] || dataStorage[32*v+2] == dataStorage[32*v+9]) && 
                    (dataStorage[32*v+1] == dataStorage[32*v+9] || dataStorage[32*v+1] == dataStorage[32*v+8]) && 
                    (dataStorage[32*v+0] == dataStorage[32*v+8] || dataStorage[32*v+0] == root[912+v])
                ) {
                    varCore |= 0x00002000;
                }
    
                /////////////////////////////
                //Main Information Suspicion
                /////////////////////////////
                //Time is handled separately
                asm("__varSetup_FloatLoop_Curr_StateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (!(varInfo&0x00000001) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvF) == abs(stateno - dataStorage[32*110+20])) {
                        float cF = (__builtin_expect(decreased != (stateno-dataStorage[32*110+20]<0), 0) ? -1 : 1); //Oftentimes, this is actually aligned
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*stateno;
                        varCore = varCore&~0x000007ff | 0x00000a00;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || stateno != dataStorage[32*110+20]) {
                        varInfo |= 0x00000001;
                    //But if it didn't change at all and the opponent's statetime is 1 with identical magnitude
                    //ALSO assume that it's a stateno variable
                    } else if (statetime == 1 && stateno && abs(vF) == stateno) {
                        formStorageF[32*v+0] = (__builtin_expect((vF<0) != (stateno<0), 0) ? -1 : 1);
                        varCore = varCore&~0x000007ff | 0x00000a00;
                        continue;
                    }
                }
                asm("__varSetup_FloatLoop_Curr_PrevStateScrutiny:");
                //If we haven't disproven this as a prevstate variable, but we don't know its type
                if (!(varInfo&0x00000002) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a prevstateno variable
                    if (changed && abs(dvF) == abs(prevstateno - dataStorage[32*110+21])) {
                        float cF = (__builtin_expect(decreased != (prevstateno-dataStorage[32*110+21]<0), 0) ? -1 : 1);
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*prevstateno;
                        varCore = varCore&~0x000007ff | 0x00000a01;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || prevstateno != dataStorage[32*110+21]) {
                        varInfo |= 0x00000002;
                    }
                }
                asm("__varSetup_FloatLoop_Curr_StateScrutinyMinus3:");
                //If we haven't disproven this as a -3 state variable, but we don't know its type
                if (!(varInfo&0x00000010) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvF) == abs(dataStorage[32*110+26] - dataStorage[32*110+0])) {
                        float cF = (__builtin_expect(decreased != (dataStorage[32*110+26]-dataStorage[32*110+0]<0), 0) ? -1 : 1); //Oftentimes, this is actually aligned
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*dataStorage[32*110+26];
                        varCore = varCore&~0x000007ff | 0x00000a03;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || dataStorage[32*110+26] != dataStorage[32*110+0]) {
                        varInfo |= 0x00000010;
                    }
                }
                asm("__varSetup_FloatLoop_Curr_PrevStateScrutinyMinus3:");
                //If we haven't disproven this as a -3 prevstate variable, but we don't know its type
                if (!(varInfo&0x00000020) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a prevstateno variable
                    if (changed && abs(dvF) == abs(dataStorage[32*111+26] - dataStorage[32*111+0])) {
                        float cF = (__builtin_expect(decreased != (dataStorage[32*111+26]-dataStorage[32*111+0]<0), 0) ? -1 : 1); //Oftentimes, this is actually aligned
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*dataStorage[32*111+26];
                        varCore = varCore&~0x000007ff | 0x00000a04;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || dataStorage[32*111+26] != dataStorage[32*111+0]) {
                        varInfo |= 0x00000020;
                    }
                }
                //Root state and prev should be monitored on the frame AFTER this one, preferably
                //This is because the root will most likely move before this helper, so checking for a matching variable here makes sense
                //Nevertheless, we'll check for safety, but not lock it
                asm("__varSetup_FloatLoop_Curr_RootStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (isHelper && !(varInfo&0x00000100) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvF) == abs(root[765] - infoStorage[5])) {
                        float cF = (__builtin_expect(decreased != (root[765]-infoStorage[5]<0), 0) ? -1 : 1); //Oftentimes, this is actually aligned
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*root[765];
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        continue;
                    //But if it didn't change at all and the opponent's statetime is 1 with identical magnitude
                    //ALSO assume that it's a stateno variable
                    } else if (root[895] == 1 && root[765] && abs(vF) == root[765]) {
                        formStorageF[32*v+0] = (__builtin_expect((vF<0) != (root[765]<0), 0) ? -1 : 1);
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        continue;
                    }
                    //Ftr, the standard "lock" doesn't apply until the prevF checker is parsed
                }
                asm("__varSetup_FloatLoop_Curr_RootPrevStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (isHelper && !(varInfo&0x00000200) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvF) == abs(root[766] - infoStorage[6])) {
                        float cF = (__builtin_expect(decreased != (root[766]-infoStorage[6]<0), 0) ? -1 : 1); //Oftentimes, this is actually aligned
                        formStorageF[32*v+0] = cF;
                        formStorageF[32*v+1] = vF - cF*root[766];
                        varCore = varCore&~0x000007ff | 0x00000a07;
                        continue;
                    }
                    //Ftr, the standard "lock" doesn't apply until the prevF checker is parsed
                }
                asm("__varSetup_FloatLoop_Curr_BeaLifeScrutiny:");
                //If we haven't disproven this as a variable responsible for monitoring our life, but we don't know its type
                if (roundstate && !(varInfo&0x00001000) && !type) { //Do NOT execute on the first pass
                    //If it happens to synchronize exactly
                    if (changed && vF == beaLife && vFO == beaLifeO) {
                        varCore = varCore&~0x000007ff | 0x00000a0e;
                        continue;
                    //If it otherwise changed/our life changed, it's not monitoring our life
                    } else if (changed || beaLife != beaLifeO) {
                        varInfo |= 0x00001000;
                    }
                }
                /*
                //As there's actually 0 utility for this, I've disabled it
                //If it feels like a state is detected during scraping, instead switch to the pattern of [state+iter]
                asm("__varSetup_FloatLoop_Curr_BeaStateScrutiny:");
                //If we haven't disproven this as a variable responsible for monitoring our stateno, but we don't know its type, and it's non-zero
                if (!(varInfo&0x00002000) && !type && vF != 0) {
                    //If it happens to synchronize exactly
                    if (changed && vF == beaState && vFO == 0) {
                        varCore = varCore&~0x000007ff | 0x00000a0f;
                        continue;
                    //In all other cases, it's not monitoring our states
                    } else {
                        varInfo |= 0x00002000;
                    }
                }
                */
    
                /////////////////////////////
                //GameTime Formula Checksum
                /////////////////////////////
                //GameTime Calculation Assistance Buffers
                int vIL[5] = {dataStorage[32*v+3], dataStorage[32*v+2], dataStorage[32*v+1], dataStorage[32*v+0], var[v]};
                float* vFL = (float*)(vIL);
                int formTemplate[16]; float* formTemplateF = (float*)(formTemplate);
                memcpy(formTemplate, formStorage+32*v, 64); //Initialize to what we currently have
    
                asm("__varSetup_FloatLoop_Curr_GTChecksum:");
                if (isFloat && ((vIL[4]&0x7f800000) == 0x7f800000 || (vIL[3]&0x7f800000) == 0x7f800000 || (vIL[2]&0x7f800000) == 0x7f800000 || (vIL[1]&0x7f800000) == 0x7f800000 || (vIL[0]&0x7f800000) == 0x7f800000)) {
                    asm("__varSetup_FloatLoop_Curr_GTChecksum_Poisoned:");
                    varCore |= 0x40000000; //Avoid poisoning the batch with Inf/NaN
                    if (type == 0x01) { //If we're somehow already scouting, then that might be problematic
                        varCore &= ~0x000007ff;
                        type = 0x00; subtype = 0x00;
                    }
                }    
                //Float Checksum
                else if (type == 0x01 && minusThree) {
                    asm("__varSetup_FloatLoop_Curr_GTChecksum_MainStasis:");
                    if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4])) { //Main Formula Stasis
                        if ((formStorage[32*v+12]&0x80000400) == 0x80000000 && gt[4] >= 1291) { //Cubic Int/Float Stasis
                            formStorage[32*v+12] |= 0x00000400;
                        }
                        if ((formStorage[32*v+13]&0x80000400) == 0x80000000 && gt[4] >= 46341) { //Quadratic Int/Float Stasis
                            formStorage[32*v+13] |= 0x00000400;
                        }
                        formStorage[32*v+17] += 1; //wait for modulus stasis
                        varCore = varCore | 0x00000800*(((varInfo+=0x00010000*((varInfo&0x000f0000)<0x000f0000))&0x000f0000)==0x000f0000);
                        if ((varCore&0x00000810) == 0x00000800) { //Locked successfully for the first time
                            varCore |= 0x00000010;
                            if (formStorage[32*v+12] < 0 || (formStorage[32*v+13]|formStorage[32*v+14]|formStorage[32*v+15])&0x00000003) { //Impossible to accurately calc specials
                                formStorage[32*v+21] = -1; //Moduli > -1
                                formStorage[32*v+23] = -1;
                                continue;
                            }
                            int rootGuess;
                            if (formStorage[32*v+13] < 0 && formStorageF[32*v+1] != 0) { //Quadratic
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_RootSeekQ:");
                                float idiotBuffer;
                                asm volatile(R"(.intel_syntax noprefix
                                      push 0x04
                                      fild DWORD PTR [esp]
                                      add esp,0x04
                                    )"
                                    : "=t"(idiotBuffer) //idiotBuffer=4
                                );
                                float coeff = formStorageF[32*v+2]*formStorageF[32*v+2] - idiotBuffer*formStorageF[32*v+1]*formStorageF[32*v+3];
                                if (coeff < 0) { //Complex roots not supported sorry :(
                                    formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                    continue;
                                }
                                if (coeff == 0) { //Trivial case
                                    rootGuess = -formStorageF[32*v+2]/(2*formStorageF[32*v+1]);
                                    if (assembleFormulaF(target,formTemplate,dataStorage,rootGuess) != 0) { //Guess didn't work that's an L
                                        formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                        continue;
                                    }
                                    formStorage[32*v+20] = rootGuess; //Store the root of this equation
                                    formStorage[32*v+23] = 1; //Modulus = 0 (one chance)
                                } else {
                                    dword rootModulus = 0; //differentiate moduli
                                    rootGuess = (sqrt(coeff)-formStorageF[32*v+2])/(2*formStorageF[32*v+1]);
                                    if (assembleFormulaF(target,formTemplate,dataStorage,rootGuess) == 0) { //Should work
                                        rootModulus |= 1;
                                        formStorage[32*v+20] = rootGuess; //Store the root of this equation
                                    }
                                    rootGuess = (-sqrt(coeff)-formStorageF[32*v+2])/(2*formStorageF[32*v+1]);
                                    if (assembleFormulaF(target,formTemplate,dataStorage,rootGuess) == 0) { //Should work
                                        rootModulus |= 2;
                                        formStorage[32*v+21] = rootGuess; //Store the root of this equation
                                    }
                                    formStorage[32*v+23] = (rootModulus ? rootModulus : -1); //Handle appropriately
                                }
                            } else { //Linear
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_RootSeekL:");
                                rootGuess = -formStorageF[32*v+3]/formStorageF[32*v+2];
                                if (assembleFormulaF(target,formTemplate,dataStorage,rootGuess) != 0) { //Guess didn't work that's an L
                                    formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                    continue;
                                }
                                formStorage[32*v+20] = rootGuess; //Store the root of this equation
                                formStorage[32*v+23] = 1; //Modulus > 0 (one chance)
                            }
                        } else { //Check for deviation
                            asm("__varSetup_FloatLoop_Curr_GTChecksum_CoeffDeviation:");
                            //StateNo Suspicion Clearance
                            if (dataStorage[32*110+0] != dataStorage[32*110+26]) {
                                formStorage[32*v+13] &= ~0x00010000;
                                formStorage[32*v+14] &= ~0x00010000;
                                formStorage[32*v+15] &= ~0x00010000;
                            }
                            //PrevStateNo Suspicion Clearance
                            if (dataStorage[32*111+0] != dataStorage[32*111+26]) {
                                formStorage[32*v+13] &= ~0x00020000;
                                formStorage[32*v+14] &= ~0x00020000;
                                formStorage[32*v+15] &= ~0x00020000;
                            }
                            //StateTime Suspicion Clearance
                            if ((dword)(dataStorage[32*112+26]) < 2) {
                                formStorage[32*v+14] &= ~0x00040000;
                            }
                        }
                        continue;
                    }
                    //StateTime suspicion confirmation
                    asm("__varSetup_FloatLoop_Curr_GTChecksum_TimeSwitch:");
                    if (formStorage[32*v+14]&0x00040000 && (formStorage[32*v+12]|formStorage[32*v+13]) >= 0) { //For now, only allow linear equations
                        formStorage[32*v+14] &= ~0x00040000; //Clearance
                        float cF = formStorageF[32*v+2];
                        //Standard
                        //var(26) = -statetime + 5 within the state
                        //ChangeState from Intro @ t=250, picked up @t=200
                        //gt = 248 | 249 | 250
                        //vf = -42 | -43 | -44
                        //tm = 048 | 049 | 001 [relative to our access]
                        //vt = -42 | -43 | +05
                        //-44 - 5 == -1*49? yes, continue
                        if (dataStorage[32*112+12] == 1 && dvF == -cF*(dataStorage[32*112+13]-1)) { //Changed by exactly Time in the opposite direction (PrevTime+1 - 1)
                            //Convert to MainInfo
                            //Offset = the assumed value @ time=0; currently, time is 1, so it's simple
                            memset(formStorage+32*v,0,96);
                            formStorageF[32*v+0] = cF;
                            formStorageF[32*v+1] = vFL[4] - cF;
                            varCore = varCore&~0x000007ff | 0x00000a02;
                            continue;
                        }
                        //-3
                        //var(26) = -statetime + 5
                        //auto-walk @ t=250, picked up @t=200
                        //gt = 248 | 249 | 250
                        //vf = -43 | -44 | -45
                        //tm = 048 | 049 | 000 [relative to our access, before actual processing]
                        //vt = -43 | -44 | +05
                        //-45 - 5 == -1*(49+1)? yes, continue
                        //changestate @ t=249, picked up @t=200
                        //gt = 248 | 249 | 250
                        //vf = -43 | -44 | -45
                        //tm = 048 | 049 | 001 [relative to our access, before actual processing]
                        //vt = -43 | -44 | +04
                        //-45 - 4 == -1*49? yes, continue
                        if ((dword)(dataStorage[32*112+26]) < 2 && dvF == -cF*(dataStorage[32*112+0]-dataStorage[32*112+26])) { //Changed as appropriate
                            //Convert to MainInfo
                            //Offset = the assumed value @ time=0; if time=1 we subtract 1, otherwise subtract 0
                            memset(formStorage+32*v,0,96);
                            formStorageF[32*v+0] = cF;
                            formStorageF[32*v+1] = vFL[4] - cF*dataStorage[32*112+26];
                            varCore = varCore&~0x000007ff | 0x00000a05;
                            continue;
                        }
                    }
                    //Outside of that simple time case, float is... complicated.
                    //Not only are there things that could go wrong on various levels, the ORGANIZATION of every element can also be different.
                    //It's for this reason that we have to organize everything in a strange manner.
                    //Note that because of how processing works, the first two iterations can be swapped freely with no problems, leaving a more reasonable set
                    asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoopHeader:");
                    { //Local scope
                        bool stasisReached = false;
                        int f = 0;
                        char intSuspicion = (
                            (formTemplate[12]&0x80000000 && formTemplate[0]==(int)(formTemplate[0]))<<0 |
                            (formTemplate[13]&0x80000000 && formTemplate[1]==(int)(formTemplate[1]))<<1 | 
                            (true                        && formTemplate[2]==(int)(formTemplate[2]))<<2 | 
                            (true                        && formTemplate[3]==(int)(formTemplate[3]))<<3
                        );
                        do {
                            //&1: Agree with d's state suspicion
                            //&2: Agree with c's state suspicion
                            //&4: Agree with b's state suspicion
                            //&240: Placement ID
                                //=000: a+b+c+d || b+c+d || c+d
                                //=016: a+b+d+c || b+d+c
                                //=032: a+c+b+d || c+d+b
                                //=048: a+c+d+b
                                //=064: a+d+b+c
                                //=080: a+d+c+b
                                //=096: b+c+a+d
                                //=112: b+c+d+a
                                //=128: b+d+a+c
                                //=144: b+d+c+a
                                //=160: c+d+a+b
                                //=176: c+d+b+a
                            //&256: Quadratic Scouting Method
                            //&1536: Cubic Scouting Method
                            //StateNo/PrevStateNo suspicion confirmation
                            if (f&0x00000004) { //Term existence (because of the bottom chunk)
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoop_StateSuspicionQ:");
                                if (formStorage[32*v+13]&0x00010000) {
                                    formTemplate[1] = (formTemplate[1]<0 ? -1 : 1);
                                    formTemplate[5] = 0x00000bf4|110*0x00010000;
                                    formTemplate[13] = formTemplate[13]&~0x00010004|0x0000000a;
                                } else if (formStorage[32*v+13]&0x00020000) {
                                    formTemplate[1] = (formTemplate[1]<0 ? -1 : 1);
                                    formTemplate[5] = 0x00000bf8|111*0x00010000;
                                    formTemplate[13] = formTemplate[13]&~0x00020004|0x0000000a;
                                }
                            }
                            if (f&0x00000002) { //Term existence (because of the bottom chunk)
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoop_StateSuspicionL:");
                                if (formStorage[32*v+14]&0x00010000) {
                                    formTemplate[2] = (formTemplate[2]<0 ? -1 : 1);
                                    formTemplate[6] = 0x00000bf4|110*0x00010000;
                                    formTemplate[14] = formTemplate[14]&~0x00010004|0x0000000a;
                                } else if (formStorage[32*v+14]&0x00020000) {
                                    formTemplate[1] = (formTemplate[2]<0 ? -1 : 1);
                                    formTemplate[6] = 0x00000bf8|111*0x00010000;
                                    formTemplate[14] = formTemplate[14]&~0x00020004|0x0000000a;
                                }
                            }
                            if (f&0x00000001) { //Term existence (because of the bottom chunk)
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoop_StateSuspicionN:");
                                if (formStorage[32*v+15]&0x00010000) {
                                    formTemplate[3] = (formTemplate[3]<0 ? -1 : 1);
                                    formTemplate[7] = 0x00000bf4|110*0x00010000;
                                    formTemplate[15] = formTemplate[15]&~0x00010004|0x0000000a;
                                } else if (formStorage[32*v+15]&0x00020000) {
                                    formTemplate[3] = (formTemplate[3]<0 ? -1 : 1);
                                    formTemplate[7] = 0x00000bf8|111*0x00010000;
                                    formTemplate[15] = formTemplate[15]&~0x00020004|0x0000000a;
                                }
                            }
                            //Misorganization Scouting
                            if (f&0x000000f0 && formTemplate[12] < 0 && !(formStorage[32*v+12]&0x00002000)) {
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoop_ReorganizeC:");
                                formTemplate[12] = formTemplate[12]&~0x00001800|((f&0x000000f0)<0x00000060 ? 0x00002000 : 0x00003000 + 0x00000800*!!(f&0x00000010));
                                formTemplate[13] = formTemplate[13]&~0x00001800|((f&0x000000f0)<0x00000020 ?
                                    0x00002800 : ((f&0x000000f0)<0x00000060 ?
                                    0x00003000 + 0x00000800*!!(f&0x00000010) : ((f&0x000000f0)<0x000000a0 ?
                                    0x00002000 : ((f&0x000000f0)<0x000000a0 ?
                                    0x00002000 : 0x00003000 + 0x00000800*!(f&0x00000010)))));
                                formTemplate[14] = formTemplate[14]&~0x00001800|((f&0x000000f0)<0x00000020 ?
                                    0x00003000 + 0x00000800*!!(f&0x00000010) : ((f&0x000000f0)>0x00000090 ?
                                    0x00002000 : (f&0x00000020 ?
                                    0x00002800 : 0x00003000 + 0x00002800*!(f&0x00000010))));
                                formTemplate[15] = formTemplate[15]&~0x00001800|(!(f&0x00000080) && (f&0x00000060) != 0x00000040 ? 0x00003000 + 0x00000800*!(f&0x00000010) : 0x00002800);
                            } else if (f&0x000000f0 && formTemplate[12] >= 0 && formTemplate[13] < 0 && !(formStorage[32*v+13]&0x00002000)) {
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoop_ReorganizeQ:");
                                formTemplate[13] = formTemplate[13]&~0x00001800|(!(f&0x00000020) ? 0x00002000 : 0x00003000);
                                formTemplate[14] = formTemplate[14]&~0x00001800|(f&0x00000030 ? 0x00003000*!(f&0x00000020) : 0x00002800);
                                formTemplate[15] = formTemplate[15]&~0x00001800|(f&0x00000030 ? 0x00002800 : 0x00003000);
                            }
                            //Cubic Mismatch Scouting
                            if (f&0x00000600 && (formStorage[32*v+12]&0x80000400) == 0x80000000) {
                                formTemplate[12] |= (f&0x00000600)>>1|0x00000400; //Type Specifier
                            }
                            //Quadratic Mismatch Scouting
                            if (f&0x00000100 && (formStorage[32*v+13]&0x80000400) == 0x80000000) {
                                formTemplate[13] |= 0x00000100|0x00000400; //Type Specifier
                            }
                            //Break here if stasis is restored
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4])) {
                                stasisReached = true;
                                break;
                            }
                            //Int Suspicion
                            //The state suspicion anti here surprisingly also accounts for heterogeneity in other cases
                            if (intSuspicion) {
                                asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoop_IntSuspicion:");
                                if (intSuspicion&0x01 && !(f&0x00000608) && !(formStorage[32*v+12]&0x00000400)) {
                                    formTemplate[0] = (int)(formTemplate[0]);
                                    formTemplate[12] = formTemplate[12]&~0x00000004|0x00000400;
                                }
                                if (intSuspicion&0x02 && !(f&0x00000104) && !(formStorage[32*v+13]&0x00000400)) {
                                    formTemplate[1] = (int)(formTemplate[1]);
                                    formTemplate[13] = formTemplate[13]&~0x00000004|0x00000400;
                                }
                                if (intSuspicion&0x04 && !(f&0x00000002)) {
                                    formTemplate[2] = (int)(formTemplate[2]);
                                    formTemplate[14] = formTemplate[14]&~0x00000004|0x00000400;
                                }
                                if (intSuspicion&0x08 && !(f&0x00000001)) {
                                    formTemplate[3] = (int)(formTemplate[3]);
                                    formTemplate[15] = formTemplate[15]&~0x00000004|0x00000400;
                                }
                                //Break here if stasis is restored
                                if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4])) {
                                    stasisReached = true;
                                    break;
                                }
                            }
                            asm("__varSetup_FloatLoop_Curr_GTChecksum_SusLoop_Next:");
                            memcpy(formTemplate,formStorage+32*v,64); //Restore
                            if ((formStorage[32*v+12]|formStorage[32*v+13]) >= 0) { break; } //Just 1 pass
                            f++;
                            if (formStorage[32*v+12] >= 0) { //Cubic DnE
                                if (f&0x00000008) { f += 0x00000008; } //End of life cycle
                                if (f&0x000000f0 && formStorage[32*v+13]&0x00002000) { //Placement already solved
                                    f = (f+0x00000100)&~0x000000ff; //Advance to mismatch detection
                                } else if ((f&0x000000f0) >= 0x00000030) { //End of placement
                                    f += 0x000000d0; //Advance to mismatch detection
                                }
                                if (f&0x00000100 && formStorage[32*v+13]&0x00000400) { break; } //Quadratic mismatch solved
                                if (f&~0x000001ff) { break; } //OoR
                            } else {
                                if (f&0x00000008) { f += 0x00000008; } //End of life cycle (could be removed later)
                                if (f&0x000000f0 && formStorage[32*v+13]&0x00002000) { //Placement already solved
                                    f = (f+0x00000100)&~0x000000ff; //Advance to mismatch detection
                                } else if ((f&0x000000f0) >= 0x000000c0) { //End of placement
                                    f += 0x00000040; //Advance to mismatch detection
                                }
                                if (f&0x00000100 && formStorage[32*v+13]&0x00000400) { //Quadratic mismatch solved
                                    f = (f+0x00000100)&~0x000000ff; //Advance to the next Cubic mismatch detection formula
                                } 
                                if (f&0x00000600 && formStorage[32*v+12]&0x00000400) { break; } //Cubic mismatch solved
                            }
                        } while (!(f&~0x000007ff));
                        if (stasisReached) {
                            memcpy(formStorage+32*v,formTemplate,64);
                            varCore = varCore | 0x00000800*(((varInfo+=0x00010000*((varInfo&0x000f0000)<0x000f0000))&0x000f0000)==0x000f0000);
                            continue;
                        } else { //Nullify currently existing suspicions
                            if (dataStorage[32*110+0] != dataStorage[32*110+26]) {
                                formStorage[32*v+13] &= ~0x00010000;
                                formStorage[32*v+14] &= ~0x00010000;
                                formStorage[32*v+15] &= ~0x00010000;
                            }
                            if (dataStorage[32*111+0] != dataStorage[32*111+26]) {
                                formStorage[32*v+13] &= ~0x00020000;
                                formStorage[32*v+14] &= ~0x00020000;
                                formStorage[32*v+15] &= ~0x00020000;
                            }
                            if ((dword)(dataStorage[32*112+26]) < 2) {
                                formStorage[32*v+14] &= ~0x00040000;
                            }
                        }
                    }
                    memcpy(formTemplate,formStorage+32*v,64);
                    //Simple Modulus Processing
                    asm("__varSetup_FloatLoop_Curr_GTChecksum_ModulusCheck:");
                    if (formStorage[32*v+17] < 256 && (formStorage[32*v+12]|formStorage[32*v+13]) >= 0) { //For now, only allow linear equations
                        bool modOK = false;
                        formStorage[32*v+17] = 256; //Don't check again
                        for (int t = 3; t <= 256; t++) { //upon overflow it will end; %2 is handled within initialization; %3~4 might not be
                            asm("__varSetup_FloatLoop_Curr_GTChecksum_ModulusLoop:");
                            if (gt[4] < t) { break; } //We haven't hit t yet so how could it be %t?
                            //Formula of type c*(t%m)+d
                            //Our initial formula most likely is of the form c*(t-t/m*m)+d = c*t+(d-t/m*m*c)
                                //i.e. gt=999, formula = 3*(gametime%5)+7 => 3*(gametime-995)+7 = 3*gametime-2978
                            //Correct with our stored gametimes
                            formTemplate[2] = formStorage[32*v+2];
                            if (formStorage[32*v+15]&0x00000004) {
                                formTemplateF[3] = formStorageF[32*v+3] + (formStorage[32*v+14]&0x00000004 ?
                                    formStorageF[32*v+2]*(formStorage[32*v+16]-formStorage[32*v+16]%t) :
                                    formStorage[32*v+2]*(formStorage[32*v+16]-formStorage[32*v+16]%t)
                                );
                            } else {
                                formTemplate[3] = formStorage[32*v+3] + (formStorage[32*v+14]&0x00000004 ?
                                    (int)(floor(formStorageF[32*v+2]*(formStorage[32*v+16]-formStorage[32*v+16]%t))) :
                                    formStorage[32*v+2]*(formStorage[32*v+16]-formStorage[32*v+16]%t)
                                );
                            }
                            formTemplate[10] = t; //Modulus
                            formTemplate[11] = 0; //Carryover reset
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) && //Main Formula Stasis Restored
                                vFL[3] == assembleFormulaF(target,formTemplate,dataStorage,gt[3]) &&
                                vFL[2] == assembleFormulaF(target,formTemplate,dataStorage,gt[2]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                modOK = true;
                                break;
                            }
                            //Formula of type c*t+t%m+d
                            //Our initial formula most likely is of the form c*t+(t-t/m*m)+d = (c+1)*t+(d-t/m*m)
                                //i.e. gt=999, formula = 3*gametime+(gametime%5)+7 => 3*gametime+(gametime-995)+7 = 4*gametime-988
                            //Correct with our stored gametimes
                            if (formStorage[32*v+14]&0x00000004) {
                                formTemplateF[2] -= 1;
                            } else {
                                formTemplate[2] -= 1;
                            }
                            if (formStorage[32*v+15]&0x00000004) {
                                formTemplateF[3] = formStorageF[32*v+3] + (formStorage[32*v+16]-formStorage[32*v+16]%t);
                            } else {
                                formTemplate[3] = formStorage[32*v+3] + (formStorage[32*v+16]-formStorage[32*v+16]%t);
                            }
                            formTemplate[10] = 0;
                            formTemplate[11] = t; //Modulus
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) && //Main Formula Stasis Restored
                                vFL[3] == assembleFormulaF(target,formTemplate,dataStorage,gt[3]) &&
                                vFL[2] == assembleFormulaF(target,formTemplate,dataStorage,gt[2]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                modOK = true;
                                break;
                            }
                            //Formula of type c*t-t%m+d (precaution)
                            if (formStorage[32*v+14]&0x00000004) {
                                formTemplateF[2] = formStorageF[32*v+2] + 1;
                            } else {
                                formTemplate[2] += 2;
                            }
                            if (formStorage[32*v+15]&0x00000004) {
                                formTemplateF[3] = formStorageF[32*v+3] - (formStorage[32*v+16]-formStorage[32*v+16]%t);
                            } else {
                                formTemplate[3] -= 2*(formStorage[32*v+16]-formStorage[32*v+16]%t);
                            }
                            formTemplate[11] *= -1; 
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) && //Main Formula Stasis Restored
                                vFL[3] == assembleFormulaF(target,formTemplate,dataStorage,gt[3]) &&
                                vFL[2] == assembleFormulaF(target,formTemplate,dataStorage,gt[2]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0]))
                            ) { //Main Formula Stasis Restored
                                modOK = true;
                                break;
                            }
                        }
                        if (modOK) {
                            asm("__varSetup_FloatLoop_Curr_GTChecksum_ModulusFinal:");
                            memcpy(formStorage+32*v,formTemplate,64);
                            varCore = varCore | 0x00000800*(((varInfo+=0x00010000*((varInfo&0x000f0000)<0x000f0000))&0x000f0000)==0x000f0000);
                            continue;
                        }
                        memcpy(formTemplate,formStorage+32*v,64);
                    }
                    //As no other reparatory measures can be made past this point, we abandon here
                    asm("__varSetup_FloatLoop_Curr_GTChecksum_Abandon:");
                    memset(formTemplate, 0, 64);
                    memset(formStorage+32*v, 0, 96);
                    varCore = varCore&~0x000007ff | 0x40000000*(((varInfo=(varInfo&~0x000f0000)+0x00100000*((varInfo&0x00f00000)<0x00f00000))&0x00f00000)>=0x00a00000);
                    type = 0x00; subtype = 0x00;
                }

                /////////////////////////////
                //Damage Storage Suspicion
                /////////////////////////////
                //NOTE: While we don't need this for SCRAPING, we may prefer having it for TAMPERING later. Keep that in mind!
                asm("__varSetup_FloatLoop_Curr_DamageStorageScrutiny:");
                //If we haven't disproven this as a scraping variable but we don't know its type
                if (!(varCore&0x10000000) && !type && //ScrapingDNE
                //We connected a scraping hit
                    globDamaged && flags&0x00000001 && 
                //It changed from 0 to exactly the damage dealt
                    (!vFO && vF == damageS)
                //Assume that it's damage storage for the time being
                ) {
                    varCore = (varCore&~0x00000fff|0x00000301);
                    continue;
                }
                /////////////////////////////
                //Proper Enemy Life Synchronization
                /////////////////////////////
                asm("__varSetup_FloatLoop_Curr_ProperLifeSync:");
                //If the type is a proper sync variable, lock it contently if we see that it's a stable synchronization
                if (type == 0x03 && subtype == 0x04 && !(suplFlags&0x02)) {
                    if (life == vF*formStorageF[32*v+2] + formStorageF[32*v+3]) {
                        if ((++formStorage[32*v+4]) >= 100) varCore |= 0x00000800;
                    //If OoR but life is still a reasonable value, abandon completely
                    } else if (life > 1 && life < lifemax-1) {
                        varCore &= ~0x00000fff;
                        if (suplFlags&0x04) { //It's fine; we can make the swap back to damage-influenced
                            varCore |= 0x0000030f;
                        } else { //Scraping-independent variable; eliminate
                            varCore |= 0x10000000;
                        }
                    }
                    continue;
                //Otherwise, if we haven't disproven it as a scraping variable but we don't know its type
                } else if (!(varCore&0x10000000) && !type &&
                //a life differential exists and we're checking for it,
                    dL && flags&0x00000002 && //LifeScreeningOK
                //the differential happens to be equivalent
                    abs(dL) == abs(dvF)
                //Assume it's life-synchronized
                ) {
                    varCore = (varCore&~0x00000fff|0x00000304);
                    formStorage[32*v+0] = dataStorage[32*v+22];
                    formStorage[32*v+4] = 0; //Begin Timer
                    //life = slope*var + const
                    //life = 9000 - sysvar(1) | slope = -1, const = 9000 (startVal = 8000, nextVal = 8010; const = startVal+life)
                    //life = sysvar(1) - 4000 | slope = 1, const = -4000 (startVal = 5000, nextVal = 4995; const = -startVal+life)
                    if (isFloat) {
                        formStorageF[32*v+1] = formStorageF[32*v+0] + lifemax*(1-2*(dvF<0));
                        formStorageF[32*v+2] = ((dvF<0)!=(dL<0) ? -1 : 1); //Slope = +1/-1
                        formStorageF[32*v+3] = -vFO*formStorageF[32*v+2] + lifeO;
                        continue;
                    } else {
                        formStorage[32*v+1] = formStorage[32*v+0] + lifemax*(1-2*(dvI<0));
                        formStorage[32*v+2] = ((dvI<0)!=(dL<0) ? -1 : 1);
                        formStorage[32*v+3] = -vIO*formStorage[32*v+2] + lifeO;
                        continue;
                    }
                }
                /////////////////////////////
                //1F Damage Storage Confirmation
                /////////////////////////////
                //This one is a bit more necessary for variable tampering than other damage storage mechanisms so just keep that in mind
                //Relative term, though (I think you can accomplish everything here with full-area tampering usually)
                asm("__varSetup_FloatLoop_Curr_1FStorage:");
                //If we haven't disproven this as a scraping variable, nor have we performed the 1F check
                if (!(varCore&0x10000010) && //ScrapingDNE|1FElapsed
                //We're screening for either a damage storage or damage-influenced variable
                    type == 0x03 && (subtype == 0x01 || subtype == 0x0f) && 
                //We did NOT connect a scraping hit
                    !(flags&0x00000001)
                ) {
                    //If the value changed to 0 from a non-microscopic value, this is 1F storage
                    if (changed && vF == 0 && abs(dvF) > 1) varCore = (varCore&~0x00000fff|0x00000b02);
                    varCore |= 0x00000010; //1F confirmation
                    continue;
                }
                /////////////////////////////
                //Damage Storage Type Determination
                /////////////////////////////
                asm("__varSetup_FloatLoop_Curr_StorageTypeDef:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We're screening for a damage storage variable
                    type == 0x03 && subtype == 0x01 && 
                //We connected a scraping hit, and the damage is different from what it was last time
                    globDamaged && flags&0x00000001 && damageS != dataStorage[32*118+21]
                ) {
                    //If it changed to this damage, it stores as the most recent damage taken regardless of origin, and can be locked.
                    if (changed && vF == damageS) {
                        varCore |= 0x00000800; continue;
                    //If it didn't change at all, lock as quasi-permanent.
                    } else if (!changed) {
                        varCore = (varCore&~0x00000fff|0x00000b03); continue;
                    //Otherwise, it's damage-influenced in some way. We just don't know how.
                    } else {
                        varCore = (varCore&~0x00000fff|0x0000030f);
                        subtype = 0x0f; //continue;
                    }
                }
                /////////////////////////////
                //Invuln Timer Processing
                /////////////////////////////
                asm("__varSetup_FloatLoop_Curr_InvulnScrutiny:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We're screening for a timer/damage-influenced variable
                    type == 0x03 && (subtype == 0x06 || subtype == 0x0f) &&
                //It changed by a value that is equivalent to the time since it was last changed
                //We COULD settle with -1, however, this may be a problem vs opponents like Claroots, who subtract 9 every 9F for damage cancelling
                //We COULD settle with trending towards 0, however, this may be a problem vs opponents like Regisseur, who instead cap the variable
                    abs(dvF) == formStorage[32*v+13]+1
                //This feels like a timer. Switch to it and calculate
                ) {
                    if (subtype != 0x06) {
                        varCore = (varCore&~0x00000fff|0x00000306);
                        formStorage[32*v+0] = 0;
                        formStorageF[32*v+1] = abs(vF-dataStorageF[32*v+22]); //Store the maximum timer here
                        formStorage[32*v+6] = 0; //Init Timer
                        formStorage[32*v+7] = (formStorage[32*v+13] < 10 ? 10 : formStorage[32*v+13]); //Init Max Timer Stasis (min=10 for Claroots)
                    } else {
                        if (formStorageF[32*v+1] < abs(vF-dataStorageF[32*v+22])) formStorageF[32*v+1] = abs(vF-dataStorageF[32*v+22]);
                        if ((formStorage[32*v+6] += formStorage[32*v+13]+1) >= 600) varCore |= 0x00000800; //Lock if it seems like enough time has passed
                    }
                    continue;
                }
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We're screening for a timer variable
                    type == 0x03 && subtype == 0x06
                ) {
                    //If it either increased to the maximum value again, or it stayed at 0, it's still technically valid
                    if (!changed && vF == 0 || vF == formStorageF[32*v+1]) {
                        if ((formStorage[32*v+6] += formStorage[32*v+13]+1) >= 600) varCore |= 0x00000800; //Lock if it seems like enough time has passed
                        continue;
                    //If it changed in an otherwise unexpected way, or the stasis time exceeded its previous maximum value, reset it to damage-influenced
                    } else if (changed || formStorage[32*v+13] > formStorage[32*v+7]) {
                        varCore = (varCore&~0x00000fff|0x0000030f);
                        subtype = 0x0f;
                    }
                }
                /////////////////////////////
                //Damage-Influenced Suspicion/Confirmation
                /////////////////////////////
                asm("__varSetup_FloatLoop_Curr_InfluenceScrutiny:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We either don't know its type, or we're screening for a damage storage/damage-influenced variable
                    (!type || type == 0x03 && subtype == 0x0f) &&
                //We connected a scraping hit, causing the value to change
                    globDamaged && changed && flags&0x00000001
                //Assume that it's damage-influenced for the time being (and potentially lock it)
                ) {
                    varCore = (varCore&~0x00000fff|0x0000030f|0x00000800*!!type);
                    formStorage[32*v+0] = dataStorage[32*v+22];
                    //adhere to the KISS principle
                    //if it went up, then assume its max is this+lifemax
                    //if it went down, assume its min is this-lifemax
                    formStorageF[32*v+1] = formStorageF[32*v+0] + lifemax*(1-2*(dvF<0));
                    formStorageF[32*v+2] = (dvF<0 ? -1 : 1); //Slope = +1/-1
                    //If 0 comes before lifemax, assume that it's going to end in 0 instead
                    //Convenient property: if a float is less than 0, its int typecast will also be less than 0
                    if ((formStorage[32*v+1] < 0) != (formStorage[32*v+0] < 0)) { formStorage[32*v+1] = 0; }
                    continue;
                }
                /////////////////////////////
                //Anti-Flag Countermeasures
                /////////////////////////////
                asm("__varSetup_FloatLoop_Curr_AntiFlag:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We're screening for a damage-influenced variable
                    type == 0x03 && subtype == 0x0f &&
                //We previously connected a scraping hit, but not this frame, even though the value changed anyway
                    flags&0x00000001 && !globDamaged && (damageSO || fallDamageSO) && changed &&
                //The value simply returned to its previous state (that being the state it was in 2F ago)
                    vF == dataStorageF[32*v+21]
                //This most likely isn't actually important enough for me to care about! Disable it.
                ) {
                    varCore = (varCore&~0x00000fff|0x10000000);
                    continue;
                }
                //Past this point is gametime formula acquisition; if we have a type or have disproven it, skip
                //Already handled NaN poisoning previously
                //Also will be skipped if we haven't obtained 2 values through -3 yet
                //Lastly, as a precautionary measure, if we double-tapped for ANY reason, skip to prevent a crash when trying to divide by delta products
                asm("__varSetup_FloatLoop_Curr_GTHeader:");
                if (!minusThree || !gt[2] || type || varCore&0x40000000 || !del32 || gt[1] && !delprod321 || gt[0] && !delprod3210) { continue; }
    
                //Immediately declare failure during RS=2 if, for reasons unknown, the past 3 variables have matched either RSTime or MatchTime
                //This is most likely not a gametime variable, but a timer variable
                if (roundstate == 2 && matchTime < 4 && ( //After 4F, let's assume it's fine
                        RSTime > 2 && abs(vFL[2]) >= RSTime-3 && abs(vFL[2]) <= RSTime-2 && abs(vFL[3]) == abs(vFL[2])+1 && abs(vFL[4]) == abs(vFL[3])+1 ||
                        matchTime == 3 && abs(vFL[2]) >= 0 && abs(vFL[2]) <= 1 && abs(vFL[3]) == abs(vFL[2])+1 && abs(vFL[4]) == abs(vFL[3])+1
                )) {
                    varCore |= 0x40000000;
                    continue;
                }
    
                ///////////////////////////
                //Linear GameTime Formula
                ///////////////////////////
                //It is impossible for all inputs to be alike, so we need to find a discrepancy
                asm("__varSetup_FloatLoop_Curr_GTLinear:");
                if (!(vIL[3] == vIL[2])) {
                    if (gt[3] < 16777216) {
                        //v = c*t + d
                        //In continuous cases:
                        //c = v[3] - v[2]
                        //d = v[3] - c*t
                        //Generally speaking:
                        //c = (v[3] - v[2])/Δ32
                        float cF = (vFL[3] - vFL[2])/del32;
                        errorRounding(cF); //Significant Digit Adjustment
                        //d = v[3] - c*t[3]
                        float dF = vFL[3] - cF*gt[3];
                        errorRounding(dF); //Significant Digit Adjustment
                        //Checksum (must satisfy all, if extras exist)
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[14] = 0x80040004; //Term Existence Flags (for completely standard formulae like this, suspect time)
                        formTemplate[15] = 0x80000804;
                        if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                            (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1])) &&
                            (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+16] = gt[3]; //Save this gametime for modulo processing later
                            //StateNo/PrevStateNo suspicion
                            if (abs(cF) == dataStorage[32*110+0] && abs(cF) == dataStorage[32*110+1] && (!gt[1] || abs(cF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*110+3]) && abs(cF) == dataStorage[32*110+26]) {
                                formStorage[32*v+14] |= 0x00010000;
                            } else if (abs(cF) == dataStorage[32*111+0] && abs(cF) == dataStorage[32*111+1] && (!gt[1] || abs(cF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*111+3]) && abs(cF) == dataStorage[32*111+26]) {
                                formStorage[32*v+14] |= 0x00020000;
                            }
                            if (abs(dF) == dataStorage[32*110+0] && abs(dF) == dataStorage[32*110+1] && (!gt[1] || abs(dF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*110+3]) && abs(dF) == dataStorage[32*110+26]) {
                                formStorage[32*v+15] |= 0x00010000;
                            } else if (abs(dF) == dataStorage[32*111+0] && abs(dF) == dataStorage[32*111+1] && (!gt[1] || abs(dF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*111+3]) && abs(dF) == dataStorage[32*111+26]) {
                                formStorage[32*v+15] |= 0x00020000;
                            }
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        //Low Modulus Processing (in a way that it's impossible to satisfy the checksum)
                        //%2 case
                        asm("__varSetup_FloatLoop_Curr_GTMod2:");
                        char gtM2; char gtM3; char delM32; float vFLi2; float vFLi3;
                        if (gt[3]%2 != gt[2]%2) {
                            gtM2 = 0; gtM3 = 1; delM32 = 1; //Locked to these
                            vFLi2 = (gt[3]%2 ? vFL[2] : vFL[3]);
                            vFLi3 = (gt[3]%2 ? vFL[3] : vFL[2]);
                            //v=c*(gametime%2)+d
                            cF = (vFLi3 - vFLi2)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[10] = 2;
                            formTemplate[14] = 0x80000004; //Remove time suspicion
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1])) &&
                                (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            //v=c*gametime+gametime%2+d
                            cF = (vFLi3 - vFLi2 - delM32)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gt[3] - gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[10] = 0;
                            formTemplate[11] = 2;
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1])) &&
                                (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            //v=c*gametime-gametime%2+d
                            cF = (vFLi3 - vFLi2 + delM32)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gt[3] + gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[11] = -2;
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1])) &&
                                (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            formTemplate[11] = 0;
                        }
                        //%3 case
                        asm("__varSetup_FloatLoop_Curr_GTMod3:");
                        if (gt[1] && gt[3]%3 != gt[2]%3) {
                            gtM2 = (gt[3]%3 > gt[2]%3 ? gt[2]%3 : gt[3]%3);
                            gtM3 = (gt[3]%3 > gt[2]%3 ? gt[3]%3 : gt[2]%3);
                            delM32 = gtM3 - gtM2;
                            vFLi2 = (gt[3]%3 > gt[2]%3 ? vFL[2] : vFL[3]);
                            vFLi3 = (gt[3]%3 > gt[2]%3 ? vFL[3] : vFL[2]);
                            //v=c*(gametime%3)+d
                            cF = (vFLi3 - vFLi2)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[10] = 3;
                            formTemplate[14] = 0x80000004; //Remove time suspicion
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            //v=c*gametime+gametime%3+d
                            cF = (vFLi3 - vFLi2 - delM32)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gt[3] - gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[10] = 0;
                            formTemplate[11] = 3;
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            //v=c*gametime-gametime%3+d
                            cF = (vFLi3 - vFLi2 + delM32)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gt[3] + gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[11] = -3;
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            formTemplate[11] = 0;
                        }
                        //%4 case
                        asm("__varSetup_FloatLoop_Curr_GTMod4:");
                        if (gt[0] && gt[3]%4 != gt[2]%4) {
                            gtM2 = (gt[3]%4 > gt[2]%4 ? gt[2]%4 : gt[3]%4);
                            gtM3 = (gt[3]%4 > gt[2]%4 ? gt[3]%4 : gt[2]%4);
                            delM32 = gtM3 - gtM2;
                            vFLi2 = (gt[3]%4 > gt[2]%4 ? vFL[2] : vFL[3]);
                            vFLi3 = (gt[3]%4 > gt[2]%4 ? vFL[3] : vFL[2]);
                            //v=c*(gametime%4)+d
                            cF = (vFLi3 - vFLi2)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[10] = 4;
                            formTemplate[14] = 0x80000004; //Remove time suspicion
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0])
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            //v=c*gametime+gametime%4+d
                            cF = (vFLi3 - vFLi2 - delM32)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gt[3] - gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[10] = 0;
                            formTemplate[11] = 4;
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0])
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            //v=c*gametime-gametime%4+d
                            cF = (vFLi3 - vFLi2 + delM32)/delM32;
                            errorRounding(cF); //Significant Digit Adjustment
                            dF = vFLi3 - cF*gt[3] + gtM3;
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[11] = -4;
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                                vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1]) &&
                                vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0])
                            ) {
                                //Save formula
                                memcpy(formStorage+32*v,formTemplate,64);
                                formStorage[32*v+17] = 256; //DNE
                                varCore = varCore&~0x000007ff | 0x00000100;
                                continue;
                            }
                            formTemplate[11] = 0;
                        }
                        memset(formTemplate,0,64);
                    }
                }
                ///////////////////////////
                //Inverting Linear Formula
                ///////////////////////////
                //It might be possible for all inputs to be alike, but we need a set that explicitly has an &1 difference
                asm("__varSetup_FloatLoop_Curr_GTInversionL:");
                if (invInd[0] != -1) {
                    char i0 = invInd[0]; char i1 = invInd[1];
                    char ni0 = invNInd[0]; char ni1 = invNInd[1]; char ni2 = invNInd[2];
                    if (gt[i1] < 16777216) {
                        ///////////////////
                        //Linear Coefficient
                        ///////////////////
                        asm("__varSetup_FloatLoop_Curr_GTInversionL_Lin:");
                        //v = c*t*(1-2*(t%2)) + d
                        //In continuous cases:
                        //c = (v[3] - v[2])/(2t-1) * (1-2*(t%2))
                        //d = v[3] - c*t
                        //Generally speaking (assuming the format of 1-2*(t&1)
                        //c = (v[3] - v[2])/(t[3]*(1-2*(t[3]&1)) - t[2]*(1-2*(t[2]&1)))
                        float cF = (vFL[i1] - vFL[i0])/(gt[i1]*gtinv[i1] - gt[i0]*gtinv[i0]);
                        errorRounding(cF); //Significant Digit Adjustment
                        //d = v[3] - c*t[3]
                        float dF = vFL[i1] - cF*gt[i1];
                        errorRounding(dF); //Significant Digit Adjustment
                        //Checksum (must satisfy all, if extras exist; if the thing that SHOULD be inverting doesn't exist, this isn't correct)
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[14] = 0x80000084; //Term Existence Flags + Inversion + Float
                        formTemplate[15] = 0x80000804;
                        if (cF && (!gt[ni0] || vFL[ni0] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni0])) &&
                            (!gt[ni1] || vFL[ni1] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni1])) &&
                            (!gt[ni2] || vFL[ni2] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni2]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            //StateNo/PrevStateNo suspicion
                            if (abs(cF) == dataStorage[32*110+0] && abs(cF) == dataStorage[32*110+1] && (!gt[1] || abs(cF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*110+3]) && abs(cF) == dataStorage[32*110+26]) {
                                formStorage[32*v+14] |= 0x00010000;
                            } else if (abs(cF) == dataStorage[32*111+0] && abs(cF) == dataStorage[32*111+1] && (!gt[1] || abs(cF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*111+3]) && abs(cF) == dataStorage[32*111+26]) {
                                formStorage[32*v+14] |= 0x00020000;
                            }
                            if (abs(dF) == dataStorage[32*110+0] && abs(dF) == dataStorage[32*110+1] && (!gt[1] || abs(dF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*110+3]) && abs(dF) == dataStorage[32*110+26]) {
                                formStorage[32*v+15] |= 0x00010000;
                            } else if (abs(dF) == dataStorage[32*111+0] && abs(dF) == dataStorage[32*111+1] && (!gt[1] || abs(dF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*111+3]) && abs(dF) == dataStorage[32*111+26]) {
                                formStorage[32*v+15] |= 0x00020000;
                            }
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        ///////////////////
                        //Constant Coefficient
                        ///////////////////
                        asm("__varSetup_FloatLoop_Curr_GTInversionL_Const:");
                        //v = c*t + d*(1-2*(t%2))
                        //In continuous cases:
                        //c = (v[3] + v[2])/(2t-1)
                        //d = (v[3] - c*t) * (1-2*(t%2))
                        //Generally speaking (assuming the format of 1-2*(t&1)
                        //c = (v[3] + v[2])/Σ32
                        cF = (vFL[i1] + vFL[i0])/(gt[i1] + gt[i0]);
                        errorRounding(cF); //Significant Digit Adjustment
                        //d = (v[3] - c*t[3])*(1-2*(t[3]&1))
                        dF = (vFL[i1] - cF*gt[i1])*gtinv[i1];
                        errorRounding(dF); //Significant Digit Adjustment
                        //Checksum (must satisfy all, if extras exist; if the thing that SHOULD be inverting doesn't exist, this isn't correct)
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[14] = 0x80000004; //Term Existence Flags + Inversion + Float
                        formTemplate[15] = 0x80000884;
                        if (dF && (!gt[ni0] || vFL[ni0] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni0])) &&
                            (!gt[ni1] || vFL[ni1] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni1])) &&
                            (!gt[ni2] || vFL[ni2] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni2]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            //StateNo/PrevStateNo suspicion
                            if (abs(cF) == dataStorage[32*110+0] && abs(cF) == dataStorage[32*110+1] && (!gt[1] || abs(cF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*110+3]) && abs(cF) == dataStorage[32*110+26]) {
                                formStorage[32*v+14] |= 0x00010000;
                            } else if (abs(cF) == dataStorage[32*111+0] && abs(cF) == dataStorage[32*111+1] && (!gt[1] || abs(cF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*111+3]) && abs(cF) == dataStorage[32*111+26]) {
                                formStorage[32*v+14] |= 0x00020000;
                            }
                            if (abs(dF) == dataStorage[32*110+0] && abs(dF) == dataStorage[32*110+1] && (!gt[1] || abs(dF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*110+3]) && abs(dF) == dataStorage[32*110+26]) {
                                formStorage[32*v+15] |= 0x00010000;
                            } else if (abs(dF) == dataStorage[32*111+0] && abs(dF) == dataStorage[32*111+1] && (!gt[1] || abs(dF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*111+3]) && abs(dF) == dataStorage[32*111+26]) {
                                formStorage[32*v+15] |= 0x00020000;
                            }
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        ///////////////////
                        //Whole-Area Inversion
                        ///////////////////
                        asm("__varSetup_FloatLoop_Curr_GTInversionL_Full:");
                        //v = (c*t + d)*(1-2*(t%2))
                        //In continuous cases:
                        //v[3] = (c*t + d)*(1-2*(t%2))
                        //v[2] = -(c*(t-1) + d)*(1-2*(t%2))
                        //c = v[3] + v[2]
                        //d = v[3]*(1-2*(t%2)) - c*t
                        //Generally speaking (assuming the format of 1-2*(t&1)
                        //c = (v[3] + v[2])/(t[3]*(1-2*(t[3]&1)) + t[2]*(1-2*(t[2]&1)))
                        cF = (vFL[i1] + vFL[i0])/(gt[i1]*gtinv[i1] + gt[i0]*gtinv[i0]);
                        errorRounding(cF); //Significant Digit Adjustment
                        //d = v[3]*(1-2*(t[3]&1)) - c*t[3]
                        dF = vFL[i1]*gtinv[i1] - cF*gt[i1];
                        errorRounding(dF); //Significant Digit Adjustment
                        //Checksum (must satisfy all, if extras exist; if the thing that SHOULD be inverting doesn't exist, this isn't correct)
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[14] = 0x80000084; //Term Existence Flags + Inversion + Float
                        if (cF && dF && (!gt[ni0] || vFL[ni0] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni0])) &&
                            (!gt[ni1] || vFL[ni1] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni1])) &&
                            (!gt[ni2] || vFL[ni2] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni2]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            //StateNo/PrevStateNo suspicion
                            if (abs(cF) == dataStorage[32*110+0] && abs(cF) == dataStorage[32*110+1] && (!gt[1] || abs(cF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*110+3]) && abs(cF) == dataStorage[32*110+26]) {
                                formStorage[32*v+14] |= 0x00010000;
                            } else if (abs(cF) == dataStorage[32*111+0] && abs(cF) == dataStorage[32*111+1] && (!gt[1] || abs(cF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cF) == dataStorage[32*111+3]) && abs(cF) == dataStorage[32*111+26]) {
                                formStorage[32*v+14] |= 0x00020000;
                            }
                            if (abs(dF) == dataStorage[32*110+0] && abs(dF) == dataStorage[32*110+1] && (!gt[1] || abs(dF) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*110+3]) && abs(dF) == dataStorage[32*110+26]) {
                                formStorage[32*v+15] |= 0x00010000;
                            } else if (abs(dF) == dataStorage[32*111+0] && abs(dF) == dataStorage[32*111+1] && (!gt[1] || abs(dF) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dF) == dataStorage[32*111+3]) && abs(dF) == dataStorage[32*111+26]) {
                                formStorage[32*v+15] |= 0x00020000;
                            }
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        memset(formTemplate,0,64);
                    }
                }
        
                ///////////////////////////
                //Standard Quadratic Formula
                ///////////////////////////
                //It might be possible for all inputs to be alike, but we need a set that explicitly has an &1 difference
                asm("__varSetup_FloatLoop_Curr_GTQuadratic:");
                if (gt[1] && !(vIL[3] == vIL[2] && vIL[2] == vIL[1])) {
                    if (gt[3] < 4096) {
                        //v = b*t**2 + c*t + d
                        //In continuous cases:
                        //b = (v[3] - 2*v[2] + v[1])/2
                        //c = v[3] - v[2] - b*(2*t - 1)
                        //d = v[3] - c*t^2 - d*t
                        //Generally speaking:
                        //b = (v[3]*Δ21 - v[2]*Δ31 + v[1]*Δ32) / (Δ21*Δ31*Δ32)
                        float bF = (vFL[3]*del21 - vFL[2]*del31 + vFL[1]*del32)/delprod321;
                        errorRounding(bF); //Significant Digit Adjustment
                        //c = (v[3] - v[2])/Δ32 - b*Σ32
                        float cF = (vFL[3] - vFL[2])/del32 - bF*sum32;
                        errorRounding(cF); //Significant Digit Adjustment
                        //d = v[3] - b*t[3]^2 - c*t[3]
                        float dF = vFL[3] - bF*gt3S - cF*gt[3];
                        errorRounding(dF); //Significant Digit Adjustment
                        //Checksum (must satisfy all, if extras exist)
                        formTemplateF[1] = bF;
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[13] = 0x80000004; //Term Existence Flags + Float
                        formTemplate[14] = 0x80000804;
                        formTemplate[15] = 0x80001004;
                        if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4]) &&
                            (!gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //Modulus not supported
                            //StateNo/PrevStateNo suspicion
                            if (abs(bF) == dataStorage[32*110+0] && abs(bF) == dataStorage[32*110+1] && abs(bF) == dataStorage[32*110+2] && (!gt[0] || abs(bF) == dataStorage[32*110+3]) && abs(bF) == dataStorage[32*110+26]) {
                                formStorage[32*v+13] |= 0x00010000;
                            } else if (abs(bF) == dataStorage[32*111+0] && abs(bF) == dataStorage[32*111+1] && abs(bF) == dataStorage[32*111+2] && (!gt[0] || abs(bF) == dataStorage[32*111+3]) && abs(bF) == dataStorage[32*111+26]) {
                                formStorage[32*v+13] |= 0x00020000;
                            }
                            if (abs(cF) == dataStorage[32*110+0] && abs(cF) == dataStorage[32*110+1] && abs(cF) == dataStorage[32*110+2] && (!gt[0] || abs(cF) == dataStorage[32*110+3]) && abs(cF) == dataStorage[32*110+26]) {
                                formStorage[32*v+14] |= 0x00010000;
                            } else if (abs(cF) == dataStorage[32*111+0] && abs(cF) == dataStorage[32*111+1] && abs(cF) == dataStorage[32*111+2] && (!gt[0] || abs(cF) == dataStorage[32*111+3]) && abs(cF) == dataStorage[32*111+26]) {
                                formStorage[32*v+14] |= 0x00020000;
                            }
                            if (abs(dF) == dataStorage[32*110+0] && abs(dF) == dataStorage[32*110+1] && abs(dF) == dataStorage[32*110+2] && (!gt[0] || abs(dF) == dataStorage[32*110+3]) && abs(dF) == dataStorage[32*110+26]) {
                                formStorage[32*v+15] |= 0x00010000;
                            } else if (abs(dF) == dataStorage[32*111+0] && abs(dF) == dataStorage[32*111+1] && abs(dF) == dataStorage[32*111+2] && (!gt[0] || abs(dF) == dataStorage[32*111+3]) && abs(dF) == dataStorage[32*111+26]) {
                                formStorage[32*v+15] |= 0x00020000;
                            }
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        memset(formTemplate,0,64);
                    }
                }
        
                ///////////////////////////
                //Inverting Quadratic Formula
                ///////////////////////////
                //It is impossible for all inputs to be alike, so we need to find a discrepancy
                asm("__varSetup_FloatLoop_Curr_GTInversionQ:");
                if (invIndQ[0] != -1) {
                    char i0 = invIndQ[0]; char i1 = invIndQ[1]; char i2 = invIndQ[2];
                    char ni0 = invNIndQ[0]; char ni1 = invNIndQ[1];
                    if (gt[i2] < 4096) {
                        ///////////////////
                        //Quadratic Coefficient
                        ///////////////////
                        asm("__varSetup_FloatLoop_Curr_GTInversionQ_Quad:");
                        //v = b*t**2*(1-(t%2)*2) + c*t + d
                        //Generally speaking:
                        //bm = (v[3]*Δ21 - v[2]*Δ31 + v[1]*Δ32) / (Δ31*(t[2]*Σ32-t[1]*Δ32))
                        float bF = (vFL[i2]*deli10Q - vFL[i1]*deli20Q + vFL[i0]*deli21Q)/(deli20Q*(gt[i1]*sumi21Q-gt[i0]*deli21Q));
                        errorRounding(bF); //Significant Digit Adjustment
                        //c = (v[3] - v[1])/Δ31 - bm*Σ31
                        float cF = (vFL[i2] - vFL[i0])/deli20Q - bF*sumi20Q;
                        errorRounding(cF); //Significant Digit Adjustment
                        //d = v[3] - bm*t[3]^2 - c*t[3]
                        float dF = vFL[i2] - bF*gti2SQ - cF*gt[i2];
                        errorRounding(dF); //Significant Digit Adjustment
                        //b = bm*m (since m is either 1 or -1)
                        bF *= gtinv[i2];
                        //Checksum (must satisfy all, if extras exist)
                        formTemplateF[1] = bF;
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[13] = 0x80000084; //Term Existence Flags + Inversion + Float
                        formTemplate[14] = 0x80000804;
                        formTemplate[15] = 0x80001004;
                        if (bF && (!gt[ni0] || vFL[ni0] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni0])) &&
                            (!gt[ni1] || vFL[ni1] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni1]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //Modulus not supported
                            //State/Prev support may be added at a later time
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        ///////////////////
                        //Full Inversion (Lunatic_Empress_X)
                        ///////////////////
                        asm("__varSetup_FloatLoop_Curr_GTInversionQ_Full:");
                        //v = (b*t**2 + c*t + d)*(1-(t%2)*2)
                        //Generally speaking:
                        //bm = (v[3]*Δ21 + v[2]*Δ31 + v[1]*Δ32) / (Δ31*(t[2]*Σ32-t[1]*Δ32))
                        bF = (vFL[i2]*deli10Q + vFL[i1]*deli20Q + vFL[i0]*deli21Q)/(deli10Q*deli20Q*deli21Q);
                        errorRounding(bF); //Significant Digit Adjustment
                        //cm = (v[3] - v[1])/Δ31 - bm*Σ31
                        cF = (vFL[i2] - vFL[i0])/deli20Q - bF*sumi20Q;
                        errorRounding(cF); //Significant Digit Adjustment
                        //dm = v[3] - bm*t[3]^2 - cm*t[3]
                        dF = vFL[i2] - bF*gti2SQ - cF*gt[i2];
                        errorRounding(dF); //Significant Digit Adjustment
                        //X = Xm*m (since m is either 1 or -1)
                        bF *= gtinv[i2];
                        cF *= gtinv[i2];
                        dF *= gtinv[i2];
                        //Checksum (must satisfy all, if extras exist)
                        formTemplateF[1] = bF;
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[14] = 0x80000884; //Term Existence Flags + Inversion + Float
                        formTemplate[15] = 0x80001084;
                        if (bF && cF && dF && (!gt[ni0] || vFL[ni0] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni0])) &&
                            (!gt[ni1] || vFL[ni1] == assembleFormulaF(target,formTemplate,dataStorage,gt[ni1]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //Modulus not supported
                            //State/Prev support may be added at a later time
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        memset(formTemplate,0,64);
                    }
                }
        
                ///////////////////////////
                //Standard Cubic Formula
                ///////////////////////////
                //It is impossible for all inputs to be alike, so we need to find a discrepancy
                asm("__varSetup_FloatLoop_Curr_GTCubic:");
                if (gt[0] && !(vIL[3] == vIL[2] && vIL[2] == vIL[1] && vIL[1] == vIL[0])) {
                    if (gt[3] < 256) {
                        //v = a*t**3 + b*t**2 + c*t + d
                        //In continuous cases:
                        //a = (v[3] - 3*v[2] + 3*v[1] - v[0])/6
                        //b = (v[3] - 2*v[2] + v[1])/2 - 3*a*(t - 1)
                        //c = v[3] - v[2] - a*(3*t*(t - 1) + 1) - b*(2*t - 1)
                        //d = v[3] - a*t^3 - b*t^2 - c*t
                        //Generally speaking:
                        //a = ((v[3]*Δ20*Δ21 - v[2]*Δ30*Δ31)*Δ10 + (v[1]*Δ30*Δ20 - v[0]*Δ31*Δ21)*Δ32) / (Δ10*Δ20*Δ30*Δ21*Δ31*Δ32)
                        float aF = ((vFL[3]*del20*del21 - vFL[2]*del30*del31)*del10 + (vFL[1]*del30*del20 - vFL[0]*del31*del21)*del32)/delprod3210;
                        errorRounding(aF); //Significant Digit Adjustment
                        //b = (v[3]*Δ21 - v[2]*Δ31 + (v[1] - a*Δ31*Δ21*Σ321)*Δ32)/(Δ31*Δ32*Δ21)
                        float bF = (vFL[3]*del21 - vFL[2]*del31 + vFL[1]*del32)/delprod321 - aF*sum321;
                        errorRounding(bF); //Significant Digit Adjustment
                        //c = (v[3] - v[2])/Δ32 - a*(t[3]*t[3]+t[2]*t[3]+t[2]*t[2]){∏32} - b*Σ32
                        float cF = (vFL[3] - vFL[2])/del32 - aF*prod32 - bF*sum32;
                        errorRounding(cF); //Significant Digit Adjustment
                        //d = v[3] - a*t[3]^3 - b*t[3]^2 - c*t[3]
                        float dF = vFL[3] - aF*gt3C - bF*gt3S - cF*gt[3];
                        errorRounding(dF); //Significant Digit Adjustment
                        //Checksum
                        formTemplateF[0] = aF;
                        formTemplateF[1] = bF;
                        formTemplateF[2] = cF;
                        formTemplateF[3] = dF;
                        formTemplate[12] = 0x80000004; //Term Existence Flags + Float
                        formTemplate[13] = 0x80000804;
                        formTemplate[14] = 0x80001004;
                        formTemplate[15] = 0x80001804;
                        if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4])) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //Modulus not supported
                            //State/Prev coefficient suspicion will be added if it's ever deemed necessary
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        memset(formTemplate,0,64);
                    }
                }
        
                ///////////////////////////
                //Pre-to Linear Formula (var(X)*gametime)
                ///////////////////////////
                //It is STATISTICALLY impossible for all inputs to be alike, so we need to find a discrepancy
                //note that, to be safe, we're using all 5
                asm("__varSetup_FloatLoop_Curr_GTPreTo:");
                if (gt[0] && !(vIL[3] == vIL[2] && vIL[2] == vIL[1] && vIL[1] == vIL[0])) {
                    bool pretoESC = false;
                    if (gt[3] < 16777216) {
                        for (byte x = 0; x < 220; x++) {
                            char w = x/2; //Variable ID in w
                            if (!isHelper && x&1) { continue; } //&1 = 0 if self, 1 if root
                            if (v == w && !(x&1)) { continue; } //var(0) = 1*var(0) is a gigabrain formula
                            if ((formStorage[32*w+31]&0x00000700) == 0x00000100 && !(v < 100 && w >= 100) && ( //Prevent cross-referencing; it's either 1 or the other
                                formStorage[32*w+14]&0x00000008 && formStorage[32*w+6] == 0x00000e40+4*v || //to prevent var(39)=gametime+var(18), var(18)=-gametime+var(39) chains
                                formStorage[32*w+15]&0x00000008 && formStorage[32*w+7] == 0x00000e40+4*v //NOTE: that <100/>=100 check means that formulae like gametime+sysvar(1) will remain supported even if it would cross-reference
                            )) continue;
                            bool wFloat = (w >= 60 && w < 100 || w >= 105);
                            //Identical syntax to vIL/vFL
                            int wIL[5] = {dataStorage[32*w+3+8*(x&1)], dataStorage[32*w+2+8*(x&1)], dataStorage[32*w+1+8*(x&1)], dataStorage[32*w+0+8*(x&1)], (x&1 ? *(root+912+w) : var[w])};
                            float* wFL = (float*)(wIL);
                            int wIT[5]; float* wFT = (float*)(wIT);
                            if (!wIL[3] && !wIL[2] || wIL[3] == wIL[2] && wIL[2] == wIL[1] && wIL[1] == wIL[0]) continue; //This must also change, as to imply some sort of random factor (both inputs also cannot be 0)
                            if (wFloat && (
                                    (wIL[4]&0x7f800000) == 0x7f800000 || 
                                    (wIL[3]&0x7f800000) == 0x7f800000 || 
                                    (wIL[2]&0x7f800000) == 0x7f800000 || 
                                    (wIL[1]&0x7f800000) == 0x7f800000 || 
                                    (wIL[0]&0x7f800000) == 0x7f800000
                            )) {
                                continue; //Avoid poisoning the batch with Inf/NaN
                            } else if (wFloat) {
                                wFT[0] = wFL[0]*gt[0];
                                wFT[1] = wFL[1]*gt[1];
                                wFT[2] = wFL[2]*gt[2];
                                wFT[3] = wFL[3]*gt[3];
                                wFT[4] = wFL[4]*gt[4];
                            } else {
                                wIT[0] = wIL[0]*gt[0];
                                wIT[1] = wIL[1]*gt[1];
                                wIT[2] = wIL[2]*gt[2];
                                wIT[3] = wIL[3]*gt[3];
                                wIT[4] = wIL[4]*gt[4];
                            }
                            ///////////////////
                            //Linear Coefficient (Mul)
                            ///////////////////
                            asm("__varSetup_FloatLoop_Curr_GTPreTo_LinMul:");
                            float cF; float dF; int varDummy;
                            formTemplate[6] = 0x00000e40+4*w | 0x00008000*(x&1); //Offset = var(w)/fvar(w)/sysvar(w)/sysfvar(w) | root if applicable
                            formTemplate[7] = 0; //Set these in advance
                            //v = c*w*t + d
                            //Continuous cases most likely cannot exist due to w's unknown nature
                            //To simplify, we can combine into 1 known term
                            //v = c*wt + d
                            //Generally speaking:
                            if (wFloat ? wFT[3] != wFT[2] : wIT[3] != wIT[2]) { //Fringe crash avoidance
                                //c = (v[3] - v[2])/(wt[3] - wt[2])
                                cF = (vFL[3] - vFL[2])/(wFloat ? wFT[3] - wFT[2] : wIT[3] - wIT[2]);
                                errorRounding(cF); //Significant Digit Adjustment
                                //d = v[3] - c*wt[3]
                                dF = vFL[3] - cF*(wFloat ? wFT[3] : wIT[3]);
                                errorRounding(dF); //Significant Digit Adjustment
                                formTemplateF[2] = cF;
                                formTemplateF[3] = dF;
                                formTemplate[14] = 0x8000000e | 0x00000010*wFloat; //Term Existence Flags + Supplement + Float
                                formTemplate[15] = 0x80000804;
                                //Checksum (must satisfy all, if extras exist)
                                if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                                    (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                                    (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                                ) {
                                    pretoESC = true;//Formula Found Flag
                                    break; //Break Loop
                                }
                            }
                            ///////////////////
                            //Linear Coefficient (Add)
                            ///////////////////
                            asm("__varSetup_FloatLoop_Curr_GTPreTo_LinAdd:");
                            //v = (c+w)*t + d
                            //Continuous cases most likely cannot exist due to w's unknown nature
                            //Generally speaking:
                            //c = (v[3] - v[2] - (wt[3] - wt[2]))/Δ32
                            cF = (vFL[3] - vFL[2] - (wFloat ? wFT[3] - wFT[2] : wIT[3] - wIT[2]))/del32;
                            errorRounding(cF); //Significant Digit Adjustment
                            //d = v[3] - c*t[3] - wt[3]
                            dF = vFL[3] - cF*gt[3] - (wFloat ? wFT[3] : wIT[3]);
                            errorRounding(dF); //Significant Digit Adjustment
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[14] = 0x8000000d | 0x00000010*wFloat; //Term Existence Flags + Supplement + Float
                            formTemplate[15] = 0x80000804;
                            //Checksum (must satisfy all, if extras exist)
                            if (vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                                (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                                (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                            ) {
                                pretoESC = true; //Formula Found Flag
                                break; //Break Loop
                            }
                            ///////////////////
                            //Constant Coefficient (Mul)
                            ///////////////////
                            asm("__varSetup_FloatLoop_Curr_GTPreTo_ConstMul:");
                            formTemplate[6] = 0; //Set these in advance
                            formTemplate[7] = 0x00000e40+4*w | 0x00008000*(x&1); //Offset = var(w)/fvar(w)/sysvar(w)/sysfvar(w) | root if applicable
                            //v = c*t + d*w
                            //Continuous cases most likely cannot exist due to w's unknown nature
                            //Generally speaking:
                            //v[3] - v[2] = c*(t[3] - t[2]) + d*(t[3] - t[2])
                            //Well, that's marginally annoying :/
                            //Use a completely different method to fetch equations then, I guess
                            if (wFloat ? wFL[2]*gt[3] != wFL[3]*gt[2] : wIL[2]*gt[3] != wIL[3]*gt[2]) {
                                //c = (w[3]*v[2] - w[2]*v[3])/(w[3]*t[2] - w[2]*t[3])
                                float buf = (wFloat ? (wFL[3]*gt[2] - wFL[2]*gt[3]) : (wIL[3]*gt[2] - wIL[2]*gt[3])); //potentially save computation time
                                cF = (wFloat ? (wFL[3]*vFL[2] - wFL[2]*vFL[3])/buf : (wIL[3]*vFL[2] - wIL[2]*vFL[3])/buf);
                                errorRounding(cF); //Significant Digit Adjustment
                                //d = (v[3]*t[2] - v[2]*t[3])/(w[3]*t[2] - w[2]*t[3])
                                dF = (vFL[3]*gt[2] - vFL[2]*gt[3])/buf;
                                errorRounding(dF); //Significant Digit Adjustment
                                //Checksum (must satisfy all, if extras exist; must also not be a literal copypaste of a variable... unless it's the root)
                                formTemplateF[2] = cF;
                                formTemplateF[3] = dF;
                                formTemplate[14] = 0x80000004; //Term Existence Flags + Supplement + Float
                                formTemplate[15] = 0x8000080e | 0x00000010*wFloat;
                                if ((cF || dF != 1 || x == (byte)(2*v+1)) &&
                                    vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                                    (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                                    (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                                ) {
                                    pretoESC = true; //Formula Found Flag
                                    break; //Break Loop
                                }
                            }
                            ///////////////////
                            //Constant Coefficient (Add)
                            ///////////////////
                            asm("__varSetup_FloatLoop_Curr_GTPreTo_ConstAdd:");
                            //v = c*t + (d+w)
                            //Continuous cases most likely cannot exist due to w's unknown nature
                            //Generally speaking:
                            //c = (v[3] - v[2] - (w[3] - w[2]))/Δ32
                            cF = (vFL[3] - vFL[2] - (wFloat ? wFL[3] - wFL[2] : wIL[3] - wIL[2]))/del32;
                            errorRounding(cF); //Significant Digit Adjustment
                            //d = v[3] - c*t[3] - w[3]
                            dF = vFL[3] - cF*gt[3] - (isFloat ? wFL[3] : wIL[3]);
                            errorRounding(dF); //Significant Digit Adjustment
                            //Checksum (must satisfy all, if extras exist; must also not be a literal copypaste of a variable... unless it's the root)
                            formTemplateF[2] = cF;
                            formTemplateF[3] = dF;
                            formTemplate[14] = 0x80000004; //Term Existence Flags + Supplement + Float
                            formTemplate[15] = 0x80000809 | 0x00000010*wFloat;
                            if ((cF || dF || x == (byte)(2*v+1)) &&
                                vFL[4] == assembleFormulaF(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                                (!gt[1] || vFL[1] == assembleFormulaF(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                                (!gt[1] || !gt[0] || vFL[0] == assembleFormulaF(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                            ) {
                                pretoESC = true; //Formula Found Flag
                                break; //Break Loop
                            }
                        }
                    }
                    //asm("__varSetup_FloatLoop_Curr_GTPreTo_Final:"); //Can't put a label here as it apparently is compiled different from expected
                    if (pretoESC) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //Modulus not supported
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue; 
                    }
                }
                //GameTime formulae couldn't be acquired, but we can keep trying a couple of times, just in case
                asm("__varSetup_FloatLoop_Curr_GTFailure:");
                varCore = varCore | 0x40000000*(((varInfo=(varInfo&~0x000f0000)+0x00100000*((varInfo&0x00f00000)<0x00f00000))&0x00f00000)>=0x00a00000);
            } while (false); }
        } else {
            //////////////////////////////////////////
            //      1F DELAY SCOPE (PREVFRAME)      //
            //////////////////////////////////////////
            //Declare local scope using do/while such that I can break to finalization using continue/etc.
            asm("__varSetup_IntLoop_PrevScope:");
            if (dataStorage[32*127+21]) { do { //please only execute if we've seen 2 values | do/while(false) for continue/break functionality
                bool isTampered = tamperedWith[4+v/30]&(1<<v%30);
                if (isTampered && !allowTamperScout) { continue; } //yea let's acquire information on something that we tampered, very smart idea
                vI = dataStorage[32*v+20]; vIO = dataStorage[32*v+21]; dvI = vI - vIO;
                increased = (dvI>0); decreased = (dvI<0); changed = (increased||decreased); 
                //Any stuff that is done regardless of locking status will be placed here
                //Past this point, there's no new information to find if the variable is locked
                if (varCore&0x00000800) { continue; }
                /////////////////////////////
                //Root,StateNo Synchronization
                /////////////////////////////
                asm("__varSetup_IntLoop_Prev_RootStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (!(varInfo&0x00000100) && !type && root) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvI) == abs(root[765] - infoStorage[5])) {
                        int cI = (__builtin_expect(decreased != (root[765]-infoStorage[5]<0), 0) ? -1 : 1); //Oftentimes, this is actually aligned
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*(int)(root[765]);
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        processed = true; continue;
                    //If it otherwise changed, invalid
                    } else if (changed || infoStorage[5] != root[765]) {
                        varInfo |= 0x00000100;
                    //But if it didn't change at all and the opponent's statetime is 1 with identical magnitude
                    //ALSO assume that it's a stateno variable
                    } else if (root[895] == 1 && root[765] && abs(vI) == root[765]) {
                        formStorage[32*v+0] = (__builtin_expect((vI<0) != (root[765]<0), 0) ? -1 : 1);
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        processed = true; continue;
                    }
                }
                asm("__varSetup_IntLoop_Prev_RootPrevStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (!(varInfo&0x00000200) && !type && root) {
                    //It changed and the differential is equivalent
                    //Assume that it's a prevstateno variable
                    if (changed && abs(dvI) == abs(root[766] - infoStorage[6])) {
                        int cI = (__builtin_expect(decreased != (root[766]-infoStorage[6]<0), 0) ? -1 : 1);
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*(int)(root[766]);
                        varCore = varCore&~0x000007ff | 0x00000a07;
                        processed = true; continue;
                    //If it otherwise changed, invalid
                    } else if (changed || infoStorage[6] != root[766]) {
                        varInfo |= 0x00000200;
                    }
                }
                /////////////////////////////
                //Aftermath-type Enemy Life Synchronization
                /////////////////////////////
                asm("__varSetup_IntLoop_Prev_AftermathSync:");
                //If the type is an aftermath sync variable, lock it contently if we see that it's a stable synchronization
                if (type == 0x03 && subtype == 0x04 && suplFlags&0x02) {
                    if (life == vI*formStorage[32*v+2] + formStorage[32*v+3]) {
                        if ((++formStorage[32*v+4]) >= 100) varCore |= 0x00000800;
                    //If OoR but life is still a reasonable value, abandon completely
                    } else if (life > 1 && life < lifemax-1) {
                        varCore &= ~0x00000fff;
                        if (suplFlags&0x04) { //It's fine; we can make the swap back to damage-influenced
                            varCore |= 0x0000030f;
                        } else { //Scraping-independent variable; eliminate
                            varCore |= 0x10000000;
                        }
                    }
                    processed = true; continue;
                }
                //If we haven't disproven it as a scraping variable and it's not NaN
                if (!(varCore&0x90000000) && !isNaN && //ScrapingDNE | LastFrameFailure
                //the type is either unknown or a damage-influenced variable
                    (!type || type == 0x03 && subtype == 0x0f) && //type==NULL,Scraping[DI]
                //a life differential exists
                    dL &&
                //the differential happens to be equivalent
                    abs(dL) == abs(dvI)
                //Assume it's life-synchronized
                ) {
                    varCore = (varCore&~0x00000fff|0x00000324|0x40*(type==0x03));
                    formStorage[32*v+0] = dataStorage[32*v+22];
                    //life = slope*var + const
                    //life = 9000 - sysvar(1) | slope = -1, const = 9000 (startVal = 8000, nextVal = 8010; const = startVal+life)
                    //life = sysvar(1) - 4000 | slope = 1, const = -4000 (startVal = 5000, nextVal = 4995; const = -startVal+life)
                    formStorage[32*v+1] = formStorage[32*v+0] + lifemax*(1-2*decreased);
                    formStorage[32*v+2] = (decreased!=(dL<0) ? -1 : 1); //Slope = +1/-1
                    formStorage[32*v+3] = -vIO*formStorage[32*v+2] + lifeO;
                    formStorage[32*v+4] = 0; //Begin Timer
                    processed = true; continue; //Using continues as breaks for my mentality
                }
                /////////////////////////////
                //No Correlation (Scraping)
                /////////////////////////////
                asm("__varSetup_IntLoop_Prev_NonScraping:");
                //If we haven't disproven it as a scraping variable
                if (!(varCore&0x90000000) && //ScrapingDNE | LastFrameFailure
                //So long as we're not currently investigating its potential as a scraping variable
                    type != 0x03 && 
                //If the match started and the variable changed somehow (or it's NaN)
                    matchStarted && (changed || isNaN) &&
                //It doesn't seem to be tied to life whatsoever, or it's a game timer variable of some type (or, again, it's NaN)
                //(Note that all time inquiries are technically 1 frame behind)
                    (!scrapingHit || isNaN || 
                        abs(dvI) == 1 && (abs(vI) >= RSTime - 2 && abs(vI) <= RSTime - 1 || matchTime >= 5 && abs(vI) >= matchTime - 2 && abs(vI) <= matchTime - 1)
                    )
                //Assume it's unimportant and seal scraping processing
                ) {
                    varCore = (varCore&~0x00000fff|0x10000000);
                }
            } while (false); }
            //Feedtapes of potential importance to place AFTER prevF handling
            //asm("__varSetup_IntLoop_Feedtape:");
            //////////////////////////////////////////
            //       MAIN/CURRENT FRAME SCOPE       //
            //////////////////////////////////////////
            //Local scope for identical reasons as the above
            //Resetting isNaN doesn't matter as for ints it's false anyway
            asm("__varSetup_IntLoop_CurrScope:");
            if (!processed) { do { //for continue/break functionality
                bool isTampered = tamperedWith[v/30]&(1<<v%30);
                if (isTampered && !allowTamperScout) { continue; } //yea let's acquire information on something that we tampered, very smart idea
                vIO = dataStorage[32*v+20]; vI = var[v]; dvI = vI - vIO;
                increased = (dvI>0); decreased = (dvI<0); changed = (increased||decreased);
                dataStorage[32*v+31-changed] += 1;
                dataStorage[32*v+29] = (changed ? dataStorage[32*v+29]+1 : 0);

                /////////////////////////////
                //Main Information Unlock processing
                /////////////////////////////
                asm("__varSetup_IntLoop_Curr_Unlocks:");
                //Force all gametime variables to unlock and nullification to be reset if this is proc'd
                if (formStorage[32*v+28]&1) {
                    varCore &= ~(0x40000000|0x00000800*(type==0x01));
                //Under certain conditions, automatically unlock for re-evaluation
                } else if (minusThree && type == 0x01 && varCore&0x00000800) {
                    asm("__varSetup_IntLoop_Curr_UnlocksGT:");
                    //StateNo Suspicion Unlock
                    if ((formStorage[32*v+13]|formStorage[32*v+14]|formStorage[32*v+15])&0x00010000 &&
                        dataStorage[32*110+0] != dataStorage[32*110+26]
                    ) {
                        varCore &= ~0x00000800;
                    //PrevStateNo Suspicion Unlock
                    } else if ((formStorage[32*v+13]|formStorage[32*v+14]|formStorage[32*v+15])&0x00020000 &&
                        dataStorage[32*111+0] != dataStorage[32*111+26]
                    ) {
                        varCore &= ~0x00000800;
                    //StateTime Suspicion Unlock
                    } else if ((dword)(dataStorage[32*112+26]) < 2 &&
                        formStorage[32*v+14]&0x00040000 &&
                        (formStorage[32*v+12]|formStorage[32*v+13]) >= 0
                    ) {
                        varCore &= ~0x00000800;
                    //Modulus Unlock
                    } else if (formStorage[32*v+17] < 256 &&
                        (formStorage[32*v+12]|formStorage[32*v+13]) >= 0
                    ) {
                        varCore &= ~0x00000800;
                    }
                }
                //If a damage-influenced variable has exceeded its range, redefine the boundaries or nullify
                if (type == 0x03 && subtype == 0x0f) {
                    //use transitive property of float/int to save processing time
                    if (formStorage[32*v+2]<0 && vI < formStorage[32*v+1]) {
                        asm("__varSetup_IntLoop_Curr_UnlocksDI_IntDesc:");
                        //the below is identical with some slight changes
                        if (abs(vI) > abs(5*formStorage[32*v+0]) && abs(vI) > abs(5*(formStorage[32*v+0]-lifemax))) {
                            varCore = varCore&~0x00000fff|0x10000000;
                            memset(formStorage+32*v,0,96);
                        } else if (formStorage[32*v+1] > 0) {
                            formStorage[32*v+1] = 0;
                        //use delCoeff instead to prevent overflow risk
                        } else if (formStorage[32*v+1]-formStorage[32*v+0] > -lifemax) {
                            formStorage[32*v+1] = formStorage[32*v+0]-lifemax;
                        } else if (formStorage[32*v+1] > -formStorage[32*v+0] && formStorage[32*v+0] > 0) {
                            formStorage[32*v+1] = -formStorage[32*v+0];
                        }
                    } else if (formStorage[32*v+2]>0 && vI > formStorage[32*v+1]) {
                        asm("__varSetup_IntLoop_Curr_UnlocksDI_IntAsc:");
                        if (abs(vI) > abs(5*formStorage[32*v+0]) && abs(vI) > abs(5*(formStorage[32*v+0]+lifemax))) {
                            varCore = varCore&~0x00000fff|0x10000000;
                            memset(formStorage+32*v,0,96);
                        } else if (formStorage[32*v+1] < 0) {
                            formStorage[32*v+1] = 0;
                        } else if (formStorage[32*v+1]-formStorage[32*v+0] < lifemax) {
                            formStorage[32*v+1] = formStorage[32*v+0]+lifemax;
                        } else if (formStorage[32*v+1] < -formStorage[32*v+0] && formStorage[32*v+0] < 0) {
                            formStorage[32*v+1] = -formStorage[32*v+0];
                        }
                    }
                }
                //Past this point, there's no new information to find if the variable is locked
                if (varCore&0x00000800) { continue; }
                //If RS=2 hasn't been checked, assume that it COULD be a gametime variable even if we already proved it false
                if ((varCore&0x40001000) == 0x40000000 && roundstate > 1 && RSTime > 0) {
                    varCore &= ~0x40000000;
                }
                //If this variable, regardless of whether we can obtain a formula or not, is synchronized with the root's identical variable in an anomalous fashion,
                //Then perhaps we should mark it (only valid if RS<2 due to potential complications)
                //In other words, if it either matches up with the root's exactly or matches up with the root's from the NEXT frame, then it's anomalous
                asm("__varSetup_IntLoop_Curr_AnomalySync:");
                if (isHelper && dataStorage[32*v+30] >= 5*dataStorage[32*v+31] && minusThree && gt[0] && //Must constantly change, must have -3
                    (dataStorage[32*v+3] == dataStorage[32*v+11] || dataStorage[32*v+3] == dataStorage[32*v+10]) &&
                    (dataStorage[32*v+2] == dataStorage[32*v+10] || dataStorage[32*v+2] == dataStorage[32*v+9]) && 
                    (dataStorage[32*v+1] == dataStorage[32*v+9] || dataStorage[32*v+1] == dataStorage[32*v+8]) && 
                    (dataStorage[32*v+0] == dataStorage[32*v+8] || dataStorage[32*v+0] == root[912+v])
                ) {
                    varCore |= 0x00002000;
                }
    
                /////////////////////////////
                //Main Information Suspicion
                /////////////////////////////
                //Time is handled separately
                asm("__varSetup_IntLoop_Curr_StateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (!(varInfo&0x00000001) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvI) == abs(stateno - dataStorage[32*110+20])) {
                        int cI = (__builtin_expect((dvI<0) != (stateno-dataStorage[32*110+20]<0), 0) ? -1 : 1);
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*stateno;
                        varCore = varCore&~0x000007ff | 0x00000a00;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || stateno != dataStorage[32*110+20]) {
                        varInfo |= 0x00000001;
                    //But if it didn't change at all and the opponent's statetime is 1 with identical magnitude
                    //ALSO assume that it's a stateno variable
                    } else if (statetime == 1 && stateno && abs(vI) == stateno) {
                        formStorage[32*v+0] = (__builtin_expect((vI<0) != (stateno<0), 0) ? -1 : 1);
                        varCore = varCore&~0x000007ff | 0x00000a00;
                        continue;
                    }
                }
                asm("__varSetup_IntLoop_Curr_PrevStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (!(varInfo&0x00000002) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a prevstateno variable
                    if (changed && dvI == abs(prevstateno - dataStorage[32*111+20])) {
                        int cI = (__builtin_expect((dvI<0) != (prevstateno-dataStorage[32*111+20]<0), 0) ? -1 : 1);
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*prevstateno;
                        varCore = varCore&~0x000007ff | 0x00000a01;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || prevstateno != dataStorage[32*111+20]) {
                        varInfo |= 0x00000002;
                    }
                }
                asm("__varSetup_IntLoop_Curr_StateScrutinyMinusThree:");
                //If we haven't disproven this as a -3 state variable, but we don't know its type, and we've read -3
                if (!(varInfo&0x00000010) && !type && minusThree) {
                    //It changed and the differential is equivalent
                    //Assume that it's a -3 stateno variable
                    if (changed && dvI == abs(dataStorage[32*110+26] - dataStorage[32*110+0])) {
                        int cI = (__builtin_expect((dvI<0) != (dataStorage[32*110+26]-dataStorage[32*110+0]<0), 0) ? -1 : 1);
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*dataStorage[32*110+26];
                        varCore = varCore&~0x000007ff | 0x00000a03;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || dataStorage[32*110+26] != dataStorage[32*110+0]) {
                        varInfo |= 0x00000010;
                    }
                }
                asm("__varSetup_IntLoop_Curr_PrevStateScrutinyMinusThree:");
                //If we haven't disproven this as a -3 state variable, but we don't know its type, and we've read -3
                if (!(varInfo&0x00000020) && !type && minusThree) {
                    //It changed and the differential is equivalent
                    //Assume that it's a -3 prevstateno variable
                    if (changed && dvI == abs(dataStorage[32*111+26] - dataStorage[32*111+0])) {
                        int cI = (__builtin_expect((dvI<0) != (dataStorage[32*111+26]-dataStorage[32*111+0]<0), 0) ? -1 : 1);
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*dataStorage[32*111+26];
                        varCore = varCore&~0x000007ff | 0x00000a04;
                        continue;
                    //If it otherwise changed, invalid
                    } else if (changed || dataStorage[32*111+26] != dataStorage[32*111+0]) {
                        varInfo |= 0x00000020;
                    }
                }
                //Root state and prev should be monitored on the frame AFTER this one, preferably
                //This is because the root will most likely move before this helper, so checking for a matching variable here makes sense
                //Nevertheless, we'll check for safety, but not lock it
                asm("__varSetup_IntLoop_Curr_RootStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (isHelper && !(varInfo&0x00000100) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a stateno variable
                    if (changed && abs(dvI) == abs(root[765] - infoStorage[5])) {
                        int cI = (__builtin_expect((dvI<0) != (root[765]-infoStorage[5]<0), 0) ? -1 : 1);
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*(int)(root[765]);
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        continue;
                    //But if it didn't change at all and the opponent's statetime is 1 with identical magnitude
                    //ALSO assume that it's a stateno variable
                    } else if (root[895] == 1 && root[765] && abs(vI) == root[765]) {
                        formStorage[32*v+0] = (__builtin_expect((vI<0) != (root[765]<0), 0) ? -1 : 1);
                        varCore = varCore&~0x000007ff | 0x00000a06;
                        continue;
                    }
                    //Otherwise wait 1F and decide again
                }
                asm("__varSetup_IntLoop_Curr_RootPrevStateScrutiny:");
                //If we haven't disproven this as a state variable, but we don't know its type
                if (isHelper && !(varInfo&0x00000200) && !type) {
                    //It changed and the differential is equivalent
                    //Assume that it's a prevstateno variable
                    if (changed && abs(dvI) == abs(root[766] - infoStorage[6])) {
                        int cI = (__builtin_expect((dvI<0) != (root[766]-infoStorage[6]<0), 0) ? -1 : 1);
                        formStorage[32*v+0] = cI;
                        formStorage[32*v+1] = vI - cI*(int)(root[766]);
                        varCore = varCore&~0x000007ff | 0x00000a07;
                        continue;
                    }
                    //Otherwise wait 1F and decide again
                }
                asm("__varSetup_IntLoop_Curr_BeaLifeScrutiny:");
                //If we haven't disproven this as a variable responsible for monitoring our life, but we don't know its type
                if (roundstate && !(varInfo&0x00001000) && !type) { //Do NOT execute on the first pass
                    //If it happens to synchronize exactly
                    if (changed && vIO == beaLifeO && vI == beaLife) {
                        varCore = varCore&~0x000007ff | 0x00000a0e;
                        continue;
                    //If it otherwise changed/our life changed, it's not monitoring our life
                    } else if (changed || beaLife != beaLifeO) {
                        varInfo |= 0x00001000;
                    }
                }
                /*
                //As there's actually 0 utility for this, I've disabled it
                //If it feels like a state is detected during scraping, instead switch to the pattern of [state+iter]
                asm("__varSetup_IntLoop_Curr_BeaStateScrutiny:");
                //If we haven't disproven this as a variable responsible for monitoring our stateno, but we don't know its type, and it's non-zero
                if (!(varInfo&0x00002000) && !type && vI != 0 && changed) {
                    //If it happens to synchronize exactly
                    if (vIO == 0 && vI == beaState) {
                        varCore = varCore&~0x000007ff | 0x00000a0f;
                        continue;
                    //In all other cases, it's not monitoring our states
                    } else {
                        varInfo |= 0x00002000;
                    }
                }
                */
    
                /////////////////////////////
                //GameTime Formula Checksum
                /////////////////////////////
                //GameTime Calculation Assistance Buffers
                int vIL[5] = {dataStorage[32*v+3], dataStorage[32*v+2], dataStorage[32*v+1], dataStorage[32*v+0], var[v]};
                float* vFL = (float*)(vIL);
                int formTemplate[16]; float* formTemplateF = (float*)(formTemplate);
                memcpy(formTemplate, formStorage+32*v, 64); //Initialize to what we currently have
    
                asm("__varSetup_IntLoop_Curr_GTChecksum:");
                if (minusThree && type == 0x01) {
                    asm("__varSetup_IntLoop_Curr_GTChecksum_MainStasis:");
                    if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4])) { //Main Formula Stasis
                        formStorage[32*v+17] += 1; //wait for modulus stasis
                        varCore = varCore | 0x00000800*(((varInfo+=0x00010000*((varInfo&0x000f0000)<0x000f0000))&0x000f0000)==0x000f0000);
                        if ((varCore&0x00000810) == 0x00000800) { //Locked successfully for the first time
                            varCore |= 0x00000010;
                            if (formStorage[32*v+12] < 0 || (formStorage[32*v+13]|formStorage[32*v+14]|formStorage[32*v+15])&0x00000003) { //Impossible to accurately calc specials
                                formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                continue;
                            }
                            //The modulus in this case is dependent on both b and c in the expression bt**2+ct+d
                            //t**2 has a default %65536, t has no mod
                            //Determine the least significant bit and divide 2**32 by it as appropriate (done by inverting the bits after a right shift)
                            int rootGuess;
                            if (formStorage[32*v+13] < 0 && formStorage[32*v+1] != 0) { //Quadratic
                                asm("__varSetup_IntLoop_Curr_GTChecksum_RootSeekQ:");
                                int coeff = formStorage[32*v+2]*formStorage[32*v+2] - 4*formStorage[32*v+1]*formStorage[32*v+3];
                                if (coeff < 0) { //Complex roots not supported sorry :(
                                    formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                    continue;
                                }
                                if (coeff == 0) { //Trivial case
                                    rootGuess = -formStorage[32*v+2]/(2*formStorage[32*v+1]);
                                    if (assembleFormulaI(target,formTemplate,dataStorage,rootGuess) != 0) { //Guess didn't work that's an L
                                        formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                        continue;
                                    }
                                    formStorage[32*v+20] = rootGuess; //Store the root of this equation
                                    //Modulus in this case refers to the curvature * 65536 (as 65536*65536 == 0 for 32-bit integers)
                                    dword rootModulus = (formStorage[32*v+1]<<16&-(formStorage[32*v+1]<<16)) >> 1;
                                    rootModulus = (rootModulus>>1&0x55555555) | (rootModulus&0x55555555)<<1;
                                    rootModulus = (rootModulus>>2&0x33333333) | (rootModulus&0x33333333)<<2;
                                    rootModulus = (rootModulus>>4&0x0f0f0f0f) | (rootModulus&0x0f0f0f0f)<<4;
                                    rootModulus = (rootModulus>>8&0x00ff00ff) | (rootModulus&0x00ff00ff)<<8;
                                    rootModulus = rootModulus>>16 | rootModulus<<16;
                                    formStorage[32*v+23] = rootModulus|1; //single root equations will always have a suitable modulus
                                } else {
                                    bool setFlag = false;
                                    dword rootModulus = 0;
                                    rootGuess = (sqrt(coeff)-formStorage[32*v+2])/(2*formStorage[32*v+1]); //note: -2147483648*gametime**2 is equivalent to -2147483648*gametime and no solutions to 2x=-1 exist; won't be picked up
                                    if (assembleFormulaI(target,formTemplate,dataStorage,rootGuess) == 0) { //Should work
                                        setFlag = true;
                                        rootModulus |= 1;
                                        formStorage[32*v+20] = rootGuess;
                                    }
                                    rootGuess = (-sqrt(coeff)-formStorage[32*v+2])/(2*formStorage[32*v+1]);
                                    if (assembleFormulaI(target,formTemplate,dataStorage,rootGuess) == 0) { //Should work
                                        setFlag = true;
                                        rootModulus |= 2;
                                        formStorage[32*v+21] = rootGuess;
                                    } else if (!setFlag) { //Nothing worked
                                        formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                        continue;
                                    }
                                    formStorage[32*v+23] = (setFlag ? rootModulus&0x00000007 : -1); //double root equations will NEVER have a suitable modulus
                                }
                            } else { //Linear
                                asm("__varSetup_IntLoop_Curr_GTChecksum_RootSeekL:");
                                rootGuess = SAFEDIV(-formStorage[32*v+3],formStorage[32*v+2]); //well we could certainly see -gametime-2147483648
                                if (assembleFormulaI(target,formTemplate,dataStorage,rootGuess) != 0) { //Guess didn't work that's an L
                                    formStorage[32*v+23] = -1; //Modulus > -1 (DNE)
                                    continue;
                                }
                                //Modulus in this case refers to the slope
                                dword rootModulus = (formStorage[32*v+2]&-formStorage[32*v+2]) >> 1;
                                rootModulus = (rootModulus>>1&0x55555555) | (rootModulus&0x55555555)<<1;
                                rootModulus = (rootModulus>>2&0x33333333) | (rootModulus&0x33333333)<<2;
                                rootModulus = (rootModulus>>4&0x0f0f0f0f) | (rootModulus&0x0f0f0f0f)<<4;
                                rootModulus = (rootModulus>>8&0x00ff00ff) | (rootModulus&0x00ff00ff)<<8;
                                rootModulus = rootModulus>>16 | rootModulus<<16;
                                formStorage[32*v+20] = rootGuess; //Store the root of this equation
                                formStorage[32*v+23] = rootModulus|1; //single root equations will ALWAYS have a suitable modulus
                            }
                        } else {
                            asm("__varSetup_IntLoop_Curr_GTChecksum_CoeffDeviation:");
                            //StateNo Suspicion Clearance
                            if (dataStorage[32*110+0] != dataStorage[32*110+26]) {
                                formStorage[32*v+13] &= ~0x00010000;
                                formStorage[32*v+14] &= ~0x00010000;
                                formStorage[32*v+15] &= ~0x00010000;
                            }
                            //PrevStateNo Suspicion Clearance
                            if (dataStorage[32*111+0] != dataStorage[32*111+26]) {
                                formStorage[32*v+13] &= ~0x00020000;
                                formStorage[32*v+14] &= ~0x00020000;
                                formStorage[32*v+15] &= ~0x00020000;
                            }
                            //StateTime Suspicion Clearance
                            if ((dword)(dataStorage[32*112+26]) < 2) {
                                formStorage[32*v+14] &= ~0x00040000;
                            }
                        }
                        continue;
                    }
                    //Time suspicion confirmation
                    asm("__varSetup_IntLoop_Curr_GTChecksum_TimeSwitch:");
                    if ((formStorage[32*v+12]|formStorage[32*v+13]) >= 0 && formStorage[32*v+14]&0x00040000) { //For now, only allow linear equations
                        formStorage[32*v+14] &= ~0x00040000; //Clearance
                        int cI = formStorage[32*v+2];
                        //Standard
                        if (dataStorage[32*112+12] == 1 && dvI == -cI*(dataStorage[32*112+13]-1)) { //Changed by exactly Time in the opposite direction (PrevTime+1 - 1)
                            //Convert to MainInfo
                            //Offset = the assumed value @ time=0; currently, time is 1, so it's simple
                            memset(formStorage+32*v, 0, 96);
                            formStorage[32*v+0] = cI;
                            formStorage[32*v+1] = vIL[4] - cI;
                            varCore = varCore&~0x000007ff | 0x00000a02;
                            continue;
                        }
                        //-3
                        if ((dword)(dataStorage[32*112+26]) < 2 && dvI == -cI*(dataStorage[32*112+0]-dataStorage[32*112+26])) { //Changed as appropriate
                            //Convert to MainInfo
                            //Offset = the assumed value @ time=0; if time=1 we subtract 1, otherwise subtract 0
                            memset(formStorage+32*v, 0, 96);
                            formStorage[32*v+0] = cI;
                            formStorage[32*v+1] = vIL[4] - cI*dataStorage[32*112+26];
                            varCore = varCore&~0x000007ff | 0x00000a05;
                            continue;
                        }
                    }
                    { //Local scope
                        bool stasisReached = false;
                        int f = 0;
                        //Even though int is less compicated than float, use a loop for state suspicion shuffling
                        do {
                            //&1: Agree with d's state suspicion
                            //&2: Agree with c's state suspicion
                            //&4: Agree with b's state suspicion
                            //StateNo/PrevStateNo suspicion confirmation
                            if (f&0x00000004) { //Term existence (because of the bottom chunk)
                                asm("__varSetup_IntLoop_Curr_GTChecksum_SusLoop_StateSuspicionQ:");
                                if (formStorage[32*v+13]&0x00010000) {
                                    formTemplate[1] = (formTemplate[1]<0 ? -1 : 1);
                                    formTemplate[5] = 0x00000bf4|110*0x00010000;
                                    formTemplate[13] = formTemplate[13]&~0x00010000|0x0000000a;
                                } else if (formStorage[32*v+13]&0x00020000) {
                                    formTemplate[1] = (formTemplate[1]<0 ? -1 : 1);
                                    formTemplate[5] = 0x00000bf8|111*0x00010000;
                                    formTemplate[13] = formTemplate[13]&~0x00020000|0x0000000a;
                                }
                            }
                            if (f&0x00000002) { //Term existence (because of the bottom chunk)
                                asm("__varSetup_IntLoop_Curr_GTChecksum_SusLoop_StateSuspicionL:");
                                if (formStorage[32*v+14]&0x00010000) {
                                    formTemplate[2] = (formTemplate[2]<0 ? -1 : 1);
                                    formTemplate[6] = 0x00000bf4|110*0x00010000;
                                    formTemplate[14] = formTemplate[14]&~0x00010000|0x0000000a;
                                } else if (formStorage[32*v+14]&0x00020000) {
                                    formTemplate[1] = (formTemplate[2]<0 ? -1 : 1);
                                    formTemplate[6] = 0x00000bf8|111*0x00010000;
                                    formTemplate[14] = formTemplate[14]&~0x00020000|0x0000000a;
                                }
                            }
                            if (f&0x00000001) { //Term existence (because of the bottom chunk)
                                asm("__varSetup_IntLoop_Curr_GTChecksum_SusLoop_StateSuspicionN:");
                                if (formStorage[32*v+15]&0x00010000) {
                                    formTemplate[3] = (formTemplate[3]<0 ? -1 : 1);
                                    formTemplate[7] = 0x00000bf4|110*0x00010000;
                                    formTemplate[15] = formTemplate[15]&~0x00010000|0x0000000a;
                                } else if (formStorage[32*v+15]&0x00020000) {
                                    formTemplate[3] = (formTemplate[3]<0 ? -1 : 1);
                                    formTemplate[7] = 0x00000bf8|111*0x00010000;
                                    formTemplate[15] = formTemplate[15]&~0x00020000|0x0000000a;
                                }
                            }
                            //Break here if stasis is restored
                            if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4])) {
                                stasisReached = true;
                                break;
                            }
                            memcpy(formTemplate,formStorage+32*v,64); //Restore
                            if (!f && (formStorage[32*v+12]|formStorage[32*v+13]) >= 0) { break; }
                            f++;
                        } while (f < 8);
                        if (stasisReached) {
                            memcpy(formStorage+32*v,formTemplate,64);
                            varCore = varCore | 0x00000800*(((varInfo+=0x00010000*((varInfo&0x000f0000)<0x000f0000))&0x000f0000)==0x000f0000);
                            continue;
                        } else { //Nullify currently existing suspicions
                            if (dataStorage[32*110+0] != dataStorage[32*110+26]) {
                                formStorage[32*v+13] &= ~0x00010000;
                                formStorage[32*v+14] &= ~0x00010000;
                                formStorage[32*v+15] &= ~0x00010000;
                            }
                            if (dataStorage[32*111+0] != dataStorage[32*111+26]) {
                                formStorage[32*v+13] &= ~0x00020000;
                                formStorage[32*v+14] &= ~0x00020000;
                                formStorage[32*v+15] &= ~0x00020000;
                            }
                            if ((dword)(dataStorage[32*112+26]) < 2) {
                                formStorage[32*v+14] &= ~0x00040000;
                            }
                        }
                    }
                    memcpy(formTemplate,formStorage+32*v,64);
                    //Simple Modulus Processing
                    asm("__varSetup_IntLoop_Curr_GTChecksum_ModulusCheck:");
                    if (formStorage[32*v+17] < 256 && (formStorage[32*v+12]|formStorage[32*v+13]) >= 0) { //For now, only allow linear equations
                        bool modOK = false;
                        formStorage[32*v+17] = 256; //Don't check again
                        for (int t = 3; t <= 256; t++) { //upon overflow it will end; %2 is handled within initialization; %3~4 might not be
                            asm("__varSetup_IntLoop_Curr_GTChecksum_ModulusLoop:");
                            if (gt[4] < t) { break; } //We haven't hit t yet so how could it be %t?
                            //Formula of type c*(t%m)+d
                            //Our initial formula most likely is of the form c*(t-t/m*m)+d = c*t+(d-t/m*m*c)
                                //i.e. gt=999, formula = 3*(gametime%5)+7 => 3*(gametime-995)+7 = 3*gametime-2978
                            //Correct with our stored gametimes
                            formTemplate[2] = formStorage[32*v+2];
                            formTemplate[3] = formStorage[32*v+3] + formStorage[32*v+2]*(formStorage[32*v+16]-formStorage[32*v+16]%t);
                            formTemplate[10] = t; //Modulus
                            formTemplate[11] = 0; //Carryover reset
                            if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) && //Main Formula Stasis Restored
                                vIL[3] == assembleFormulaI(target,formTemplate,dataStorage,gt[3]) &&
                                vIL[2] == assembleFormulaI(target,formTemplate,dataStorage,gt[2]) &&
                                vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                modOK = true;
                                break;
                            }
                            //Formula of type c*t+t%m+d
                            //Our initial formula most likely is of the form c*t+(t-t/m*m)+d = (c+1)*t+(d-t/m*m)
                                //i.e. gt=999, formula = 3*gametime+(gametime%5)+7 => 3*gametime+(gametime-995)+7 = 4*gametime-988
                            //Correct with our stored gametimes
                            formTemplate[2] -= 1;
                            formTemplate[3] = formStorage[32*v+3] + (formStorage[32*v+16]-formStorage[32*v+16]%t);
                            formTemplate[10] = 0;
                            formTemplate[11] = t; //Modulus
                            if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) && //Main Formula Stasis Restored
                                vIL[3] == assembleFormulaI(target,formTemplate,dataStorage,gt[3]) &&
                                vIL[2] == assembleFormulaI(target,formTemplate,dataStorage,gt[2]) &&
                                vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                modOK = true;
                                break;
                            }
                            //Formula of type c*t-t%m+d (precaution)
                            formTemplate[2] += 2;
                            formTemplate[3] -= 2*(formStorage[32*v+16]-formStorage[32*v+16]%t);
                            formTemplate[11] *= -1; 
                            if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) && //Main Formula Stasis Restored
                                vIL[3] == assembleFormulaI(target,formTemplate,dataStorage,gt[3]) &&
                                vIL[2] == assembleFormulaI(target,formTemplate,dataStorage,gt[2]) &&
                                vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                                (!gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0]))
                            ) {
                                modOK = true;
                                break;
                            }
                            asm("__varSetup_IntLoop_Curr_GTChecksum_ModulusNext:");
                        }
                        if (modOK) {
                            asm("__varSetup_IntLoop_Curr_GTChecksum_ModulusFinal:");
                            memcpy(formStorage+32*v,formTemplate,64);
                            varCore = varCore | 0x00000800*(((varInfo+=0x00010000*((varInfo&0x000f0000)<0x000f0000))&0x000f0000)==0x000f0000);
                            continue;
                        }
                        memcpy(formTemplate,formStorage+32*v,64);
                    }
                    //As no other reparatory measures can be made past this point, we abandon here
                    asm("__varSetup_IntLoop_Curr_GTChecksum_Abandon:");
                    memset(formTemplate, 0, 64);
                    memset(formStorage+32*v, 0, 96);
                    varCore = varCore&~0x000007ff | 0x40000000*(((varInfo=(varInfo&~0x000f0000)+0x00100000*((varInfo&0x00f00000)<0x00f00000))&0x00f00000)>=0x00a00000);
                    type = 0x00; subtype = 0x00;
                }
    
                /////////////////////////////
                //Damage Storage Suspicion
                /////////////////////////////
                //NOTE: While we don't need this for SCRAPING, we may prefer having it for TAMPERING later. Keep that in mind!
                asm("__varSetup_IntLoop_Curr_DamageStorageScrutiny:");
                //If we haven't disproven this as a scraping variable but we don't know its type
                if (!(varCore&0x10000000) && !type && //ScrapingDNE
                //We connected a scraping hit
                    globDamaged && flags&0x00000001 && 
                //It changed from 0 to exactly the damage dealt
                    !vIO && vI == damageS
                //Assume that it's damage storage for the time being
                ) {
                    varCore = (varCore&~0x00000fff|0x00000301);
                    continue;
                }
                /////////////////////////////
                //Proper Enemy Life Synchronization
                /////////////////////////////
                asm("__varSetup_IntLoop_Curr_ProperLifeSync:");
                //If the type is a proper sync variable, lock it contently if we see that it's a stable synchronization
                if (type == 0x03 && subtype == 0x04 && !(suplFlags&0x02)) {
                    if (life == vI*formStorage[32*v+2] + formStorage[32*v+3]) {
                        if ((++formStorage[32*v+4]) >= 100) varCore |= 0x00000800;
                    //If OoR but life is still a reasonable value, abandon completely
                    } else if (life > 1 && life < lifemax-1) {
                        varCore &= ~0x00000fff;
                        if (suplFlags&0x04) { //It's fine; we can make the swap back to damage-influenced
                            varCore |= 0x0000030f;
                        } else { //Scraping-independent variable; eliminate
                            varCore |= 0x10000000;
                        }
                    }
                    continue;
                }
                //If we haven't disproven it as a scraping variable but we don't know its type
                if (!(varCore&0x10000000) && !type &&
                //a life differential exists and we're checking for it,
                    dL && flags&0x00000002 && //LifeScreeningOK
                //the differential happens to be equivalent
                    abs(dL) == abs(dvI)
                //Assume it's life-synchronized
                ) {
                    varCore = (varCore&~0x00000fff|0x00000304);
                    formStorage[32*v+0] = dataStorage[32*v+22];
                    formStorage[32*v+4] = 0; //Begin Timer
                    //life = slope*var + const
                    //life = 9000 - sysvar(1) | slope = -1, const = 9000 (startVal = 8000, nextVal = 8010; const = startVal+life)
                    //life = sysvar(1) - 4000 | slope = 1, const = -4000 (startVal = 5000, nextVal = 4995; const = -startVal+life)
                    formStorage[32*v+1] = formStorage[32*v+0] + lifemax*(1-2*(dvI<0));
                    formStorage[32*v+2] = ((dvI<0)!=(dL<0) ? -1 : 1);
                    formStorage[32*v+3] = -vIO*formStorage[32*v+2] + lifeO;
                    continue;
                }
                /////////////////////////////
                //1F Damage Storage Confirmation
                /////////////////////////////
                asm("__varSetup_IntLoop_Curr_1FStorage:");
                //If we haven't disproven this as a scraping variable, nor have we performed the 1F check
                if (!(varCore&0x10000010) && //ScrapingDNE|1FElapsed
                //We're screening for either a damage storage or damage-influenced variable
                    type == 0x03 && (subtype == 0x01 || subtype == 0x0f) && 
                //We did NOT connect a scraping hit, and the value changed
                    !(flags&0x00000001)
                ) {
                    //If the value changed to 0 from a non-microscopic value, this is 1F storage
                    if (changed && vI == 0 && abs(dvI) > 1) varCore = (varCore&~0x00000fff|0x00000b02);
                    varCore |= 0x00000010; //1F confirmation
                    continue;
                }
                /////////////////////////////
                //Damage Storage Type Determination
                /////////////////////////////
                asm("__varSetup_IntLoop_Curr_StorageTypeDef:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We're screening for a damage storage variable
                    type == 0x03 && subtype == 0x01 && 
                //We connected a scraping hit, and the damage is different from what it was last time
                    globDamaged && flags&0x00000001 && damageS != dataStorage[32*118+21]
                ) {
                    //If it changed to this damage, it stores as the most recent damage taken regardless of origin, and can be locked.
                    if (changed && vI == damageS) {
                        varCore |= 0x00000800; continue;
                    //If it didn't change at all, lock as Quasi-permanent.
                    } else if (!changed) {
                        varCore = (varCore&~0x00000fff|0x00000b03); continue;
                    //Otherwise, it's damage-influenced in some way. We just don't know how.
                    } else {
                        varCore = (varCore&~0x00000fff|0x0000030f);
                        subtype = 0x0f; //continue;
                    }
                }
                /////////////////////////////
                //Invuln Timer Processing
                /////////////////////////////
                asm("__varSetup_IntLoop_Curr_InvulnScrutiny:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We're screening for a timer/damage-influenced variable
                    type == 0x03 && (subtype == 0x06 || subtype == 0x0f) &&
                //It changed by a value that is equivalent to the time since it was last changed
                //We COULD settle with -1, however, this may be a problem vs opponents like Claroots, who subtract 9 every 9F for damage cancelling
                //We COULD settle with trending towards 0, however, this may be a problem vs opponents like Regisseur, who instead cap the variable
                    abs(dvI) == formStorage[32*v+13]+1
                //This feels like a timer. Switch to it and calculate
                ) {
                    if (subtype != 0x06) {
                        varCore = (varCore&~0x00000fff|0x00000306);
                        formStorage[32*v+0] = 0;
                        formStorage[32*v+1] = abs(vI-dataStorage[32*v+22]); //Store the maximum timer here
                        formStorage[32*v+6] = 0; //Init Timer
                        formStorage[32*v+7] = (formStorage[32*v+13] < 10 ? 10 : formStorage[32*v+13]); //Init Max Timer Stasis (min=10 for Claroots)
                    } else {
                        if (formStorageF[32*v+1] < abs(vI-dataStorage[32*v+22])) formStorageF[32*v+1] = abs(vI-dataStorage[32*v+22]);
                        if ((formStorage[32*v+6] += formStorage[32*v+13]+1) >= 600) varCore |= 0x00000800; //Lock if it seems like enough time has passed
                    }
                    continue;
                }
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We're screening for a timer variable
                    type == 0x03 && subtype == 0x06
                ) {
                    //If it either increased to the maximum value again, or it stayed at 0, it's still technically valid
                    if (!changed && vI == 0 || vI == formStorage[32*v+1]) {
                        if ((formStorage[32*v+6] += 1) >= 600) { varCore |= 0x00000800; } //Lock if it seems like enough time has passed
                        continue;
                    //If it changed in an otherwise unexpected way, or the stasis time exceeded its previous maximum value, reset it to damage-influenced
                    } else if (changed || formStorage[32*v+13] > formStorage[32*v+7]) {
                        varCore = (varCore&~0x00000fff|0x0000030f);
                        subtype = 0x0f;
                    }  
                }
                /////////////////////////////
                //Damage-Influenced Suspicion/Confirmation
                /////////////////////////////
                asm("__varSetup_IntLoop_Curr_InfluenceScrutiny:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We either don't know its type, or we're screening for a damage storage/damage-influenced variable
                    (!type || type == 0x03 && subtype == 0x0f) &&
                //We connected a scraping hit, causing the value to change
                    globDamaged && changed && flags&0x00000001
                //Assume that it's damage-influenced for the time being (and potentially lock it)
                ) {
                    varCore = (varCore&~0x00000fff|0x0000030f|0x00000800*!!type);
                    formStorage[32*v+0] = dataStorage[32*v+22];
                    //adhere to the KISS principle
                    //if it went up, then assume its max is this+lifemax
                    //if it went down, assume its min is this-lifemax
                    formStorage[32*v+1] = formStorage[32*v+0] + lifemax*(1-2*(dvI<0));
                    formStorage[32*v+2] = (dvI<0 ? -1 : 1);
                    //If 0 comes before lifemax, assume that it's going to end in 0 instead
                    if ((formStorage[32*v+1] < 0) != (formStorage[32*v+0] < 0)) { formStorage[32*v+1] = 0; }
                    continue;
                }
                /////////////////////////////
                //Anti-Flag Countermeasures
                /////////////////////////////
                asm("__varSetup_IntLoop_Curr_AntiFlag:");
                //If we haven't disproven this as a scraping variable
                if (!(varCore&0x10000000) && //ScrapingDNE
                //We either don't know its type, we're screening for a damage-influenced variable
                    type == 0x03 && subtype == 0x0f &&
                //We previously connected a scraping hit, but not this frame, even though the value changed anyway
                    flags&0x00000001 && !globDamaged && (damageSO || fallDamageSO) && changed &&
                //The value simply returned to its previous state (that being the state it was in 2F ago)
                    vI == dataStorage[32*v+21]
                //This most likely isn't actually important enough for me to care about! Disable it.
                ) {
                    varCore = (varCore&~0x00000fff|0x10000000);
                    continue;
                }
                //Past this point is gametime formula acquisition; if we have a type or have disproven it, skip
                //Also will be skipped if we haven't obtained 2 values through -3 yet
                //Lastly, as a precautionary measure, if we double-tapped for ANY reason, skip to prevent a crash when trying to divide by delta products
                asm("__varSetup_IntLoop_Curr_GTHeader:");
                if (!minusThree || !gt[2] || type || varCore&0x40000000 || !del32 || gt[1] && !delprod321 || gt[0] && !delprod3210) { continue; }
    
                //Immediately declare failure during RS=2 if, for reasons unknown, the past 3 variables have matched either RSTime or MatchTime
                //This is most likely not a gametime variable, but a timer variable
                if (roundstate == 2 && (
                        RSTime>2 && abs(vIL[2]) >= RSTime-3 && abs(vIL[2]) <= RSTime-2 && abs(vIL[3]) == abs(vIL[2])+1 && abs(vIL[4]) == abs(vIL[3])+1 ||
                        matchTime>2 && abs(vIL[2]) >= matchTime-3 && abs(vIL[2]) <= matchTime-2 && abs(vIL[3]) == abs(vIL[2])+1 && abs(vIL[4]) == abs(vIL[3])+1
                )) {
                    varCore |= 0x40000000;
                    continue;
                }
    
                ///////////////////////////
                //Linear GameTime Formula
                ///////////////////////////
                //It is impossible for all inputs to be alike, so we need to find a discrepancy
                asm("__varSetup_IntLoop_Curr_GTLinear:");
                if (!(vIL[3] == vIL[2])) {
                    int cI = (vIL[3] - vIL[2])/del32;
                    int dI = vIL[3] - cI*gt[3];
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[14] = 0x80040000; //Term Existence Flags (for completely standard formulae like this, suspect time)
                    formTemplate[15] = 0x80000800;
                    if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                        (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1])) &&
                        (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                    ) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+16] = gt[3]; //Save this gametime for modulo processing later
                        //StateNo/PrevStateNo suspicion
                        if (abs(cI) == dataStorage[32*110+0] && abs(cI) == dataStorage[32*110+1] && (!gt[1] || abs(cI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*110+3]) && abs(cI) == dataStorage[32*110+26]) {
                            formStorage[32*v+14] |= 0x00010000;
                        } else if (abs(cI) == dataStorage[32*111+0] && abs(cI) == dataStorage[32*111+1] && (!gt[1] || abs(cI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*111+3]) && abs(cI) == dataStorage[32*111+26]) {
                            formStorage[32*v+14] |= 0x00020000;
                        }
                        if (abs(dI) == dataStorage[32*110+0] && abs(dI) == dataStorage[32*110+1] && (!gt[1] || abs(dI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*110+3]) && abs(dI) == dataStorage[32*110+26]) {
                            formStorage[32*v+15] |= 0x00010000;
                        } else if (abs(dI) == dataStorage[32*111+0] && abs(dI) == dataStorage[32*111+1] && (!gt[1] || abs(dI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*111+3]) && abs(dI) == dataStorage[32*111+26]) {
                            formStorage[32*v+15] |= 0x00020000;
                        }
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    //Low Modulus Processing (in a way that it's impossible to satisfy the checksum)
                    //%2 case
                    asm("__varSetup_IntLoop_Curr_GTMod2:");
                    char gtM2; char gtM3; char delM32; int vILi2; int vILi3;
                    if (gt[3]%2 != gt[2]%2) {
                        gtM2 = 0; gtM3 = 1; delM32 = 1; //Locked to these
                        vILi2 = (gt[3]%2 ? vIL[2] : vIL[3]);
                        vILi3 = (gt[3]%2 ? vIL[3] : vIL[2]);
                        //v=c*(gametime%2)+d
                        cI = (vILi3 - vILi2)/delM32;
                        dI = vILi3 - cI*gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[10] = 2;
                        formTemplate[14] = 0x80000000; //Remove time suspicion
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1])) &&
                            (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        //v=c*gametime+gametime%2+d
                        cI = (vILi3 - vILi2 - delM32)/delM32;
                        dI = vILi3 - cI*gt[3] - gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[10] = 0;
                        formTemplate[11] = 2;
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1])) &&
                            (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        //v=c*gametime-gametime%2+d
                        cI = (vILi3 - vILi2 + delM32)/delM32;
                        dI = vILi3 - cI*gt[3] + gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[11] = -2;
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1])) &&
                            (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0])) //conditions are quasi-identical so the compiler optimizes
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        formTemplate[11] = 0;
                    }
                    //%3 case
                    asm("__varSetup_IntLoop_Curr_GTMod3:");
                    if (gt[1] && gt[3]%3 != gt[2]%3) {
                        gtM2 = (gt[3]%3 > gt[2]%3 ? gt[2]%3 : gt[3]%3);
                        gtM3 = (gt[3]%3 > gt[2]%3 ? gt[3]%3 : gt[2]%3);
                        delM32 = gtM3 - gtM2;
                        vILi2 = (gt[3]%3 > gt[2]%3 ? vIL[2] : vIL[3]);
                        vILi3 = (gt[3]%3 > gt[2]%3 ? vIL[3] : vIL[2]);
                        //v=c*(gametime%3)+d
                        cI = (vILi3 - vILi2)/delM32;
                        dI = vILi3 - cI*gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[10] = 3;
                        formTemplate[14] = 0x80000000; //Remove time suspicion
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                            (!gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        //v=c*gametime+gametime%3+d
                        cI = (vILi3 - vILi2 - delM32)/delM32;
                        dI = vILi3 - cI*gt[3] - gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[10] = 0;
                        formTemplate[11] = 3;
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                            (!gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        //v=c*gametime-gametime%3+d
                        cI = (vILi3 - vILi2 + delM32)/delM32;
                        dI = vILi3 - cI*gt[3] + gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[11] = -3;
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                            (!gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0]))
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        formTemplate[11] = 0;
                    }
                    //%4 case
                    asm("__varSetup_IntLoop_Curr_GTMod4:");
                    if (gt[0] && gt[3]%4 != gt[2]%4) {
                        gtM2 = (gt[3]%4 > gt[2]%4 ? gt[2]%4 : gt[3]%4);
                        gtM3 = (gt[3]%4 > gt[2]%4 ? gt[3]%4 : gt[2]%4);
                        delM32 = gtM3 - gtM2;
                        vILi2 = (gt[3]%4 > gt[2]%4 ? vIL[2] : vIL[3]);
                        vILi3 = (gt[3]%4 > gt[2]%4 ? vIL[3] : vIL[2]);
                        //v=c*(gametime%4)+d
                        cI = (vILi3 - vILi2)/delM32;
                        dI = vILi3 - cI*gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[10] = 4;
                        formTemplate[14] = 0x80000000; //Remove time suspicion
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                            vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0])
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        //v=c*gametime+gametime%4+d
                        cI = (vILi3 - vILi2 - delM32)/delM32;
                        dI = vILi3 - cI*gt[3] - gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[10] = 0;
                        formTemplate[11] = 4;
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                            vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0])
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        //v=c*gametime-gametime%4+d
                        cI = (vILi3 - vILi2 + delM32)/delM32;
                        dI = vILi3 - cI*gt[3] + gtM3;
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[11] = -4;
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                            vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1]) &&
                            vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0])
                        ) {
                            //Save formula
                            memcpy(formStorage+32*v,formTemplate,64);
                            formStorage[32*v+17] = 256; //DNE
                            varCore = varCore&~0x000007ff | 0x00000100;
                            continue;
                        }
                        formTemplate[11] = 0;
                    }
                    memset(formTemplate,0,64);
                }
                ///////////////////////////
                //Inverting Linear Formula
                ///////////////////////////
                //It might be possible for all inputs to be alike, but we need a set that explicitly has an &1 difference
                asm("__varSetup_IntLoop_Curr_GTInversionL:");
                if (invInd[0] != -1) {
                    char i0 = invInd[0]; char i1 = invInd[1];
                    char ni0 = invNInd[0]; char ni1 = invNInd[1]; char ni2 = invNInd[2];
                    ///////////////////
                    //Linear Coefficient
                    ///////////////////
                    asm("__varSetup_IntLoop_Curr_GTInversionL_Lin:");
                    int cI = SAFEDIV((vIL[i1] - vIL[i0]), (gt[i1]*gtinv[i1] - gt[i0]*gtinv[i0]));
                    int dI = vIL[i1] - cI*gt[i1];
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[14] = 0x80000080; //Term Existence Flags + Inversion
                    formTemplate[15] = 0x80000800;
                    if (cI && (!gt[ni0] || vIL[ni0] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni0])) &&
                        (!gt[ni1] || vIL[ni1] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni1])) &&
                        (!gt[ni2] || vIL[ni2] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni2]))
                    ) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //DNE
                        //StateNo/PrevStateNo suspicion
                        if (abs(cI) == dataStorage[32*110+0] && abs(cI) == dataStorage[32*110+1] && (!gt[1] || abs(cI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*110+3]) && abs(cI) == dataStorage[32*110+26]) {
                            formStorage[32*v+14] |= 0x00010000;
                        } else if (abs(cI) == dataStorage[32*111+0] && abs(cI) == dataStorage[32*111+1] && (!gt[1] || abs(cI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*111+3]) && abs(cI) == dataStorage[32*111+26]) {
                            formStorage[32*v+14] |= 0x00020000;
                        }
                        if (abs(dI) == dataStorage[32*110+0] && abs(dI) == dataStorage[32*110+1] && (!gt[1] || abs(dI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*110+3]) && abs(dI) == dataStorage[32*110+26]) {
                            formStorage[32*v+15] |= 0x00010000;
                        } else if (abs(dI) == dataStorage[32*111+0] && abs(dI) == dataStorage[32*111+1] && (!gt[1] || abs(dI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*111+3]) && abs(dI) == dataStorage[32*111+26]) {
                            formStorage[32*v+15] |= 0x00020000;
                        }
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    ///////////////////
                    //Constant Coefficient
                    ///////////////////
                    asm("__varSetup_IntLoop_Curr_GTInversionL_Const:");
                    cI = SAFEDIV((vIL[i1] + vIL[i0]), (gt[i1] + gt[i0]));
                    dI = (vIL[i1] - cI*gt[i1])*gtinv[i1];
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[14] = 0x80000000; //Term Existence Flags + Inversion
                    formTemplate[15] = 0x80000880;
                    if (dI && (!gt[ni0] || vIL[ni0] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni0])) &&
                        (!gt[ni1] || vIL[ni1] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni1])) &&
                        (!gt[ni2] || vIL[ni2] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni2]))
                    ) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //DNE
                        //StateNo/PrevStateNo suspicion
                        if (abs(cI) == dataStorage[32*110+0] && abs(cI) == dataStorage[32*110+1] && (!gt[1] || abs(cI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*110+3]) && abs(cI) == dataStorage[32*110+26]) {
                            formStorage[32*v+14] |= 0x00010000;
                        } else if (abs(cI) == dataStorage[32*111+0] && abs(cI) == dataStorage[32*111+1] && (!gt[1] || abs(cI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*111+3]) && abs(cI) == dataStorage[32*111+26]) {
                            formStorage[32*v+14] |= 0x00020000;
                        }
                        if (abs(dI) == dataStorage[32*110+0] && abs(dI) == dataStorage[32*110+1] && (!gt[1] || abs(dI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*110+3]) && abs(dI) == dataStorage[32*110+26]) {
                            formStorage[32*v+15] |= 0x00010000;
                        } else if (abs(dI) == dataStorage[32*111+0] && abs(dI) == dataStorage[32*111+1] && (!gt[1] || abs(dI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*111+3]) && abs(dI) == dataStorage[32*111+26]) {
                            formStorage[32*v+15] |= 0x00020000;
                        }
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    ///////////////////
                    //Whole-Area Inversion
                    ///////////////////
                    asm("__varSetup_IntLoop_Curr_GTInversionL_Full:");
                    cI = SAFEDIV((vIL[i1] + vIL[i0]), (gt[i1]*gtinv[i1] + gt[i0]*gtinv[i0])); //there is a risk of the denominator being abnormal
                    dI = vIL[i1]*gtinv[i1] - cI*gt[i1];
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[14] = 0x80000080; //Term Existence Flags + Inversion
                    if (cI && dI && (!gt[ni0] || vIL[ni0] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni0])) &&
                        (!gt[ni1] || vIL[ni1] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni1])) &&
                        (!gt[ni2] || vIL[ni2] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni2]))
                    ) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //DNE
                        //StateNo/PrevStateNo suspicion
                        if (abs(cI) == dataStorage[32*110+0] && abs(cI) == dataStorage[32*110+1] && (!gt[1] || abs(cI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*110+3]) && abs(cI) == dataStorage[32*110+26]) {
                            formStorage[32*v+14] |= 0x00010000;
                        } else if (abs(cI) == dataStorage[32*111+0] && abs(cI) == dataStorage[32*111+1] && (!gt[1] || abs(cI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(cI) == dataStorage[32*111+3]) && abs(cI) == dataStorage[32*111+26]) {
                            formStorage[32*v+14] |= 0x00020000;
                        }
                        if (abs(dI) == dataStorage[32*110+0] && abs(dI) == dataStorage[32*110+1] && (!gt[1] || abs(dI) == dataStorage[32*110+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*110+3]) && abs(dI) == dataStorage[32*110+26]) {
                            formStorage[32*v+15] |= 0x00010000;
                        } else if (abs(dI) == dataStorage[32*111+0] && abs(dI) == dataStorage[32*111+1] && (!gt[1] || abs(dI) == dataStorage[32*111+2]) && (!gt[1] || !gt[0] || abs(dI) == dataStorage[32*111+3]) && abs(dI) == dataStorage[32*111+26]) {
                            formStorage[32*v+15] |= 0x00020000;
                        }
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    memset(formTemplate,0,64);
                }
        
                ///////////////////////////
                //Standard Quadratic Formula
                ///////////////////////////
                //It might be possible for all inputs to be alike, but we need a set that explicitly has an &1 difference
                asm("__varSetup_IntLoop_Curr_GTQuadratic:");
                if (gt[1] && !(vIL[3] == vIL[2] && vIL[2] == vIL[1])) {
                    int bI = (vIL[3]*del21 - vIL[2]*del31 + vIL[1]*del32)/delprod321;
                    int cI = (vIL[3] - vIL[2])/del32 - bI*sum32;
                    int dI = vIL[3] - bI*gt3S - cI*gt[3];
                    formTemplate[1] = bI;
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[13] = 0x80000000; //Term Existence Flags
                    formTemplate[14] = 0x80000800;
                    formTemplate[15] = 0x80001000;
                    if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4]) &&
                        (!gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0]))
                    ) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //Modulus not supported
                        //StateNo/PrevStateNo suspicion
                        if (abs(bI) == dataStorage[32*110+0] && abs(bI) == dataStorage[32*110+1] && abs(bI) == dataStorage[32*110+2] && (!gt[0] || abs(bI) == dataStorage[32*110+3]) && abs(bI) == dataStorage[32*110+26]) {
                            formStorage[32*v+13] |= 0x00010000;
                        } else if (abs(bI) == dataStorage[32*111+0] && abs(bI) == dataStorage[32*111+1] && abs(bI) == dataStorage[32*111+2] && (!gt[0] || abs(bI) == dataStorage[32*111+3]) && abs(bI) == dataStorage[32*111+26]) {
                            formStorage[32*v+13] |= 0x00020000;
                        }
                        if (abs(cI) == dataStorage[32*110+0] && abs(cI) == dataStorage[32*110+1] && abs(cI) == dataStorage[32*110+2] && (!gt[0] || abs(cI) == dataStorage[32*110+3]) && abs(cI) == dataStorage[32*110+26]) {
                            formStorage[32*v+14] |= 0x00010000;
                        } else if (abs(cI) == dataStorage[32*111+0] && abs(cI) == dataStorage[32*111+1] && abs(cI) == dataStorage[32*111+2] && (!gt[0] || abs(cI) == dataStorage[32*111+3]) && abs(cI) == dataStorage[32*111+26]) {
                            formStorage[32*v+14] |= 0x00020000;
                        }
                        if (abs(dI) == dataStorage[32*110+0] && abs(dI) == dataStorage[32*110+1] && abs(dI) == dataStorage[32*110+2] && (!gt[0] || abs(dI) == dataStorage[32*110+3]) && abs(dI) == dataStorage[32*110+26]) {
                            formStorage[32*v+15] |= 0x00010000;
                        } else if (abs(dI) == dataStorage[32*111+0] && abs(dI) == dataStorage[32*111+1] && abs(dI) == dataStorage[32*111+2] && (!gt[0] || abs(dI) == dataStorage[32*111+3]) && abs(dI) == dataStorage[32*111+26]) {
                            formStorage[32*v+15] |= 0x00020000;
                        }
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    memset(formTemplate,0,64);
                }
        
                ///////////////////////////
                //Inverting Quadratic Formula
                ///////////////////////////
                //It is impossible for all inputs to be alike, so we need to find a discrepancy
                asm("__varSetup_IntLoop_Curr_GTInversionQ:");
                if (invIndQ[0] != -1) {
                    char i0 = invIndQ[0]; char i1 = invIndQ[1]; char i2 = invIndQ[2];
                    char ni0 = invNIndQ[0]; char ni1 = invNIndQ[1];
                    ///////////////////
                    //Quadratic Coefficient
                    ///////////////////
                    asm("__varSetup_IntLoop_Curr_GTInversionQ_Quad:");
                    int bI = SAFEDIV((vIL[i2]*deli10Q - vIL[i1]*deli20Q + vIL[i0]*deli21Q), (deli20Q*(gt[i1]*sumi21Q-gt[i0]*deli21Q)));
                    int cI = SAFEDIV((vIL[i2] - vIL[i0]), deli20Q) - bI*sumi20Q;
                    int dI = vIL[i2] - bI*gti2SQ - cI*gt[i2];
                    bI *= gtinv[i2];
                    formTemplate[1] = bI;
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[13] = 0x80000080; //Term Existence Flags + Inversion
                    formTemplate[14] = 0x80000800;
                    formTemplate[15] = 0x80001000;
                    if (bI && (!gt[ni0] || vIL[ni0] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni0])) &&
                        (!gt[ni1] || vIL[ni1] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni1]))
                    ) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //Modulus not supported
                        //State/Prev support may be added at a later time
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    ///////////////////
                    //Full Inversion (Lunatic_Empress_X)
                    ///////////////////
                    asm("__varSetup_IntLoop_Curr_GTInversionQ_Full:");
                    bI = SAFEDIV((vIL[i2]*deli10Q + vIL[i1]*deli20Q + vIL[i0]*deli21Q), (deli10Q*deli20Q*deli21Q));
                    cI = SAFEDIV((vIL[i2] - vIL[i0]), deli20Q) - bI*sumi20Q;
                    dI = vIL[i2] - bI*gti2SQ - cI*gt[i2];
                    bI *= gtinv[i2];
                    cI *= gtinv[i2];
                    dI *= gtinv[i2];
                    formTemplate[1] = bI;
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[14] = 0x80000880; //Term Existence Flags + Inversion
                    formTemplate[15] = 0x80001080;
                    if (bI && cI && dI && (!gt[ni0] || vIL[ni0] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni0])) &&
                        (!gt[ni1] || vIL[ni1] == assembleFormulaI(target,formTemplate,dataStorage,gt[ni1]))
                    ) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //Modulus not supported
                        //State/Prev support may be added at a later time
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    memset(formTemplate,0,64);
                }
        
                ///////////////////////////
                //Standard Cubic Formula
                ///////////////////////////
                //It is impossible for all inputs to be alike, so we need to find a discrepancy
                asm("__varSetup_IntLoop_Curr_GTCubic:");
                if (gt[0] && !(vIL[3] == vIL[2] && vIL[2] == vIL[1] && vIL[1] == vIL[0])) {
                    int aI = ((vIL[3]*del20*del21 - vIL[2]*del30*del31)*del10 + (vIL[1]*del30*del20 - vIL[0]*del31*del21)*del32)/delprod3210;
                    int bI = (vIL[3]*del21 - vIL[2]*del31 + vIL[1]*del32)/delprod321 - aI*sum321;
                    int cI = (vIL[3] - vIL[2])/del32 - aI*prod32 - bI*sum32;
                    int dI = vIL[3] - aI*gt3C - bI*gt3S - cI*gt[3];
                    formTemplate[0] = aI;
                    formTemplate[1] = bI;
                    formTemplate[2] = cI;
                    formTemplate[3] = dI;
                    formTemplate[12] = 0x80000000; //Term Existence Flags
                    formTemplate[13] = 0x80000800;
                    formTemplate[14] = 0x80001000;
                    formTemplate[15] = 0x80001800;
                    if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4])) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //Modulus not supported
                        //State/Prev coefficient suspicion will be added if it's ever deemed necessary
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue;
                    }
                    memset(formTemplate,0,64);
                }
        
                ///////////////////////////
                //Pre-to Linear Formula (var(X)*gametime)
                ///////////////////////////
                //It is STATISTICALLY impossible for all inputs to be alike, so we need to find a discrepancy
                //note that, to be safe, we're using all 5
                asm("__varSetup_IntLoop_Curr_GTPreTo:");
                if (gt[0] && !(vIL[3] == vIL[2] && vIL[2] == vIL[1] && vIL[1] == vIL[0])) {
                    bool pretoESC = false;
                    for (byte x = 0; x < 220; x++) {
                        char w = x/2; //Variable ID in w
                        if (!isHelper && x&1) { continue; } //&1 = 0 if self, 1 if root
                        if (v == w && !(x&1)) { continue; } //var(0) = 1*var(0) is a gigabrain formula
                        if ((formStorage[32*w+31]&0x00000700) == 0x00000100 && !(v < 100 && w >= 100) && ( //Prevent cross-referencing; it's either 1 or the other
                            formStorage[32*w+14]&0x00000008 && formStorage[32*w+6] == 0x00000e40+4*v || //to prevent var(39)=gametime+var(18), var(18)=-gametime+var(39) chains
                            formStorage[32*w+15]&0x00000008 && formStorage[32*w+7] == 0x00000e40+4*v //NOTE: that <100/>=100 check means that formulae like gametime+sysvar(1) will remain supported even if it would cross-reference
                        )) { continue; }
                        bool wFloat = (w >= 60 && w < 100 || w >= 105);
                        //Identical syntax to vIL/vFL
                        int wIL[5] = {dataStorage[32*w+3+8*(x&1)], dataStorage[32*w+2+8*(x&1)], dataStorage[32*w+1+8*(x&1)], dataStorage[32*w+0+8*(x&1)], (x&1 ? *(root+912+w) : var[w])};
                        float* wFL = (float*)(wIL);
                        int wIT[5]; int* wFTF = wIT;
                        if (!wIL[3] && !wIL[2] || wIL[3] == wIL[2] && wIL[2] == wIL[1] && wIL[1] == wIL[0]) { //This must also change, as to imply some sort of random factor (both inputs also cannot be 0)
                            continue;
                        }
                        if (wFloat && ((wIL[4]&0x7f800000) == 0x7f800000 || (wIL[3]&0x7f800000) == 0x7f800000 || (wIL[2]&0x7f800000) == 0x7f800000 || (wIL[1]&0x7f800000) == 0x7f800000 || (wIL[0]&0x7f800000) == 0x7f800000)) {
                            continue; //Avoid poisoning the batch with Inf/NaN
                        } else if (wFloat) {
                        //For now, I'll only support flooring with floats (very few people use ceil)
                            wFTF[0] = (int)(floor(wFL[0]))*gt[0];
                            wFTF[1] = (int)(floor(wFL[1]))*gt[1];
                            wFTF[2] = (int)(floor(wFL[2]))*gt[2];
                            wFTF[3] = (int)(floor(wFL[3]))*gt[3];
                            wFTF[4] = (int)(floor(wFL[4]))*gt[4];
                        } else {
                            wIT[0] = wIL[0]*gt[0];
                            wIT[1] = wIL[1]*gt[1];
                            wIT[2] = wIL[2]*gt[2];
                            wIT[3] = wIL[3]*gt[3];
                            wIT[4] = wIL[4]*gt[4];
                        }
                        ///////////////////
                        //Linear Coefficient (Mul)
                        ///////////////////
                        asm("__varSetup_IntLoop_Curr_GTPreTo_LinMul:");
                        int cI; int dI; int varDummy;
                        formTemplate[6] = 0x00000e40+4*w | 0x00008000*(x&1); //Offset = var(w)/fvar(w)/sysvar(w)/sysfvar(w) | root if applicable
                        formTemplate[7] = 0; //Set these in advance
                        if (wFloat ? wFTF[3] != wFTF[2] : wIT[3] != wIT[2]) { //Fringe crash avoidance
                            cI = SAFEDIV((vIL[3] - vIL[2]),(wFloat ? wFTF[3] - wFTF[2] : wIT[3] - wIT[2])); //Fringe crash avoidance 2
                            dI = vIL[3] - cI*(wFloat ? wFTF[3] : wIT[3]);
                            formTemplate[2] = cI;
                            formTemplate[3] = dI;
                            formTemplate[14] = 0x8000000a | 0x00000010*wFloat; //Term Existence Flags + Supplement
                            formTemplate[15] = 0x80000800;
                            if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                                (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                                (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                            ) {
                                pretoESC = true;//Formula Found Flag
                                break; //Break Loop
                            }
                        }
                        ///////////////////
                        //Linear Coefficient (Add)
                        ///////////////////
                        asm("__varSetup_IntLoop_Curr_GTPreTo_LinAdd:");
                        cI = (vIL[3] - vIL[2] - (wFloat ? wFTF[3] - wFTF[2] : wIT[3] - wIT[2]))/del32;
                        dI = vIL[3] - cI*gt[3] - (wFloat ? wFTF[3] : wIT[3]);
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[14] = 0x80000009 | 0x00000010*wFloat; //Term Existence Flags + Supplement
                        formTemplate[15] = 0x80000800;
                        if (vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                            (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                            (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                        ) {
                            pretoESC = true;//Formula Found Flag
                            break; //Break Loop
                        }
                        ///////////////////
                        //Constant Coefficient (Mul)
                        ///////////////////
                        asm("__varSetup_IntLoop_Curr_GTPreTo_ConstMul:");
                        formTemplate[6] = 0; //Set these in advance
                        formTemplate[7] = 0x00000e40+4*w | 0x00008000*(x&1); //Offset = var(w)/fvar(w)/sysvar(w)/sysfvar(w) | root if applicable
                        if (wFloat ? (int)(floor(wFL[2]))*gt[3] != (int)(floor(wFL[3]))*gt[2] : wIL[2]*gt[3] != wIL[3]*gt[2]) {
                            int buf = (wFloat ? ((int)(floor(wFL[3]))*gt[2] - (int)(floor(wFL[2]))*gt[3]) : (wIL[3]*gt[2] - wIL[2]*gt[3])); //potentially save computation time
                            cI = SAFEDIV((wFloat ? ((int)(floor(wFL[3]))*vIL[2] - (int)(floor(wFL[2]))*vIL[3]) : (wIL[3]*vIL[2] - wIL[2]*vIL[3])),buf); //fringe crash avoidance
                            dI = SAFEDIV((vIL[3]*gt[2] - vIL[2]*gt[3]),buf); //pain
                            formTemplate[2] = cI;
                            formTemplate[3] = dI;
                            formTemplate[14] = 0x80000000; //Term Existence Flags + Supplement
                            formTemplate[15] = 0x8000080a | 0x00000010*wFloat;
                            if ((cI || dI != 1 || x == (byte)(2*v+1)) &&
                                vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                                (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                                (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                            ) {
                                pretoESC = true;//Formula Found Flag
                                break; //Break Loop
                            }
                        }
                        ///////////////////
                        //Constant Coefficient (Add)
                        ///////////////////
                        asm("__varSetup_IntLoop_Curr_GTPreTo_ConstAdd:");
                        cI = (vIL[3] - vIL[2] - (wFloat ? (int)(floor(wFL[3])) - (int)(floor(wFL[2])) : wIL[3] - wIL[2]))/del32;
                        dI = vIL[3] - cI*gt[3] - (wFloat ? (int)(floor(wFL[3])) : wIL[3]);
                        formTemplate[2] = cI;
                        formTemplate[3] = dI;
                        formTemplate[14] = 0x80000000; //Term Existence Flags + Supplement
                        formTemplate[15] = 0x80000809 | 0x00000010*wFloat;
                        if ((cI || dI || x == (byte)(2*v+1)) &&
                            vIL[4] == assembleFormulaI(target,formTemplate,dataStorage,gt[4],(int)(wIL+4)) &&
                            (!gt[1] || vIL[1] == assembleFormulaI(target,formTemplate,dataStorage,gt[1],(int)(wIL+1))) &&
                            (!gt[1] || !gt[0] || vIL[0] == assembleFormulaI(target,formTemplate,dataStorage,gt[0],(int)(wIL+0))) //conditions are quasi-identical so the compiler optimizes
                        ) {
                            pretoESC = true;//Formula Found Flag
                            break; //Break Loop
                        }
                        asm("__varSetup_IntLoop_Curr_GTPreTo_Next:");
                    }
                    //asm("__varSetup_IntLoop_Curr_GTPreTo_Final:"); //Can't put a label here as it apparently is compiled different from expected
                    if (pretoESC) {
                        //Save formula
                        memcpy(formStorage+32*v,formTemplate,64);
                        formStorage[32*v+17] = 256; //Modulus not supported
                        varCore = varCore&~0x000007ff | 0x00000100;
                        continue; 
                    }
                }
                //GameTime formulae couldn't be acquired, but we can keep trying a couple of times, just in case
                asm("__varSetup_IntLoop_Curr_GTFailure:");
                varCore = varCore | 0x40000000*(((varInfo=(varInfo&~0x000f0000)+0x00100000*((varInfo&0x00f00000)<0x00f00000))&0x00f00000)>=0x00a00000);
            } while (false); }
        }
        asm("__varSetup_MainLoop_Preserve:");
        formStorage[32*v+30] = (int)(varInfo);
        formStorage[32*v+31] = (int)(varCore | 0x00001000*(roundstate>1&&RSTime>0));
        asm("__varSetup_MainLoop_Next:");
    }}
    //Main Cleanup
    asm("__varSetup_Cleanup:");
    if (isHelper) { //root,state/prev storage
        infoStorage[5] = root[765];
        infoStorage[6] = root[766];
    }
    memcpy(tamperedWith+4, tamperedWith, 16); //Hold it in last frame storage
    memset(scrapingInfo, 0, 128); //Clear this buffer
        //&15: Variable Subclass
        //  =00 [MI]: StateNo
        //  =01 [MI]: PrevStateNo
        //  =02 [MI]: Time
        //  =03 [MI]: StateNo (Escape-type)
        //  =04 [MI]: PrevStateNo (Escape-type)
        //  =05 [MI]: Time (Escape-type)
        //  =06 [MI]: Root,StateNo
        //  =07 [MI]: Root,PrevStateNo
        //  =08 [MI]: Root,Time
        //  =14 [MI]: Beatrix,Life Storage (Claroots, various)
        //  =15 [MI]: Beatrix,StateNo Storage (_DragonicSeele, DC-Shaving-VM)
        //  =01 [SC]: Recent Damage Storage (Initial Entry)
        //  =02 [SC]: 1F Damage Storage
        //  =03 [SC]: Quasi-Permanent Damage Storage
        //  =04 [SC]: Synchronization (Initial Entry)
        //  =05 [SC]: Invuln/Pseudo-Invuln/Damage Cancelling Flags (Initial Entry)
        //  =06 [SC]: Damage Cancel (Life storage upon hit)
        //  =07 [SC]: Combo Cancel (HitCount storage)
        //  =15 [SC]: Generic Damage-Influenced (Initial Entry)
        //&1792: Currently Screened Variable Class
        //  =0000: N/A
        //  =0256: GameTime
        //  =0512: Main Info
        //  =0768: Scraping
        //  =1024: ID (will not be assigned in this function, but can be written to manually)
    //32512~32639: Preserved Scraping Variable Information
        //+000: Life Synchronization Address
        //+004: ^ Variable Info Address
        //+008: Damage Influenced Address I
        //+012: ^ Variable Info Address
        //+016: Damage Influenced Address II
        //+020: ^ Variable Info Address
        //+024: Damage Influenced Address III
        //+028: ^ Variable Info Address
        //+032: Timer Address
        //+036: ^ Variable Info Address
        //+040: BeaLife Address
        //+064: Damage Storage Address I      < Endpoint of Zenia Saturn
        //+068: Damage Storage Address II
        //+072: Damage Storage Address III
        //+076: Damage Storage Address IV
        //+080: Damage Storage Address V      < Endpoint of _DragonicSeele
        //+084: Damage Storage Address VI     < Endpoint of DC-Shaving-VM
        //+088: Damage Storage Address VII
        //+092: Damage Storage Address VIII   < Endpoint of Claroots
        //+096: BeaState Storage Address I    < Endpoint of Blonde Wolf
        //+100: BeaState Storage Address II
        //+104: BeaState Storage Address III
        //+108: BeaState Storage Address IV
        //+112: BeaState Storage Address V    < Endpoint of _DragonicSeele
        //+116: BeaState Storage Address VI   < Endpoint of DC-Shaving-VM
        //+120: BeaState Storage Address VII
        //+124: BeaState Storage Address VIII
    for (char v = 127; v >= 0; v--) {
        asm("__varSetup_Cleanup_Loop:");
        if (minusThree) {
            asm("__varSetup_Cleanup_Minus3Feedtape:");
            //memcpy cannot be run in a way that makes sense, so use raw asm to run it backwards
            asm(".intel_syntax noprefix\n"
                    "std;"
                    "mov ecx,0x0000000b;" //note that this includes root and parent being given garbage data; this will be corrected later (except gametime)
                    "rep movsd;"
                    "add esi,0x2c;"
                    "add edi,0x2c;"
                    "cld;"
                : 
                : "S"(dataStorage+32*v+10), "D"(dataStorage+32*v+11)
            );
        }
        dataStorage[32*v+21] = dataStorage[32*v+20]; //gametime gets ported to gametime-1
        if (v < 110) { //Variables get stored as-is
            asm("__varSetup_Cleanup_Variable:");
            if (minusThree) {
                dataStorage[32*v+0] = var[v];
                dataStorage[32*v+4] = (isHelper ? *(parent+912+v) : 0);
                dataStorage[32*v+8] = (isHelper ? *(root+912+v) : 0);
            }
            dataStorage[32*v+20] = var[v];
            if (firstRun) { dataStorage[32*v+22] = var[v]; } //Initial value storage
            if (!(tamperedWith[v/30]&(1<<v%30))) { dataStorage[32*v+23] = var[v]; } //Non-tampered storage
            if (!(formStorage[32*v+31]&0x00000800)) continue; //Please only do so if locked.
            char type = (formStorage[32*v+31]&0x00000700)>>8;
            char subtype = formStorage[32*v+31]&0x0000000f;
            bool isFloat = (v >= 60 && v < 100 || v >= 105);
            //Life Synchronization Storage
            if (type == 0x03 && subtype == 0x04 && !scrapingInfo[0]) {
                asm("__varSetup_Cleanup_LifeSyncVar:");
                scrapingInfo[0] = (int)(var+v)|isFloat;
                scrapingInfo[1] = (int)(formStorage+32*v);
            //Damage-Influenced Storage
            } else if (type == 0x03 && subtype == 0x0f && !scrapingInfo[6]) {
                asm("__varSetup_Cleanup_DamageInfluencedVar:");
                char i = 2+2*(!!scrapingInfo[2]+!!scrapingInfo[4]);
                scrapingInfo[i] = (int)(var+v)|isFloat;
                scrapingInfo[i+1] = (int)(formStorage+32*v);
            //Invuln Timer Storage
            } else if (type == 0x03 && subtype == 0x05 && !scrapingInfo[8]) {
                asm("__varSetup_Cleanup_InvulnTimerVar:");
                scrapingInfo[8] = (int)(var+v)|isFloat;
                scrapingInfo[9] = (int)(formStorage+32*v);
            //BeaLife Storage
            } else if (type == 0x02 && subtype == 0x0e && !scrapingInfo[10]) {
                asm("__varSetup_Cleanup_BeaLifeVar:");
                scrapingInfo[10] = (int)(var+v)|isFloat;
            //Damage Storage
            } else if (type == 0x03 && subtype >= 0x01 && subtype <= 0x03 && !scrapingInfo[23]) {
                asm("__varSetup_Cleanup_DamageStorageVar:");
                char i = 16;
                do { //Fetch the first empty slot
                    if (!scrapingInfo[i]) { break; }
                } while (++i < 22);
                scrapingInfo[i] = (int)(var+v)|isFloat;
            }
        } else if (v < 127) { 
            asm("__varSetup_Cleanup_NonVariable:");
            int* ref = (int*)(formStorage[32*v+30]&0xfffffffc); //shut up gcc
            switch (v) {
                case 110: //stateno
                case 111: //prevstateno
                    asm("__varSetup_Cleanup_States:");
                    if (minusThree) { //Store values recorded in -3 space
                        dataStorage[32*v+0] = dataStorage[32*v+26];
                        dataStorage[32*v+4] = dataStorage[32*v+27];
                        dataStorage[32*v+8] = dataStorage[32*v+28];
                    }
                    dataStorage[32*v+20] = target[765+(v-110)];
                    if (firstRun) { dataStorage[32*v+22] = target[765+(v-110)]; } //Why not? Could be handy
                    break;
                case 112: //statetime
                    asm("__varSetup_Cleanup_StateTime:");
                    if (minusThree) { //Store values recorded in -3 space
                        dataStorage[32*v+0] = dataStorage[32*v+26];
                        dataStorage[32*v+4] = dataStorage[32*v+27];
                        dataStorage[32*v+8] = dataStorage[32*v+28];
                    }
                    dataStorage[32*v+20] = statetime;
                    break;
                case 125: //SPECIAL
                    asm("__varSetup_Cleanup_SPECIAL:");
                    if (!ref) { break; }
                    if (minusThree) dataStorage[32*v+0] = dataStorage[32*v+25];
                    dataStorage[32*v+20] = *ref;
                    scrapingInfo[2] = formStorage[32*v+30];
                    scrapingInfo[3] = (int)(formStorage+32*v);
                    break;
                default: //otherwise, assume that it stayed constant and keep premonition
                    asm("__varSetup_Cleanup_MiscNonVariable:");
                    if (minusThree) dataStorage[32*v+0] = dataStorage[32*v+25];
                    dataStorage[32*v+20] = dataStorage[32*v+25];
            }
        } else {
            asm("__varSetup_Cleanup_GameTime:");
            if (minusThree) dataStorage[32*v+0] = gametime;
            dataStorage[32*v+20] = gametime;
            if (firstRun) { dataStorage[32*v+22] = gametime; } //Initialization Flag
        }
        asm("__varSetup_Cleanup_Next:");
    }
    asm("__varSetup_End:");
    memset(tamperedWith, 0, 16); //Reset tampering flags
    return;
}

extern "C" void normalcyPenetration (int* target, int* dataStorage, int* source, int* formStorage, int* fallbackStorage, int flags, int gametime, int stateno, int prevstateno, int statetime, int life, int damage) {
    if (!(flags&0x7fffffff)) { return; } //are you daft?
    float* dataStorageF = (float*)(dataStorage);
    int* softTampering = formStorage+32*127+20;
    int* root = *(int**)(target+2441);
    int* var = target+912; float* fvar = (float*)(var);
    int gtS = gametime*gametime; int gtC = gtS*gametime;
    int gtI = 1-2*(gametime&1);
    asm("__normalcyPenetration_Backup:");
    int stateTMP = target[765]; //GT Formulae with these coefficients should refer to them
    if (stateno != -1) {
        target[765] = stateno; //Therefore, for the time being, exchange if need be. We'll restore it later
    }
    int prevTMP = target[766];
    if (prevstateno != -1) {
        target[766] = prevstateno; //Exchange
    }
    bool fallback = false;
    memset(softTampering,0,16); //Nullify this
    //Format of flags:
        //&15: GameTime Defense Penetration Methodology
        //   =01: Standard
        //   =02: c*gametime+d => c+gametime+d
        //        Oniwarud 3rd Killer (var(36)!=gametime+13592468 even though var(36):=gametime*13592468 is written)
        //   =03: c*gametime+d => c*gametime*d
        //        in this context, d is set to 1 if it's 0
        //&16: -3 State/Prev/Time Variable Tampering
        //&32: Main State/Prev/Time Variable Tampering (part of Pandora Variable Tampering)
        //&64: Root State/Prev Variable Tampering (part of Pandora Variable Tampering)
        //   Only used in a helper context. root,stateno-type variables
        //&128: Unknown Formula == All Formula
        //&768: Unknown Float Formula Penetration Methodology
        //   =256: Substitute Float with QNaN
        //         Most edits that have strange fvar formulae support this method
        //   =512: Substitute Float with Infinity
        //         Blocking-Heart
        //   =768: Substitute Float with -Infinity
        //         Precaution
        //&3072: Unknown Formula Penetration Methodology
        //   =1024: Substitute with SourceVar
        //          For -3 Infiltration, MT=A Helper > Body Substitution, etc.
        //&12288: Life Variable Tampering Methodology
        //      =04096: Exact
        //      =08192: AddToCurr
        //      =12288: AddToInit
        //&16384: Misc. Scraping Variable Tampering
        //&32768: Fallback Formula Usage
        //< 0: target,isParent
        //    For obvious reasons, do NOT do anything to sysvar if this is flagged
    char v = 0;
    do {
        asm("__normalcyPenetration_Loop:");
        bool isFloat = (v >= 60 && v < 100 || v >= 105);
        int* formBasis = (fallback ? fallbackStorage : formStorage);
        int* dataBasis = formBasis-4096;
        float* dataBasisF = (float*)(dataBasis); //typecasts
        float* formBasisF = (float*)(formBasis);
        int* infoBasis = dataBasis+8160; //NOTE: should account for fallback processing; dataStorage is for our own criteria
        dword varInfo = formBasis[32*v+30];
        dword varCore = formBasis[32*v+31];
        char type = (varCore&0x00000700) >> 8;
        char subtype = varCore&0x0000000f;
        if (flags&0x0000000f && type == 0x01 && !(flags&0x00000080 && (flags&0x00000c00 || isFloat && flags&0x00000300))) { //GameTime Defense Penetration
            asm("__normalcyPenetration_Loop_GTPenetration:");
            int formTemplate[16]; float* formTemplateF = (float*)(formTemplate);
            memcpy(formTemplate,formBasis+32*v,64);
            switch (flags&0x0000000f) {
                case 0x00000002: //c*gametime+d => c+gametime+d
                    asm("__normalcyPenetration_Loop_GTPenetration_Oniwarud:");
                    if ((formTemplate[14]|formTemplate[15])&0x00000004) {
                        formTemplateF[3] = (formTemplate[14]&0x00000004 ? formTemplateF[2] : formTemplate[2]) + (formTemplate[15]&0x00000004 ? formTemplateF[3] : formTemplate[3]);
                        formTemplate[14] &= ~0x00000004;
                        formTemplate[15] |= 0x00000004;
                    } else {
                        formTemplate[3] += formTemplate[2];
                    }
                    formTemplate[2] = 1;
                    break;
                case 0x00000003: //c*gametime+d => c*gametime*d
                    asm("__normalcyPenetration_Loop_GTPenetration_InvOniwarud:");
                    if ((formTemplate[14]|formTemplate[15])&0x00000004) {
                        formTemplateF[2] = (formTemplate[14]&0x00000004 ? formTemplateF[2] : formTemplate[2]) * (formTemplate[15]&0x00000004 ? formTemplateF[3] + !formTemplateF[3] : formTemplate[3] + !formTemplate[3]);
                        formTemplate[14] |= 0x00000004;
                        formTemplate[15] &= ~0x00000004;
                    } else if (formTemplate[3]) {
                        formTemplate[2] *= formTemplate[3];
                    }
                    formTemplate[3] = 0;
                    break;
            }
            asm("__normalcyPenetration_Loop_GTPenetration_Finalization:");
            if (formTemplate[12]&0x00000008) { formTemplate[4] &= ~0x007f0000; } //Only use raw offsets here
            if (formTemplate[13]&0x00000008) { formTemplate[5] &= ~0x007f0000; } //(Hence why we exchanged previously)
            if (formTemplate[14]&0x00000008) { formTemplate[6] &= ~0x007f0000; }
            if (formTemplate[15]&0x00000008) { formTemplate[7] &= ~0x007f0000; }
            if (isFloat) {
                fvar[v] = assembleFormulaF(target,formTemplate,dataStorage,gametime);
            } else {
                var[v] = assembleFormulaI(target,formTemplate,dataStorage,gametime);
            }
            softTampering[v/30] |= 1<<(v%30);
            fallback = false;
            v++; continue;
        }
        //-3.StateNo / -3.PrevStateNo / -3.Time
        //StateNo / PrevStateNo / Time
        if (type == 0x02 && (flags&0x00000010 && subtype >= 0x03 && subtype <= 0x05 || flags&0x00000020 && subtype < 0x03)) {
            asm("__normalcyPenetration_Loop_MainWithMinusThree:");
            int input = (!(subtype%0x03) ? (stateno == -1 ? stateTMP : stateno) :
                (subtype%0x03 == 0x01 ? (prevstateno == -1 ? prevTMP : prevstateno) : (statetime == -1 ? target[895] : statetime))
            );
            if (isFloat) {
                fvar[v] = input*formBasisF[32*v+0] + formBasisF[32*v+1];
            } else {
                var[v] = input*formBasis[32*v+0] + formBasis[32*v+1];
            }
            softTampering[v/30] |= 1<<(v%30);
            fallback = false;
            v++; continue;
        }
        //Root.StateNo / Root.PrevStateNo
        if (flags&0x00000040 && type == 0x02 && subtype >= 0x06 && subtype <= 0x07) { //0x08 might be included in the future, but not now
            asm("__normalcyPenetration_Loop_RootMain:");
            int input = (subtype == 0x06 ? (stateno == -1 ? root[765] : stateno) : (prevstateno == -1 ? root[766] : prevstateno));
            if (isFloat) {
                fvar[v] = input*formBasisF[32*v+0] + formBasisF[32*v+1];
            } else {
                var[v] = input*formBasis[32*v+0] + formBasis[32*v+1];
            }
            softTampering[v/30] |= 1<<(v%30);
            fallback = false;
            v++; continue;
        }
        //Unknown Formula Substitution
        if (flags&0x00000300 && (!type && dataBasis[32*v+30] > 5*dataBasis[32*v+31] || type == 0x01 && flags&0x00000080) && isFloat) {
            asm("__normalcyPenetration_Loop_UnknownFloatSub:");
            switch (flags&0x00000300) {
                case 0x00000100: //NaN
                    var[v] = 2147483647;
                    break;
                case 0x00000200: //Infinity
                    var[v] = 2139095040;
                    break;
                case 0x00000300: //-Infinity
                    var[v] = -8388608;
                    break;
            }
            softTampering[v/30] |= 1<<(v%30);
            fallback = false;
            v++; continue;
        }
        if (flags&0x00000c00 && (!type && dataBasis[32*v+30] > 5*dataBasis[32*v+31] || type == 0x01 && flags&0x00000080)) {
            asm("__normalcyPenetration_Loop_UnknownSub:");
            switch (flags&0x00000c00) {
                case 0x00000400: //Remote Substitution
                    var[v] = *(source+912+v);
                    break;
            }
            softTampering[v/30] |= 1<<(v%30);
            fallback = false;
            v++; continue;
        }
        //Damage-Influenced/Life Variable Tampering
        #warning TEMP VALUE PUT INTO SUBTYPE: FIX THIS
        if (!fallback && flags&0x00003000 && type == 0x03 && subtype != 0x10) { //0x10 is TMP
            asm("__normalcyPenetration_Loop_LifeVar:");
            if (isFloat) {
                fvar[v] = (flags&0x00002000 ? (flags&0x00001000 ? dataStorageF[32*v+56] : dataStorageF[32*v+60]) + life : life);
            } else {
                var[v] = (flags&0x00002000 ? (flags&0x00001000 ? dataStorage[32*v+56] : dataStorage[32*v+60]) + life : life);
            }
            softTampering[v/30] |= 1<<(v%30);
            fallback = false;
            v++; continue;
        }
        //Damage Variable Tampering
        #warning TEMP VALUE PUT INTO SUBTYPE: FIX THIS TOO
        if (!fallback && flags&0x00004000 && type == 0x03 && subtype == 0x10) { //0x10 is TMP
            asm("__normalcyPenetration_Loop_DamageVar:");
            if (isFloat) {
                fvar[v] = damage;
            } else {
                var[v] = damage;
            }
            softTampering[v/30] |= 1<<(v%30);
            fallback = false;
            v++; continue;
        }
        asm("__normalcyPenetration_Loop_Next:");
        if (fallback || !(flags&0x00008000)) {
            fallback = false;
            v++; continue;
        } else {
            fallback = true;
        }
    } while (v < 110-10*(flags<0));
    asm("__normalcyPenetration_Cleanup:");
    target[765] = stateTMP; //Restore from backup
    target[766] = prevTMP;
    return;
}

extern "C" void varTamper (int* self, int* tamperInfo, int* varStorage, int gametime, int* tamperedOffset, bool targetIsParent, bool nonCT, bool tamperOK, bool restituteOK) { //exec(132) for now
    //Just in case we put it in our own data for some reason
    if ((dword)(tamperedOffset) < 13424) { tamperedOffset = (int*)((int)(self)+(int)(tamperedOffset)); }
    int* target = (targetIsParent ? *(int**)(self+2440) : self);
    int* var = target+912; float* fvar = (float*)(var);
    //Variable Restitution
    if (restituteOK) {
        asm("__varTamper_Restitute:");
        char ti = 109/30; dword tw = 1 << 109%30;
        char v = 109; char vmin = 100*nonCT;
        if (targetIsParent) {
            ti = 99/30; tw = 1 << 99%30;
            v = 99; vmin = 0;
        }
        asm("__varTamper_Restitute_Loop:");
        do {
            if (*(tamperedOffset+ti)&tw) {
                var[v] = *(varStorage+32*v+23);
                *(tamperedOffset+ti) &= ~tw;
            }
            if (!(tw >>= 1)) {
                tw = 0x20000000;
                ti--;
            }
        } while (--v >= vmin);
    }
    if (!tamperOK) { return; } //nothing to do here
    asm("__varTamper_Tampering:");
    //Identical to the definition in the restitution function
    {
        dword tamperFlags = (dword)(tamperInfo[127*32+0]);
        dword tamperFlagsSys = (dword)(tamperInfo[127*32+1]);
        float* tamperInfoF = (float*)(tamperInfo);
        char v = 109; char vmin = 100*nonCT;
        if (targetIsParent) {
            v = 99; vmin = 0;
        }
        asm("__varTamper_TamperingLoop:");
        do {
            bool isFloat = false;
            char fa; char p = 1; char varID;
            if (v >= 105) { //sysfvar bounds check
                if ((tamperFlagsSys>>24&0x07) > 4) { tamperFlagsSys = (tamperFlagsSys&~(0x07<<24)|4<<24); }
                varID = v-105;
                if (varID < (tamperFlagsSys>>16&0x07)) { //sysfvar oob
                    v = 105;
                    continue;
                } else if (varID > (tamperFlagsSys>>24&0x07)) {
                    varID = (tamperFlagsSys>>24&0x07);
                    v = 105+varID;
                }
                fa = 113; 
                isFloat = true;
            } else if (v >= 100) { //sysvar bounds check
                if ((tamperFlagsSys>>24&0x07) > 4) { tamperFlagsSys = (tamperFlagsSys&~(0x07<<24)|4<<24); }
                varID = v-100;
                if (varID < (tamperFlagsSys&0x07)) { //sysvar oob
                    v = 100;
                    continue;
                } else if (varID > (tamperFlagsSys>>8&0x07)) {
                    varID = (tamperFlagsSys>>8&0x07);
                    v = 100+varID;
                }
                fa = 112;
            } else if (v >= 60) { //fvar bounds check
                if ((tamperFlags>>24&0x3f) > 39) { tamperFlags = (tamperFlags&~(0x3f<<24)|39<<24); }
                varID = v-60;
                if (varID < (tamperFlags>>16&0x3f)) { //fvar oob
                    v = 60;
                    continue;
                } else if (varID > (tamperFlags>>24&0x3f)) {
                    varID = (tamperFlags>>24&0x3f);
                    v = 60+varID;
                }
                fa = 111;
                isFloat = true;
            } else { //var bounds check
                if ((tamperFlags>>8&0x3f) > 59) { tamperFlags = (tamperFlags&~(0x3f<<8)|59<<8); }
                varID = v;
                if (varID < (tamperFlags&0x3f)) { //var oob
                    break;
                } else if (varID > (tamperFlags>>8&0x3f)) {
                    varID = (tamperFlags>>8&0x3f);
                    v = varID;
                }
                fa = 110;
            }
            if (v < 100) {
                asm("__varTamper_TamperingLoop_CheckParity:");
                dword parityFlags = tamperInfo[127*32+2+(v>=60)];
                switch (parityFlags>>24&0x03) {
                    case 1:
                        asm("__varTamper_TamperingLoop_CheckParity_Modulo:");
                        p = 1 + ((varID+(parityFlags&0x0000003f))%((parityFlags>>8&0x0000003f)+1) >= (parityFlags>>16&0x0000003f));
                        break;
                    case 2:
                        asm("__varTamper_TamperingLoop_CheckParity_InRange:");
                        p = 1 + (v>=60 ? 
                            fvar[v] >= tamperInfoF[127*32+6] && fvar[v] <= tamperInfoF[127*32+7] :
                            var[v] >= tamperInfo[127*32+4] && var[v] <= tamperInfo[127*32+5]
                        );
                        break;
                    case 3:
                        asm("__varTamper_TamperingLoop_CheckParity_OutRange:");
                        p = 1 + (v>=60 ? 
                            fvar[v] < tamperInfoF[127*32+6] || fvar[v] > tamperInfoF[127*32+7] :
                            var[v] < tamperInfo[127*32+4] || var[v] > tamperInfo[127*32+5]
                        );
                        break;
                }
            }
            asm("__varTamper_TamperingLoop_FetchInfo:");
            char ti = v/30; int tw = 1<<v%30;
            dword* tamperElem = (dword*)(tamperInfo+32*v+16+(p-1));
            if (!(tamperElem[0]&0x80000000)) { tamperElem = (dword*)(tamperInfo+32*fa+16+(p-1)); }
            //If RNG's involved, randomly decide which protocol to reference
            tamperElem += (rand()%((tamperElem[0]>>20&0x0f) + 1))*p&0x0f;
            int protocol = tamperElem[0];
            char operand = (protocol>>17&0x07); //Fetch protocol
            if (!operand) { continue; } //Need a protocol
            int a;
            int adjType = ((protocol&0x00003fff)-1)/1000;
            int adjSubType = ((protocol&0x00003fff)-1)%1000;
            char basisType = (protocol>>15&0x03);
            int adjTweak = (protocol&0x00004000 ? -1 : 1);
            if (isFloat) {
                asm("__varTamper_TamperingLoop_Float:");
                float adj = *(float*)(tamperElem-16);
                float basis;
                int* rvar = var; float* rfvar = (float*)(rvar);
                if (protocol&0x00003fff) {
                    switch (adjType) {
                        case 0: //Add RNG
                            asm("__varTamper_TamperingLoop_Float_AdjAddRandom:");
                            a = rand()%(adjSubType+1)*adjTweak;
                            adj += a;
                            break;
                        case 1: //Mul RNG
                            asm("__varTamper_TamperingLoop_Float_AdjMulRandom:");
                            a = rand()%(adjSubType+1)*adjTweak;
                            adj *= a;
                            break;
                        case 2: //Div RNG
                            asm("__varTamper_TamperingLoop_Float_AdjDivRandom:");
                            a = (rand()%(adjSubType+1)+1)*adjTweak;
                            adj /= a;
                            break;
                        case 4: //Add Root,Var(X)
                            asm("__varTamper_TamperingLoop_Float_AdjRootVarSetup:");
                            if (target[7]) { rvar = (int*)(target[2441]+912); rfvar = (float*)(rvar); }
                            //Identical to var(X) otherwise
                        case 3: //Add Var(X)
                            asm("__varTamper_TamperingLoop_Float_AdjVarSetup:");
                            switch (adjSubType) {
                                case 110:
                                    asm("__varTamper_TamperingLoop_Float_AdjAddRandVar:");
                                    a = rand()%60;
                                    break;
                                case 111:
                                    asm("__varTamper_TamperingLoop_Float_AdjAddRandFVar:");
                                    a = rand()%40+60;
                                    break;
                                case 112:
                                    asm("__varTamper_TamperingLoop_Float_AdjAddRandSysVar:");
                                    a = rand()%5+100;
                                    break;
                                case 113:
                                    asm("__varTamper_TamperingLoop_Float_AdjAddRandSysFVar:");
                                    a = rand()%5+105;
                                    break;
                                case 114:
                                    asm("__varTamper_TamperingLoop_Float_AdjAddRandAnyVar:");
                                    a = rand()%110;
                                    break;
                                case 115:
                                    asm("__varTamper_TamperingLoop_Float_AdjAddSyncVar:");
                                    a = v;
                                    break;
                                default:
                                    asm("__varTamper_TamperingLoop_Float_AdjAddSpecVar:");
                                    a = adjSubType;
                            }
                            asm("__varTamper_TamperingLoop_Float_AdjAddVar:");
                            if (a < 60 || a >= 100 && a < 105) {
                                adj += rvar[a]*adjTweak;
                            } else {
                                adj += rfvar[a]*adjTweak;
                            }
                            break;
                    }
                }
                asm("__varTamper_TamperingLoop_Float_PostAdj:");
                switch (basisType) {
                    case 1:
                        asm("__varTamper_TamperingLoop_Float_BasisIsVar:");
                        basis = fvar[v];
                        break;
                    case 2:
                        asm("__varTamper_TamperingLoop_Float_BasisIsUntampered:");
                        basis = *(float*)(varStorage+32*v+23);
                        break;
                    case 3:
                        asm("__varTamper_TamperingLoop_Float_BasisIsInit:");
                        basis = *(float*)(varStorage+32*v+22);
                        break;
                    default:
                        asm("__varTamper_TamperingLoop_Float_BasisIs0:");
                        basis = 0;
                }
                asm("__varTamper_TamperingLoop_Float_PostBasis:");
                switch (operand) {
                    case 1:
                        asm("__varTamper_TamperingLoop_Float_AddAdj:");
                        fvar[v] = basis + adj;
                        break;
                    case 2:
                        asm("__varTamper_TamperingLoop_Float_MulAdj:");
                        fvar[v] = basis*adj;
                        break;
                    case 3:
                        asm("__varTamper_TamperingLoop_Float_DivAdj:");
                        fvar[v] = basis/adj; //Division by 0 will return infinity
                        break;
                    case 4:
                        asm("__varTamper_TamperingLoop_Float_OrAdj:");
                        fvar[v] = (int)(floor(basis))|(int)(floor(adj)); //floor is the most common
                        break;
                    case 5:
                        asm("__varTamper_TamperingLoop_Float_AndAdj:");
                        fvar[v] = (int)(floor(basis))&(int)(floor(adj));
                        break;
                    case 6:
                        asm("__varTamper_TamperingLoop_Float_XorAdj:");
                        fvar[v] = (int)(floor(basis))^(int)(floor(adj));
                        break;
                }
            } else {
                asm("__varTamper_TamperingLoop_Int:");
                int adj = *(tamperElem-16);
                int basis;
                int* rvar = var; float* rfvar = (float*)(rvar);
                if (protocol&0x00003fff) {
                    switch (adjType) {
                        case 0: //Add RNG
                            asm("__varTamper_TamperingLoop_Int_AdjAddRandom:");
                            a = rand()%(adjSubType+1)*adjTweak;
                            adj += a;
                            break;
                        case 1: //Mul RNG
                            asm("__varTamper_TamperingLoop_Int_AdjMulRandom:");
                            a = rand()%(adjSubType+1)*adjTweak;
                            adj *= a;
                            break;
                        case 2: //Div RNG
                            asm("__varTamper_TamperingLoop_Int_AdjDivRandom:");
                            a = (rand()%(adjSubType+1)+1)*adjTweak;
                            if (a == 0 || adj == -2147483648 && a == -1) { break; } //Division by 0 crash avoidance
                            adj /= a;
                            break;
                        case 4: //Add Root,Var(X)
                            asm("__varTamper_TamperingLoop_Int_AdjRootVarSetup:");
                            if (target[7]) { rvar = (int*)(target[2441]+912); rfvar = (float*)(rvar); }
                            //Identical to var(X) otherwise
                        case 3: //Add Var(X)
                            asm("__varTamper_TamperingLoop_Int_AdjVarSetup:");
                            switch (adjSubType) {
                                case 110:
                                    asm("__varTamper_TamperingLoop_Int_AdjAddRandVar:");
                                    a = rand()%60;
                                    break;
                                case 111:
                                    asm("__varTamper_TamperingLoop_Int_AdjAddRandFVar:");
                                    a = rand()%40+60;
                                    break;
                                case 112:
                                    asm("__varTamper_TamperingLoop_Int_AdjAddRandSysVar:");
                                    a = rand()%5+100;
                                    break;
                                case 113:
                                    asm("__varTamper_TamperingLoop_Int_AdjAddRandSysFVar:");
                                    a = rand()%5+105;
                                    break;
                                case 114:
                                    asm("__varTamper_TamperingLoop_Int_AdjAddRandAnyVar:");
                                    a = rand()%110;
                                    break;
                                case 115:
                                    asm("__varTamper_TamperingLoop_Int_AdjAddSyncVar:");
                                    a = v;
                                    break;
                                default:
                                    asm("__varTamper_TamperingLoop_Int_AdjAddSpecVar:");
                                    a = adjSubType;
                            }
                            asm("__varTamper_TamperingLoop_Int_AdjAddVar:");
                            if (a < 60 || a >= 100 && a < 105) {
                                adj += rvar[a]*adjTweak;
                            } else {
                                adj += (int)(rfvar[a])*adjTweak;
                            }
                            break;
                    }
                }
                asm("__varTamper_TamperingLoop_Int_PostAdj:");
                switch (basisType) {
                    case 1:
                        asm("__varTamper_TamperingLoop_Int_BasisIsVar:");
                        basis = var[v];
                        break;
                    case 2:
                        asm("__varTamper_TamperingLoop_Int_BasisIsUntampered:");
                        basis = *(varStorage+32*v+23);
                        break;
                    case 3:
                        asm("__varTamper_TamperingLoop_Int_BasisIsInit:");
                        basis = *(varStorage+32*v+22);
                        break;
                    default:
                        asm("__varTamper_TamperingLoop_Int_BasisIs0:");
                        basis = 0;
                }
                asm("__varTamper_TamperingLoop_Int_PostBasis:");
                switch (operand) {
                    case 1:
                        asm("__varTamper_TamperingLoop_Int_AddAdj:");
                        var[v] = basis + adj;
                        break;
                    case 2:
                        asm("__varTamper_TamperingLoop_Int_MulAdj:");
                        var[v] = basis*adj;
                        break;
                    case 3:
                        asm("__varTamper_TamperingLoop_Int_DivAdj:");
                        if (adj == 0 || basis == -2147483648 && adj == -1) { adj = 1; } //Division by 0 crash avoidance
                        var[v] = basis/adj;
                        break;
                    case 4:
                        asm("__varTamper_TamperingLoop_Int_OrAdj:");
                        var[v] = basis|adj;
                        break;
                    case 5:
                        asm("__varTamper_TamperingLoop_Int_AndAdj:");
                        var[v] = basis&adj;
                        break;
                    case 6:
                        asm("__varTamper_TamperingLoop_Int_XorAdj:");
                        var[v] = basis^adj;
                        break;
                }
            }
            asm("__varTamper_TamperingLoop_MarkAndNext:");
            tamperedOffset[ti] |= tw;
        } while (--v >= vmin);
    }
    return;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// EXPLORATION INFORMATION BLOCK
////////////////////////////////////////////////////////////////////////////////////////////////

/*
----------------
.ExplExec (ID = 192)
--------------------------------------------
Handles:
  - Exploration Clone initialization
  - State Storage
  - Crash Avoidance (...mostly, some QGP is still required; for instance there's nothing detecting DC-012)
  - Annoying Kain Loops
  - Non-static ChangeState handling
Does NOT handle:
  - how massive it is
Param0 = Exploration Clone Structure
Param1 = State ID
Param2 = Beatrix's AnimData
Param3 = State Information Storage Pointer
Param4 = Exploration Source
Param5 = Exploration Clone
Param6 = DEK StateNo
Param7 = Flags
      &1: isHelper Flag
      &2: isSelf Flag (Exploitative Exploration)
      &4: defRead Flag (CONFIG)
      &8: timeGrant Flag (CONFIG)
      &16: execOnly Flag (-3 Exploration/Remote Checksum)
      &32: execMinus Flag (-3 Exploration)
      &64: explodSeal Flag (Body Exploration, -3 Exploration)
      &128: projSeal Flag (-3 Exploration)
*/
asm(R"(.intel_syntax noprefix
_XC0:                                                     # ========================================
  mov    eax,0x00000000                                   # Check for initialization
  test   eax,eax                                          #
  jnz    _XC0_HANDLER                                     #
  mov    DWORD PTR [ecx+0x01],ecx                         #
  sub    ecx,offset _XC0                                  # Get absolute position
  lea    eax,[ecx+_XC0_POINTERMAP]                        # We need to MANUALLY SPECIFY the jmp table
  mov    DWORD PTR [ecx+__explExec_StCon_Switch+0x30],eax                   # -Os
  add    eax,0x60                                         # prev=24 cases
  mov    DWORD PTR [ecx+__explExec_StCon_Switch+0x43],eax
# mov    DWORD PTR [ecx+__explExec_StCon_Switch+0x3e],eax                   # -O3 (probably the poitnermap itself also needs to be changed)
# add    eax,0x60                                         # prev=24 cases
# mov    DWORD PTR [ecx+__explExec_StCon_DestroySelf-0x04],eax
  add    DWORD PTR [ecx+__eE$expParse_TypeByType_OperSEHInit+0x1a],ecx      # General
  add    DWORD PTR [ecx+__eE$expParse_TypeByType_OperSEHInit+0x24],ecx
  add    DWORD PTR [ecx+__eE$expParse_TypeByType_TriggerSEHInit+0x19],ecx
  add    DWORD PTR [ecx+__eE$expParse_TypeByType_TriggerSEHInit+0x23],ecx
  add    DWORD PTR [ecx+__eE$stConExec_SEHInit+0x17],ecx
  add    DWORD PTR [ecx+__eE$stConExec_SEHInit+0x21],ecx
  mov    eax,0x0000003f                                   # Loop through the pointermap
_XC0_PREHANDLER_Loop_POINTERMAP:                          # ---------------------------------------
  add    DWORD PTR [ecx+eax*4+_XC0_POINTERMAP],ecx        #
  dec    eax                                              #
  jns    _XC0_PREHANDLER_Loop_POINTERMAP                  #
_XC0_HANDLER:                                             # ---------------------------------------
  push   DWORD PTR [edx+0x1c]                             # Prepare Stack
  push   DWORD PTR [edx+0x18]                             # 
  push   DWORD PTR [edx+0x14]                             # 
  push   DWORD PTR [edx+0x10]                             # 
  push   DWORD PTR [edx+0x0c]                             # 
  push   DWORD PTR [edx+0x08]                             # 
  push   DWORD PTR [edx+0x04]                             # 
  push   DWORD PTR [edx]                                  #
  finit                                                   #
  call   _explExec                                        # Call C++ Function
  add    esp,0x20                                         # Repair stack
  finit                                                   #
  ret                                                     #
_XC0_POINTERMAP:                                          # ========================================
  .int __explExec_StCon_EnvShake
  .int __explExec_StCon_Default
  .int __explExec_StCon_EnvColor
  .int __explExec_StCon_GameMakeAnim
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Pause
  .int __explExec_StCon_Default
  .int __explExec_StCon_SuperPause
  .int __explExec_StCon_MakeDust
  .int __explExec_StCon_PalFX
  .int __explExec_StCon_Default
  .int __explExec_StCon_Explod
  .int __explExec_StCon_Default
  .int __explExec_StCon_ExplodBindTime
  .int __explExec_StCon_RemoveExplod
  .int __explExec_StCon_Default
  .int __explExec_StCon_ModifyExplod
  .int __explExec_StCon_ChangeState
  .int __explExec_StCon_ChangeState
  .int __explExec_StCon_Default
  .int __explExec_StCon_VarXXX
  .int __explExec_StCon_VarXXX
  .int __explExec_StCon_VarXXX
  .int __explExec_StCon_VarRangeSet
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_LifeSet
  .int __explExec_StCon_LifeAdd
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_Default
  .int __explExec_StCon_HitBy
  .int __explExec_StCon_Default
  .int __explExec_StCon_ChangeAnim
  .int __explExec_StCon_ChangeAnim
  .int __explExec_StCon_PlaySnd
  .int __explExec_StCon_StopSnd
  .int __explExec_StCon_Default
  .int __explExec_StCon_HitDef
  .int __explExec_StCon_HitDef
  .int __explExec_StCon_Projectile
  .int __explExec_StCon_Default
  .balign 0x10, 0x00
)");

/*
---------------
.ExplMapCount (ID = 193)
--------------------------------------------
MUST BE COMPILED ALONGSIDE .ExplExec
Searches for the information corresponding to our exploration data
Crash states are auto-blacklisted
  Param0: The aforementioned exploration data
        This is a secondary output of the following format:
        &0x00000007: Maximum Priority
        &0x00000100: Time != 0 Flag
  Param1: The state code to source everything from
  Param2: Whitelist (all must match)
        &0x00007fff: Standard Flag Information
        &0x00008000: Overflow Mask Flag
        &0x00010000: HitBy value = SCA,AA,AT,AP
        &0x00020000: HitBy value2 = SCA,AA,AT,AP
        &0x00100000: HitDef attr = ,AA,AT,AP
        &0x00200000: HitDef attr = SCA,AA,AT,AP
        &0x00400000: P1PauseTime Acquisition
        &0x00800000: HitDef Lingering
        &0x20000000: ChangeState > Value Reroute
        &0x40000000: ChangeState > Mask Reroute (for states like 5070)
        &0x80000000: Crash states are also acceptable, we'll sort it manually
  Param3: Blacklist (none must match)
        &0x00007fff: Standard Flag Information
        &0x00008000: ChangeAnim (ANY)
        &0x00010000: HitBy value = SCA,AA,AT,AP
        &0x00020000: HitBy value2 = SCA,AA,AT,AP
        &0x00040000: HitBy value/value2 = ANY
        &0x00100000: HitDef attr = ???,AA,AT,AP
        &0x00200000: ReversalDef attr = SCA,AA,AT,AP
        &0x01000000: Common MT=H Blacklist
  Param4: BitWhitelist (all must match)
        0x000000ff: Variable Tampering (>=110 is NULL)
        0x0000ff00: Parent Variable Tampering (>=110 is NULL)
        0x00ff0000: HitOverride Nullification
  Param5: Time+Prio (all must match)
        0x000000ff: Time.Min
        0x0000ff00: Time.Max
        0x00070000: Prio.Min (1 if 0)
        0x00700000: Prio.Max (7 if 0)
  Param6: ChangeState Value
  Param7: The position of the state to return (0 is 1st, 1 is 2nd, etc.)
        If -1, the total count will be returned
        If it's OoB otherwise, it'll return a random value (i use -2 for this)
*/
asm(R"(.intel_syntax noprefix
_XC1:                                                     # ========================================
  finit                                                   # Usual Precaution
  push   0x00000000                                       # Prepare Stack
  push   DWORD PTR [edx+0x1c]                             #
  push   DWORD PTR [edx+0x18]                             #
  push   DWORD PTR [edx+0x14]                             #
  push   DWORD PTR [edx+0x10]                             #
  push   DWORD PTR [edx+0x0c]                             #
  push   DWORD PTR [edx+0x08]                             #
  push   DWORD PTR [edx+0x04]                             #
  push   edx                                              #
  call   _explMapCount                                    # Call C++ Function
  add    esp,0x24                                         # Repair stack
  finit                                                   #
  mov    DWORD PTR [esp+0x64],eax                         # Returns int
  ret                                                     #
  .balign 0x10, 0x00
)");

int scanAnim (int* animData, int value, int elem, int &C1Time, int &C2Time) {
    asm("__scanAnim:");
    int animCount = *(*(int**)(animData)+3);
    int* animDex = *(*(int***)(animData)+5);
    int* animMap = *(*(int***)(animData)+6);
    int a = 0;
    //Special flag if elem=0: Will asssess the anim with ID value, not with number value
    if (elem == 0) {
        a = value;
        elem = 1;
    } else {
        do {
            asm("__scanAnim_IDAcquisition_Loop:");
            if (animMap[4*a+3] == value) { break; }
            asm("__scanAnim_IDAcquisition_Next:");
        } while (++a < animCount);
    }
    if (a >= animCount) { return -1; } //Couldn't find it or it doesn't exist :/
    asm("__scanAnim_IDAcquired:");
    int* anim = *(int**)(animDex+5*a);
    int elemCount = anim[3];
    if (elemCount == 0) { return -2; } //Empty Animation | Instant assert failure in array.h line 110
    int* elemDex = *(int**)(anim+5);
    int e = elem-1; int elemtime = 0;
    if (e >= elemCount) { e = 0; } //If OoB/ID-derived, start at 1 instead
    C1Time = -1; C2Time = -1;
    if (elemCount == 1 && elemDex[1] == 0) { C1Time = -2; } //0F animation countermeasures
    do {
        asm("__scanAnim_ClsnSeek_Loop:");
        if (C1Time < 0 && elemDex[12*e+10] != 0 && *(*(int**)(elemDex+12*e+10)+1) != 0) { //Clsn1 Found
            C1Time = (C1Time != -1 ? -3 : elemtime);
        }
        if (C2Time < 0 && elemDex[12*e+11] != 0 && *(*(int**)(elemDex+12*e+11)+1) != 0) { //Clsn2 Found
            C2Time = elemtime;
        }
        elemtime = elemDex[12*e+1]; //Maximum ElemTime is the way to go here
        asm("__scanAnim_ClsnSeek_Next:");
    } while (++e < elemCount && elemtime >= 0 && (C1Time == -1 || C2Time == -1)); //If we already found both clsn/have overflowed time, then there's no point in searching further?
    asm("__scanAnim_return:");
    return a;
}


//Actually execute the state controller
bool stConExec(int* stateController, int* explClone) {
    int buf; byte dumC; byte dumD;
    asm(".intel_syntax noprefix\n"
        "__eE$stConExec_SEHInit:"
            "pusha;" //We don't know how the fuck this wild ride's gonna crash if it does, so just prep the entire stack/register setup in advance
            "push 0x00000000;" //SEH Flags
            "push 0x00000000;" //SEH HandlerInfo
            "push eax;" //SEH TMP Storage
            "push eax;" //SEH TMP Storage
            "push DWORD PTR fs:[0x00000000];" //FS:[0] push
            "mov DWORD PTR fs:[0x00000000],esp;"
            "lea eax,__eE$stConExec_SEHRepair;" //SEH Repair 
            "mov DWORD PTR [esp+0x08],eax;"
            "lea eax,__eE$stConExec_SEHCatch;" //SEH Catch Address
            "mov DWORD PTR [esp+0x04],eax;"
            "jmp __eE$stConExec_SEHTry;" //Skip over the handler code
        "__eE$stConExec_SEHCatch:"
            "push ebp;" //Usual function header
            "mov ebp,esp;"
            "push ebx;"
            "mov ebx,DWORD PTR [ebp+0x10];" //idc about unwinding so just grab context
            "mov edx,DWORD PTR [ebp+0x0c];" //fetch ERR structure, aka esp at the time 
            "mov eax,DWORD PTR [edx+0x14];" //edi Restoration
            "mov DWORD PTR [ebx+0x0000009c],eax;"
            "mov eax,DWORD PTR [edx+0x18];" //esi Restoration
            "mov DWORD PTR [ebx+0x000000a0],eax;"
            "mov eax,DWORD PTR [edx+0x24];" //ebx Restoration
            "mov DWORD PTR [ebx+0x000000a4],eax;" //edx is unneeded btw
            "mov eax,DWORD PTR [edx+0x2c];" //ecx Restoration
            "mov DWORD PTR [ebx+0x000000ac],eax;" //ecx Restoration
            "mov DWORD PTR [ebx+0x000000b0],0xff8901f4;" //eax -> CRASHFLAG
            "mov eax,DWORD PTR [edx+0x1c];" //ebp Restoration
            "mov DWORD PTR [ebx+0x000000b4],eax;"
            "mov eax,DWORD PTR [edx+0x08];" //eip -> operEvalStackRepair
            "mov DWORD PTR [ebx+0x000000b8],eax;"
            "mov DWORD PTR [ebx+0x000000c4],edx;" //Stack Adjustment
            "xor eax,eax;" //We handled it, continue
            "pop ebx;"
            "mov esp,ebp;"
            "pop ebp;"
            "ret;"
        "__eE$stConExec_SEHTry:"
            "sub esp,0x14;" //Allocate space
            "push esp;" //Push Stack
            "push edi;" //Push Clone
            "mov eax,0x0047e800;" //Store the calling function address
            "call eax;" //CALL FetchRelatives(explClone, STACKDEST)
            "mov DWORD PTR [esp+0x00],esi;" //Push State Controller
            "push edi;" //Push Clone
            "mov edx,0x0046e750;" //Store the calling function address
            "call edx;" //CALL stConExec(explClone, stateController, RELATIVES)
            "add esp,0x20;" //repair stack
        "__eE$stConExec_SEHRepair:"
            "pop DWORD PTR fs:[0x00000000];" //Repair SEH Framework
            "add esp,0x30;"
        : "=a"(buf), "=d"(dumD), "=c"(dumC)
        : "S"(stateController), "D"(explClone)
    );
    return (buf != 0xff8901f4);
};

extern "C" void explExec (int* source, int ID, int* animDataBea, int* infoStorage, int* explSource, int* explClone, int DEKState, dword execFlags) {
    int pos = 0;
    int* stateCode = *(*(int***)(source)+241);
    int stateMax = *(*(int**)(stateCode)+3);
    int* stateMap = *(*(int***)(stateCode)+6);
    int* animData = *(*(int***)(source)+243);
    int* animDataSelf = *(*(int***)(explSource)+243);
    bool isHelper = !!(execFlags&0x01); //Self-explanatory
    bool isSelf = !!(execFlags&0x02); //Exploitative? Infiltrative?
    bool defRead = !!(execFlags&0x04); //Allow direct reading of controllers regardless of code execution
    bool useTime = !!(execFlags&0x08); //15F bonus runtime (exploration only)
    bool execOnly = !!(execFlags&0x10); //Do not explore, simply execute the state (-3 Exploration)
    bool execMinus = !!(execFlags&0x20); //Do not perform persistent processing (-3 Exploration)
    bool explodSeal = !!(execFlags&0x40); //Do not perform Explod modifications (Body, -3 Exploration)
    bool projSeal = !!(execFlags&0x80); //Do not perform Projectile summoning (-3 Exploration)
    //Negative State Exploration is unsupported as most of the time it's code execution.
    //However, in the interest of -3 Exploration support, we'll allow negative states as well
        //Be careful of Noyo's -2147483648 req. in the future
    int stateno;
    if (execOnly) {
        asm("__explExec_ExecStateSeek:");
        //If we're executing the state, search for it and fetch its ID
        stateno = ID;
        ID = 0;
        do { //Just find the state
            asm("__explExec_ExecStateSeek_Loop:");
            if (stateMap[4*ID+2] == stateno) { break; }
            asm("__explExec_ExecStateSeek_Next:");
        } while (++ID < stateMax);
        asm("__explExec_ExecStateCheck:");
        if (ID == stateMax) { return; } //Doesn't exist
        pos = 1024; //Invalid pos as a dummy that won't interfere with exploration
    } else {
        asm("__explExec_ExplStateSeek:");
        //If we're exploring the state based on ID, fetch the state as appropriate
        stateno = stateMap[4*ID+2];
        pos = ID; //TMP
        while (--pos >= 0) {
            asm("__explExec_ExplDupeSeek_Loop:");
            if (stateMap[4*pos+2] == stateno) { break; }
            asm("__explExec_ExplDupeSeek_Next:");
        }
        asm("__explExec_ExplDupeCheck:");
        if (pos >= 0) { return; } //Overlapping states have identical code/can't be accessed normally
        pos = 0;
        //Search for a free/identical slot
        do {
            asm("__explExec_ExplPosSeek_Loop:");
            //Found a free space, re-init and use it
            //I'll investigate common despite it potentially being a storage hog
            //I have 1024 general spots rather than 15 specific spots, so the 55 states found in common can fit in a lot more smoothly
            if (infoStorage[24*pos] == -1) {
                memset(infoStorage+24*pos+1, 0, 92);
                memset(infoStorage+24*pos+7, -1, 24);
                break;
            }
            //Found ourself
            if (infoStorage[24*pos] == ID) { break; }
            //Duplicate state checking is done earlier
            asm("__explExec_ExplPosSeek_Next:");
        } while (++pos < 1024);
        asm("__explExec_ExplPosCheck:");
        if (pos == 1024) { //We've successfully filled the buffer which is marginally annoying :/
            return;
        } //DNE
        //No matter what the situation, flag that we visited
        infoStorage[24*pos+20+2*isSelf+!isHelper] |= 0x40000000;
    }
    asm("__explExec_Clear:");
    int* stateHeader = *(*(int***)(stateCode)+5)+41*ID;
    int* stateConList = *(*(int***)(stateHeader)+5);
    int stCW = 25; //*(*(int**)(stateHeader)+1)<<2; //If people bother tampering with this, uncomment
    int stateConCount = *(*(int**)(stateHeader)+3);
    dword flags = 0; dword flagsRead = 0; //Exploration Flags

    //////////////////////////////////////////////////
    ///////////////// OVERFLOW BLOCK /////////////////
    //////////////////////////////////////////////////
    if (stateConCount > 512 && !execOnly) { //If we're executing, not checking, then we'll just handle it normally
        asm("__explExec_Overflow:");
        flags = 0x00008000;
        const byte ALIVE_UID = 0;
        const byte ALIVE_NONZERO = 1;
        const byte PALNO = 2;
        //Add when ready
        //Ultra-Instant Death / Potential Resurrection
        //3620-3067 = 553
        if (stateConCount >= 554 && (stateConList[553*stCW+3] && (dword)(stateConList[553*stCW+2]) > 1
            || stateConCount >= 555 && stateConList[554*stCW+3] && (dword)(stateConList[554*stCW+2]) > 1
            || stateConCount >= 556 && stateConList[555*stCW+3] && (dword)(stateConList[555*stCW+2]) > 1)
        ) {
            flags |= 1<<ALIVE_NONZERO;
        } else if (stateConCount >= 553 && stateConList[552*stCW+3]) {
            if ((dword)(stateConList[552*stCW+2]) < 2) {
                flags |= 1<<ALIVE_UID;
            } else {
                flags |= 1<<ALIVE_NONZERO;
            }
        }
        //PalNo Tampering
        //5060-3067 = 1993
        if (stateConCount >= 1993 && (stateConList[1992*stCW+3]
            || stateConCount >= 1994 && stateConList[1993*stCW+3]
            || stateConCount >= 1995 && stateConList[1994*stCW+3]
            || stateConCount >= 1996 && stateConList[1995*stCW+3])
        ) {
            flags |= 1<<PALNO;
        }
        asm("__explExec_OverflowFlagWrite:");
        //Flag Storage (Overflow type)
        infoStorage[24*pos] = ID;
        infoStorage[24*pos+1+isSelf] |= ((flags&0xffff)<<16*!isHelper);
        return;
    }
    //////////////////////////////////////////////////
    /////////////// NON-OVERFLOW BLOCK ///////////////
    //////////////////////////////////////////////////
    asm("__explExec_StandardHandling:");
    //Non-Overflow uses standard processing
    int expOut = 0; float expOutF = 0.0; bool expFloat = false; //expression outputs
    int animID = -1; int animElemID; //ChangeAnim value/elem IDs
    int stExecFlag; //State Controller Execution flag
    int buf; int buf1; int buf2; int* point; //generic buffers
    byte timeS; //Stored time
    byte prioS; byte prio; //Stored/Active priority (1 if reading, 2 if infiltrative, 5 if exploitative; there's a bonus of 1 if we're using body exploration in the latter 2)
    byte C1Time = 0xff; byte C2Time = 0xff; bool CAExec = false; //ClsnX times, non-read flag
    dword changed = 0; dword stateConType;
    bool zeroLength = false; //ChangeAnim-related
    //Use MUGEN's data to save on our own coding effort
    int* stackI = (int*)(0x004a19ac);
    int stackIBackup = *stackI;
    int* stackD = (int*)(0x004b6e60); float* stackDF = (float*)(stackD);
    int stackDBackup[stackIBackup+1];
    memcpy(stackDBackup, stackD, 4*(stackIBackup+1));
    //Similar to the above
    int expTMPData[5]; byte expTMPType[5];
    int s = 0; //Best just to put this here
    //Partial Flags
    dword changedRead; int animIDB; bool stateStasis = true;
    dword partialList[20]; //Things that will be assigned upon finalization
    memset(partialList, 0, 80);
    memset(partialList+3, -1, 24); //State Dest, Time should be set to 255 consistently
    //Various values that might be tweaked throughout
    byte time = 0; //Time-inclusive runs up to 15F
    int statetype = 1; int movetype = 0; int physics = 4;
    int statetypeR = 1; int movetypeR = 0; int physicsR = 4;

    /////////////////////////////////////////////////
    /////////////// EXPRESSION PARSER ///////////////
    /////////////////////////////////////////////////
    //Designed explicitly to prevent crashes in 99% of situations that I can think of
    //I suppose the only exception is in helper(X) et al...
    auto expParse = [&] (int* expStruct, int QGP = -1, bool useLiteral = false) {
        asm("__eE$expParse:");
        //Constant
        if (expStruct[1] == 0) {
            asm("__eE$expParse_Constant:");
            expOut = expStruct[2];
            expOutF = *(float*)(&expOut);
            expFloat = !!expStruct[0];
            return true;
        }
        byte len = ((dword)(expStruct[2]) > 127 ? 127 : expStruct[2]);
        if (len == 0) { //Lol Crash
//expFoundCrash:
            asm("__eE$expParse_FoundCrash:");
            return false;
        }
        int* expData = (int*)(expStruct[0]);
        byte* expType = (byte*)(expStruct[1]);
        int* expPoint;
        //Literal
        if (useLiteral && (len == 2 || len == 3)) { //Special checks
            //Var(X) type
            asm("__eE$expParse_LiteralCheckVar:");
            if (expType[0] == 0x00 && expType[1] == 0x03 && (len == 2 || expType[2] == 0x03 && (*(int*)(expData[2]) == TR1_Floor || *(int*)(expData[2]) == TR1_Ceil))) {
                expPoint = (int*)(expData[1]);
                switch (*expPoint) {
                    case TR1_Var:
                        asm("__eE$expParse_LiteralIsVar:");
                        if ((dword)(expData[0]) >= 60) { break; }
                        expOut = 0xf4000000 + expData[0];
                        expFloat = false;
                        return true;
                    case TR1_FVar:
                        asm("__eE$expParse_LiteralIsFVar:");
                        if ((dword)(expData[0]) >= 40) { break; }
                        expOut = 0xf400003c + expData[0];
                        expFloat = false;
                        return true;
                    case TR1_SysVar:
                        asm("__eE$expParse_LiteralIsSysVar:");
                        if ((dword)(expData[0]) >= 5) { break; }
                        expOut = 0xf4000064 + expData[0];
                        expFloat = false;
                        return true;
                    case TR1_SysFVar:
                        asm("__eE$expParse_LiteralIsSysFVar:");
                        if ((dword)(expData[0]) >= 5) { break; }
                        expOut = 0xf4000069 + expData[0];
                        expFloat = false;
                        return true;
                }
            }
        }
        asm("__eE$expParse_NonLiteral:");
        byte p = 0;
        int i = 0; *stackI = -1; byte lenE = 0;
        byte dumD = 0; byte dumC = 0;
        do {
            asm("__eE$expParse_TypeByType_Loop:");
            switch (expType[p]) {
                //Certain Operators have an AssertInt protocol
                //Also, there's no convenient ASM, so we need to use specific workarounds.
                case 0x02:
                    asm("__eE$expParse_TypeByType_Oper:");
                    lenE = 2 - (expData[p] == OP1_BitNOT || expData[p] == OP1_LogicNOT || expData[p] == OP1_Negate) + (expData[p] >= OP2_RangeExEx && expData[p] <= OP2_RangeInIn);
                    //Malformed expression
                    if (i < 2*lenE || lenE == 3 && (p+1 >= len || expData[p+1] != OP2_Equal && expData[p+1] != OP2_Inequal)) {
                        return false;
                    //If BitNOT, check for a float
                    } else if (expData[p] == OP1_BitNOT && stackD[i-1] == 0x01) {
                        return false;
                    //Assert int in only the right position
                    } else if (expData[p] == OP2_IntAssign && stackD[i-1] == 0x01) {
                        return false;
                    //Assert int in both positions
                    } else if ((expData[p] == OP2_Mod || expData[p] >= OP2_BitAND && expData[p] <= OP2_BitXOR) && (stackD[i-1] == 0x01 || stackD[i-3] == 0x01)) {
                        return false;
                    //-2147483648 / -1 is a division by 0, but SHOULD be handled by SEH, so there's no problem
                    }
                    switch (lenE) {
                        case 1:
                            asm("__eE$expParse_TypeByType_OperIs1Input:");
                            expTMPData[0] = stackD[i-2]; expTMPType[0] = stackD[i-1];
                            expTMPData[1] = expData[p]; expTMPType[1] = expType[p];
                            break;
                        case 3:
                            asm("__eE$expParse_TypeByType_OperIsRange:");
                            expTMPData[0] = stackD[i-6]; expTMPType[0] = stackD[i-5];
                            expTMPData[1] = stackD[i-4]; expTMPType[1] = stackD[i-3];
                            expTMPData[2] = stackD[i-2]; expTMPType[2] = stackD[i-1];
                            expTMPData[3] = expData[p]; expTMPType[3] = expType[p];
                            lenE++; p++; //Skip the equality operator as we know it's included or it's malformed; also, for our purposes of expression creation, length is 4
                            expTMPData[4] = expData[p]; expTMPType[4] = expType[p];
                            break;
                        case 2:
                            asm("__eE$expParse_TypeByType_OperIs2Input:");
                            expTMPData[0] = stackD[i-4]; expTMPType[0] = stackD[i-3];
                            expTMPData[1] = stackD[i-2]; expTMPType[1] = stackD[i-1];
                            expTMPData[2] = expData[p]; expTMPType[2] = expType[p];
                            break;
                    }
                    asm("__eE$expParse_TypeByType_OperTypeFound:");
                    //Evaluate operation using some cheaty workarounds
                    asm(".intel_syntax noprefix\n"
                        "__eE$expParse_TypeByType_OperSEHInit:"
                            "pusha;" //We don't know how the fuck this wild ride's gonna crash if it does, so just prep the entire stack/register setup in advance
                            "push 0x00000000;" //SEH Flags
                            "push 0x00000000;" //SEH HandlerInfo
                            "push eax;" //SEH TMP Storage
                            "push eax;" //SEH TMP Storage
                            "push DWORD PTR fs:[0x00000000];" //FS:[0] push
                            "mov DWORD PTR fs:[0x00000000],esp;"
                            "push ecx;" //Push length
                            "push edx;" //Push Types
                            "push eax;" //Push Data
                            "lea eax,__eE$expParse_TypeByType_OperSEHRepair;" //SEH Repair 
                            "mov DWORD PTR [esp+0x14],eax;"
                            "lea eax,__eE$expParse_TypeByType_OperSEHCatch;" //SEH Catch Address
                            "mov DWORD PTR [esp+0x10],eax;"
                            "jmp __eE$expParse_TypeByType_OperSEHTry;" //Skip over the handler code
                        "__eE$expParse_TypeByType_OperSEHCatch:"
                        //...wait, this labels and even goes so far as to transcend C++'s native label support
                        //could i. in theory.
                            "push ebp;" //Usual function header
                            "mov ebp,esp;"
                            "push ebx;"
                            "mov ebx,DWORD PTR [ebp+0x10];" //idc about unwinding so just grab context
                            "mov edx,DWORD PTR [ebp+0x0c];" //fetch ERR structure, aka esp at the time 
                            "mov eax,DWORD PTR [edx+0x14];" //edi Restoration
                            "mov DWORD PTR [ebx+0x0000009c],eax;"
                            "mov eax,DWORD PTR [edx+0x18];" //esi Restoration
                            "mov DWORD PTR [ebx+0x000000a0],eax;"
                            "mov eax,DWORD PTR [edx+0x24];" //ebx Restoration
                            "mov DWORD PTR [ebx+0x000000a4],eax;" //edx is unneeded btw
                            "mov BYTE PTR [ebx+0x000000ac],0x01;" //ecx -> isFloat
                            "mov DWORD PTR [ebx+0x000000b0],0xff8901f4;" //eax -> CRASHFLAG
                            "mov eax,DWORD PTR [edx+0x1c];" //ebp Restoration
                            "mov DWORD PTR [ebx+0x000000b4],eax;"
                            "mov eax,DWORD PTR [edx+0x08];" //eip -> operEvalStackRepair
                            "mov DWORD PTR [ebx+0x000000b8],eax;"
                            "mov DWORD PTR [ebx+0x000000c4],edx;" //Stack Adjustment
                            "xor eax,eax;" //We handled it, continue
                            "pop ebx;"
                            "mov esp,ebp;"
                            "pop ebp;"
                            "ret;"
                        "__eE$expParse_TypeByType_OperSEHTry:"
                            "mov edx,esp;" //Expression HOLP
                            "push esp;" //Dummy
                            "push esp;" //Store Directly on the stack
                            "push esp;" //Store Directly on the stack
                            "push edx;" //Expression
                            "push ebx;" //Our clone of interest
                            "mov edx,0x00407780;" //Store the calling function address
                            "call edx;" //CALL ExpressionParse(Enemy,SimplifiedCode,esp+0x0c,esp+0x10)
                            "lea ecx,[eax-0x01];" //expFloat
                            "mov eax,DWORD PTR [esp+0x0c];" //expOut
                            "mov edx,DWORD PTR [esp+0x10];" //expOutF
                            "add esp,0x20;" //Repair stack
                        "__eE$expParse_TypeByType_OperSEHRepair:"
                            "pop DWORD PTR fs:[0x00000000];" //Repair SEH Framework
                            "add esp,0x30;"
                        : "=a"(expOut), "=d"(expOutF), "=c"(expFloat)
                        : "a"(expTMPData), "c"(lenE+1), "d"(expTMPType), "b"(explClone)
                    );
                    if (expFloat && expOut == 0xff8901f4) { return false; } //Hidden fuckery
                    lenE -= 1 + (lenE == 4);
                    //lenE = 0 if it's a 1-op; in other words, keep position the same and overwrite the only argument
                    //lenE = 1 if it's a 2-op; in other words, deduct position by 2 and overwrite the first argument
                    //lenE = 2 if it's a range; in other words, deduct position by 4 and overwrite the left side
                    *stackI = (i -= 2*lenE) - 1;
                    if (expFloat) {
                        stackDF[i-2] = expOutF; stackD[i-1] = 1; 
                    } else {
                        stackD[i-2] = expOut; stackD[i-1] = 0; 
                    }
                    break;
                //Certain Triggers have strange behavior or we might want to manipulate them at certain moments
                case 0x03:
                    asm("__eE$expParse_TypeByType_Trigger:");
                    expPoint = (int*)(expData[p]);
                    //AnimElemTime(<1) will result in a crash
                    if (*expPoint == TR1_AnimElemTime && (stackD[i-1] == 0x01 ? (int)(stackDF[i-2]) : stackD[i-2]) < 1) {
                        return false;
                    //AnimElemNo on a 0F animation will result in a crash
                    } else if (*expPoint == TR1_AnimElemNo && zeroLength) {
                        return false;
                    //In certain cases, it might be necessary to specify a special parameter at runtime
                    //This is essentially that.
                    } else if (QGP != -1) {
                        //C_Kain and others' 130 <-> 131
                        //Requires EnemyNear,HitDefAttr = SCA,AA,AT,AP to be always true; requires random to be 0
                        //So if random is 999 and we nullify HitDefAttr, logically it won't work, right?
                        if (QGP == 0 && (*expPoint == TR0_Random || *expPoint == TR0_HitDefAttr)) {
                            asm("__eE$expParse_TypeByType_TriggerKainCheckBranch:");
                            stackD[i] = (*expPoint == TR0_HitDefAttr);
                            stackD[i+1] = 0;
                            *stackI = (i += 2) - 1;
                            break;
                        } else if (QGP == 1 && (*expPoint == TR0_Random || *expPoint == TR0_HitDefAttr)) {
                            asm("__eE$expParse_TypeByType_TriggerKainCheckNonBranch:");
                            stackD[i] = 999*(*expPoint == TR0_Random);
                            stackD[i+1] = 0;
                            *stackI = (i += 2) - 1;
                            break;
                        }
                    }
                    asm("__eE$expParse_TypeByType_TriggerNonCrash:");
                    //Evaluate trigger using the standard handler
                    asm(".intel_syntax noprefix\n"
                    "__eE$expParse_TypeByType_TriggerSEHInit:"
                        "pusha;" //We don't know how the fuck this wild ride's gonna crash if it does, so just prep the entire stack/register setup in advance
                        "push 0x00000000;" //SEH Flags
                        "push 0x00000000;" //SEH HandlerInfo
                        "push eax;" //SEH TMP Storage
                        "push eax;" //SEH TMP Storage
                        "push DWORD PTR fs:[0x00000000];" //FS:[0] push
                        "mov DWORD PTR fs:[0x00000000],esp;"
                        "push eax;" //Push Trigger
                        "push ecx;" //Push Clone
                        "lea eax,__eE$expParse_TypeByType_TriggerSEHRepair;" //SEH Repair 
                        "mov DWORD PTR [esp+0x10],eax;"
                        "lea eax,__eE$expParse_TypeByType_TriggerSEHCatch;" //SEH Catch Address
                        "mov DWORD PTR [esp+0x0c],eax;"
                        "jmp __eE$expParse_TypeByType_TriggerSEHTry;" //Skip over the handler code
                    "__eE$expParse_TypeByType_TriggerSEHCatch:"
                        "push ebp;" //Usual function header
                        "mov ebp,esp;"
                        "push ebx;"
                        "mov ebx,DWORD PTR [ebp+0x10];" //idc about unwinding so just grab context
                        "mov edx,DWORD PTR [ebp+0x0c];" //fetch ERR structure, aka esp at the time 
                        "mov eax,DWORD PTR [edx+0x14];" //edi Restoration
                        "mov DWORD PTR [ebx+0x0000009c],eax;"
                        "mov eax,DWORD PTR [edx+0x18];" //esi Restoration
                        "mov DWORD PTR [ebx+0x000000a0],eax;"
                        "mov eax,DWORD PTR [edx+0x24];" //ebx Restoration
                        "mov DWORD PTR [ebx+0x000000a4],eax;" //edx is unneeded btw
                        "mov eax,DWORD PTR [edx+0x2c];" //ecx Restoration
                        "mov DWORD PTR [ebx+0x000000ac],eax;" //ecx Restoration
                        "mov DWORD PTR [ebx+0x000000b0],0xff8901f4;" //eax -> CRASHFLAG
                        "mov eax,DWORD PTR [edx+0x1c];" //ebp Restoration
                        "mov DWORD PTR [ebx+0x000000b4],eax;"
                        "mov eax,DWORD PTR [edx+0x08];" //eip -> operEvalStackRepair
                        "mov DWORD PTR [ebx+0x000000b8],eax;"
                        "mov DWORD PTR [ebx+0x000000c4],edx;" //Stack Adjustment
                        "xor eax,eax;" //We handled it, continue
                        "pop ebx;"
                        "mov esp,ebp;"
                        "pop ebp;"
                        "ret;"
                    "__eE$expParse_TypeByType_TriggerSEHTry:"
                        "mov edx,0x0047aa60;" //Store the calling function address
                        "call edx;" //CALL TriggerParse(Simplified)
                        "add esp,0x08;" //repair stack
                    "__eE$expParse_TypeByType_TriggerSEHRepair:"
                        "pop DWORD PTR fs:[0x00000000];" //Repair SEH Framework
                        "add esp,0x30;"
                        : "=a"(i), "=d"(dumD), "=c"(dumC)
                        : "a"(expData[p]), "c"(explClone)
                    );
                    if (i == 0xff8901f4) { return false; }
                    i = *stackI + 1;
                    break;
                //Ints/Floats only need to be pushed to the stack
                default:
                    asm("__eE$expParse_TypeByType_Number:");
                    stackD[i] = expData[p];
                    stackD[i+1] = expType[p];
                    *stackI = (i += 2) - 1;
            }
            asm("__eE$expParse_TypeByType_Next:");
        } while (++p < len);
        //Finally, process the remainder
        asm("__eE$expParse_CheckAndWrite:");
        if (i != 2) { //Malformed expression
            return false;
        }
        expOut = stackD[0];
        expOutF = stackDF[0];
        expFloat = (stackD[1] == 0x01);
        asm("__eE$expParse_return:");
        return true;
    };

    /////////////////////////////////////////////////
    /////////////// EXECUTION HANDLER ///////////////
    /////////////////////////////////////////////////
    //In order to determine whether a state controller is allowed to execute or not, parse each trigger with our built-in parser
    //This should also hopefully avoid crashes
    auto rollExecution = [&] (int* stateController, int QGP = -1) {
        //asm("__eE$rollExecution:");
        //Emulate persistent behavior
        if (*((byte*)(explClone)+3068+s) != 0x00) { return 0; }
        int tc = *(*(int**)(stateController)+3);
        int* trigDex = *(*(int***)(stateController)+5);
        int* trigMap = *(*(int***)(stateController)+6);
        int t; int ta = 0; int ti = 0;
        //TriggerAll has a -1 in the last position
        if (trigMap[4*(tc-1)+2] == -1) {
            ta = trigMap[4*(tc-1)+3]+1; //Store the total for later
            t = tc-ta; //First TriggerAll is probably here
            do {
                //asm("__eE$rollExecution_TriggerAll_Loop:");
                ASSERT_FAIL(expParse(trigDex+3*t, QGP)) { return -1; }
                if (expFloat ? expOutF == 0 || expOutF != expOutF : expOut == 0) { return 0; } //Since it's 0 and part of a triggerall, can't do anything with it
                //asm("__eE$rollExecution_TriggerAll_Next:");
            } while (++t < tc);
        }
        //asm("__eE$rollExecution_StandardTrigger:");
        t = 0; ti = 0; //Prep
        do {
            //asm("__eE$rollExecution_TriggerX_Loop:");
            ASSERT_FAIL(expParse(trigDex+3*t, QGP)) { return -1; } //Well that sucks huh it would've crashed here
            if (expFloat ? expOutF == 0 || expOutF != expOutF : expOut == 0) { //Since it's 0 and part of a trigger, search for the next one
                while (++t < tc-ta) { //Lightweight loop
                    //asm("__eE$rollExecution_TriggerX_NextTrig_Loop:");
                    if (trigMap[4*t+2] != ti) {
                        ti = trigMap[4*t+2];
                        break;
                    }
                    //asm("__eE$rollExecution_TriggerX_NextTrig_Next:");
                }
                //asm("__eE$rollExecution_TriggerX_FailCheck:");
                if (t == tc-ta) { return 0; } //If we've run out of triggers, congratulations! We've failed
            } else { //Since it's 1 and part of a trigger, ensure that we stay in bounds
                //asm("__eE$rollExecution_TriggerX_BoundCheck:");
                if (t == tc-ta-1 || trigMap[4*(t+1)+2] != ti) { //If we've moved to the next trigger or have simply reached the end, congratulations! We've passed
                    return 1;
                }
                t++;
            }
            //asm("__eE$rollExecution_TriggerX_Next:");
        } while (t < tc-ta);
        //asm("__eE$rollExecution_return:");
        return 0; //Precaution
    };

    /////////////////////////////////////////////////
    ////////////// CONSTANT DEFINITION //////////////
    /////////////////////////////////////////////////
    //NO_CHANGE constants (as encoded by MUGEN itself)
    const int NO_CHANGE = 32751;
    const float NO_CHANGE_F = -32760.0;
    //Exploration Byte Indices
    const byte MOVETYPE_A = 0;
    const byte MOVETYPE_H = 1;
    const byte MOVETYPE_U = 2;
    const byte HITFALL = 3;
    const byte CLSN1_GRANT = 4;
    const byte CLSN2_GRANT = 5;
    const byte INSTANT_DEATH = 6;
    const byte LIFE_DEC = 7;
    const byte NORMAL_GEN = 8;
    const byte PLAYER_GEN = 9;
    const byte PAUSE_DELETE = 12;
    const byte SUPER_DELETE = 13;
    //Changed Bookkeeping Indices
    const byte CSTATETYPE = 0;
    const byte CMOVETYPE = 1;
    const byte CPHYSICS = 2;
    const byte CLIFE = 5;
    const byte CHANGEANIM = 15;
    const byte HITBY = 16;
    const byte ARMOR = 17;
    const byte HITDEF = 18;
    const byte NOKO = 19;
    const byte UNHITTABLE = 28;
    const byte OTIYA = 29;
    const byte DESTROY = 30;
    const byte CHANGESTATE = 31;

    //////////////////////////////////////////////////
    ////////////// CLONE INITIALIZATION //////////////
    //////////////////////////////////////////////////
    asm("__explExec_CloneInit:");
    //Copy to the exploration clone
    memcpy(explClone, explSource, 13412);
    memcpy(explClone+912, source+912, 440); //Variable data should be a direct clone (var(X) Address Storage)
    //Target Data
    memcpy(explClone+3392, *(int**)(explClone+136), 48);
    memcpy(explClone+3424, *(*(int***)(explClone+136)+5), 256);
    memcpy(explClone+3488, *(*(int***)(explClone+136)+6), 128);
    explClone[136] = (int)(explClone+3392);
    explClone[3392+5] = (int)(explClone+3424);
    explClone[3392+6] = (int)(explClone+3488);
    if (explClone[3392+2] > 1) { //TargetXXX State Controller freeze prevention
        explClone[3392+2] = 1;
        explClone[3392+10] = explClone[3392+9];
        for (int t = explClone[3392+9]+1; t < explClone[3392+3]; t++) explClone[3424+(t<<2)] = 0;
    }
    //Name/Etc. Data
    memcpy(explClone+3520, *(int**)(explClone), 984);
    memcpy(explClone+3766, *(int**)(source)+246, 772); //AI file information should be a direct clone (crash avoidance)
    //explClone[0] = (int)(explClone+3520); //To be implemented in the loop system
    //Anim Data
    memcpy(explClone+3968, *(int**)(explClone+1263), 64);
    explClone[1263] = (int)(explClone+3968);
    //Parent Data (Root is unchanged)
    if (isHelper) {
        asm("__explExec_HelperInit:");
        //Copy memory as expected
        memcpy(explClone+4096, *(int**)(explClone+2440), 13412);
        explClone[7] = 1;
        explClone[2438] = -2108901244; //Unique isHelper ID as it's impossible to guess which helper it would be (may as well ensure none of them match)
        explClone[2440] = (int)(explClone+4096); //Unconditional
        for (int a = 0; a < 8192; a++) {
            if (explClone[a] == (int)(explSource)) {
                explClone[a] = (int)(explClone);
            } else if (explClone[a] != explSource[2441] && explClone[a] == explSource[2440]) {
                explClone[a] = (int)(explClone+4096);
            } else if (explClone[a] == explSource[0]) {
                explClone[a] = (int)(explClone+3520);
            }
        }
    } else {
        asm("__explExec_NonHelperInit:");
        //Reset
        memset(explClone+4096, 0, 16384);
        explClone[7] = 0;
        explClone[2438] = 0;
        explClone[2440] = 0;
        explClone[2441] = 0;
        for (int a = 0; a < 4096; a++) {
            if (explClone[a] == (int)(explSource)) {
                explClone[a] = (int)(explClone);
            } else if (explClone[a] == explSource[0]) {
                explClone[a] = (int)(explClone+3520);
            }
        }
    }
    asm("__explExec_PlayerBackup:");
    //Back up important player data
    for (int p = 0; p < 63; p++) {
        asm("__explExec_PlayerBackup_Loop:");
        point = *(*(int***)(0x004b5b4c)+0x2dd5+p); //Player[p]
        if (point != nullptr) {
            memcpy(explClone+16384+144*p+0, point+86, 16); //isActive / isFrozen / Life / LifeMax
            memcpy(explClone+16384+144*p+4, point+762, 20); //DefCode / ThrowCode / ThrowOwner / StateNo / PrevStateNo
            *(explClone+16384+144*p+9) = point[905]; //Alive
            memcpy(explClone+16384+144*p+10, point+912, 460); //Variables / UnhittableTime / HitByAttr+2 / HitByTime+2
            *(explClone+16384+144*p+125) = point[1265]; //PalNo
            memcpy(explClone+16384+144*p+126, point+2438, 20); //HelperID / ParentID / Parent / Root / HelperType
            if (point[762] != 0) { *(explClone+16384+144*p+143) = **(int**)(point+762); } //InnerDefCode (Root Deleter Countermeasures)
        }
        asm("__explExec_PlayerBackup_Next:");
    }
    asm("__explExec_CompleteInit:");
    memcpy(explClone+28672, *(int**)(0x004b5b4c), 51536); //MUGEN Backup (Player Erasure Countermeasures)
    //Set up the exploration clone for proper exploration
    if (!execOnly) {
        explClone[88] = explClone[89]/(1+(explClone[89]>1)); //Life = LifeMax/2 (if 1 or negative it'll just be LifeMax)
        explClone[104] = 0; //Pos Y = 0 (precaution)
        explClone[110] = 0; //Vel Y = 0 (precaution)
        explClone[601] = (int)(*explSource)+948; //Command Pointer = User (it LOOKS incorrect but it is, i promise)
        explClone[762] = (int)(stateCode); //State Code = Source
        explClone[763] = 0; //Throw Code = NULLPTR
        explClone[764] = -1; //Throw Player = None
        explClone[765] = stateno; //StateNo = Blatant
        explClone[766] = -201326482; //PrevStateNo = 0xf400006e (var(110) = prev)
        memset(explClone+767, 0, stateConCount); //Persistent reset
        explClone[895] = 0; //Time = 0
        explClone[896] = 1; //StateType = S
        explClone[897] = 0; //MoveType = I
        explClone[898] = 4; //Physics = INVALID
        explClone[899] = 1; //Ctrl = 1 (we're just now entering)
        explClone[902] = 0; //HitPauseTime = 0
        explClone[905] = (source[905] == 0 ? 1 : source[905]); //Alive = Source (if 0, it's reset to 1 as a precaution)
        explClone[1022] = 0; //UnhittableTime = 0
        *(word*)(explClone+1023) = 4096; //HitBy Value = INVALID
        *(word*)(explClone+1024) = 4096; //HitBy Value2 = INVALID
        explClone[1025] = -244; //HitBy Time = -244
        explClone[1026] = -244; //HitBy Time2 = -244
        explClone[1037] = 0; //GetHitVar(Damage) = 0 (seems like it'd be annoying!!!!)
        explClone[1052] = 0; //GetHitVar(Fall) = 0 (if not reset properly, Nothingnull will crash!!!!)
        explClone[1053] = 0; //GetHitVar(Fall.Damage) = 0 (seems like it'd be annoying!!!!)
        memset(explClone+1070, 0, 160); //Armor Nullification
        explClone[1110] = 0; //HitDef Activity Flag = 0 (even with a blank attr, HitDef activity will still be flagged if activated)
        explClone[1264] = 0; //ChangeAnim2 = NULLPTR
        explClone[1265] = source[1265]; //PalNo = Source
        explClone[3520+241] = (int)(stateCode); //SelfStateCode = Source (Precaution)
        explClone[3520+243] = (int)(animDataSelf); //SelfAnimData = Orig (for selfanimexist)
        explClone[3968+0] = (int)(animDataBea); //AnimData = Beatrix's
        explClone[3968+3] = 1; //ID = 1, aka Anim 0 (it's highly unlikely that the opponent will only have 1 animation)
        explClone[3968+5] = (animElemID = 2); //AnimElemID = 2
        explClone[3968+6] = 2; //AnimElemTime(1) = 2
        explClone[3968+7] = 2; //AnimElemLoopTime(1) = 2
    }
    //If we're executing a state in a standard way upon entry, execute header 
    if (explClone[895] == 0 && !execMinus) {
        //Inspect the header for certain elements
        if (stateHeader[1] != -1) { //StateType
            explClone[896] = *(stateHeader+0x01);
            changed |= 1<<CSTATETYPE;
        }
        if (stateHeader[2] != -1) { //MoveType
            explClone[897] = *(stateHeader+0x02);
            changed |= 1<<CMOVETYPE;
        }
        if (stateHeader[3] != -1) { //Physics
            explClone[898] = *(stateHeader+0x03);
            changed |= 1<<CPHYSICS;
        }
        ASSERT_FAIL(expParse(stateHeader+4)) { goto expOTIYA; } //Juggle
        ASSERT_FAIL(expParse(stateHeader+7)) { goto expOTIYA; } //FaceP2 (meaningless)
        ASSERT_FAIL(expParse(stateHeader+10)) { goto expOTIYA; } //HitDefPersist
        if (expFloat) { expOut = (int)(expOutF); } //Truncate
        if (expOut != 0) { //Mark persistence
            partialList[1] = 0x00008000;
        } else { //Otherwise clear (exec precaution)
            explClone[1110] = 0;
            explClone[1113] = 0;
            explClone[1153] = 0;
        }
        ASSERT_FAIL(expParse(stateHeader+13)) { goto expOTIYA; } //MoveHitPersist
        if (expFloat) { expOut = (int)(expOutF); } //Truncate
        if (!execOnly || expOut == 0) { //Reset if applicable
            explClone[908] = 0;
            explClone[909] = 0;
        }
        ASSERT_FAIL(expParse(stateHeader+16)) { goto expOTIYA; } //HitCountPersist
        if (expFloat) { expOut = (int)(expOutF); } //Truncate
        if (expOut == 0) { //Reset if applicable
            explClone[1255] = 0;
            explClone[1256] = 0;
        }
        if (stateHeader[19] != 0) { ASSERT_FAIL(expParse(stateHeader+20)) { goto expOTIYA; } } //SprPriority
        ASSERT_FAIL(expParse(stateHeader+23)) { goto expOTIYA; } //Vel X
        if (!expFloat || expOut != *(int*)(&NO_CHANGE_F)) { *(float*)(explClone+109) = explClone[100]*(expFloat ? expOutF : expOut); } //needs facing mul
        ASSERT_FAIL(expParse(stateHeader+26)) { goto expOTIYA; } //Vel Y
        if (!expFloat || expOut != *(int*)(&NO_CHANGE_F)) { *(float*)(explClone+110) = (expFloat ? expOutF : expOut); }
        ASSERT_FAIL(expParse(stateHeader+29)) { goto expOTIYA; } //Vel Z
        if (!expFloat || expOut != *(int*)(&NO_CHANGE_F)) { *(float*)(explClone+111) = (expFloat ? expOutF : expOut); }
        ASSERT_FAIL(expParse(stateHeader+32)) { goto expOTIYA; } //Ctrl
        if (expFloat || expOut != -1) {
            if (expFloat) { expOut = (int)(expOutF); } //Truncate
            if (expOut == 0 || expOut == 1) { explClone[898] = (expOut == 1); } //Invalid doesn't change anything
        }
        ASSERT_FAIL(expParse(stateHeader+35)) { goto expOTIYA; } //Anim
        if (expFloat) { expOut = (int)(expOutF); } //Truncate
        if (expOut >= 0) { //Negative is ignored
            animID = scanAnim(animData, expOut, 1, buf1, buf2);
            if (animID == -2) { //0-length will crash immediately
                goto expOTIYA;
            } else if (animID != -1) { //-1 is NO_CHANGE
                *(*(int**)(explClone+1263)+0) = (int)(animData);
                *(*(int**)(explClone+1263)+3) = animID;
                *(*(int**)(explClone+1263)+5) = (animElemID = 0);
                *(*(int**)(explClone+1263)+6) = 0;
                *(*(int**)(explClone+1263)+7) = 0;
                zeroLength = (buf1 == -2 || buf1 == -3); //-3 is a 0-length but exists flag
                if (zeroLength && buf1 == -3) { buf1 = 0; } //Clear
                if (buf1 >= 0 || buf1 == -3) { C1Time = (buf1 > 254 ? 254 : buf1); }
                if (buf2 >= 0) { C2Time = (buf2 > 254 ? 254 : buf2); }
                changed |= 1<<CHANGEANIM;
                CAExec = true; 
            }
        }
        ASSERT_FAIL(expParse(stateHeader+38)) { goto expOTIYA; } //PowerAdd
        if (execOnly) { //Only execute if needed
            if (expFloat) { expOut = (int)(expOutF); } //Truncate
            explClone[92] += expOut;
            if (explClone[92] > explClone[93]) { explClone[92] = explClone[93]; }
        }
    }
    //From this point on, what's actually executed may differ slightly from what COULD be executed.
    //This secondary changed variable reflects what's read regardless of stExecFlag.
    changedRead = changed;
    animIDB = animID;
    do {
        asm("__explExec_Frame_Loop:");
        s = 0;
        //Def-Level Backup
        memcpy(explClone+8192, explClone, 32768);
        do {
            asm("__explExec_StCon_Loop:");
            stateConType = stateConList[s*stCW+4];
            //As reading will read, well, every state controller, we keep going past changestates/et al. even though we won't execute
            stExecFlag = (stateStasis ? rollExecution(stateConList+s*stCW) : 0);
            if (stExecFlag < 0) { goto expOTIYA; } //Lol
            if (stExecFlag || time == 0 && defRead) { //Manually check
                prio = (stExecFlag ? (3-isHelper)*(1+isSelf) : 1);
                point = (int*)(stateConList[s*stCW+24]);
                asm("__explExec_StCon_Switch:");
                switch (stateConType) {
                    ///////////////////////////////////////////////////
                    /////////////////EXPLORATION BLOCK/////////////////
                    ///////////////////////////////////////////////////
                    //State Changes should have their destinations recorded
                    case SC_ChangeState:
                    case SC_SelfState:
                        asm("__explExec_StCon_ChangeState:");
                        ASSERT_FAIL(expParse(stateConList+s*stCW+9, -1)) { goto expOTIYA; } //Hidden fuckery in Ctrl (B-Koakuma)
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6, -1, true)) { goto expOTIYA; }
                        if (execOnly) { //No need to preserve
                            if (stExecFlag) { stateStasis = false; }
                            goto __explExec_SC_ForceDefault;
                        }
                        //Permanent Girl Mizuka's ChangeState, PrevStateNo ChangeState
                        //Special conditions involving variables will be returned as 0xf4000000~0xf400006e
                        //Aka -201326592 ~ -201326482
                        if (!expFloat && expOut >= -201326592 && expOut <= -201326482) { 
                            partialList[3] = expOut;
                            if (stExecFlag) { stateStasis = false; }
                            break;
                        }
                        if (expFloat) { expOut = (int)(*(float*)(&expOut)); } //Truncate float
                        if (expOut < 0) { expOut = 0; } //Negative is sent to 0 instead (Internal specification)
__explExec_SC_ChangeState_PostExp: //TagOut/TagIn return here
                        asm("__explExec_StCon_ChangeState_PostExp:");
                        if (expOut == stateno) { //Loop found ?_?
                            if (stExecFlag || partialList[3] == -1) { partialList[3] = stateno; }
                            if (stExecFlag && time == 0) { goto expOTIYA; } //Uh oh.
                            if (stExecFlag) { stateStasis = false; }
                            break;
                        }
                        //DEK et al. are marginally annoying because every state is treated as a special one.
                        //If all states are special in this same way, none of them are :S
                        //99.9% of the time it's in so many ChangeStates that we can skip past them
                        //Most of the time it's a static value as well (except some edits who have special handlers)
                        //For comparison (meaningless, but teaches the concept),
                            //D.P.Syaoren: ChangeState in position 1, 999988800
                            //A-Suika: ChangeState in position 1, 2141330000
                            //M-Reimu (v2): ChangeState in position 2, EXP
                        //So basically if it's an expression or whatever and we've already simplified it, don't store it either to be safe
                        //Otherwise, do so
                        //List of states that I can discard
                        //120,130~132,140,150~155 (too common)
                            //But if StateType is out of range, this state becomes a crash state
                        if (expOut == 120 || expOut >= 130 && expOut <= 132 || expOut == 140 || expOut >= 150 && expOut <= 155) {
                            if (*(dword*)(explClone+896) > 3) { goto expOTIYA; } //If a potentially problematic StateType is what we're using, though, abandon immediately
                            //Uh oh looks like we found a KAIN state
                            //This is default common behavior but, for no reason whatsoever, it has the potential to break horribly
                            if (time == 0 && (stateno == 130 && expOut == 131 || stateno == 131 && expOut == 130)) {
                                memset(explClone+602, stateno==131, 128); //1 if stateno = 131, 0 if stateno = 130 (basically force stickage normally)
                                buf = rollExecution(stateConList+s*stCW, 0); //QGP 0
                                if (buf < 0) { goto expOTIYA; } //WHY HERE
                                if (buf) {
                                    memset(explClone+602, stateno==130, 128); //The exact opposite (to force a branch normally)
                                    buf = rollExecution(stateConList+s*stCW, 1); //QGP 1
                                    if (buf <= 0) { goto expOTIYA; } //Well then. Seems like this mysteriously only activates when RNG is on its side. Hmmmmm....
                                }
                            }
                            partialList[19] |= 2048; //Guard ChangeState Exists
                            if (stExecFlag) { stateStasis = false; } //Any further data acquisition would be unreasonable
                            break;
                        //0 (too common)
                        //52 (too common?)
                        //5150 (too common)
                        //A potential DEK state
                        //Any expression (too unpredictable)
                        } else if (expOut == 0 || expOut == 52 || expOut == 5150 || expOut == DEKState || *(stateConList+s*stCW+7) != 0) {
                            if (stExecFlag) { stateStasis = false; }
                            break;
                        }
                        if (stExecFlag || partialList[3] == -1) { partialList[3] = expOut; }
                        if (stExecFlag) { stateStasis = false; }
                        break;
                    case SC_TagOut: //ChangeState StateNo equivalent
                    case SC_TagIn:
                        asm("__explExec_StCon_TagOut:");
                        if (execOnly) { //No need to preserve
                            if (stExecFlag) { stateStasis = false; }
                            goto __explExec_SC_ForceDefault;
                        }
                        expOut = explClone[765];
                        goto __explExec_SC_ChangeState_PostExp;
                    //LifeSet <= 0 = Instant Death
                    case SC_LifeSet:
                        asm("__explExec_StCon_LifeSet:");
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; }
                        if (execOnly) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        //If we see instant death, set the flag
                        if (expOut <= 0) {
                            changedRead |= 1<<INSTANT_DEATH;
                            if (stExecFlag) { changed |= 1<<INSTANT_DEATH; }
                        //Otherwise, clear the respective flag if we set it this frame
                        } else if (stExecFlag && changed&1<<CLIFE) {
                            changed &= ~(1<<INSTANT_DEATH);
                            changedRead &= ~(1<<INSTANT_DEATH);
                            //If life-X is a relevant thing, add a case for life decrementation here
                        } else if (changedRead&1<<CLIFE) {
                            changedRead &= ~(1<<INSTANT_DEATH);
                        }
                        changedRead |= 1<<CLIFE;
                        if (stExecFlag) {
                            changed |= 1<<CLIFE;
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                        }
                        break;
                    //LifeAdd <= 0 = Decrementation
                    case SC_LifeAdd:
                        asm("__explExec_StCon_LifeAdd:");
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; }
                        if (execOnly) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        //Special case for Instant Death
                        if (expOut < 0 && expOut < -explClone[89] && stateConList[s*stCW+9] != 0) { //This is just instant death with aspirations
                            changed |= 1<<INSTANT_DEATH;
                        //If we see decrementation, flag it
                        } else if (expOut < 0) {
                            changed |= 1<<LIFE_DEC;
                        //If the value is equal to 0, then we can completely ignore it. We don't even need to set the change flags here.
                        //...Unless kill = 0.
                        } else if (expOut == 0 && stateConList[s*stCW+9] != 0) {
                            break;
                        //Otherwise, clear the flag if we set it this frame (both Instant Death and Life Decrementation)
                        //TODO: Check for an overall increase rather than assuming it always is
                        } else if (stExecFlag && changed&1<<CLIFE || changedRead&1<<CLIFE) {
                            //..only under the pretense that it executes even when fatal
                            //(!!0123456789, Yukkuri ONI-MIKO-R)
                            buf = explClone[88]; //Store Life
                            explClone[88] = 0; //The previous modulation was fatal. What now?
                            buf1 = rollExecution(stateConList+s*stCW);
                            if (buf1 < 0) { goto expOTIYA; }
                            if (buf1) { //Still activates, so deflag
                                if (stExecFlag) { changed &= ~(1<<INSTANT_DEATH|1<<LIFE_DEC); }
                                changedRead &= ~(1<<INSTANT_DEATH|1<<LIFE_DEC);
                            }
                            explClone[88] = buf; //Restore
                        }
                        if (expOut != 0 || explClone[88] == 0 && stateConList[s*stCW+9] == 0) { //Non-zero change required, otherwise it's a NOP basically
                            changedRead |= 1<<CLIFE;
                            if (stExecFlag) {
                                changed |= 1<<CLIFE;
                                ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                            }
                        }
                        break;
                    //SPECIAL PROCESSING
                    //HitBy should store its attribute and which value it currently has
                    case SC_HitBy: //Good news, these only have static values (except time)
                        //Lower priority 
                        asm("__explExec_StCon_HitBy:");
                        if (execOnly || changed&1<<UNHITTABLE || !stExecFlag && changedRead&1<<UNHITTABLE || explClone[1022]) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        prioS = partialList[0]>>12&7;
                        if (prio < prioS) { break; }
                        timeS = partialList[8]&255;
                        buf = stateConList[s*stCW+8]&4095;
                        if (!stateConList[s*stCW+11]) { buf = 4095; } //Time = 0 results in an SCA,AA,AT,AP at the end of the frame (assume variables are unpredictable)
                        //If we've already changed this frame, update
                        if (prioS == 1 && changedRead&1<<HITBY || prio == prioS && time == timeS && changed&1<<HITBY) {
                            partialList[0] &= ~(4095<<16*!!stateConList[s*stCW+12]);
                            partialList[0] |= (buf|32768)<<16*!!stateConList[s*stCW+12];
                        //If the info is better and it's in our favor, completely replace the current information (we need to keep Armor Nullification tho)
                        //Always write the first time we see anything
                        } else if (!prioS || buf&7 && buf&4088 && (prio > prioS || !(partialList[0]&7 && partialList[0]&4088 || partialList[0]&7<<16 && partialList[0]&4088<<16))) {
                            partialList[0] = ((buf|32768)<<16*!!stateConList[s*stCW+12])|prio<<12|partialList[0]&7<<28;
                            if (stExecFlag) {
                                partialList[8] = partialList[8]&~255|time;
                            }
                        }
                        changedRead |= 1<<HITBY;
                        if (stExecFlag) {
                            changed |= 1<<HITBY;
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                        }
                        break;
                    //SPECIAL PROCESSING
                    //ChangeAnim should store C1Time and C2Time, if able
                    case SC_ChangeAnim:
                    case SC_ChangeAnim2: //Assume identical
                        asm("__explExec_StCon_ChangeAnim:");
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; } //Value
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        if (expOut < 0) { expOut = 0; } //Negative becomes 0
                        buf = expOut; //Tmp storage
                        ASSERT_FAIL(expParse(stateConList+s*stCW+9)) { goto expOTIYA; } //Elem
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        if (expOut < 1) { expOut = 1; } //Anything below 1 defaults to 1
                        animID = scanAnim(animData, buf, expOut, buf1, buf2);
                        if (animID == -2) { goto expOTIYA; } //0-length will crash immediately. This elem in particular may also freeze immediately
                        if (animID == -1) { break; } //If invalid, it won't change
                        if (execOnly) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        //So long as the time is less than the already existing CXTime, we store CXTime unconditionally as that means it got interrupted
                        if (stExecFlag || !(changed&1<<CHANGEANIM)) {
                            zeroLength = (buf1 == -2 || buf1 == -3); //-3 is a 0-length but exists flag
                            if (zeroLength && buf1 == -3) { buf1 = 0; } //Clear
                            if (C1Time == 255 || time <= C1Time || stExecFlag && !CAExec) { C1Time = (buf1 < 0 ? 255 : (buf1+time > 254 ? 254 : buf1+time)); }
                            if (C2Time == 255 || time <= C2Time || stExecFlag && !CAExec) { C2Time = (buf2 < 0 ? 255 : (buf2+time > 254 ? 254 : buf2+time)); }
                            CAExec |= stExecFlag;
                        }
                        changedRead |= 1<<CHANGEANIM;
                        if (stExecFlag) {
                            changed |= 1<<CHANGEANIM;
                            explClone[3520+243] = (int)(animData); //SelfAnimData = Desired
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                            explClone[3520+243] = (int)(animDataSelf); //SelfAnimData = Orig
                        }
                        break;
                    //SPECIAL PROCESSING
                    //Sneakily Identical, mostly static values
                    case SC_HitDef:
                    case SC_ReversalDef:
                        //Worse priority so don't execute
                        asm("__explExec_StCon_HitDef:");
                        prioS = partialList[1]>>28&7;
                        if (prio < prioS) { break; }
                        timeS = partialList[8]>>16&255;
                        ASSERT_FAIL(expParse(point+50)) { goto expOTIYA; } //P1PauseTime
                        if (execOnly) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        //Update Information
                        if (prioS == 1 && changedRead&1<<HITDEF || prio == prioS && time == timeS && changed&1<<HITDEF) {
                            if (!point[105]) {
                                partialList[1] = point[1]&4095 | 4096*((point[0]&3)==1) | 16384*(expOut>1) | prio<<28;
                            } else {
                                partialList[1] = 8192*(point[105]&7 && point[105]&4088) | 16384*(expOut>1) | prio<<28;
                            }
                        //If the info is better and it's in our favor, completely replace the current information
                        //Furthermore, auto-write HitDef info if we haven't seen a HitDef/ReversalDef yet
                        } else if (!point[105] && (!prioS || point[1]&4088 && (prio > prioS || !(partialList[1]&4088)))) {
                            partialList[1] = point[1]&4095 | 4096*((point[0]&3)==1) | 16384*(expOut>1) | prio<<28;
                            if (stExecFlag) {
                                partialList[8] = partialList[8]&~(255<<16)|(time<<16);
                            }
                        //ReversalDef should not replace HitDef; even still it needs p1pausetime to be valid
                        //Furthermore, auto-write ReversalDef info if we haven't seen a HitDef yet
                        } else if (!prioS || point[105]&7 && point[105]&4088 && expOut > 1 && !(partialList[1]&4088) && (prio > prioS || !(partialList[1]&16384))) {
                            partialList[1] = 8192*(point[105]&7&&point[105]&4088) | 16384*(expOut>1) | prio<<28;
                            if (stExecFlag) {
                                partialList[8] = partialList[8]&~(255<<16)|(time<<16);
                            }
                        }
                        changedRead |= 1<<HITDEF;
                        if (stExecFlag) {
                            changed |= 1<<HITDEF;
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                        }
                        break;
                    //HFD is blatant AND doesn't need any other checks! Crazy
                    case SC_HitFallDamage:
                        asm("__explExec_StCon_HitFallDamage:");
                        changedRead |= 1<<HITFALL;
                        if (stExecFlag) {
                            changed |= 1<<HITFALL;
                            stConExec(stateConList+s*stCW, explClone); //Execute normally
                        }
                        break;
                    //StateTypeSet is blatant; we're managing flags in a changed-detection manner so I don't have to worry about setting
                    case SC_StateTypeSet:
                        asm("__explExec_StCon_StateTypeSet:");
                        if (stateConList[s*stCW+6] != -1) {
                            changedRead |= 1<<CSTATETYPE;
                            statetypeR = stateConList[s*stCW+6]; //Read-OK Variations
                            if (stExecFlag) { changed |= 1<<CSTATETYPE; }
                        }
                        if (stateConList[s*stCW+9] != -1) {
                            changedRead |= 1<<CMOVETYPE;
                            movetypeR = stateConList[s*stCW+6];
                            if (stExecFlag) { changed |= 1<<CMOVETYPE; }
                        }
                        if (stateConList[s*stCW+12] != -1) {
                            changedRead |= 1<<CPHYSICS;
                            physicsR = stateConList[s*stCW+6];
                            if (stExecFlag) { changed |= 1<<CPHYSICS; }
                        }
                        if (stExecFlag) {
                            stConExec(stateConList+s*stCW, explClone); //Execute normally
                        }
                        break;
                    //SPECIAL PROCESSING
                    //HitOverride is weird because we want to handle it based on flags
                    case SC_HitOverride:
                        //Worse priority so don't execute
                        asm("__explExec_StCon_HitOverride:");
                        prioS = partialList[0]>>28&7;
                        if (prio < prioS) { break; }
                        timeS = partialList[8]>>8&255;
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; } //Slot
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        if ((dword)(expOut) > 7) { break; } //Invalid slots do nothing
                        buf1 = expOut;
                        ASSERT_FAIL(expParse(point+4)) { goto expOTIYA; } //Time
                        if (execOnly) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        buf2 = *point; //Attr
                        if (expOut == 0) { buf2 = 0; } //Time = 0 results in a NULL at the end of the frame
                        //If we've already changed this frame, update
                        if (prioS == 1 && changedRead&1<<ARMOR || prio == prioS && time == timeS && changed&1<<ARMOR) {
                            partialList[19] = partialList[19]&~(1<<20+buf1)|!(buf2&7&&buf2&4088)<<20+buf1;
                        //If the info is better and it's in our favor, completely replace the current information (we need to keep HitBy though)
                        //Unconditionally assign if it's the first time reading
                        } else if (!prioS || (buf2&4095) != 4095 && (prio > prioS || !(partialList[0]&255<<20))) {
                            partialList[0] = partialList[0]&268435455|prio<<28;
                            partialList[19] = partialList[19]&~(255<<20)|1<<20+buf1;
                        }
                        changedRead |= 1<<ARMOR;
                        if (stExecFlag) {
                            changed |= 1<<ARMOR;
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                        }
                        break;
                    //SPECIAL PROCESSING
                    //If NoKO exists, Instant Death can't exactly be used in any way, so we'll nullify it at the end of processing
                    case SC_AssertSpecial:
                        asm("__explExec_StCon_AssertSpecial:");
                        if (stateConList[s*stCW+6] == 2 && stateConList[s*stCW+15] == 0
                            || stateConList[s*stCW+9] == 2 && stateConList[s*stCW+18] == 0
                            || stateConList[s*stCW+12] == 2 && stateConList[s*stCW+21] == 0
                        ) {
                            changedRead |= 1<<NOKO;
                            if (stExecFlag) { changed |= 1<<NOKO; }
                        }
                        break;
                    //Check for MoveTime
                    case SC_Pause:
                        asm("__explExec_StCon_Pause:");
                        ASSERT_FAIL(expParse(point)) { goto expOTIYA; } //Time
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        buf1 = expOut;
                        ASSERT_FAIL(expParse(point+3)) { goto expOTIYA; } //MoveTime
                        if (execOnly) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        buf2 = expOut;
                        //If it gets nullified (note: we explicitly require nullification because of most opponents pausing every frame)
                        if (buf1 < 1 || buf2 < 1) {
                            changedRead |= 1<<PAUSE_DELETE;
                            if (stExecFlag) { changed |= 1<<PAUSE_DELETE; }
                        //Otherwise, clear the respective flag if we set it this frame
                        } else {
                            if (stExecFlag) { changed &= ~(1<<PAUSE_DELETE); }
                            changedRead &= ~(1<<PAUSE_DELETE);
                        }
                        break;
                    case SC_SuperPause:
                        asm("__explExec_StCon_SuperPause:");
                        ASSERT_FAIL(expParse(point)) { goto expOTIYA; } //Time
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        buf1 = expOut;
                        ASSERT_FAIL(expParse(point+3)) { goto expOTIYA; } //MoveTime
                        if (execOnly) { goto __explExec_SC_ForceDefault; } //No need to preserve
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        buf2 = expOut;
                        //If it gets nullified (note: we explicitly require nullification because of most opponents pausing every frame)
                        if (buf1 < 1 || buf2 < 1) {
                            changedRead |= 1<<SUPER_DELETE;
                            if (stExecFlag) { changed |= 1<<SUPER_DELETE; }
                        //Otherwise, clear the respective flag if we set it this frame
                        } else {
                            if (stExecFlag) { changed &= ~(1<<SUPER_DELETE); }
                            changedRead &= ~(1<<SUPER_DELETE);
                        }
                        //In addition, if we have the unhittable flag and are executing this pause, note it
                        if (buf1 > 0) {
                            ASSERT_FAIL(expParse(point+31)) { goto expOTIYA; } //Unhittable
                            if (expFloat) { expOut = (int)(expOutF); } //Truncate
                            if (expOut) {
                                changedRead |= 1<<UNHITTABLE;
                                if (stExecFlag) { 
                                    changed |= 1<<UNHITTABLE;
                                    explClone[1022] = (explClone[1022] < buf1 ? buf1 : explClone[1022]); //store for later
                                }
                            }
                        }
                        break;
                    //If this happens to correspond to an Erase->Summon, it's a crash state
                    //Otherwise, check for HelperType (and store the first one we find)
                    case SC_Helper:
                        asm("__explExec_StCon_Helper:");
                        if (stExecFlag && changed&1<<DESTROY) { goto expOTIYA; } //COOL.
                        if (point[15] != 1) { //HelperType = Normal/Proj
                            changedRead |= 1<<NORMAL_GEN;
                            if (stExecFlag) { changed |= 1<<NORMAL_GEN; }
                        } else {
                            changedRead |= 1<<PLAYER_GEN;
                            if (stExecFlag) { changed |= 1<<PLAYER_GEN; }
                        }
                        //Store the first helperID
                        if (partialList[2] == 0) {
                            ASSERT_FAIL(expParse(point+12)) { goto expOTIYA; } //HelperID
                            if (expFloat) { expOut = (int)(expOutF); } //Truncate
                            if (expOut > 0) { partialList[2] = expOut; } //I'll choose to ignore helperID 0
                        }
                        //Assume Helper occupancy, do not actually parse unless needed
                        //This also means that a really sneaky Helper could bean us but eh, don't care
                        if (execOnly) { goto __explExec_SC_ForceDefault; }
                        break;
                    //DestroySelf only has an effect if it's a helper
                    //For the sake of crash avoidance, I'll avoid actually executing it
                    //Furthermore, if any Explod or Helper is executed after this point, i'll flag it as a crash
                    case SC_DestroySelf:
                        asm("__explExec_StCon_DestroySelf:");
                        if (!isHelper) { break; }
                        changedRead |= 1<<DESTROY;
                        if (stExecFlag) { changed |= 1<<DESTROY; }
                        break;
                    ///////////////////////////////////////////////////
                    ///////////////NON-EXPLORATION BLOCK///////////////
                    ///////////////////////////////////////////////////
                    //Certain state controllers have processing I'd rather avoid if I can help it
                    //No particular reason besides a hidden annoyance factor
                    //Variables being tampered with might have an erroneous var number; deal with it
                    case SC_VarSet:
                    case SC_VarAdd:
                    case SC_VarRandom:
                    case SC_ParentVarSet:
                    case SC_ParentVarAdd:
                        asm("__explExec_StCon_VarXXX:");
                        if (!stExecFlag) { break; } //Needs execution
                        ASSERT_FAIL(expParse(stateConList+s*stCW+9)) { goto expOTIYA; } //v/fv/sv/sfv = #
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        switch (stateConList[s*stCW+14]*(stateConType != SC_VarRandom)) {
                            case 0:
                                if ((dword)(expOut) >= 60) { break; }
                                partialList[12+expOut/30+4*(stateConType >= SC_ParentVarSet)] |= 1<<expOut%30;
                                break;
                            case 1:
                                if ((dword)(expOut) >= 40) { break; }
                                partialList[14+expOut/30+4*(stateConType >= SC_ParentVarSet)] |= 1<<expOut%30;
                                break;
                            case 3:
                            case 4:
                                if ((dword)(expOut) >= 5 || stateConType >= SC_ParentVarSet) { break; }
                                partialList[15] |= 1<<expOut+10+5*(stateConList[s*stCW+14]==4);
                                break;
                        }
                        ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                        break;
                    case SC_VarRangeSet:
                        asm("__explExec_StCon_VarRangeSet:");
                        if (!stExecFlag) { break; } //For now, I won't care unless we're actually executing it
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; } //first
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        buf = expOut;
                        ASSERT_FAIL(expParse(stateConList+s*stCW+9)) { goto expOTIYA; } //last
                        if (expFloat) { expOut = (int)(expOutF); } //Truncate
                        if (buf >= 0 && expOut < 60-20*(stateConList[s*stCW+17]==1) && buf <= expOut) {
                            if (stateConList[s*stCW+17]==1) { buf += 60; expOut += 60; } //adjust to float
                            do {
                                partialList[12+buf/30] |= 1<<buf%30;
                            } while (++buf <= expOut);
                        }
                        ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; } //Execute normally
                        break;
                    case SC_PlaySnd:
                        asm("__explExec_StCon_PlaySnd:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(expParse(point)) { goto expOTIYA; } //Channel
                        ASSERT_FAIL(expParse(point+13)) { goto expOTIYA; } //LowPriority
                        break;
                    //StopSnd and SndPan should read only the channel
                    case SC_StopSnd:
                    case SC_SndPan:
                        asm("__explExec_StCon_StopSnd:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; }
                        break;
                    //Projectiles should either have a removetime of 0/projhitanim = -1 if temporary or a yScale of 0 otherwise
                    case SC_Projectile:
                        asm("__explExec_StCon_Projectile:");
                        if (!stExecFlag || projSeal) { break; }
                        buf1 = (int)(explClone);
                        if (explClone[2441]) { buf1 = explClone[2441]; }
                        point = **(int***)((int*)(buf1)+135);
                        buf = point[9]; buf2 = point[10];
                        if (buf2-buf < point[2]) { //No free space: use either the one directly before the first or directly after the last
                            buf = (buf ? buf-1 : buf2+1);
                        } else { //Otherwise: constantly loop until a free space is found
                            buf1 = point[6];
                            do {
                                if (!*((int*)(buf1)+4*buf)) { break; }
                            } while (++buf < point[3]);
                        }
                        if (buf >= point[3]) { break; } //Proj occupancy somehow
                        point = *(int**)(point+5) + 183*buf;
                        ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; }
                        if (execOnly) { break; } //No need to modify
                        point[4] = -1; //ProjHitAnim
                        point[5] = -1; //ProjRemAnim
                        point[6] = -1; //ProjCancelAnim
                        if ((dword)(point[15]) >= 100000) { //If it doesn't disappear within 30 minutes or so, most likely it will never go away at all
                            point[22] = 0; //Scale.Y
                        } else { //Since it will go away, set removetime to 0
                            point[15] = 0;
                        }
                        break;
                    //EnvShake should process everything, but not actually execute
                    case SC_EnvShake:
                        asm("__explExec_StCon_EnvShake:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(expParse(point)) { goto expOTIYA; } //Time
                        ASSERT_FAIL(expParse(point+3)) { goto expOTIYA; } //Dir
                        ASSERT_FAIL(expParse(point+6)) { goto expOTIYA; } //Freq
                        ASSERT_FAIL(expParse(point+9)) { goto expOTIYA; } //Ampl
                        ASSERT_FAIL(expParse(point+12)) { goto expOTIYA; } //Phase
                        break;
                    //FallEnvShake should just be skipped
                    case SC_FallEnvShake:
                        break;
                    //EnvColor should process everything, but not actually execute
                    case SC_EnvColor:
                        asm("__explExec_StCon_EnvColor:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(expParse(stateConList+s*stCW+9)) { goto expOTIYA; } //Time
                        ASSERT_FAIL(expParse(stateConList+s*stCW+12)) { goto expOTIYA; }//Under
                        ASSERT_FAIL(expParse(stateConList+s*stCW+15)) { goto expOTIYA; }//Value.R
                        ASSERT_FAIL(expParse(stateConList+s*stCW+18)) { goto expOTIYA; }//Value.G
                        ASSERT_FAIL(expParse(stateConList+s*stCW+21)) { goto expOTIYA; }//Value.B
                        break;
                    //GameMakeAnim should process everything, but not actually execute
                    case SC_GameMakeAnim:
                        asm("__explExec_StCon_GameMakeAnim:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; } //Value
                        ASSERT_FAIL(expParse(stateConList+s*stCW+9)) { goto expOTIYA; } //Under
                        ASSERT_FAIL(expParse(stateConList+s*stCW+12)) { goto expOTIYA; }//Random
                        ASSERT_FAIL(expParse(stateConList+s*stCW+15)) { goto expOTIYA; }//Pos.X
                        ASSERT_FAIL(expParse(stateConList+s*stCW+18)) { goto expOTIYA; }//Pos.Y
                        break;
                    //MakeDust should process everything, but not actually execute
                    case SC_MakeDust:
                        asm("__explExec_StCon_MakeDust:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; } //Pos.X
                        ASSERT_FAIL(expParse(stateConList+s*stCW+9)) { goto expOTIYA; } //Pos.Y
                        ASSERT_FAIL(expParse(stateConList+s*stCW+12)) { goto expOTIYA; }//Spacing
                        break;
                    //PalFX should process everything, but not actually execute
                    case SC_BGPalFX:
                        asm("__explExec_StCon_PalFX:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(expParse(point+1)) { goto expOTIYA; } //Time
                        ASSERT_FAIL(expParse(point+4)) { goto expOTIYA; } //Color
                        ASSERT_FAIL(expParse(point+7)) { goto expOTIYA; } //Add.R
                        ASSERT_FAIL(expParse(point+10)) { goto expOTIYA; } //Add.G
                        ASSERT_FAIL(expParse(point+13)) { goto expOTIYA; } //Add.B
                        ASSERT_FAIL(expParse(point+16)) { goto expOTIYA; } //Mul.R
                        ASSERT_FAIL(expParse(point+19)) { goto expOTIYA; } //Mul.G
                        ASSERT_FAIL(expParse(point+22)) { goto expOTIYA; } //Mul.B
                        ASSERT_FAIL(expParse(point+25)) { goto expOTIYA; } //SinAdd.R
                        ASSERT_FAIL(expParse(point+28)) { goto expOTIYA; } //SinAdd.G
                        ASSERT_FAIL(expParse(point+31)) { goto expOTIYA; } //SinAdd.B
                        ASSERT_FAIL(expParse(point+34)) { goto expOTIYA; } //InvertAll
                        ASSERT_FAIL(expParse(point+37)) { goto expOTIYA; } //SinAdd.Period
                        break;
                    //Explod executes nothing if an Explod cannot be generated
                    //Helpers can use Explods freely. It's best not to interfere with the body, though.
                    case SC_Explod:
                        asm("__explExec_StCon_Explod:");
                        if (!stExecFlag || explodSeal) { break; }
                        ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; }
                        break;
                    //RemoveExplod processes ID unconditionally
                    case SC_RemoveExplod:
                        asm("__explExec_StCon_RemoveExplod:");
                        if (!stExecFlag) { break; }
                        if (!explodSeal) {
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; }
                        } else {
                            ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; } //ID
                        }
                        break;
                    //ExplodBindTime processes both ID and Time unconditionally
                    case SC_ExplodBindTime:
                        asm("__explExec_StCon_ExplodBindTime:");
                        if (!stExecFlag) { break; }
                        if (!explodSeal) {
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; }
                        } else {
                            ASSERT_FAIL(expParse(stateConList+s*stCW+6)) { goto expOTIYA; } //ID
                            ASSERT_FAIL(expParse(stateConList+s*stCW+9)) { goto expOTIYA; } //Time
                        }
                        break;
                    //ModifyExplod will only parse ID if an Explod cannot be found
                    case SC_ModifyExplod:
                        asm("__explExec_StCon_ModifyExplod:");
                        if (!stExecFlag) { break; }
                        if (!explodSeal) {
                            ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; }
                        } else {
                            ASSERT_FAIL(expParse(point)) { goto expOTIYA; } //ID
                        }
                        break;
                    //I'll allow the Clipboard instructions because of some interesting interactions (Tiny_Despair)
                    //I think it's worth the potential crash risk as I won't be executing things unconditionally
                    //Furthermore I'm using try/catch so crashes shouldn't happen (outside of error messages)
                    //So in all other cases, simply execute the state controller
                    default:
__explExec_SC_ForceDefault:
                        asm("__explExec_StCon_Default:");
                        if (!stExecFlag) { break; }
                        ASSERT_FAIL(stConExec(stateConList+s*stCW, explClone)) { goto expOTIYA; }
                }
            }
            asm("__explExec_StCon_Persistent:");
            //Persistent handling goes here
            if (!execMinus) {
                if (stateConList[s*stCW+2]) { 
                    if (stExecFlag && (dword)(stateConList[s*stCW+2]) < 256) { *((byte*)(explClone)+3068+s) = stateConList[s*stCW+2]; }
                    if (*((byte*)(explClone)+3068+s) > 128) {
                        *((byte*)(explClone)+3068+s) = 0;
                    } else if (*((byte*)(explClone)+3068+s) != 0) {
                        *((byte*)(explClone)+3068+s) -= 1;
                    }
                } else if (stExecFlag) { //persistent = 0
                    *((byte*)(explClone)+3068+s) = 1;
                }
            }
            asm("__explExec_StCon_Next:");
        } while (++s < stateConCount && (stateStasis || time == 0 && defRead));
        asm("__explExec_Frame_Evaluate:");
        if (!execOnly) {
            //If priority is higher than what we have stored, we can store time/etc., so just declare it here
            prio = (3-isHelper)*(1+isSelf);
            //If, by some miracle, certain things changed through assignment or other means, this is the moment where I'll record them
            //Instant Death/Life Decrementation through assignment
            //This will also serve as a backup measure for instant death; it's incredibly unlikely that the opponent will deduct from lifemax/2 to 0 unless they want to kill something
            if (explClone[88] < explClone[8192+88]) {
                changedRead |= 1<<(explClone[88] < 1 ? INSTANT_DEATH : LIFE_DEC);
                changed |= 1<<(explClone[88] < 1 ? INSTANT_DEATH : LIFE_DEC);
            }
            //ChangeState through assignment
            if (stateStasis && explClone[765] != stateno) {
                partialList[3] = stateno;
                stateStasis = false;
            }
            //StateTypeSet through assignment
            if (!(changed&1<<CSTATETYPE) && explClone[896] != statetype) {
                changedRead |= 1<<CSTATETYPE;
                changed |= 1<<CSTATETYPE;
            }
            if (!(changed&1<<CMOVETYPE) && explClone[897] != movetype) {
                changedRead |= 1<<CMOVETYPE;
                changed |= 1<<CMOVETYPE;
            }
            if (!(changed&1<<CPHYSICS) && explClone[898] != physics) {
                changedRead |= 1<<CPHYSICS;
                changed |= 1<<CPHYSICS;
            }
            //Def 52 has special behavior; if physics is unchanged @ t=0 or is explicitly A, it's a crash state
            if (stateno == 52 && time == 0 && (!(changed&1<<CPHYSICS) || explClone[898] == 3)) { goto expOTIYA; }
            //Def 140 has special behavior; if statetype is changed to something invalid, it's a crash state
            if (stateno == 140 && changed&1<<CSTATETYPE && (explClone[896] < 1 || explClone[896] > 3)) { goto expOTIYA; }
            //Unhittable will completely nullify any HitBy executed on this frame
            if (changed&1<<UNHITTABLE || (changedRead&1<<UNHITTABLE && prioS < 2) || explClone[1022]) {
                prioS = partialList[0]>>12&7;
                timeS = partialList[8]&255;
                //If we've already written to it this frame, or we haven't written to it at all, update
                if (!prioS || prioS == 1 && changedRead&1<<HITBY || prio == prioS && time == timeS && changed&1<<HITBY) {
                    partialList[0] &= ~(4095|4095<<16); //Treated as a HitBy NUL, HitBy2 NUL
                    partialList[0] |= 32768|32768<<16;
                //If we haven't written to it at all, bind
                } else if (!prioS) {
                    partialList[0] = (32768|32768<<16|prio<<12|partialList[0]&7<<28);
                    if (changed&1<<UNHITTABLE || explClone[1022]) {
                        partialList[8] = partialList[8]&~255|time;
                    }
                }
                changedRead |= 1<<HITBY;
                if (changed&1<<UNHITTABLE || explClone[1022]) {
                    changed |= 1<<HITBY;
                }
            //Otherwise, check for HitBy through assignment
            } else if (!(changed&1<<HITBY) && (*(word*)(explClone+1023) != 4096 || *(word*)(explClone+1024) != 4096)) {
                buf1 = (explClone[1025] ? *(word*)(explClone+1023)&4095 : 4095);
                buf2 = (explClone[1026] ? *(word*)(explClone+1024)&4095 : 4095);
                buf = buf1&buf2;
                //If we've already changed this frame, update
                if (prioS == 1 && changedRead&1<<HITBY || prio == prioS && time == timeS && changed&1<<HITBY) {
                    partialList[0] &= ~(4095*(buf1!=4096)|4095*(buf2!=4096)<<16);
                    partialList[0] |= (buf1&4095|32768)*(buf1!=4096)|(buf2&4095|32768)*(buf2!=4096)<<16;
                //If the info is better and it's in our favor, completely replace the current information (we need to keep Armor Nullification tho)
                //Always write the first time we see anything
                } else if (!prioS || buf&7 && buf&4088 && (prio > prioS || !(partialList[0]&7 && partialList[0]&4088 || partialList[0]&7<<16 && partialList[0]&4088<<16))) {
                    partialList[0] = (buf1&4095|32768)*(buf1!=4096)|(buf2&4095|32768)*(buf2!=4096)<<16|prio<<12|partialList[0]&7<<28;
                    if (stExecFlag) {
                        partialList[8] = partialList[8]&~255|time;
                    }
                }
                changedRead |= 1<<HITBY;
                changed |= 1<<HITBY;
            }
            //HitOverride through assignment
            //until someone actually does this, i'll comment it out...
            /*if (explClone[1070] || explClone[1075] || explClone[1080] || explClone[1085] || explClone[1090] || explClone[1095] || explClone[1100] || explClone[1105]) {
                codecodecodecode;
            }*/
            //HitDef through assignment
            if (explClone[1110]) {
                prioS = partialList[1]>>28&7;
                //If we've already changed this frame, update
                if (changed&1<<HITDEF) {
                    if (!explClone[1153]) {
                        partialList[1] = explClone[1113]&4095 | 4096*((explClone[1112]&3)==1) | 16384*(explClone[1131]>1) | prio<<28;
                    } else {
                        partialList[1] = 8192*(explClone[1153]&7 && explClone[1153]&4088) | 16384*(explClone[1131]>1) | prio<<28;
                    }
                //If we don't have a HitDefAttr stored, completely replace the current information
                } else if (!explClone[1153] && (!prioS || explClone[1113]&4088 && (prio > prioS || !(partialList[1]&4088)))) {
                    partialList[1] = explClone[1113]&4095 | 4096*((explClone[1112]&3)==1) | 16384*(explClone[1131]>1) | prio<<28;
                    partialList[8] = partialList[8]&~(255<<16)|(time<<16);
                //ReversalDef should not replace HitDef; even still it needs p1pausetime to be valid
                } else if (!prioS || explClone[1153]&7 && explClone[1153]&4088 && !(partialList[1]&4088) && explClone[1131] > 1) {
                    partialList[1] = 8192*(point[105]&7&&point[105]&4088) | 16384*(expOut>1) | prio<<28;
                    partialList[8] = partialList[8]&~(255<<16)|(time<<16);
                }
                changedRead |= 1<<HITDEF;
                changed |= 1<<HITDEF;
            }
            //ChangeAnim through assignment
            if (!(changed&1<<CHANGEANIM) && (animIDB == -1 ? explClone[3968+0] != (int)(animDataBea) : explClone[3968+3] != animIDB)) {
                animID = scanAnim(animData, explClone[3968+3], 0, buf1, buf2);
                if (animID == -1 || animID == -2) { goto expOTIYA; } //0-length will crash immediately and an invalid one will also probably crash in this case
                //So long as the time is less than the already existing CXTime, we store CXTime unconditionally as that means it got interrupted
                buf = explClone[3968+7]; //AnimElemTime with loop accounted for
                zeroLength = (buf1 == -2 || buf1 == -3); //-3 is a 0-length but exists flag
                if (zeroLength && buf1 == -3) { buf1 = 0; } //Clear
                if (C1Time == 255 || time <= C1Time) { C1Time = (buf1 < 0 ? 255 : (buf1-buf < time ? time : (buf1+time > 254 ? 254 : buf1+time))); }
                if (C2Time == 255 || time <= C2Time) { C2Time = (buf2 < 0 ? 255 : (buf2-buf < time ? time : (buf2+time > 254 ? 254 : buf2+time))); }
                changedRead |= 1<<CHANGEANIM;
                changed |= 1<<CHANGEANIM;
                CAExec = true;
            }
            //Variable Assignment
            buf = 0;
            do {
                asm("__explExec_Frame_VarCheck_Loop:");
                if (*(explClone+912+buf) != *(explClone+8192+912+buf)) {
                    partialList[12+buf/30] |= 1<<buf%30;
                }
                asm("__explExec_Frame_VarCheck_Next:");
            } while (++buf < 110);
            asm("__explExec_Frame_ParentEval:");
            if (isHelper) {
                buf = 0;
                do {
                    asm("__explExec_Frame_ParentCheck_Loop:");
                    if (*(explClone+4096+912+buf) != *(explClone+12288+912+buf)) {
                        partialList[16+buf/30] |= 1<<buf%30;
                    }
                    asm("__explExec_Frame_ParentCheck_Next:");
                } while (++buf < 100);
            }
    
            //If we've read NoKO, clear instant death flags
            asm("__explExec_Frame_SpecialProcessing:");
            if (changedRead&1<<NOKO) { 
                changedRead &= ~(1<<INSTANT_DEATH | 1<<HITFALL);
            }
            if (changed&1<<NOKO) {
                changed &= ~(1<<INSTANT_DEATH | 1<<HITFALL);
            }
            //SPECIAL: If alive is somehow set to 0, even if NoKO exists, it's Instant Death
            if (explClone[905] == 0) {
                changedRead |= 1<<INSTANT_DEATH;
                changed |= 1<<INSTANT_DEATH;
            }
            //FLAGS :D
            //MT=A/H states (Slots 0~1)
            asm("__explExec_Frame_FlagBind:");
            if ((changed|changedRead)&1<<CMOVETYPE) {
                buf = (changed&1<<CMOVETYPE ? explClone[897] : movetypeR);
                if (buf == 1 || buf == 2 && !time) { //MT=H must be at t=0
                    prioS = partialList[10]>>4*(buf==2)&15;
                    flagsRead |= 1<<(buf==1 ? MOVETYPE_A : MOVETYPE_H);
                    if (changed&1<<CMOVETYPE) {
                        flags |= 1<<(buf==1 ? MOVETYPE_A : MOVETYPE_H);
                        if (prio > prioS) {
                            partialList[4] = partialList[4]&~(255<<8*(buf==2))|time<<8*(buf==2);
                            partialList[10] = partialList[10]&~(15<<4*(buf==2))|prio<<4*(buf==2);
                        }
                    } else if (prioS < 1) {
                        partialList[10] = partialList[10]|1<<4*(buf==2);
                    }
                }
            //MT=U states should instead save the time until it changes (Slot 2)
            } else if (time == ((partialList[4]>>16+1)&255)) {
                flagsRead |= 1<<MOVETYPE_U;
                flags |= 1<<MOVETYPE_U;
                partialList[4] = partialList[4]&~(255<<16)|time<<16;
                partialList[10] = partialList[10]&~(15<<8)|prio<<8;
            }
            //HitFallDamage (Slot 3)
            if ((changed|changedRead)&1<<HITFALL) {
                prioS = partialList[10]>>12&15;
                flagsRead |= 1<<HITFALL;
                if (changed&1<<HITFALL) {
                    flags |= 1<<HITFALL;
                    if (prio > prioS) {
                        partialList[4] = partialList[4]&~(255<<24)|time<<24;
                        partialList[10] = partialList[10]&~(15<<12)|prio<<12;
                    }
                } else if (prioS < 1) {
                    partialList[10] = partialList[10]|1<<12;
                }
            }
            //Clsn1,Clsn2 Granting is processed at the end of the state (aka post-time processing)
            //Instant Death, Life Decrementation, Normal Generation, Player Generation, etc. etc. (Slots 6~15)
            buf = INSTANT_DEATH;
            do {
                if ((changed|changedRead)&1<<buf) {
                    prioS = partialList[10+buf/8]>>4*(buf&7)&15;
                    flagsRead |= 1<<buf;
                    if (changed&1<<buf) {
                        flags |= 1<<buf;
                        if (prio > prioS) {
                            partialList[4+buf/4] = partialList[4+buf/4]&~(255<<8*(buf&3))|time<<8*(buf&3);
                            partialList[10+buf/8] = partialList[10+buf/8]&~(15<<4*(buf&7))|prio<<4*(buf&7);
                        }
                    } else if (prioS < 1) {
                        partialList[10+buf/8] = partialList[10+buf/8]|1<<4*(buf&7);
                    }
                }
            } while (++buf < 16); //yes, this includes 0x8000; we'll delete it in post
            //Bookkeeping (excluding the last frame)
            if (time != 15*useTime && stateStasis) {
                explClone[88] = explClone[89]/(1+(explClone[89]>1)); //Life
                explClone[766] = -201326482; //PrevStateNo = 0xf400006e (just in case)
                explClone[895] += 1; //Time Increment (++ with pointers doesn't work)
                statetypeR = (statetype = explClone[896]); //StateType Storage Update
                movetypeR = (movetype = explClone[897]); //MoveType Storage Update
                physicsR = (physics = explClone[898]); //Physics Storage Update
                explClone[902] = 0; //HitPauseTime = 0 (just in case)
                explClone[1022] -= (explClone[1022] != 0); //Unhittable--;
                *(word*)(explClone+1023) = 4096; //HitBy Value = INVALID
                *(word*)(explClone+1024) = 4096; //HitBy Value2 = INVALID
                memset(explClone+1070, 0, 160); //HitOverride Nullification
                explClone[1110] = 0; //HitDef Activity Flag = 0 
                //[animDex+5*ID+2]
                //Anim processing
                animID = explClone[3968+3];
                animElemID = explClone[3968+5];
                explClone[3968+6] += 1; //AnimElemTime(1) Increment (threw out HPT)
                if (explClone[3968+7] >= *(*(**(dword****)(explClone+3968)+5)+5*animID+2)) {
                    explClone[3968+5] = (animElemID = *(*(**(int****)(explClone+3968)+5)+5*animID+4)); //AnimElemID Reset
                    explClone[3968+7] = *(*(**(int****)(explClone+3968)+5)+5*animID+3); //AnimElemLoopTime(1) Reset
                } else {
                    explClone[3968+7] += 1; //AnimElemLoopTime(1) Increment
                    while (animElemID < *(*(*(**(int*****)(explClone+3968)+5)+5*animID)+10) && explClone[3968+7] >= *(*(*(*(**(int******)(explClone+3968)+5)+5*animID)+5)+12*(animElemID+1))) {
                        animElemID++;
                    }
                    explClone[3968+5] = animElemID;
                }
                animIDB = (explClone[3968+0] == (int)(animDataBea) ? (animID = -1) : animID); //Anim Storage Update
                changed = 0; changedRead = 0; //Current frame flag reset
            }
        }
        asm("__explExec_Frame_Next:");
    } while (++time <= 15*useTime && stateStasis && !execOnly);
    asm("__explExec_Finalization:");
    if (!execOnly) {
        //If the animation changed, check for granting
        asm("__explExec_ClsnGrantCheck:");
        if ((flags|flagsRead)&1<<CHANGEANIM) {
            //Clsn1 Granting (slot 4)
            if (C1Time != 255) {
                flagsRead |= 1<<CLSN1_GRANT;
                if (flags&1<<CHANGEANIM) {
                    flags |= 1<<CLSN1_GRANT;
                    partialList[5] = partialList[5]&~255|C1Time;
                    partialList[10] = partialList[10]&~(15<<16)|prio<<16; //prio is guaranteed to carry over from the previous step due to how the loop is structured
                } else {
                    partialList[10] = partialList[10]|1<<16;
                }
            }
            //Clsn2 Granting (slot 5)
            if (C2Time != 255) {
                flagsRead |= 1<<CLSN2_GRANT;
                if (flags&1<<CHANGEANIM) {
                    flags |= 1<<CLSN2_GRANT;
                    partialList[5] = partialList[5]&~(255<<8)|C2Time<<8;
                    partialList[10] = partialList[10]&~(15<<20)|prio<<20;
                } else {
                    partialList[10] = partialList[10]|1<<20;
                }
            }
        }
        flags = (flags|flagsRead)&0x00007fff; //Merge flags to prep for storage
        //HitBy timesaving
        buf = (partialList[0]&32768 ? partialList[0]&4095 : 4095);
        buf &= (partialList[0]>>16&32768 ? partialList[0]>>16&4095 : 4095);
        //In order to ensure that various bits of bookkeeping are satisfied, we'll only store as a valid state under the following pretenses:
        //- A flag is proc'd, except MT=A/MT=U
        //  - Standard flags OK
        //  - HitBy is parsed and it seems valid
        //  - A valid HitDef or pause acquisition state is found
        //  - System Variables are tampered with (might be a problem with some edits, but with others it could be useful)
        //  - Armor is nullified
        //- It doesn't feel like a completely pointless state
        //  - We parsed a valid ChangeState and something changed from assumed defaults
        //  - We parsed a valid ChangeState and the value is 5050, 5100, or 5110
        if (!((flags&~(1<<MOVETYPE_A|1<<MOVETYPE_U)
            || partialList[0]&0x80008000 && buf&7 && buf&4088
            || partialList[1]&4088 || (partialList[1]&24576) == 24576
            || partialList[15]&1047552
            || partialList[19]&267386880)
            || !(stateStasis || partialList[3] == -1) && (
                (statetype != 1 || movetype != 0 || physics != 0 && physics != 4) ||
                partialList[3] == 5050 || partialList[3] == 5100 || partialList[3] == 5110
            )
        )) { 
            goto expRestore;
        }
        //ChangeState Parsing
        asm("__explExec_ChangeStateStorage:");
        prioS = infoStorage[24*pos+23]>>16&15;
        if (!stateStasis && prio > prioS) {
            infoStorage[24*pos+23] = infoStorage[24*pos+23]&~(15<<16)|prio<<16;
            infoStorage[24*pos+12] = infoStorage[24*pos+12]&~(255<<24)|partialList[8]&(255<<24);
            infoStorage[24*pos+7] = partialList[3];
        } else if (prioS < 1 && partialList[3] != -1) {
            infoStorage[24*pos+23] = infoStorage[24*pos+23]|1<<16;
            infoStorage[24*pos+7] = partialList[3];
        }
        //HitBy Parsing
        asm("__explExec_FlagStorage:");
        prio = partialList[0]>>12&7;
        prioS = infoStorage[24*pos+4]>>12&7;
        if (prio && !prioS || prio > prioS && buf&7 && buf&4088) { //prio will be set to 0 if it doesn't exist because i'm smart :)
            infoStorage[24*pos+4] = infoStorage[24*pos+4]&1879048192|partialList[0]&~1879048192;
            infoStorage[24*pos+12] = infoStorage[24*pos+12]&~255|partialList[8]&255;
        }
        //HitDef/P1PauseTime Parsing
        prio = partialList[1]>>28&7;
        prioS = infoStorage[24*pos+5]>>28&7;
        if (prio && !prioS || partialList[1]&4088 && (prio > prioS || !(infoStorage[24*pos+5]&4088))
            || !((partialList[1]|infoStorage[24*pos+5])&4088) && partialList[1]&16384 && (prio > prioS || !(infoStorage[24*pos+5]&16384))
        ) {
            infoStorage[24*pos+5] = partialList[1];
            infoStorage[24*pos+12] = infoStorage[24*pos+12]&~(255<<8)|partialList[8]&255<<8;
        }
        //Armor Nullification Parsing
        prio = partialList[0]>>28&7;
        prioS = infoStorage[24*pos+4]>>28&7;
        if (prio && !prioS || prio > prioS && partialList[8]&255<<16) { //prio will be set to 0 if it doesn't exist because i'm smart :)
            infoStorage[24*pos+4] = infoStorage[24*pos+4]&~1879048192|partialList[0]&1879048192;
            infoStorage[24*pos+12] = infoStorage[24*pos+12]&~(255<<16)|partialList[8]&255<<16;
            infoStorage[24*pos+23] = infoStorage[24*pos+23]&~267386880|partialList[19]&267386880;
        }
        //Generic Flag Storage
        buf = 0;
        do {
            asm("__explExec_FlagStorage_Loop:");
            prio = partialList[10+buf/8]>>4*(buf&7)&15;
            prioS = infoStorage[24*pos+14+buf/8]>>4*(buf&7)&15;
            if (prio > prioS) { //prio will be set to 0 if it doesn't exist because i'm smart :)
                infoStorage[24*pos+14+buf/8] = infoStorage[24*pos+14+buf/8]&~(15<<4*(buf&7))|prio<<4*(buf&7);
                infoStorage[24*pos+8+buf/4] = infoStorage[24*pos+8+buf/4]&~(255<<8*(buf&3))|partialList[4+buf/4]&(255<<8*(buf&3));
                if (buf == NORMAL_GEN || buf == PLAYER_GEN) { //Store the helperID
                    infoStorage[24*pos+6] = partialList[2];
                }
            }
            asm("__explExec_FlagStorage_Next:");
        } while (++buf < 16);
        //Variable Tampering Storage (just store it directly)
        asm("__explExec_VarFlagStorage_Loop:");
        do {
            switch (buf) {
                case 23:
                    infoStorage[24*pos+23] |= partialList[19]&1023;
                    break;
                case 19:
                    infoStorage[24*pos+19] |= partialList[15]&1048575;
                    break;
                default:
                    infoStorage[24*pos+buf] |= partialList[buf-4]&1073741823;
            }
        } while (++buf < 24);
        infoStorage[24*pos] = ID;
        infoStorage[24*pos+1+isSelf] |= (flags<<16*!isHelper);
    }
    goto expRestore;
expOTIYA:
    asm("__explExec_expOTIYA:");
    //MUGEN Killing States should be marked as such regardless of situation during exploration
    if (!execOnly) {
        infoStorage[24*pos] = ID;
        infoStorage[24*pos+20+2*isSelf+!isHelper] |= 0x80000000;
    }
expRestore:
    asm("__explExec_expRestore:");
    //Restore Stack
    *stackI = stackIBackup;
    memcpy(stackD, stackDBackup, 4*(stackIBackup+1));
    //Restore important player data
    for (int p = 0; p < 63; p++) {
        asm("__explExec_PlayerRestore_Loop:");
        point = *(*(int***)(0x004b5b4c)+0x2dd5+p); //Player[p]
        if (point != nullptr) {
            memcpy(point+86, explClone+16384+144*p+0, 16); //isActive / isFrozen / Life / LifeMax
            memcpy(point+762, explClone+16384+144*p+4, 20); //DefCode / ThrowCode / ThrowOwner / StateNo / PrevStateNo
            point[905] = *(explClone+16384+144*p+9); //Alive
            memcpy(point+912, explClone+16384+144*p+10, 460); //Variables / UnhittableTime / HitByAttr+2 / HitByTime+2
            point[1265] = *(explClone+16384+144*p+125); //PalNo
            memcpy(point+2438, explClone+16384+144*p+126, 20); //HelperID / ParentID / Parent / Root / HelperType
            if (point[762] != 0) { **(int**)(point+762) = *(explClone+16384+144*p+143); } //InnerDefCode (Root Deleter Countermeasures)
        }
        asm("__explExec_PlayerRestore_Next:");
    }
    asm("__explExec_MUGENRestore:");
    memcpy(*(int**)(0x004b5b4c), explClone+28672, 51536); //MUGEN Restoration (Player Erasure Countermeasures)
    return;
}

extern "C" int explMapCount (int &infoCast, int* stateCode, dword whitelist, dword blacklist, dword bitwhitelist, dword prioInfo, int changestate, int statePos, bool maskCheck = false) {
    int pos = 0;
    byte timeMin;
    int* infoStorage = (int*)(infoCast);
    infoCast = 0x00000000; //Return NULL
    int prioOut = 0; int count = 0;
    byte minTime = (prioInfo&0xff); byte maxTime = (prioInfo>>8&0xff);
    byte minPrio = (prioInfo>>16&0x07 ? prioInfo>>16&0x07 : !(whitelist&0x00008000)); //Overflow has prio 0
    byte maxPrio = (prioInfo>>20&0x07 ? prioInfo>>20&0x07 : 7);
    if (minTime > maxTime || minPrio > maxPrio) { return (statePos == -1 ? 0 : -1); } //Physically impossible
    int* defList = *((int**)(*stateCode)+6);
    int s = (maskCheck ? statePos : 0); //If we're explicitly doing mask processing, we only have to check the state corresponding to the ChangeState
    int IDList[maskCheck ? 1 : 4096];
    do {
        asm("__explMapCount_Loop:");
        int* infoEntry = infoStorage+24*s;
        byte prioW = 0xff; byte timeW = 0x00; byte prioB = 0x00; byte timeB = 0xff; //Init some TMP values
        if (infoEntry[0] == -1) { break; } //Somehow we hit a terminator
        int stateno = (infoEntry[0] < 0 ? ~infoEntry[0] : defList[infoEntry[0]*4+2]);
        if (!(whitelist&0x80000000) && (infoEntry[20]|infoEntry[21]|infoEntry[22]|infoEntry[23])&0x80000000) { continue; } //Skip Crash states
        if (blacklist&0x01000000 && ( //Common Blacklist
            stateno == 0 || stateno >= 10 && stateno <= 12 || stateno == 20 || stateno == 40 || stateno == 45 || stateno >= 50 && stateno <= 52 ||  //Usuals
            stateno == 100 || stateno == 105 || stateno == 106 || stateno == 110 || stateno == 115 ||  //Dash
            stateno == 120 || stateno >= 130 && stateno <= 132 || stateno == 140 || stateno >= 150 && stateno <= 155 ||  //Guard States
            //stateno == 170 || stateno == 175 || stateno == 180 || stateno == 190 || stateno == 191 //Lose/Draw/Win/Intro States
            stateno == 5000 || stateno == 5010 || stateno == 5020 || stateno == 5070 || stateno == 5080 ||  //HitShake
            stateno == 5001 || stateno == 5011 || stateno == 5071 || stateno == 5081 ||  //HitSlide
            stateno == 5030 || stateno == 5035 || stateno == 5040 || stateno == 5050 ||  //Falling
            stateno == 5100 || stateno == 5101 || stateno == 5110 || stateno == 5120 || stateno == 5150 ||  //LieDown
            stateno == 5200 || stateno == 5201 || stateno == 5210 //FallRecover
        )) {
            continue;
        }
        if (whitelist&0x40000000 && !maskCheck) { //Mask reroute
            asm("__explMapCount_ChangeStateMaskLoop:");
            if (infoEntry[7] == -1) { continue; } //ChangeState must exist
            if (infoEntry[7] >= -201326592 && infoEntry[7] <= -201326482) { continue; } //ChangeState must not be a special value
            if ((infoEntry[23]>>16&0x07) < minPrio || (infoEntry[23]>>16&0x07) > maxPrio) { continue; } //ChangeState must be within the priority bounds
            int s2 = 0;
            {
                int sp = 0;
                int defCount = *((int*)(*stateCode)+3);
                do { //Find the state in the defList
                    if (stateno == defList[sp]) { break; }
                } while (++sp < defCount);
                if (sp >= defCount) { continue; } //StateNo Invalid
                do { //Find the state in our exploration databank
                    if (infoStorage[24*s2] == sp) { break; }
                } while (++s2 < 4096 && infoStorage[s2] != -1);
                if (s2 >= 4096 || infoStorage[s2] == -1) { continue; } //Not in databank
            }
            byte timeParity = infoEntry[16]>>24&0xff;
            if (timeParity > maxTime) { continue; } //Negative time doesn't exist
            prioW = (infoEntry[23]>>16&0x07); timeW = timeParity;
            int tmpI = (int)(infoStorage);
            dword prioInfo2 = (maxTime == 0xff ? prioInfo : prioInfo&~0x0000ff00|(maxTime-timeParity)<<8); //Adjust maxTime
            int maskPos = explMapCount(tmpI, stateCode, whitelist, blacklist&~(0x80000000*timeParity), bitwhitelist, prioInfo2, changestate, s2, true);
            if (maskPos < 0) { continue; }
            if ((infoCast>>8&0x07) < prioW) {
                infoCast = timeW|prioW<<8;
            } else if ((infoCast>>8&0x07) == prioW && timeW < (infoCast&0xff)) {
                infoCast = infoCast&~0xff|timeW;
            }
            if (count == statePos) { return s; } //Return the position of this state
            IDList[count] = s; //Otherwise write this state to the list
            count++;
        } else if (whitelist&0x20000000 && changestate != -1) { //ChangeState reroute (changestate == -1 is a special blacklist measure)
            asm("__explMapCount_ChangeStateValueLoop:");
            if (changestate == -201326592 ? (infoEntry[7] < -201326592 || infoEntry[7] > -201326483) : infoEntry[7] != changestate) { continue; } //ChangeState must match (var(XX) will check any var(XX))
            if ((infoEntry[23]>>16&0x07) < minPrio || (infoEntry[23]>>16&0x07) > maxPrio) { continue; } //ChangeState must be within the priority bounds
            if ((infoEntry[16]>>24&0xff) < minTime || (infoEntry[16]>>24&0xff) > maxTime) { continue; } //ChangeState must execute at the right time
            prioW = (infoEntry[23]>>16&0x07); timeW = infoEntry[16]>>24&0xff;
            if ((infoCast>>8&0x07) < prioW) {
                infoCast = timeW|prioW<<8;
            } else if ((infoCast>>8&0x07) == prioW && timeW < (infoCast&0xff)) {
                infoCast = infoCast&~0xff|timeW;
            }
            if (maskCheck || count == statePos) { return s; } //Return the position of this state
            IDList[count] = s; //Otherwise, write this state to the list
            count++;
        } else {
            asm("__explMapCount_MainLoop:");
            word bitMerge = (infoEntry[1]|infoEntry[2]|infoEntry[3])|((infoEntry[1]|infoEntry[2]|infoEntry[3])>>16);
            if ((bitMerge^whitelist)&0x8000) { continue; } //Overflow must match
            if (((bitMerge&=0x7fff)&(whitelist&0x7fff)) != (whitelist&0x7fff)) { continue; } //Main bits must match as well (sneaky assignments)
            if (bitMerge) {
                asm("__explMapCount_BitCheck:");
                word tmpWL = (whitelist&0x7fff); word tmpBL = (blacklist&0x7fff);
                byte b = 0;
                do {
                    asm("__explMapCount_BitCheck_Loop:");
                    if (!(bitMerge&0x01)) { continue; } //No point
                    byte time = (infoEntry[8+b/4]>>8*(b&3))&0xff; byte prio = (infoEntry[14+b/8]>>4*(b&7))&0x0f;
                    if (tmpWL&bitMerge&0x01 && prio<=prioW) {
                        timeW = (prio<prioW||time>timeW ? time : timeW);
                        prioW = prio;
                    } else if (tmpBL&~bitMerge&0x01 && prio>=prioB) {
                        timeB = (prio<prioB||time<timeB ? time : timeB);
                        prioB = prio;
                    }
                    asm("__explMapCount_BitCheck_Next:");
                } while (b++, tmpWL >>= 1, tmpBL >>= 1, bitMerge >>= 1, (tmpWL || tmpBL) && bitMerge);
            }
            if (blacklist&0x8000) { //ChangeAnim
                asm("__explMapCount_ChangeAnimCheck:");
                byte time = infoEntry[11]>>24&0xff; byte prio = infoEntry[15]>>28&0x0f;
                if (prio > maxPrio) { continue; } //OoR
                if (prio && prio>=prioB) {
                    timeB = (prio<prioB||time<timeB ? time : timeB);
                    prioB = prio;
                }
            }
            if ((whitelist|blacklist)&0x00010000 || blacklist&0x00040000) { //HitBy [value]
                asm("__explMapCount_HitBy1Check:");
                bool attrOK = (infoEntry[4]&0x0007 && infoEntry[4]&0x0ff8);
                byte time = infoEntry[12]&0xff; byte prio = (infoEntry[4]>>12&0x07);
                if (whitelist&0x00010000 && !(prio && attrOK)) { continue; } //Explicitly must be SCA and ,AA,AT,AP
                if (whitelist&0x00010000 && prio<=prioW) {
                    timeW = (prio<prioW||time>timeW ? time : timeW);
                    prioW = prio;
                } else if ((blacklist&0x00010000 && attrOK || blacklist&0x00040000) && prio>=prioB) {
                    timeB = (prio<prioB||time<timeB ? time : timeB);
                    prioB = prio;
                }
            }
            if ((whitelist|blacklist)&0x00020000 || blacklist&0x00040000) { //HitBy [value2]
                asm("__explMapCount_HitBy2Check:");
                bool attrOK = (infoEntry[4]&0x00070000 && infoEntry[4]&0x0ff80000);
                byte time = infoEntry[12]&0xff; byte prio = (infoEntry[4]>>12&0x07);
                if (whitelist&0x00020000 && !(prio && attrOK)) { continue; } //Explicitly must be SCA and ,AA,AT,AP
                if (whitelist&0x00020000 && prio<=prioW) {
                    timeW = (prio<prioW||time>timeW ? time : timeW);
                    prioW = prio;
                } else if ((blacklist&0x00020000 && attrOK || blacklist&0x00040000) && prio>=prioB) {
                    timeB = (prio<prioB||time<timeB ? time : timeB);
                    prioB = prio;
                }
            }
            if (whitelist&0x00300000) { //HitDef [whitelist]
                asm("__explMapCount_HitDefWCheck:");
                if (infoEntry[5]&0x2000) { continue; } //ReversalDef DNE
                bool attrOK = (infoEntry[5]&0x0ff8);
                bool SCA = (infoEntry[5]&0x0007);
                if (!(SCA ? whitelist&0x00200000 : whitelist&0x00100000) || !attrOK) { continue; } //Explicitly must match
                byte time = infoEntry[12]>>8&0xff; byte prio = (infoEntry[5]>>28&0x07);
                if (prio<=prioW) {
                    timeW = (prio<prioW||time>timeW ? time : timeW);
                    prioW = prio;
                }
            }
            if (infoEntry[5]&0x2000 ? blacklist&0x00200000 : blacklist&0x00100000 && infoEntry[5]&0x0ff8) { //Hit/ReversalDef [blacklist]
                asm("__explMapCount_HitRevDefBCheck:");
                byte time = infoEntry[12]>>8&0xff; byte prio = (infoEntry[5]>>28&0x07);
                if (prio>=prioB) {
                    timeB = (prio<prioB||time<timeB ? time : timeB);
                    prioB = prio;
                }
            }
            if (whitelist&0x00400000) { //PauseTime Acquisition
                asm("__explMapCount_P1PauseCheck:");
                char time = infoEntry[12]>>8&0xff; char prio = (infoEntry[5]>>28&0x07);
                if (!(prio || infoEntry[5]&0x4000)) { continue; } //Explicitly must match
                if (prio<=prioW) {
                    timeW = (prio<prioW||time>timeW ? time : timeW);
                    prioW = prio;
                }
            }
            if (whitelist&0x00800000) { //Lingering
                asm("__explMapCount_LingerCheck:");
                byte time = infoEntry[12]>>8&0xff; byte prio = (infoEntry[5]>>28&0x07);
                if (!(prio || infoEntry[5]&0x8000)) { continue; } //Explicitly must match
                if (prio<=prioW) {
                    timeW = (prio<prioW||time>timeW ? time : timeW);
                    prioW = prio;
                }
            }
            if ((bitwhitelist&0xff) < 111) { //Variable Tampering
                asm("__explMapCount_VarSetCheck:");
                byte bit = (bitwhitelist&0xff);
                if (!(bit==110 ? (infoEntry[16]|infoEntry[17]|infoEntry[18]|infoEntry[19]&0x000fffff)&0x3fffffff : infoEntry[16+bit/30]&1<<bit%30)) { continue; } //Tampering must exist
            }
            if ((bitwhitelist>>8&0xff) < 111) { //Parent Variable Tampering
                asm("__explMapCount_ParentVarSetCheck:");
                byte bit = (bitwhitelist>>8&0xff);
                if (!(bit==110 ? (infoEntry[20]|infoEntry[21]|infoEntry[22]|infoEntry[23]&0x000003ff)&0x3fffffff : infoEntry[20+bit/30]&1<<bit%30)) { continue; } //Tampering must exist
            }
            if (bitwhitelist>>16&0xff) { //Armor Nullification
                asm("__explMapCount_ArmorNullCheck:");
                byte bits = (bitwhitelist>>16&0xff);
                byte time = infoEntry[12]>>16&0xff; byte prio = (infoEntry[4]>>28&0x07);
                if (!(prio || (infoEntry[23]>>20&bits) != bits)) { continue; } //Explicitly must match
                if (prio<=prioW) {
                    timeW = (prio<prioW||time>timeW ? time : timeW);
                    prioW = prio;
                }
            }
            if (whitelist&0x20000000 && changestate == -1) { //ChangeState Blacklist
                asm("__explMapCount_ChangeStateBCheck:");
                byte time = !!(infoEntry[16]>>24&0xff); byte prio = (infoEntry[23]>>16&0x07);
                if (infoEntry[7] != -1 && prio>=prioB) {
                    timeB = (prio<prioB||time<timeB ? time : timeB);
                    prioB = prio;
                }
            }
            //Final checks
            asm("__explMapCount_CleaningChecks:");
            if (!(prioW >= minPrio && prioW <= maxPrio && timeW >= minTime && timeW <= maxTime)) { continue; } //OoR Whitelist
            if (prioB >= minPrio && prioB <= maxPrio && timeB >= minTime && timeB <= maxTime) { continue; } //IR Blacklist
            if ((infoCast>>8&0x07) < prioW) {
                infoCast = timeW|prioW<<8;
            } else if ((infoCast>>8&0x07) == prioW && timeW < (infoCast&0xff)) {
                infoCast = infoCast&~0xff|timeW;
            }
            if (maskCheck || count == statePos) { return s; } //Return the position of this state
            IDList[count] = s; //Otherwise, write this state to the list
            count++;
        }
        asm("__explMapCount_Next:");
    } while (!maskCheck && ++s < 4096 && infoStorage[24*s] != -1);
    if (statePos == -1) { return count; } //Return the count if we're searching for it
    if (maskCheck || !count) { return -1; } //If we haven't already returned anything we missed it
    infoCast = (int)(infoStorage); //Repair
    return IDList[rand()%count]; //Fetch a randomly selected position
}

///////////////////////////
// Aesthetic Processing  //
///////////////////////////

/*void rotateAndSplit (int** innerSpriteData, int sourceNo, int destNo, int destNo2, float angle, int flipFlags, bool palIsGradient) {
    asm("__rotateAndSplit:");
    int* sourceSpr = (int*)(0); //Blatant
    int* destSpr = (int*)(0); //Front/bottom half
    int* destSpr2 = (int*)(0); //Back/top half
    { //Fetch Sprites
        int s = 0; int sm = (int)(innerSpriteData[2]); int so = (int)(innerSpriteData[1]);
        int* sD = innerSpriteData[5]; short* sL = innerSpriteData[6]; 
        do {
            if (s == sm) { return; }
            if (sL[0]) {
                if (!sourceSpr && sL[2] == (sourceNo>>16)&0xffff && sL[3] == sourceNo&0xffff) {
                    sourceSpr = (int*)((int)(sD) + so*s); //Much to my chargin I need to do an int typecast
                    if (destSpr && (destNo2 == -1 || destSpr2)) { break; }
                } else if (!destSpr && sL[2] == (destNo>>16)&0xffff && sL[3] == destNo&0xffff) {
                    destSpr = (int*)((int)(sD) + so*s);
                    if (sourceSpr && (destNo2 == -1 || destSpr2)) { break; }
                } else if (destNo2 != -1 && !destSpr2 && sL[2] == (destNo2>>16)&0xffff && sL[3] == destNo2&0xffff) {
                    destSpr2 = (int*)((int)(sD) + so*s);
                    if (sourceSpr && destSpr) { break; }
                }
            }
            sL += 4;
        } while (++s <= sm);
    }
    float angleFixed = angle;
    //This thing will store a constant 1000 somewhere that I don't particularly care about if I don't use the below method
    asm(".intel_syntax noprefix\n
        sub esp,0x04;
        push 0x447a0000;
        fld DWORD PTR [eax];
        fimul DWORD PTR [esp];
        fnstcw WORD PTR [esp+0x06];
        fstcw WORD PTR [esp+0x04];
        or BYTE PTR [esp+0x05],0x0c;
        fldcw WORD PTR [esp+0x04];
        frndint;
        fidiv DWORD PTR [esp];
        fldcw DWORD PTR [esp+0x06];
        fstp DWORD PTR [eax];
        add esp,0x08"
        : 
        : "a"(&angleFixed)
    );
    int xm = sourceSpr[0]; int ym = sourceSpr[1]; //x and y widths
    int x = 0; int xo = (flipFlags&1 ? xm-1 : 0); //initial x/y values
    int y = 0; int yo = (flipFlags&2 ? ym-1 : 0);
    if (palIsGradient) { //Use area mapping as it guarantees smoother results
        asm("__rotateAndSplit_AreaMap:");
        do {

        } while (++y < ym);
    } else { //Use sampling because... that's probably what MUGEN uses (or at least it feels like what MUGEN uses)
        asm("__rotateAndSplit_Sample:");
        do {
            
        } while (++y < ym);
    }



}*/

////////////////////////////////////////////////////////////////////////////////////////////////
// DEBUGGING BLOCK
////////////////////////////////////////////////////////////////////////////////////////////////

/*
----------------
.RaiseWarning (ID = 248)
--------------------------------------------
Raises a warning to the debug clipboard.
Specify a string through stack params.
*/
asm(R"(.intel_syntax noprefix
_XF8:                                                     # ========================================
  push edx                                                # %s -> CustomString
  push DWORD PTR [ebx+0x00000bf4]                         # %i -> StateNo
  push DWORD PTR [ebx+0x04]                               # %i -> ID
  lea ecx,[ebx+0x20]                                      # 
  push ecx                                                # %s -> PlayerName
  push DWORD PTR [eax+0x0000b3fc]                         # %4i -> GameTime
  push 0x004a394c                                         # %4i: Warning: Player %s (%i) in state %i: %s
  push DWORD PTR [eax+0x0000c530]                         # Debug Clipboard
  mov eax,0x004131f0                                      # 
  call eax                                                # Raise the warning
  add esp,0x1c                                            # Correct stack
  ret                                                     # 
  .balign 0x10, 0x00
)");

/*
----------------
.DefStall (ID = 249)
--------------------------------------------
Lock up processing until manual debugging occurs
Equivalent to a non-crashing breakpoint
*/
asm(R"(.intel_syntax noprefix
_XF9:                                                     # ========================================
  mov WORD PTR [ecx+0x06],0xfeeb                          # Reset to allow for a multi-use breakpoint
_XF9.Stall:                                               # ---------------------------------------
  jmp _XF9.Stall                                          # 
  ret                                                     # 
  .balign 0x10, 0x00
)");

/*
----------------
.Profiling (ID = 250)
--------------------------------------------
Function Depends on Param0.
Param0 = 0: Fetches QueryPerformanceFrequency and binds it to the address 0x004b28f8.
            Use this only once at the start of the round.
            You will need to handle the flushing yourself.
            (Furthermore, all addresses between 0x004b28f4 (Counter) and 0x004b2dfc will be reset to 0.)
Param0 = 1: Fetches QueryPerformanceCounter and binds it to the address 0x004b2900+16*PlayerNo.
            This should be used at the start of each operating cycle
Param0 = 2: Fetches QueryPerformanceCounter and subtracts it from the address 0x004b2900+16*PlayerNo.
            This differential is added to 0x004b2908+16*PlayerNo.
            This should be used at the end of each operating cycle
You will need to do the work of incrementing the counter at 0x004b28f4 by 1 yourself. Perhaps using another action flag is good?
*/
asm(R"(.intel_syntax noprefix
_XFA:                                                     # ========================================  
  cmp DWORD PTR [edx],0x00                                # If zero, initialize
  jnz _XFA.PerformanceCounter                             # 
_XFA.PerformanceFrequency:                                # ---------------------------------------
  lea ecx,[ecx+_XFA.QueryFString-_XFA]                    # QueryPerformanceFrequency string location
  call _XFA.Setup                                         # 
  push 0x004b28f8                                         # Frequency Locale
  call eax                                                # Fetch quest
  xor ecx,ecx                                             # 
  mov ch,0x01                                             # 0x400 to 0 pls
  mov edi,0x004b2900                                      # 
  xor eax,eax                                             # 
  rep stosd                                               # 
  jmp _XFA.Finit                                          # 
_XFA.PerformanceCounter:                                  # ---------------------------------------
  lea ecx,[ecx+_XFA.QueryCString-_XFA]                    # QueryPerformanceCounter string location
  mov ebp,DWORD PTR [edx]                                 # Just so that we're clear
  call _XFA.Setup                                         # 
  mov ecx,0x004b28f0                                      # 
  mov edx,DWORD PTR [ebx+0x08]                            # PlayerID
  shl edx,0x04                                            # 16 multiplier
  lea ebx,[ecx+edx]                                       # 
  mov esi,DWORD PTR [ebx]                                 # Fetch LDWORD
  mov edi,DWORD PTR [ebx+0x04]                            # Fetch UDWORD
  push ebx                                                # 
  call eax                                                # Fetch quest
  cmp ebp,0x01                                            # If 1, we're done
  je _XFA.Finit                                           # 
  sub DWORD PTR [ebx],esi                                 # Get the differential
  mov eax,DWORD PTR [ebx]                                 # For addition
  sbb DWORD PTR [ebx+0x04],edi                            # Something like QWORD Subtraction
  mov edx,DWORD PTR [ebx+0x04]                            # For addition
  add DWORD PTR [ebx+0x08],eax                            # Increment
  adc DWORD PTR [ebx+0x0c],edx                            # etc.
_XFA.Finit:                                               # ---------------------------------------
  ret                                                     # 
_XFA.Setup:                                               # =======================================
  push ecx                                                # Fetch the desired function (set up the stack properly for the next step)
  push 0x0049f604                                         # Push KERNEL32
  call DWORD PTR [0x0049f0b8]                             # Execute GetModuleHandle
  push eax                                                # Push Handle
  call DWORD PTR [0x0049f130]                             # Execute GetProcAddress
  ret                                                     # 
_XFA.QueryFString:                                        # ---------------------------------------
  .string "QueryPerformanceFrequency"                     #
_XFA.QueryCString:                                        # ---------------------------------------
  .string "QueryPerformanceCounter"                       #
  .balign 0x10, 0x00
)");

/*
----------------
.Custom (ID = 255)
--------------------------------------------
make your own stuff

This is ALSO the region for the exec stack, containing xvar(512~767), so it's merely a completely blank area
*/
asm(R"(.intel_syntax noprefix
_XFF:                                            # ========================================
  .balign 0x0400, 0x00;
  .int 0x00000000;                             
  .balign 0x0400, 0x00;
)");

/*
############################################
# constants
############################################
to prevent idiot stupid dumb bad disease :)
*/

asm(R"(
_DCONST_100:
  .double 100.0
_DCONST_PSEUDOPI:
  .double 3.14
_DCONST_PSEUDOE:
  .double 2.72
_FCONST_HALF:
  .float 0.5
_FCONST_ONE:
  .float 1.0
_DCONST_HALFSQRT2:
  .int 0x28f5c28f
  .int 0x3fd28f5c
_DCONST_4:
  .double 4.0
_DCONST_5:
  .double 5.0
_DCONST_1:
  .double 1.0
_DCONST_Neg1:
  .double -1.0
)");