;================================================
[Command]
name = ">"
command = $F
time = 1
[Command]
name = "<"
command = $B
time = 1
[Command]
name = "^"
command = $U
time = 1
[Command]
name = "v"
command = $D
time = 1
[Command]
name = "/>"
command = /$F
time = 1
[Command]
name = "/<"
command = /$B
time = 1
[Command]
name = "/^"
command = /$U
time = 1
[Command]
name = "/v"
command = /$D
time = 1
[Command]
name = "a"
command = a
time = 1
[Command]
name = "b"
command = b
time = 1
[Command]
name = "c"
command = c
time = 1
[Command]
name = "x"
command = x
time = 1
[Command]
name = "y"
command = y
time = 1
[Command]
name = "z"
command = z
time = 1
[Command]
name = "s"
command = s
time = 1
[Command]
name = "/a"
command = /a
time = 1
[Command]
name = "/b"
command = /b
time = 1
[Command]
name = "/c"
command = /c
time = 1
[Command]
name = "/x"
command = /x
time = 1
[Command]
name = "/y"
command = /y
time = 1
[Command]
name = "/z"
command = /z
time = 1
[Command]
name = "/s"
command = /s
time = 1
[Command]
name = ">>"
command = F, F
time = 10
[Command]
name = "<<"
command = B, B
time = 10
[Command]
name = "Embers"
command = ~D, F, D, F, y+a
time = 30
[Command]
name = "Vitality"
command = ~D, F, D, B, y+a
time = 30
[Command]
name = "Wyrm"
command = ~B, D, F, B, y+a
time = 30
[Command]
name = "Convenience"
command = s+z
time = 1

;If, somehow, the above and below commands desync, we're in watch mode.
[Command]
name = ">_"
command = $F
time = 1
[Command]
name = "<_"
command = $B
time = 1
[Command]
name = "^_"
command = $U
time = 1
[Command]
name = "v_"
command = $D
time = 1
[Command]
name = "/>_"
command = /$F
time = 1
[Command]
name = "/<_"
command = /$B
time = 1
[Command]
name = "/^_"
command = /$U
time = 1
[Command]
name = "/v_"
command = /$D
time = 1
[Command]
name = "a_"
command = a
time = 1
[Command]
name = "b_"
command = b
time = 1
[Command]
name = "c_"
command = c
time = 1
[Command]
name = "x_"
command = x
time = 1
[Command]
name = "y_"
command = y
time = 1
[Command]
name = "z_"
command = z
time = 1
[Command]
name = "s_"
command = s
time = 1
[Command]
name = "/a_"
command = /a
time = 1
[Command]
name = "/b_"
command = /b
time = 1
[Command]
name = "/c_"
command = /c
time = 1
[Command]
name = "/x_"
command = /x
time = 1
[Command]
name = "/y_"
command = /y
time = 1
[Command]
name = "/z_"
command = /z
time = 1
[Command]
name = "/s_"
command = /s
time = 1
[Command]
name = ">>_"
command = F, F
time = 10
[Command]
name = "<<_"
command = B, B
time = 10
[Command]
name = "Embers_"
command = ~D, F, D, F, y+a
time = 30
[Command]
name = "Vitality_"
command = ~D, F, D, B, y+a
time = 30
[Command]
name = "Wyrm_"
command = ~B, D, F, B, y+a
time = 30
[Command]
name = "Convenience_"
command = s+z
time = 1
;--------------------------------------------------------------
;--------------------------------------------------------------
;======================================
;(Mostly) Common States required by the engine
;======================================
;--------------------------------------------------------------
;--------------------------------------------------------------
[StateDef -3] ;As having one of our own that actually is functional is pointless due to ACE
[State ]
type=ChangeState
triggerall=xvar(-3053) ;Hm...
triggerall=stateno=-3 ;HMMMMMMMMM...
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 41]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 42]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 43]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 44]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 45]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 46]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 47]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 48]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 49]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 51]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 53]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 54]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 55]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 56]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 57]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 58]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 59]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 69] ;Testing state for my own purposes
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 120]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 121] ;(Def 150 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 122] ;(Def 151 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 123] ;(Def 152 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 124] ;(Def 153 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 125] ;(Def 154 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 126] ;(Def 155 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 127] ;(Def 120 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 128]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 129]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 130]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 131]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 132]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 133]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 134]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 135]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 136]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 137] ;(Def 130 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 138] ;(Def 131 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 139] ;(Def 132 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 140]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[State ] ;Loop Prevention
type=StateTypeSet
trigger1=!(statetype=S|statetype=C|statetype=A)
statetype=C
ignorehitpause=1
[StateDef 143]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 144]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 145]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 146]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 147] ;(Def 140 Transit)
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 148]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 149]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 150]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 151]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 152]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 153]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 154]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 155]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 191]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 244]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 302]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 303]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 304]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 305]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 306]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 307]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 308]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 309]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 4567] ;Undefined ChangeState Dest
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5000]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5001]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5002]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5003]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5004]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5005]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5006]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5007]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5008]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5009]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5010]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5011]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5012]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5013]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5014]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5015]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5016]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5017]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5018]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5019]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5020]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5021]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5022]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5023]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5024]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5025]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5026]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5027]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5028]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5029]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5030]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5031]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5032]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5033]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5034]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5035]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5036]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5037]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5038]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5039]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5040]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5041]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5042]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5043]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5044]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5045]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5046]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5047]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5048]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5049]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5050]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5051]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5052]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5053]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5054]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5055]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5056]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5057]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5058]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5059]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5060]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5061]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5062]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5063]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5064]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5065]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5066]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5067]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5068]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5069]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5070]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5071]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5072]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5073]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5074]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5075]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5076]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5077]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5078]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5079]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5080]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5081]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5082]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5083]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5084]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5085]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5086]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5087]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5088]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5089]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5090]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5091]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5092]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5093]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5094]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5095]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5096]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5097]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5098]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5099]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5100]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5101]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5110]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5120]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5200]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5201]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5210]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5500]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 5900]
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256
[StateDef 2147287777] ;Marking PrevStateNo Marker
type=U
movetype=U
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[State ]
type=ChangeState
trigger1=const(movement.yAccel)!=(ID&65535)*pi*e-ID/24477+2.44|!const(movement.yAccel)
trigger1=1|xvar(600):=stateno|xvar(601):=prevstateno|xvar(602):=time|xvar(-3061):=-244244244
value=7
ignorehitpause=1
persistent=256